export default [
    {
        name: "username",
        type: "text",
        label: "Username",
        inputProps: {
            placeholder: "Enter your username",
        },
        conClass: "",
        required: true,
    },
    {
        name: "password",
        type: "password",
        label: "Password",
        inputProps: {
            placeholder: "Enter your password",
        },
        conClass: "",
        required: true,
    },
];
