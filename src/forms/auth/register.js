import { formatKEY } from "../formatAllData";

export const userKey = `user${formatKEY}`;

export default (isLoading = false, { countries = [] }) => [
    {
        name: `${userKey}first_name`,
        type: "text",
        label: "First Name",
        inputProps: {
            placeholder: "Enter your first name",
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: `${userKey}email`,
        type: "email",
        label: "Email",
        inputProps: {
            placeholder: "Enter your email",
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: "phone_code_id",
        type: "select",
        label: "Dial code",
        inputProps: {
            placeholder: "Select dial code",
            isLoading,
            options: [],
        },
        conClass: "col-md-4",
        required: true,
    },
    {
        name: `${userKey}username`,
        type: "tel",
        label: "Phone Number",
        inputProps: {
            placeholder: "Enter your phone number",
        },
        conClass: "col-md-8",
        required: true,
    },
    {
        name: "selling_model_id",
        type: "select",
        label: "What is your mode of selling?",
        inputProps: {
            isLoading,
            placeholder: "Select an option",
            options: [
                {
                    label: "E-commerce store",
                    value: "E-commerce store",
                },
                {
                    label: "Retail store",
                    value: "Retail store",
                },
                {
                    label: "Social Media",
                    value: "Social Media",
                },
                {
                    label: "Other",
                    value: "Other",
                },
            ],
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: "relevant_details",
        type: "textarea",
        label: "URL/Store location/Relevant details",
        inputProps: {
            placeholder: "Enter your URL/Store location/Relevant details",
        },
        conClass: "col-md-12",
        required: true,
    },
];
