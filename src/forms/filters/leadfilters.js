import formTypes from "../../components/form/formTypes";

const formKeys = {
    q: "q",
    project: "project",
    date: "date",
    status: "status",
    source: "source",
    campaign: "campaign",
    source_of_campaigning: "source_of_campaigning",
    created_by: "created_by"
};

export default (
    isLoading = false,
    { project = [], sources = [], lead_status = [] }
) => [
        {
            name: formKeys.q,
            type: formTypes.search,
            inputProps: {
                placeholder: "Search",
            },
            conClass: "col-md-3",
            required: false,
        },
        {
            name: formKeys.date,
            type: formTypes.daterange,
            inputProps: {
                placeholder: "Date",
            },
            conClass: "col-md-3",
            required: false,
        },
        {
            name: "project",
            type: formTypes.normalSelect,
            inputProps: {
                options: project.map((x) => ({ label: x.title, value: x.id })),
                placeholder: "Project",
                isLoading,
                isClearable: true,
            },
            conClass: "col-md-3 col-6",
            required: false,
        },
        {
            name: formKeys.status,
            type: formTypes.normalSelect,
            inputProps: {
                options: lead_status.map((x) => ({ label: x.title, value: x.id })),
                placeholder: "Status",
                isLoading,
                isClearable: true,
            },
            conClass: "col-md-3",
            required: false,
        },
        {
            name: formKeys.source,
            type: formTypes.normalSelect,
            inputProps: {
                options: sources.map((x) => ({
                    label: x.title,
                    value: x.id,
                })),
                placeholder: "All Source",
                isLoading,
                isClearable: true,
            },
            conClass: "col-md-3 col-6",
            required: false,
        },
    ];

export const campaignIp = (campaigns = []) => {
    return {
        name: formKeys.campaign,
        type: formTypes.normalSelect,
        inputProps: {
            options: campaigns.map((x) => ({
                label: x.title,
                value: x.id,
                primary_sources: Array.isArray(x.primary_sources)
                    ? x.primary_sources
                    : [],
            })),
            placeholder: "All Campaign",
        },
        conClass: "col-md-3 col-6",
        required: true,
    };
};
export const primarySrcIp = (primary_sources = []) => {
    return {
        name: formKeys.source_of_campaigning,
        type: formTypes.normalSelect,
        inputProps: {
            options: primary_sources.map((x) => ({
                label: x.title,
                value: x.id,
            })),
            placeholder: "All Primary Source Campaigning",
        },
        conClass: "col-md-3",
        required: true,
    };
};



export const getInitValues = (values) => {
    return {
        [formKeys.q]: values?.q,
        [formKeys.created_by]: values?.created_by,
        [formKeys.status]: values?.status,
        [formKeys.source]: values?.source,
        [formKeys.campaign]: values?.campaign,
        [formKeys.source_of_campaigning]: values?.source_of_campaigning,
        [formKeys.date]: {
            to: values?.max_date ? new Date(values.max_date) : null,
            from: values?.min_date ? new Date(values.min_date) : null,
        }
    }
}