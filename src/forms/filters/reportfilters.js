export default () => [
    {
        name: "search",
        type: "search",
        inputProps: {
            placeholder: "Search",
        },
        conClass: "col-md-4 col-6",
        required: true,
    },
    {
        name: "sales_person",
        type: "select",
        inputProps: {
            options: [],
            placeholder: "Sales person",
        },
        conClass: "col-md-2 col-6",
        required: true,
    },
    {
        name: "project",
        type: "select",
        inputProps: {
            options: [
                {
                    label: "Plutus",
                    value: "Plutus",
                },
            ],
            placeholder: "Project",
        },
        conClass: "col-md-2 col-6",
        required: true,
    },
    {
        name: "enquiry_date",
        type: "date",
        inputProps: {
            placeholder: "Date",
        },
        conClass: "col-md-2 col-6",
        required: true,
    },
];

//source project date search
