export default (
    isLoading = false,
    { project = [], sources = [] }
) => [
        {
            name: "q",
            type: "search",
            inputProps: {
                placeholder: "Search",
            },
            conClass: "col-md-4",
            required: false,
        },
        {
            name: "date",
            type: "daterange",
            inputProps: {
                placeholder: "Date",
            },
            conClass: "col-md-4",
            required: false,
        },
        {
            name: "project",
            type: "select",
            inputProps: {
                options: project.map((x) => ({ label: x.title, value: x.id })),
                placeholder: "Project",
                isLoading,
                isClearable: true,
            },
            conClass: "col-md-4 col-6",
            required: false,
        },
        {
            name: "source",
            type: "select",
            inputProps: {
                options: sources.map((x) => ({
                    label: x.title,
                    value: x.id,
                    campaigns: Array.isArray(x.campaigns) ? x.campaigns : [],
                })),
                placeholder: "Source",
                isLoading,
                isClearable: true,
            },
            conClass: "col-md-3 col-6",
            required: false,
        },
    ];

export const campaignIp = (campaigns = []) => {
    return {
        name: "campaign",
        type: "select",
        inputProps: {
            options: campaigns.map((x) => ({
                label: x.title,
                value: x.id,
                primary_sources: Array.isArray(x.primary_sources)
                    ? x.primary_sources
                    : [],
            })),
            placeholder: "Campaign",
        },
        conClass: "col-md-3 col-6",
        required: true,
    };
};

export const primarySrcIp = (primary_sources = []) => {
    return {
        name: "source_of_campaigning",
        type: "select",
        inputProps: {
            options: primary_sources.map((x) => ({
                label: x.title,
                value: x.id,
            })),
            placeholder: "Primary Source Campaigning",
        },
        conClass: "col-md-3",
        required: true,
    };
};



const getSelectValue = (object = [], value) => (object.filter(function (e) { return e.id === value }))


export const getInitValues = ({ query = {}, masterData }) => {
    const source = getSelectValue(masterData[1].sources, parseInt(query.source))
    const project = getSelectValue(masterData[1].project, parseInt(query.project))
    const campaign = source.length > 0 ? getSelectValue(source[0].campaigns, parseInt(query.campaign)) : []
    return {
        q: query?.q ?? "",
        source: source.length > 0 ? { label: source[0].title, value: source[0].id } : "NA",
        project: project.length > 0 ? { label: project[0].title, value: project[0].id } : "NA",
        campaign: campaign.length > 0 ? { label: campaign[0].title, value: campaign[0].id } : "NA",
    };
};