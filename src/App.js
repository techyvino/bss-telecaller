import React from "react";
import Pages from "./pages/Pages";
import { ToastContainer } from "react-toastify";
import UserProvider from "./components/auth/UserProvider";
import MasterData from "./components/common/MasterData";

const App = () => {
    return (
        <UserProvider>
            <MasterData>
                <Pages />
                <ToastContainer
                    position="bottom-center"
                    autoClose={3000}
                    closeOnClick
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
            </MasterData>
        </UserProvider>
    );
};

export default App;
