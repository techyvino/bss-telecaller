import React from "react";
import AddLead from "../tele-caller/AddLead";

const AddLeads = () => {
    return (
        <AddLead
            title="Add New Lead"
            desc="Fill the below form to add a new lead"
        />
    );
};

export default AddLeads;
