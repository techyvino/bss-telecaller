import React from "react";
import Section from "../../components/layout/Section";
import BookingTable from "../../components/sales-manager/booking/BookingTable";
import { useMasterValues } from "../../components/common/MasterData";
import { isArray } from "../../utils";
import { salesManagerUrls } from "../../apiService/urls";
import useQuery from "../../hooks/useQuery";
import { useHistory } from "react-router-dom";
import UrlParamTabList from "../../components/common/UrlParamTabList";
import keysToQuery from "../../utils/keysToQuery";
import DynamicContent from "../../components/common/DynamicContent";

export const BookingTabs = ({ filters = {} }) => {
    const q = keysToQuery(filters);
    return (
        <UrlParamTabList
            url={`${salesManagerUrls.bookingTabs}${q}`}
        >
            {({ id, is_approve }) => (
                <BookingTable
                    tab={id}
                    filters={filters}
                    isApprove={is_approve}
                />
            )}
        </UrlParamTabList>
    );
};

export const ProjectSelect = ({ onChange, value, options = [] }) => {
    return (
        <select
            className="form-control"
            onChange={(e) => onChange(e.target.value)}
            value={value}
        >
            <option value="">All Projects</option>
            {options.map((x) => (
                <option value={x.id} key={x.id}>
                    {x.title}
                </option>
            ))}
        </select>
    );
};

const ProjectWiseBookings = () => {
    const [fetching, data] = useMasterValues();
    const options = isArray(data?.project) ? data.project : [];
    const query = useQuery();
    const history = useHistory();
    const project_id = query.get("project") ?? "";

    const isLoaded = options.length > 0;

    return (
        <DynamicContent isLoaded={isLoaded} fetching={fetching}>
            {isLoaded && (
                <>
                    <div className="row">
                        <div className="col-md-3 mb-3">
                            <ProjectSelect
                                options={options}
                                onChange={(val) => {
                                    if (val) {
                                        query.set("project", val);
                                    } else {
                                        query.delete("project");
                                    }
                                    history.push({
                                        search: `?${query.toString()}`,
                                    });
                                }}
                                value={project_id}
                            />
                        </div>
                    </div>
                    <BookingTabs filters={{ project_id }} />
                </>
            )}
        </DynamicContent>
    );
};

const Bookings = () => {
    return (
        <Section fluid>
            <ProjectWiseBookings />
        </Section>
    );
};

export default Bookings;
