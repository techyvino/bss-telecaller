import React from "react";
import Section from "../../components/layout/Section";
import Avail from "../../components/sales-manager/availability/Avail";

const Availability = () => {
    return (
        <Section>
            <Avail />
        </Section>
    );
};

export default Availability;
