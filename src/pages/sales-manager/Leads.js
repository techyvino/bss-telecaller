import React from "react";
import { useHistory } from "react-router";
import DynamicContent from "../../components/common/DynamicContent";
import { useMasterValues } from "../../components/common/MasterData";
import Section from "../../components/layout/Section";
import LeadTabs from "../../components/sales-manager/leads/LeadTabs";
import useQuery from "../../hooks/useQuery";
import { isArray } from "../../utils";



const ProjectLeads = ({ options = [], isGm }) => {
    const query = useQuery();
    const filters = JSON.parse(query.get("filters"))
    const history = useHistory();
    const project = query.get("project") ?? "";

    return (
        <>
            <div className="row">
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("project", val);
                            } else {
                                query.delete("project");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={project}
                    >
                        <option value="">
                            All Project
                            </option>
                        {options.map((x) => (
                            <option value={x.id} key={x.id}>
                                {x.title}
                            </option>
                        ))}
                    </select>
                </div>
            </div>
            <LeadTabs isGm={isGm} filters={filters} projectID={project} />
        </>
    );
};

export const LeadWithProject = ({ isGm }) => {
    const [fetching, data] = useMasterValues();
    const options = isArray(data?.project) ? data.project : [];
    const isLoaded = options.length > 0;

    return (
        <DynamicContent isLoaded={isLoaded} fetching={fetching}>
            {isLoaded && <ProjectLeads isGm={isGm} options={options} />}
        </DynamicContent>
    );
};

const Leads = ({ isGm }) => (
    <Section fluid>
        <LeadWithProject isGm={isGm} />
    </Section>
);

export default Leads;
