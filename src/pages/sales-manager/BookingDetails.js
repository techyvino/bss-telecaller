import React, { useEffect } from "react";
import { Printer } from "react-feather";
import { useParams } from "react-router-dom";
import { salesManagerUrls } from "../../apiService/urls";
import DynamicContent from "../../components/common/DynamicContent";
import Section from "../../components/layout/Section";
import { AptBasicDetail } from "../../components/sales-manager/availability/ApartDetails";
import ApplicantDetails, {
    Address,
    CompanyDetails,
} from "../../components/sales-manager/bookingDetails/ApplicantDetails";
import PaymentDetails from "../../components/sales-manager/bookingDetails/PaymentDetails";
import PricingDetails from "../../components/sales-manager/bookingDetails/PricingDetails";
import useFetchData from "../../hooks/http/useFetchData";
import { isArray } from "../../utils";

const Con = ({ title, children }) => {
    return (
        <div className="border bg-white mb-3">
            <div className="p-3 f-18 f-500 border-bottom">{title}</div>
            <div className="p-3">{children}</div>
        </div>
    );
};

const Details = ({ data = {} }) => {
    const appCompany = data.company_details.filter(
        (x) => x.type === "Applicant"
    )[0];
    const coAppCompany = data.company_details.filter(
        (x) => x.type === "Co Applicant"
    )[0];

    return (
        <>
            <div className="d-flex justify-content-end mb-3">
                <button
                    onClick={() => window.print()}
                    className="btn btn-outline-dark"
                >
                    <Printer width={18} height={18} className="mr-1" /> Print
                    Page
                </button>
            </div>
            <div id="section-to-print">
                <Con title="Applicant Details">
                    <ApplicantDetails data={data} />
                </Con>
                {appCompany && (
                    <Con title="Applicant Company Details">
                        <CompanyDetails data={appCompany} />
                    </Con>
                )}
                {data.coapplicantdetails && (
                    <Con title="Co Applicant Details">
                        <ApplicantDetails data={coAppCompany} />
                    </Con>
                )}
                {coAppCompany && (
                    <Con title="Co Applicant Company Details">
                        <CompanyDetails data={coAppCompany} />
                    </Con>
                )}
                {isArray(data.address) &&
                    data.address.map((addr) => (
                        <Con title={`${addr.type} Address`}>
                            <Address data={addr} />
                        </Con>
                    ))}
                <h5 className="pt-2 pb-2 text-underline">Apartment Details</h5>
                <div className="mb-3">
                    <AptBasicDetail data={data.apartment_details} />
                </div>
                <h5 className="pt-2 pb-2 text-underline">Cost Breakups</h5>
                <PricingDetails
                    breakup={data.price_breakup}
                    net={data?.net_amount}
                    totalA={data?.total_basic_amount}
                />
                
                <h5 className="pt-2 pb-2 text-underline">Payment Details</h5>
                <PaymentDetails list={data.payments} />
            </div>
        </>
    );
};

const Bookings = () => {
    const [state, { setUrl }] = useFetchData();
    const { bookingId } = useParams();

    useEffect(() => {
        if (bookingId) {
            setUrl(`${salesManagerUrls.booking}${bookingId}/`);
        }
    }, [setUrl, bookingId]);
    const isLoaded = !!state?.data?.id;
    return (
        <Section>
            <DynamicContent fetching={state.fetching} isLoaded={isLoaded}>
                {isLoaded && <Details data={state.data} />}
            </DynamicContent>
        </Section>
    );
};

export default Bookings;
