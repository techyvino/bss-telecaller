import React from "react";
import EditLead from "../tele-caller/EditLead";

const EditLeads = () => {
    return (
        <EditLead title="Edit Lead" desc="Fill the below form to edit lead" />
    );
};

export default EditLeads;
