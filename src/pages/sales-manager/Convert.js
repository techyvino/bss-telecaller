import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import urls from "../../apiService/urls";
import DynamicContent from "../../components/common/DynamicContent";
import Section from "../../components/layout/Section";
import BookingForm from "../../components/sales-manager/booking/BookingForm";
import useFetchData from "../../hooks/http/useFetchData";

const Convert = () => {
    const { leadId } = useParams();
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(urls.leadDetail(leadId));
    }, [setUrl, leadId]);

    const isLoaded = !!state?.data?.id;

    return (
        <Section>
            <DynamicContent fetching={state.fetching} isLoaded={isLoaded}>
                {isLoaded && (
                    <div className="card">
                        <div className="card-header">
                            <div>
                                <h4 className="mb-0">Booking</h4>
                                <p className="mb-0 text-black-50">
                                    Fill the below form to convert lead
                                </p>
                            </div>
                        </div>
                        <div className="card-body">
                            <BookingForm leadInit={state.data} />
                        </div>
                    </div>
                )}
            </DynamicContent>
        </Section>
    );
};

export default Convert;
