import React, { useEffect } from "react";
import { salesManagerUrls } from "../../apiService/urls";
import DynamicContent from "../../components/common/DynamicContent";
import Section from "../../components/layout/Section";
import MasterReports from "../../components/sales-manager/availability/MasterReports";
import useFetchData from "../../hooks/http/useFetchData";
import useQuery from "../../hooks/useQuery";
import GoBack from "../../components/common/GoBack";

const TowerAvailability = () => {
    const query = useQuery();
    const project = query.get("project");
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        if (project) {
            setUrl(salesManagerUrls.masterReport(project));
        }
    }, [setUrl, project]);

    const isLoaded = !!state?.data?.report_1;

    return (
        <Section>
            <div className="mb-3">
                <GoBack />
            </div>
            <DynamicContent isLoaded={isLoaded} fetching={state.fetching}>
                {isLoaded && <MasterReports {...state.data} />}
            </DynamicContent>
        </Section>
    );
};

export default TowerAvailability;
