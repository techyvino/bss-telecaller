import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Section from "../../components/layout/Section";
import ApartDetails from "../../components/sales-manager/availability/ApartDetails";
import useFetchData from "../../hooks/http/useFetchData";
import { salesManagerUrls } from "../../apiService/urls";
import DynamicContent from "../../components/common/DynamicContent";
import GoBack from "../../components/common/GoBack";
import keysToQuery from "../../utils/keysToQuery";
import Lightbox from "react-image-lightbox";
import 'react-image-lightbox/style.css';

const GenerateLink = ({
    id = "",
    price_breakup = {}
}) => {
    const q = keysToQuery({
        id,
        plc: price_breakup?.plc,
        floor_rise_rate: price_breakup?.floor_rise_rate,
        no_of_carpark: price_breakup?.no_of_carpark,
        basic_rate: price_breakup?.base_rate,
        amenities_charges: price_breakup?.amenities_charges,
    });

    return (
        <Link to={`/cost-sheet-form${q}`} className="btn btn-theme">
            Generate Cost Sheet
        </Link>
    );
};

const FloorPlan = ({ id = "", floor_plan = [] }) => {
    const [toggle, setToggle] = useState(0)
    const [photoIndex, setPhotoIndex] = useState(0)
    return (
        <div>

            <button onClick={() => setToggle(1)} className="btn btn-theme">
                View Floor Plan
            </button>
            {toggle ?
                (
                    <Lightbox
                        mainSrc={floor_plan[photoIndex].image}
                        nextSrc={floor_plan[(photoIndex + 1) % floor_plan.length].image}
                        prevSrc={floor_plan[(photoIndex + floor_plan.length - 1) % floor_plan.length].image}
                        onCloseRequest={() => setToggle(0)}

                        onMovePrevRequest={() => setPhotoIndex((photoIndex + floor_plan.length - 1) % floor_plan.length)}
                        onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % floor_plan.length,)}
                    />
                )
                : ""}
        </div>
    );
};

const Apartment = () => {
    const { apartmentId } = useParams();
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(salesManagerUrls.aprtDetail(apartmentId));
    }, [setUrl, apartmentId]);

    const isLoaded = !!state?.data?.id;

    return (
        <Section>
            <div className="mb-3">
                <GoBack />
            </div>
            <DynamicContent fetching={state.fetching} isLoaded={isLoaded}>
                {isLoaded && (
                    <div>
                        <div className="flex-between mb-3 flex-wrap">
                            <h4 className="text-center m-0 text-uppercase">
                                {state?.data?.floor?.tower?.project?.title ??""}
                            </h4>
                            {state.data.floor_plan.length >= 1 &&
                                <FloorPlan {...state.data} />
                            }
                            <GenerateLink {...state.data} />

                        </div>
                        <ApartDetails data={state.data} />
                    </div>
                )}
            </DynamicContent>
        </Section>
    );
};

export default Apartment;
