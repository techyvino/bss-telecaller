import React from "react";
import { useHistory } from "react-router";
import { isArray } from "validate.js";
import { useMasterValues } from "../../components/common/MasterData";
import Section from "../../components/layout/Section";
import useTeamList from "../../components/tele-caller-team-lead/useTeamList";
import useQuery from "../../hooks/useQuery";
import { BookingTabs, ProjectSelect } from "../sales-manager/Bookings";

const SalesTeamSelect = ({ options = [], value, onChange }) => {
    return (
        <select
            className="form-control"
            onChange={(e) => onChange(e.target.value)}
            value={value}
        >
            <option value="">All Employees</option>
            {options.map((x) => (
                <option value={x.value} key={x.value}>
                    {x.label}
                </option>
            ))}
        </select>
    );
};

const TableWithFilters = () => {
    const data = useMasterValues()[1];
    const options = isArray(data?.project) ? data.project : [];
    const teamOptions = useTeamList()[1];

    const query = useQuery();
    const history = useHistory();
    const project_id = query.get("project") ?? "";
    const employee_id = query.get("employee") ?? "";

    return (
        <>
            <div className="row">
                <div className="col-md-3 mb-3">
                    <ProjectSelect
                        onChange={(val) => {
                            if (val) {
                                query.set("project", val);
                            } else {
                                query.delete("project");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        options={options}
                        value={project_id}
                    />
                </div>
                <div className="col-md-3 mb-3">
                    <SalesTeamSelect
                        onChange={(val) => {
                            if (val) {
                                query.set("employee", val);
                            } else {
                                query.delete("employee");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        options={teamOptions}
                        value={employee_id}
                    />
                </div>
            </div>
            <BookingTabs filters={{ project_id, employee_id }} />
        </>
    );
};

const Bookings = () => {
    return (
        <Section fluid>
            <TableWithFilters />
        </Section>
    );
};

export default Bookings;
