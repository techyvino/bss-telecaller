import React from "react";
import { useHistory } from "react-router-dom";
import { isArray } from "validate.js";
import DynamicContent from "../../components/common/DynamicContent";
import { useMasterValues } from "../../components/common/MasterData";
import Section from "../../components/layout/Section";
import LeadTabs from "../../components/sales-manager/leads/LeadTabs";
import useTeamList from "../../components/tele-caller-team-lead/useTeamList";
import useQuery from "../../hooks/useQuery";

const QSelect = ({ options = [], value, onChange }) => {
    return (
        <select
            className="form-control"
            onChange={(e) => onChange(e.target.value)}
            value={value}
        >
            {options.map((x) => (
                <option value={x.value} key={x.value}>
                    {x.label}
                </option>
            ))}
        </select>
    );
};

const TeamTable = ({ team = [], projects = [] }) => {
    const query = useQuery();
    const history = useHistory();
    const employeeId = query.get("employee") || team?.[0]?.value;
    const filters = JSON.parse(query.get("filters"))

    return (
        <>
            <div className="row">
                <div className="col-md-3 form-group">
                    <QSelect
                        value={employeeId}
                        qName="employee"
                        options={team}
                        onChange={(val) => {
                            query.set("employee", val);
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                    />
                </div>
            </div>
            {employeeId && (
                <LeadTabs employeeId={employeeId} filters={filters} />
            )}
        </>
    );
};

const mapProject = (x) => ({ label: x.title, value: x.id });

const TeamDetails = () => {
    const [fetching, team] = useTeamList();
    const [masterFetching, data] = useMasterValues();
    const projects = isArray(data?.project)
        ? data?.project.map(mapProject)
        : [];
    const isLoaded = team.length > 0 && projects.length > 0;

    return (
        <Section fluid>
            <DynamicContent
                fetching={fetching || masterFetching}
                isLoaded={isLoaded}
            >
                {isLoaded && <TeamTable team={team} projects={projects} />}
            </DynamicContent>
        </Section>
    );
};

export default TeamDetails;
