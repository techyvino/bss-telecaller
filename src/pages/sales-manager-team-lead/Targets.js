import React from "react";
import Tabs, { TabBtnList, TabPane } from "../../components/common/Tabs";
import Section from "../../components/layout/Section";
import MyTargets from "../../components/tele-caller-team-lead/targets/MyTargets";
import TeamTargets from "../../components/tele-caller-team-lead/targets/TeamTargets";

const tabs = ["My Target", "Team Target"];

const Targets = () => {
    return (
        <Section>
            <Tabs tabs={tabs}>
                <TabBtnList />
                <TabPane tabId={tabs[0]}>
                    <MyTargets />
                </TabPane>
                <TabPane tabId={tabs[1]}>
                    <TeamTargets type_id={2} />
                </TabPane>
            </Tabs>
        </Section>
    );
};

export default Targets;
