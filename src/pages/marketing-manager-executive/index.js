import React, { lazy } from "react";
import AuthRoute from "../../components/auth/AuthRoute";
import RoleRoutes from "../RoleRoutes";

export const pathName = "/marketing-manager-executive";

export const paths = {
    dashboard: "/dashboard",
    reports: "/reports",
    master: "/master-data",
    budget: "/campaign-budget",
    addBudget: "/campaign-budget/add",
    addProject: "/project/add",
    editProject: "/project/edit/:projectId",
    search:"/search",
};

export const landing = `${pathName}${paths.dashboard}`;
export const pages = {
    landing,
    reports: `${pathName}${paths.reports}`,
    master: `${pathName}${paths.master}`,
    budget: `${pathName}${paths.budget}`,
    target: `${pathName}${paths.target}`,
    addBudget: `${pathName}${paths.addBudget}`,
    addProject: `${pathName}${paths.addProject}`,
    editProject: (id) =>
        `${pathName}${paths.editProject.replace(":projectId", id)}`,
};


export const menu = [
    {
        title: "Dashboard",
        to: landing,
    },
    {
        title: "Reports",
        to: pages.reports,
    },
    {
        title: "Master Data",
        to: pages.master,
    },
    {
        title: "Campaign Budget",
        to: pages.budget,
    },
];

export const routes = [
    {
        path: paths.dashboard,
        component: lazy(() => import("../marketing-manager/Dashboard")),
    },

    {
        path: paths.reports,
        component: lazy(() => import("../marketing-manager/Reports")),
    },
    {
        path: paths.master,
        component: lazy(() => import("../marketing-manager/MasterData")),
    },
    {
        path: paths.addProject,
        component: lazy(() => import("../marketing-manager/AddProject")),
    },
    {
        path: paths.editProject,
        component: lazy(() => import("../marketing-manager/EditProject")),
    },
    {
        path: paths.budget,
        component: lazy(() => import("../marketing-manager/Budget")),
        exact: true,
    },
    {
        path: paths.addBudget,
        component: lazy(() => import("../marketing-manager/AddBudget")),
    },
    {
        path: paths.search,
        component: lazy(() => import("../OverallSearchResult")),
    },
];

export const allowRoles = [10];

export default (
    <AuthRoute path={pathName}>
        <RoleRoutes
            title="Marketing Manager Executive"
            routes={routes}
            menu={menu}
            landing={landing}
            allowRoles={allowRoles}
        />
    </AuthRoute>
);
