import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import urls from "../../apiService/urls";
import DynamicContent from "../../components/common/DynamicContent";
import GoBack from "../../components/common/GoBack";
import Section from "../../components/layout/Section";
import LeadDetail from "../../components/tele-caller/leads/LeadDetail";
import useFetchData from "../../hooks/http/useFetchData";

const LeadDetails = () => {
    const [state, { setUrl }] = useFetchData();
    const { leadId } = useParams();

    useEffect(() => {
        setUrl(urls.leadDetail(leadId));
    }, [setUrl, leadId]);

    return (
        <Section>
            <div className="row">
                <div className="offset-md-2 col-md-8">
                    <div className="mb-3">
                        <GoBack />
                    </div>
                    <DynamicContent
                        fetching={state.fetching}
                        isLoaded={state.data && state.data.id}
                    >
                        {state.data && state.data.id && (
                            <LeadDetail data={state.data} />
                        )}
                    </DynamicContent>
                </div>
            </div>
        </Section>
    );
};

export default LeadDetails;
