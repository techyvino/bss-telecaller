import React from "react";
import Section from "../../components/layout/Section";
import Targets from "../../components/tele-caller/targets/Targets";

const Target = () => {
    return (
        <Section>
            <Targets />
        </Section>
    );
};

export default Target;
