import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import urls from "../../apiService/urls";
import DynamicContent from "../../components/common/DynamicContent";
import Section from "../../components/layout/Section";
import EditLeadForm from "../../components/tele-caller/leads/EditLeadForm";
import useFetchData from "../../hooks/http/useFetchData";
import GoBack from "../../components/common/GoBack";

const EditLead = ({
    title = "Edit Enquiry",
    desc = "Fill the below form to edit enquiry",
}) => {

    const [state, { setUrl }] = useFetchData();
    const { leadId } = useParams();

    useEffect(() => {
        setUrl(urls.leadDetail(leadId));
    }, [setUrl, leadId]);

    return (
        <Section>
            <div className="row">
                <div className="col-lg-12">
                    <div className="d-flex justify-content-between align-items-end border-bottom mb-3 pb-2">
                        <div className="mr-2">
                            <h4 className="mb-0">{title}</h4>
                            <p className="mb-0 text-black-50">{desc}</p>
                        </div>
                        <div className="flex-shrink-0">
                            <GoBack />
                        </div>
                    </div>
                    <DynamicContent
                        fetching={state.fetching}
                        isLoaded={state.data && state.data.id}
                    >
                        {state.data && state.data.id && (
                            <EditLeadForm data={state.data} />
                        )}
                    </DynamicContent>
                </div>
            </div>
        </Section>
    );
};

export default EditLead;
