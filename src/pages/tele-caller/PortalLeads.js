import React from 'react';
import Section from '../../components/layout/Section';
import PortalLeadTabs from '../../components/tele-caller/PortalLeads/PortalLeadTabs';


const PortalLeads = () => {
    return (
        <Section fluid>
            <PortalLeadTabs />
        </Section>
    );
}

export default PortalLeads;