import React from "react";
import Section from "../../components/layout/Section";
import LeadTabs from "../../components/tele-caller/superreceptionist/LeadTabs";


const SuperReceptionist = () => {
    return (
        <Section fluid>
            <LeadTabs />
        </Section>
    );
};

export default SuperReceptionist;
