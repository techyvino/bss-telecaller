import React, { lazy } from "react";
import AuthRoute from "../../components/auth/AuthRoute";
import SalesReport from "../../components/layout/SalesReport";
import RoleRoutes from "../RoleRoutes";

export const pathName = "/tele-caller";

export const paths = {
    addEnquiry: "/add-enquiry",
    enquiry: "/enquiry",
    reports: "/reports",
    targets: "/targets",
    search:"/search",
    portalLeads:"/portal-leads",
    superReceptionist:"/super-receptionist",
};

export const landing = `${pathName}${paths.addEnquiry}`;

export const menu = [
    {
        title: "Add Enquiry",
        to: landing,
    },
    {
        title: "Enquiry",
        to: `${pathName}${paths.enquiry}`,
    },
    {
        title: "Reports",
        to: `${pathName}${paths.reports}`,
    },
    {
        title: "Targets",
        to: `${pathName}${paths.targets}`,
    },
    {
        title: "Portal Leads",
        to: `${pathName}${paths.portalLeads}`,
    },
    {
        title: "Super Receptionist",
        to: `${pathName}${paths.superReceptionist}`,
    },
];

export const routes = [
    {
        path: paths.enquiry,
        exact: true,
        component: lazy(() => import("./Leads")),
    },
    {
        path: paths.reports,
        component: lazy(() => import("./Reports")),
    },
    {
        path: paths.addEnquiry,
        component: lazy(() => import("./AddLead")),
    },
    {
        path: `${paths.enquiry}/edit/:leadId`,
        component: lazy(() => import("./EditLead")),
    },
    {
        path: `${paths.enquiry}/view/:leadId`,
        component: lazy(() => import("./LeadDetails")),
    },
    {
        path: paths.targets,
        component: lazy(() => import("./Target")),
    },
    {
        path: `${paths.search}/take-lead/:leadId`,
        exact:true,
        component: lazy(() => import("../TakeNewLead")),
    },
    {
        path: paths.search,
        component: lazy(() => import("../OverallSearchResult")),
    },
    {
        path: paths.portalLeads,
        exact: true,
        component: lazy(() => import("./PortalLeads")),
    },
    {
        path: paths.superReceptionist,
        component: lazy(() => import("./SuperReceptionist")),
    },
    {
        path: `${paths.portalLeads}/add-enquiry/:portalleadId`,
        exact: true,
        component: lazy(() => import("./AddEnquiry")),
    },
];

export const allowRoles = [1, 2];

export default (
    <AuthRoute path={pathName}>
        <RoleRoutes
            title="Telecaller"
            routes={routes}
            menu={menu}
            landing={landing}
            allowRoles={allowRoles}
            headerRight={<SalesReport />}
        />
    </AuthRoute>
);
