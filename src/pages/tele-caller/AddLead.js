import React from "react";
import { ArrowLeft } from "react-feather";
import { useHistory } from "react-router-dom";
import Section from "../../components/layout/Section";
import AddLeadForm from "../../components/tele-caller/leads/AddLeadForm";

const AddLead = ({
    title = "Add Enquiry",
    desc = "Fill the below form to add a new enquiry",
}) => {
    const history = useHistory();

    return (
        <Section>
            <div className="card">
                <div className="card-header">
                    <div className="d-flex justify-content-between align-items-end">
                        <div className="mr-2">
                            <h4 className="mb-0">{title}</h4>
                            <p className="mb-0 text-black-50">{desc}</p>
                        </div>
                        <div className="flex-shrink-0">
                            <button
                                onClick={history.goBack}
                                className="btn p-0"
                            >
                                <ArrowLeft size={18} /> Go Back
                            </button>
                        </div>
                    </div>
                </div>
                <div className="card-body">
                    <AddLeadForm />
                </div>
            </div>
        </Section>
    );
};

export default AddLead;
