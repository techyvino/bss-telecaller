import React from "react";
import Section from "../../components/layout/Section";
import LeadTabs from "../../components/tele-caller/leads/LeadTabs";

const Leads = () => {
    return (
        <Section fluid>
            <LeadTabs isEdit />
        </Section>
    );
};

export default Leads;
