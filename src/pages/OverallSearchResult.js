import React, { useEffect } from "react";
import { Link, useRouteMatch } from "react-router-dom";
import urls from "../apiService/urls";
import DateFormat from "../components/common/DateFormat";
import TableList from "../components/common/TableList";
import Section from "../components/layout/Section";
import LeadEmail from "../components/tele-caller/leads/LeadEmail";
import LeadNumber from "../components/tele-caller/leads/LeadNumber";
import ResendEnquiry from "../components/tele-caller/leads/LeadReEnquiry";
import Status from "../components/tele-caller/leads/Status";
import usePagination from "../hooks/http/usePagination";
import useQuery from "../hooks/useQuery";


const EnquiryAction = ({ id, enquiry_action }) => {
    let { url } = useRouteMatch();
    if (enquiry_action === 1) {
        return (
            <Link to={`${url}/take-lead/${id}`}>
                Take the Enquiry
            </Link>
        )
    } else if (enquiry_action === 2) {
        return <ResendEnquiry id={id} />
    }

    return (
        <div>-</div>
    )
}

const tbData = [
    {
        th: "Enquiry No",
        td: ({ id, lead_no = "" }) => lead_no,
    },
    {
        th: "Project",
        td: ({ project }) => project?.title ?? "",
    },
    {
        th: "Enquiry On",
        td: ({ enquiry_date }) => (
            <DateFormat format="ll" date={enquiry_date} />
        ),
    },
    {
        th: "Customer Name",
        td: ({ name }) => name,
    },
    {
        th: "Customer Email",
        td: LeadEmail,
    },
    {
        th: "Customer Phone",
        td: LeadNumber,
    },
    {
        th: "Source",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign?.source?.title ?? "",
    },
    {
        th: "Campaign",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign.title ?? "",
    },
    {
        th: "Primary Source",
        td: ({ source_of_campaigning }) => source_of_campaigning?.title ?? "",
    },
    {
        th: "Status",
        td: ({ status = {} }) => <Status {...status} />,
    },
    {
        th: "Created On",
        td: ({ created_on }) => <DateFormat date={created_on} />,
    },
    {
        th: "Tele-caller",
        td: ({ created_by }) =>
            created_by?.user
                ? `${created_by.user.first_name} ${created_by.user.last_name}`
                : "",
    },
    {
        th: "Assigned On",
        td: ({ assigned_on }) => <DateFormat date={assigned_on} />,
    },
    {
        th: "Sales Manager",
        td: ({ assigned_to }) =>
            assigned_to && assigned_to.user
                ? `${assigned_to.user.first_name} ${assigned_to.user.last_name}`
                : "",
    },
    {
        th: "Followup Date",
        td: ({ follow_up_date }) => (
            <DateFormat date={follow_up_date} format="ll" />
        ),
    },
    {
        th: "Re-Enquiry Feedback",
        td: ({ re_enquiry_feedback }) => re_enquiry_feedback ?? "-",
    },
    {
        th: "Re-Enquiry On",
        td: ({ re_enquiry_on }) => re_enquiry_on ? <DateFormat date={re_enquiry_on} /> : "-",
    },
    {
        th: "Re-Enquiry",
        td: ({ id, enquiry_action, }) => <EnquiryAction enquiry_action={enquiry_action} id={id} />,
    },
];

const OverAllSearchResult = () => {
    const query = useQuery()
    const [state, { setUrl, setPage }] = usePagination();
    const q = query.get('q')

    useEffect(() => {
        const filters = { q: q }
        setUrl({
            baseUrl: `${urls.search}`,
            filters: { ...filters }
        });
    }, [setUrl, q]);
    return (
        <Section fluid>
            <h3> Search Result</h3>
            <TableList
                pageState={state}
                onPageClick={setPage}
                tableData={tbData}
            />
        </Section>
    )
}

export default OverAllSearchResult;