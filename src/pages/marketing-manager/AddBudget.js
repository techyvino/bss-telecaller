import React from "react";
import Section, { SectionTitle } from "../../components/layout/Section";
import AddBudgetForm from "../../components/marketing-manager/budget/AddBudgetForm";

const AddBudget = () => {
    return (
        <Section>
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <SectionTitle title="Add Budget" />
                    <AddBudgetForm />
                </div>
            </div>
        </Section>
    );
};

export default AddBudget;
