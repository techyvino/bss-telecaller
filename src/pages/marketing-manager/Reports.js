import React from "react";
import { isArray } from "validate.js";
import DynamicContent from "../../components/common/DynamicContent";
import { useMasterValues } from "../../components/common/MasterData";
import Section from "../../components/layout/Section";
import ReportTabs from "../../components/marketing-manager/reports/ReportTabs";

const Reports = () => {
    const [fetching, data] = useMasterValues();
    const options = isArray(data?.project) ? data.project : [];
    const isLoaded = options.length > 0;
    return (
        <Section fluid>
            <DynamicContent isLoaded={isLoaded} fetching={fetching}>
                {isLoaded && <ReportTabs />}
            </DynamicContent>
        </Section>
    );
};

export default Reports;
