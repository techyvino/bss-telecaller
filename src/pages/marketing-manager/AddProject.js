import React from "react";
import Section, { SectionTitle } from "../../components/layout/Section";
import AddProjectForm from "../../components/marketing-manager/master/AddProjectForm";

const AddBudget = () => {
    return (
        <Section>
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <SectionTitle title="Add Project" />
                    <AddProjectForm />
                </div>
            </div>
        </Section>
    );
};

export default AddBudget;
