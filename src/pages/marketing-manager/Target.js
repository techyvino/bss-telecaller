import React from "react";
import Section from "../../components/layout/Section";
import TargetList from "../../components/marketing-manager/target/TargetList";

const Target = () => {
    return (
        <Section>
            <TargetList />
        </Section>
    );
};

export default Target;
