import React from "react";
import Section from "../../components/layout/Section";
import PortalLeadTabs from "../../components/marketing-manager/portalleads/PortalLeadTabs";

const PortalLeads = () => {
    return (
        <Section fluid>
            <PortalLeadTabs />
        </Section>
    );
};

export default PortalLeads;
