import React from "react";
import Section from "../../components/layout/Section";
import BudgetList from "../../components/marketing-manager/budget/BudgetList";

const Budget = () => {
    return (
        <Section>
            <BudgetList />
        </Section>
    );
};

export default Budget;
