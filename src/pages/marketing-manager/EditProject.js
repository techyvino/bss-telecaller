import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { masterUrls } from "../../apiService/urls";
import DynamicContent from "../../components/common/DynamicContent";
import Section, { SectionTitle } from "../../components/layout/Section";
import { EditProjectForm } from "../../components/marketing-manager/master/AddProjectForm";
import useFetchData from "../../hooks/http/useFetchData";

const AddBudget = () => {
    const { projectId } = useParams();
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        if (projectId) {
            setUrl(masterUrls.addProject + `${projectId}/`);
        }
    }, [setUrl, projectId]);

    return (
        <Section>
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <SectionTitle title="Edit Project" />
                    <DynamicContent
                        fetching={state.fetching}
                        isLoaded={state.data && state.data.id}
                    >
                        {state.data && state.data.id && (
                            <EditProjectForm data={state?.data} />
                        )}
                    </DynamicContent>
                </div>
            </div>
        </Section>
    );
};

export default AddBudget;
