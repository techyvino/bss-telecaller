import React from "react";
import Section from "../../components/layout/Section";
import LeadTabs from "../../components/marketing-manager/superreceptionist/LeadTabs";

const SuperReceptionist = () => {
    return (
        <Section fluid>
            <LeadTabs />
        </Section>
    );
};

export default SuperReceptionist;
