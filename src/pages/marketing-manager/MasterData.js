import React from "react";
import Section from "../../components/layout/Section";
import MasterTabs from "../../components/marketing-manager/master/MasterTabs";

const MasterData = () => {
    return (
        <Section fluid>
            <MasterTabs />
        </Section>
    );
};

export default MasterData;
