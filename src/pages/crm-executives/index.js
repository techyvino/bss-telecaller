import React, { lazy } from "react";
import AuthRoute from "../../components/auth/AuthRoute";
import RoleRoutes from "../RoleRoutes";

export const pathName = "/crm-executives";

export const paths = {
    crm: "/crm",
};

export const landing = `${pathName}${paths.crm}`;

export const menu = [
    {
        title: "CRM",
        to: landing,
    },
];

export const routes = [
    {
        path: paths.crm,
        exact: true,
        component: lazy(() => import("./crm")),
    },
];

export const allowRoles = [11];

export default (
    <AuthRoute path={pathName}>
        <RoleRoutes
            title="CRM - Executives"
            routes={routes}
            menu={menu}
            landing={landing}
            allowRoles={allowRoles}
        />
    </AuthRoute>
);
