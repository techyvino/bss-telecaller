import React, { useEffect, useState } from "react";
import { crmExecutiveUrls } from "../../apiService/urls";
import DynamicContent from "../../components/common/DynamicContent";
import { useMasterValues } from "../../components/common/MasterData";
import Tabs from "../../components/common/Tabs";
import Section from "../../components/layout/Section";
import Leads from "../../components/sales-general-manager/salesteam/Leads";
import LeadsTable from "../../components/sales-manager/leads/LeadsTable";
import LeadTabs from "../../components/sales-manager/leads/LeadTabs";
import usePagination from "../../hooks/http/usePagination";

import { isArray } from "../../utils";

const tabs = [
    "Customer Master",
    "Customer Payments",
    "Bank Reconcilation",
    "Bulk Data",
    "Mailing",
    "Projects",
];
const CRM = () => {
    const [fetching, data] = useMasterValues();

    const [state, { setUrl, setPage }] = usePagination();

    // useEffect(() => {
    //     setUrl({
    //         baseUrl: crmExecutiveUrls.leads,
    //     });
    // }, [setUrl]);

    const isLoaded = isArray(data.project) && data.project.length > 0;
    // console.log(data.project);

    return (
        <Section fluid>
            <Tabs tabs={tabs}>
                <DynamicContent fetching={fetching} isLoaded={isLoaded}>
                    {isLoaded && <LeadsTable projects={data.project} />}
                </DynamicContent>
            </Tabs>
        </Section>
    );
};
export default CRM;
