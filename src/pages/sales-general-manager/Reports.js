import React from "react";
import Tabs, { TabBtnList, TabPane } from "../../components/common/Tabs";
import Section from "../../components/layout/Section";
import BoookingReport from "../../components/sales-general-manager/reports/BookingReport";
import CancellationReport from "../../components/sales-general-manager/reports/CancellationReport";
import ProjectWiseReport from "../../components/sales-general-manager/reports/ProjectWiseReport";
import SalesReport from "../../components/sales-general-manager/reports/SalesReport";
import SalesSheetReport from "../../components/sales-general-manager/reports/SalesSheetReport";
import SwappingReport from "../../components/sales-general-manager/reports/SwappingReport";

const tabs = [
    "Daily DSR REPORT",
    "Weekly  Sales Review Report",
    "Booking Report",
    "Sales Sheet Report",
    "Cancellation Report",
    "Swapping Report"
];

const Reports = () => {
    return (
        <Section fluid>
            <Tabs tabs={tabs}>
                <TabBtnList />
                <TabPane tabId={tabs[0]}>
                    <ProjectWiseReport />
                </TabPane>
                <TabPane tabId={tabs[1]}>
                    <SalesReport />
                </TabPane>
                <TabPane tabId={tabs[2]}>
                    <BoookingReport />
                </TabPane>
                <TabPane tabId={tabs[3]}>
                    <SalesSheetReport />
                </TabPane>
                <TabPane tabId={tabs[4]}>
                    <CancellationReport />
                </TabPane>
                <TabPane tabId={tabs[5]}>
                    <SwappingReport />
                </TabPane>
            </Tabs>
        </Section>
    );
};

export default Reports;
