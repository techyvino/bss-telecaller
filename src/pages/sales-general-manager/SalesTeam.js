import React from "react";
import DynamicContent from "../../components/common/DynamicContent";
import { useMasterValues } from "../../components/common/MasterData";
import Section from "../../components/layout/Section";
import Leads from "../../components/sales-general-manager/salesteam/Leads";
import { isArray } from "../../utils";

const SalesTeam = () => {
    const [fetching, data] = useMasterValues();
    const isLoaded = isArray(data.project) && data.project.length > 0;
    console.log(data);

    return (
        <Section fluid>
            <DynamicContent fetching={fetching} isLoaded={isLoaded}>
                {isLoaded && <Leads projects={data.project} />}
            </DynamicContent>
        </Section>
    );
};

export default SalesTeam;
