import React from "react";
import Section from "../../components/layout/Section";
import TargetList from "../../components/sales-general-manager/target/TargetList";

const SalesTeam = () => {
    return (
        <Section>
           <TargetList />
        </Section>
    );
};

export default SalesTeam;
