/* eslint-disable eqeqeq */
import React, { useEffect } from "react";
import { useHistory } from "react-router";
import { salesGmUrls } from "../../apiService/urls";
import { useMasterValues } from "../../components/common/MasterData";
import Section from "../../components/layout/Section";
import useFetchData from "../../hooks/http/useFetchData";
import useQuery from "../../hooks/useQuery";
import { isArray } from "../../utils";
import { BookingTabs, ProjectSelect } from "../sales-manager/Bookings";

const TableWithFilters = ({ teams = [], projects = [] }) => {
    const query = useQuery();
    const history = useHistory();
    const project_id = query.get("project") ?? "";
    const team = query.get("team") ?? "";
    const employee_id = query.get("employee") ?? "";

    const team_mates = teams.filter((x) => x.value == team)?.[0]?.team_mates;
    const employees = isArray(team_mates) ? team_mates : [];

    return (
        <>
            <div className="row">
                <div className="col-md-3 mb-3">
                    <ProjectSelect
                        onChange={(val) => {
                            if (val) {
                                query.set("project", val);
                            } else {
                                query.delete("project");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        options={projects}
                        value={project_id}
                    />
                </div>
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("team", val);
                            } else {
                                query.delete("team");
                            }
                            query.delete("employee");
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={team}
                    >
                        <option value="">All Teams</option>
                        {teams.map((x) => (
                            <option value={x.value} key={x.value}>
                                {x.label}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("employee", val);
                            } else {
                                query.delete("employee");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={employee_id}
                    >
                        <option value="">All Sales Person</option>
                        {employees.map((x) => (
                            <option value={x.id} key={x.id}>
                                {`${x?.user?.first_name ?? ""} ${
                                    x?.user?.last_name ?? ""
                                }`}
                            </option>
                        ))}
                    </select>
                </div>
            </div>
            <BookingTabs filters={{ project_id, team, employee_id }} />
        </>
    );
};

const mapTeams = (team = []) =>
    team.map((x) => ({
        value: x.head.id,
        label: `${x?.head?.user?.first_name ?? ""} ${
            x?.head?.user?.last_name ?? ""
        }`,
        team_mates: x.team_mates,
    }));

const Bookings = () => {
    const [state, { setUrl }] = useFetchData();
    const data = useMasterValues()[1];
    const projects = isArray(data?.project) ? data.project : [];

    useEffect(() => {
        setUrl(salesGmUrls.salesTeamLead);
    }, [setUrl]);

    return (
        <Section fluid>
            <TableWithFilters
                teams={isArray(state.data) ? mapTeams(state.data) : []}
                projects={projects}
            />
        </Section>
    );
};

export default Bookings;
