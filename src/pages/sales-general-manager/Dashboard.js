import React from 'react'
import Section from "../../components/layout/Section";
import FtdList from '../../components/sales-general-manager/dashboard/FtdList';

const Dashboard = () => {
    return (
        <Section>
            <FtdList />
        </Section>
    )
}

export default Dashboard
