import React from 'react'
import AddLeadForm from '../../components/front-office-executive/AddLeadForm'

const CustomerForm = () => {
    return <AddLeadForm />
}

export default CustomerForm
