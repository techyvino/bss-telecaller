import React from "react";
import Tabs, { TabBtnList, TabPane } from "../../components/common/Tabs";
import Section from "../../components/layout/Section";
import BookingReport from "../../components/sales-coordinator/reports/BookingReport";
import SalesReport from "../../components/sales-coordinator/reports/SalesReport";
import SiteVisitReport from "../../components/sales-coordinator/reports/SiteVisitReport";

const tabs = ["Sales Sheet", "Site Visit", "Booking"];

const Reports = () => {
    return (
        <Section fluid>
            <Tabs qName="tab" tabs={tabs}>
                <TabBtnList />
                <TabPane tabId={tabs[0]}>
                    <SalesReport />
                </TabPane>
                <TabPane tabId={tabs[1]}>
                    <SiteVisitReport />
                </TabPane>
                <TabPane tabId={tabs[2]}>
                    <BookingReport />
                </TabPane>
            </Tabs>
        </Section>
    );
};

export default Reports;
