import React from "react";
import Section from "../../components/layout/Section";
import BlockApartments from "../../components/sales-coordinator/availabilty/BlockApartments";

const Availability = () => {
    return (
        <Section>
            <BlockApartments />
        </Section>
    );
};

export default Availability;
