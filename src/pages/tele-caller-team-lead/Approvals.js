import React from 'react';
import { useHistory } from 'react-router-dom';
import { isArray } from 'validate.js';
import DynamicContent from '../../components/common/DynamicContent';
import { useMasterValues } from '../../components/common/MasterData';
import Section from '../../components/layout/Section';
import ApprovalTabs from '../../components/tele-caller-team-lead/approvals/ApprovalTabs';
import useQuery from '../../hooks/useQuery';



const ProjectLeads = ({ options = [] }) => {
    const query = useQuery();
    const filters = JSON.parse(query.get("filters"))
    const history = useHistory();
    const project = query.get("project") ?? "";

    return (
        <>
            <div className="row">
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("project", val);
                            } else {
                                query.delete("project");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={project}
                    >
                        <option value="">
                            All Project
                        </option>
                        {options.map((x) => (
                            <option value={x.id} key={x.id}>
                                {x.title}
                            </option>
                        ))}
                    </select>
                </div>
            </div>
            <ApprovalTabs filters={filters} projectID={project} />
        </>
    );
};


const Approvals = () => {
    const [fetching, data] = useMasterValues();
    const options = isArray(data?.project) ? data.project : [];
    const isLoaded = options.length > 0;
    return (
        <Section fluid>
            <DynamicContent isLoaded={isLoaded} fetching={fetching}>

                {isLoaded && <ProjectLeads options={options} />}
            </DynamicContent>
        </Section>
    );
}

export default Approvals;