import React from "react";
import Section from "../../components/layout/Section";
import AssignSalesManger from "../../components/tele-caller-team-lead/team/assignSalesManager";

const TeamDetails = () => {

    return (
        <Section fluid>
            <AssignSalesManger />
        </Section>
    );
};

export default TeamDetails;
