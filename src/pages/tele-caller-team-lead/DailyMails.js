import React from 'react'
import Section from "../../components/layout/Section";
import ReportsTable from '../../components/reports/ReportsTable';

const DailyMails = () => {
    return (
        <Section>
            <ReportsTable />
        </Section>
    )
}

export default DailyMails
