import React from 'react'
import { isArray } from 'validate.js'
import DynamicContent from '../../components/common/DynamicContent'
import { useMasterValues } from '../../components/common/MasterData'
import Section from '../../components/layout/Section'
import TeamReportTabs from '../../components/tele-caller-team-lead/reports/TeamReportTabs'

const Reports = () => {
    const [fetching, data] = useMasterValues();
    const options = isArray(data?.sales_manager) ? data.sales_manager : [];
    const isLoaded = options.length > 0;
    return (
        <Section fluid>
            <DynamicContent isLoaded={isLoaded} fetching={fetching}>
                <TeamReportTabs options={options} />
            </DynamicContent>
        </Section>
    )
}

export default Reports
