import React from "react";
import Section from "../../components/layout/Section";
import TeamLeadsTable from "../../components/tele-caller-team-lead/leads/TeamLeads";

const TeamLeads = () => {

    return (
        <Section fluid>
            <TeamLeadsTable />
        </Section>
    );
};

export default TeamLeads;
