import React from "react";

const ReportsTable = () => {
    return (
        <table className="table table-bordered table-responsive-l">
            <thead>
                <tr className="f-14">
                    <th scope="col">#</th>
                    <th scope="col">Project</th>
                    <th scope="col">Lead Id </th>
                    <th scope="col">Status</th>
                    <th scope="col">Customer Name</th>
                    <th scope="col">Customer Email</th>
                    <th scope="col">Customer Phone</th>
                    <th scope="col">Source</th>
                    <th scope="col">Campaign</th>
                    <th scope="col">Primary Source</th>
                    <th scope="col">Sales Manager</th>
                    <th scope="col">Date Entered</th>
                    <th scope="col">Date Assigned</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Plutus</td>
                    <td>12345</td>
                    <td>
                        <span className="badge badge-danger">HOT</span>
                    </td>
                    <td>Dilip</td>
                    <td>dilip@billiontags.com</td>
                    <td>8778075612</td>
                    <td>Tele</td>
                    <td>Paper Ad</td>
                    <td>Hindu</td>
                    <td>Jack</td>
                    <td>22 Oct 2020</td>
                    <td>22 Oct 2020</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Plutus</td>
                    <td>12345</td>
                    <td>
                        <span className="badge badge-secondary">COLD</span>
                    </td>
                    <td>Dilip</td>
                    <td>dilip@billiontags.com</td>
                    <td>8778075612</td>
                    <td>Tele</td>
                    <td>Paper Ad</td>
                    <td>Hindu</td>
                    <td>Jack</td>
                    <td>22 Oct 2020</td>
                    <td>22 Oct 2020</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Plutus</td>
                    <td>12345</td>
                    <td>
                        <span className="badge badge-danger">HOT</span>
                    </td>
                    <td>Dilip</td>
                    <td>dilip@billiontags.com</td>
                    <td>8778075612</td>
                    <td>Tele</td>
                    <td>Paper Ad</td>
                    <td>Hindu</td>
                    <td>Jack</td>
                    <td>22 Oct 2020</td>
                    <td>22 Oct 2020</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Plutus</td>
                    <td>12345</td>
                    <td>
                        <span className="badge badge-warning">WARM</span>
                    </td>
                    <td>Dilip</td>
                    <td>dilip@billiontags.com</td>
                    <td>8778075612</td>
                    <td>Tele</td>
                    <td>Paper Ad</td>
                    <td>Hindu</td>
                    <td>Jack</td>
                    <td>22 Oct 2020</td>
                    <td>22 Oct 2020</td>
                </tr>
            </tbody>
        </table>
    );
};

export default ReportsTable;
