import React from "react";
import reportfilters from "../../forms/filters/reportfilters";
import useFormReducer from "../../hooks/form/useFormReducer";
import FieldArray from "../form/FieldArray";
import { FormProvider } from "../form/Form";

const ReportFilters = () => {
    const form = useFormReducer();

    return (
        <FormProvider form={form}>
            <div className="form-row">
                <FieldArray allIds={reportfilters()} />
                <div className="col-md-2 form-group">
                    <button className="btn btn-outline-black btn-block">
                        Submit
                    </button>
                </div>
            </div>
        </FormProvider>
    );
};

export default ReportFilters;
