import React from "react";
import Tabs, { TabBtnList, TabPane } from "../common/Tabs";
import ReportFilters from "./ReportFilters";
import ReportsTable from "./ReportsTable";

const tabs = [
    "Today Assigned Enquiries",
    "Overall Assigned Enquiries",
];

//date search project sales person

const ReportTabs = () => {
    return (
        <Tabs tabs={tabs}>
            <TabBtnList />
            <TabPane tabId={tabs[0]}>
                <ReportFilters />
                <ReportsTable />
            </TabPane>
            <TabPane tabId={tabs[1]}>
                <ReportFilters />
                <ReportsTable />
            </TabPane>
        </Tabs>
    );
};

export default ReportTabs;
