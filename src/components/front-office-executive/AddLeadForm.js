import React, { useCallback, useMemo, useState } from "react";
import useFormReducer from "../../hooks/form/useFormReducer";
import FieldArray from "../form/FieldArray";
import { FormProvider } from "../form/Form";
import { form1, form2, formKeys, init } from "./addLeads";
// import logo from "../../images/baashyaam-logo.png";
import { useMasterValues } from "../common/MasterData";
import SubmitBtn from "../form/SubmitBtn";
import useSubmit from "../../hooks/http/useSubmit";
import { resetForm, setErrors, setValues } from "../../hooks/form/formReducer";
import useScrollToError from "../../hooks/form/useScrollToError";
import validateAll from "../form/validateAll";
import formatAllData from "../form/formatAllData";
import urls from "../../apiService/urls";
import useSalesTeam from "../tele-caller/leads/useSalesTeam";
import dayjs from "dayjs";
import { toBoolean } from "../../utils";
import DynamicContent from "../common/DynamicContent";
import {
    isEmailorNA,
    isNA,
    isPhoneorNA,
} from "../tele-caller/leads/leadValidations";
import FieldCon from "../form/FieldCon";
import Input from "../form/Input";
import useField from "../../hooks/form/useField";
import useNumber from "../tele-caller/leads/useNumber";
import NumberModal, { SuccessModal } from "../tele-caller/leads/NumberModal";

const CheckLeadIp = ({
    disabled = false,
    placeholder = "",
    onBlur = null,
    label = "",
    required = false,
    conClass = "",
    isChecking = false,
    name,
}) => {
    const { value, err, setValue } = useField(name);

    return (
        <FieldCon
            conClass={conClass}
            required={required}
            label={label}
            infoText={isChecking ? "Checking duplication..." : ""}
            err={err}
        >
            <Input
                placeholder={placeholder}
                value={value}
                setValue={(val) => setValue(val)}
                onBlur={() => onBlur(value)}
                disabled={disabled}
            />
        </FieldCon>
    );
};

const Form = ({ masterData }) => {
    const [successData, setSuccess] = useState(null);

    const clearSuccess = useCallback(() => {
        setSuccess(null);
    }, []);

    const scrolltoError = useScrollToError();

    const value = useFormReducer({ values: init });
    const [{ values }, dispatch] = value;

    const { flat_type = [], ...master } = masterData;

    const [fetching, submit] = useSubmit({
        success: (data) => {
            dispatch(resetForm());
            dispatch(setValues(init));
            setSuccess(data);
        },
    });

    const allIds1 = useMemo(() => form1(master), [master]);

    const project_id = values[formKeys.project_id]?.value;
    const salesTeam = useSalesTeam(project_id);
    const allIds2 = useMemo(() => form2(flat_type, salesTeam), [
        flat_type,
        salesTeam,
    ]);

    const onSubmit = (e) => {
        e.preventDefault();
        const allIds = [...allIds1, ...allIds2];
        const errors = validateAll(allIds, values);
        const numberErr = isPhoneorNA(values[formKeys.number], true);
        if (numberErr) {
            Object.assign(errors, { [formKeys.number]: numberErr });
        }
        const emailErr = isEmailorNA(values[formKeys.email], true);
        if (emailErr) {
            Object.assign(errors, { [formKeys.email]: emailErr });
        }
        if (Object.keys(errors).length > 0) {
            dispatch(setErrors(errors));
            scrolltoError();
            return false;
        }
        const data = formatAllData(allIds, values);
 
        Object.assign(data, {
            enquiry_date: dayjs().format("YYYY-MM-DD"),
            status_id: 8,
        });
        if (!isNA(values[formKeys.email])) {
            Object.assign(data, {
                email: values[formKeys.email],
            });
        }
        if (!isNA(values[formKeys.number])) {
            Object.assign(data, {
                number: values[formKeys.number],
            });
        }
        submit({
            url: urls.leads,
            method: "POST",
            data,
        });
    };

    const { isChecking, numberValid, onNumberBlur, clearValid } = useNumber();

    return (
        <section className="pt-5 pb-5">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-10 offset-md-1 col-lg-6 offset-lg-3">
                        <div className="text-center">
                            <h3 className="text-center mb-5">
                                CUSTOMER ENQUIRY FORM
                            </h3>
                        </div>
                        <form
                            className="form-spacing-large form-uppercase-label pb-5"
                            autoComplete="off"
                            noValidate
                            onSubmit={onSubmit}
                        >
                            <FormProvider form={value}>
                                <div className="row">
                                    <FieldArray allIds={allIds1} />
                                    <CheckLeadIp
                                        name={formKeys.number}
                                        conClass="col-md-8"
                                        required
                                        label="Phone Number"
                                        placeholder="Enter number"
                                        onBlur={onNumberBlur}
                                        isChecking={isChecking}
                                    />
                                    <CheckLeadIp
                                        name={formKeys.email}
                                        conClass="col-md-12"
                                        required
                                        label="Email"
                                        placeholder="Enter email"
                                        onBlur={onNumberBlur}
                                        isChecking={isChecking}
                                    />
                                    {numberValid && numberValid.id && (
                                        <NumberModal
                                            enquiry={numberValid}
                                            close={clearValid}
                                        />
                                    )}
                                </div>
                                <div className="row">
                                    <FieldArray allIds={allIds2} />
                                </div>
                                <SubmitBtn
                                    disabled={isChecking}
                                    fetching={fetching}
                                    className="btn btn-theme btn-block"
                                >
                                    Submit
                                </SubmitBtn>
                            </FormProvider>
                        </form>
                        {successData && successData.id && (
                            <SuccessModal
                                enquiry={successData}
                                close={clearSuccess}
                            />
                        )}
                    </div>
                </div>
            </div>
        </section>
    );
};

const AddLeadForm = () => {
    const [isloading, data] = useMasterValues();

    const isLoaded = toBoolean(data?.project);

    return (
        <DynamicContent isLoaded={isLoaded} fetching={isloading}>
            {isLoaded && <Form masterData={data} />}
        </DynamicContent>
    );
};

export default AddLeadForm;
