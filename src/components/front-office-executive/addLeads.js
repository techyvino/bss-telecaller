import { isArray } from "../../utils";
import { salutations } from "../common/master";
import formTypes from "../form/formTypes";

export const formKeys = {
    project_id: "project_id",
    enquiry_date: "enquiry_date",
    salutation: "salutation",
    name: "name",
    number_code: "primary_code_id",
    number: "number",
    email: "email",
    source: "source",
    campaign: "campaign",
    source_of_campaigning_id: "source_of_campaigning_id",
    address: "address",
    area: "area",
    city: "city",
    pincode: "pincode",
    customer_interest_id: "customer_interest_id",
    sales_manager: "assigned_to_id",
    reference: "know_us",
    feedback: "feedback",
};

// Transport Required
const mapOpt = (opt) => ({ label: opt.title, value: opt.id });
const mapOpts = (options = []) => options.map(mapOpt);

//country
const getDialCode = (opt) => ({ label: opt.dial_string, value: opt.id });
const getDialCodes = (options = []) => options.map(getDialCode);

const mapAssignTo = (x) => ({
    label: x.user ? `${x.user.first_name} ${x.user.last_name}` : x.emp_id,
    value: x.id,
});

const references = [
    { value: "Social Media", label: "Social Media" },
    { value: "Print Advertisement", label: "Print Advertisement" },
    { value: "Radio Advertisement", label: "Radio Advertisement" },
    { value: "TV Advertisement", label: "TV Advertisement" },
    { value: "Hoardings", label: "Hoardings" },
    { value: "Customer Reference", label: "Customer Reference" },
    { value: "Employee Reference", label: "Employee Reference" },
    { value: "Management Reference", label: "Management Reference" },
];

export const sId = { label: "Direct Walkin", value: 10 };

export const init = {
    [formKeys.reference]: references[0],
    [formKeys.number_code]: { value: 1, label: "+91 (IND)" },
    [formKeys.salutation]: salutations[0],
    [formKeys.source_of_campaigning_id]: sId,
};

export const form1 = ({ project = [], country = [] }) => [
    {
        name: formKeys.project_id,
        type: formTypes.select,
        label: "Project",
        inputProps: {
            options: mapOpts(project),
            placeholder: "Select an option",
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: formKeys.salutation,
        type: formTypes.select,
        label: "Salutation",
        inputProps: {
            placeholder: "Saluation",
            options: salutations,
        },
        conClass: "col-md-4",
        required: true,
    },
    {
        name: formKeys.name,
        type: formTypes.text,
        label: "Full Name",
        inputProps: {
            placeholder: "Enter full name",
        },
        conClass: "col-md-8",
        required: true,
    },
    {
        name: formKeys.number_code,
        type: formTypes.select,
        label: "Country Code",
        inputProps: {
            placeholder: "Select an option",
            options: getDialCodes(country),
        },
        conClass: "col-md-4",
        required: true,
    },
];

export const form2 = (flat_type = [], team) => [
    {
        name: formKeys.source_of_campaigning_id,
        type: formTypes.select,
        label: "Source",
        inputProps: {
            options: [sId],
            placeholder: "Select an option",
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: formKeys.address,
        type: formTypes.textarea,
        label: "Present Address",
        inputProps: {
            placeholder: "Enter present address",
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: formKeys.customer_interest_id,
        type: formTypes.select,
        label: "Customer Interest",
        inputProps: {
            placeholder: "Select an option",
            options: flat_type.map((x) => ({ label: x.title, value: x.id })),
            isMulti: true,
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: formKeys.sales_manager,
        type: formTypes.select,
        label: "Sales Manager",
        inputProps: {
            options: isArray(team?.data) ? team.data.map(mapAssignTo) : [],
            placeholder: "Select an option",
            isloading: team?.fetching,
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: formKeys.reference,
        type: formTypes.select,
        label: "How did you hear about us",
        inputProps: {
            options: references,
            placeholder: "Select an option",
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: formKeys.feedback,
        type: formTypes.textarea,
        label: "Feedback",
        inputProps: {
            placeholder: "Enter your feedback",
        },
        conClass: "col-md-12",
        required: true,
    },
];
