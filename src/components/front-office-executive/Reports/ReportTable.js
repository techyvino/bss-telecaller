import React, { useEffect } from "react";
import { useHistory } from "react-router";
import { frontOfficeExecutiveUrls } from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import useQuery from "../../../hooks/useQuery";
import { getFilters } from "../../../utils/keysToQuery";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import ReportFilters from "./ReportFilters";


const badgeCls = (id) => {
    switch (id) {
        case 1:
            return "badge-danger";
        case 2:
            return "badge-warning";
        case 3:
            return "badge-secondary";
        default:
            return "";
    }
};

const Status = ({ id, title }) => {
    return (
        <span className={`badge text-uppercase ${badgeCls(id)}`}>{title}</span>
    );
};

const CustomerDetails = ({
    name,
    number,
    secondary_email,
    email,
    secondary_number,
}) => {
    return (
        <div>
            <div>{name}</div>
            <div>
                {email} {secondary_email ? ` / ${secondary_email}` : ""}
            </div>
            <div>
                {number} {secondary_number ? ` / ${secondary_number}` : ""}
            </div>
        </div>
    );
};

const tbData = [
    {
        th: "Project",
        td: (data) => data.project.title,
    },
    {
        th: "Lead Id",
        td: (data) => data?.lead_no,
    },
    {
        th: "Status",
        td: (data) => <Status {...(data.status || {})} />,
    },
    {
        th: "Customer Details",
        td: (data) => <CustomerDetails {...data} />,
    },
    {
        th: "Source",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning.campaign
                ? source_of_campaigning.campaign.source
                    ? source_of_campaigning.campaign.source.title
                    : ""
                : "",
    },
    {
        th: "Campaign",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning.campaign
                ? source_of_campaigning.campaign.title
                : "",
    },
    {
        th: "Primary Source",
        td: ({ source_of_campaigning }) => source_of_campaigning.title,
    },
    {
        th: "Sales Manager",
        td: ({ assigned_to }) =>
            assigned_to && assigned_to.user
                ? `${assigned_to.user.first_name} ${assigned_to.user.last_name}`
                : "",
    },
    {
        th: "Telecaller",
        td: ({ created_by }) =>
            created_by && created_by.user
                ? `${created_by.user.first_name} ${created_by.user.last_name}`
                : "",
    },
    {
        th: "Date Entered",
        td: (data) => <DateFormat date={data.created_on} />,
    },
    {
        th: "Date Assigned",
        td: (data) => <DateFormat date={data.assigned_on} />,
    },
];

const ReportTable = ({ tab, project }) => {
    const query = useQuery();
    const history = useHistory();
    const [state, { setUrl, setPage }] = usePagination();
    const filterQuery = query.get("filters") || "";

    const onFilter = (f = {}) => {
        query.set("filters", JSON.stringify(f));
        history.replace({
            search: `?${query.toString()}`,
        });
    };

    useEffect(() => {
        const tableFilters = getFilters(filterQuery)
        setUrl({
            baseUrl: frontOfficeExecutiveUrls.report,
            filters: { ...tableFilters, tab, project },
        });
    }, [setUrl, tab, filterQuery, project]);
    return (
        <>
            <ReportFilters onFilter={onFilter} />
            <TableList
                pageState={state}
                onPageClick={setPage}
                tableData={tbData}
            />
        </>
    );
};

export default ReportTable;
