import React from "react";
import { frontOfficeExecutiveUrls } from "../../../apiService/urls";
import useQuery from "../../../hooks/useQuery";
import keysToQuery from "../../../utils/keysToQuery";
import UrlParamTabList from "../../common/UrlParamTabList";
import ReportTable from "./ReportTable";

const ReportTab = ({ projectID }) => {
    const query = useQuery();
    const filters = JSON.parse(query.get("filters"))

    const renderTable = ({ id }) => {
        return (
            <>
                <ReportTable tab={id} filters={filters} project={projectID} />
            </>
        );
    };

    const url = `${frontOfficeExecutiveUrls.report_tab}${keysToQuery(filters ? { project: projectID, ...filters } : { project: projectID, })}`;

    return (
        <UrlParamTabList url={url}>{renderTable}</UrlParamTabList>
    );
};

export default ReportTab;