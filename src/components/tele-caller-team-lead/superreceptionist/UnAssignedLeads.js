import React, { useEffect } from 'react';
import urls from '../../../apiService/urls';
import usePagination from '../../../hooks/http/usePagination';
import useToggle from '../../../hooks/useToggle';
import DateFormat from '../../common/DateFormat';
import TableList from '../../common/TableList';
import AssignLeadsModal from './AssignLeadsModel';

const tableData = [
    {
        th: "Call Date & Time",
        td: ({ start_time }) => <DateFormat date={start_time} />
    },
    {
        th: "Customer Number",
        td: ({ caller_id }) => caller_id
    },
    {
        th: "Call Status",
        td: ({ call_status }) => call_status
    },
    {
        th: "Duration",
        td: ({ duration }) => duration
    },
    {
        th: "Tele-Caller / Sales Manager",
        td: ({ employee }) => employee?.user?.first_name ??  ""
    },

]

const UnAssignedLeads = () => {
    const [pageState, { setUrl, setPage }] = usePagination();
    const { toggle, onTrue, onFalse } = useToggle()

    useEffect(() => {
        setUrl({
            baseUrl: `${urls.superReceptionist}`,
            filters: { tab: 0 }
        });
    }, [setUrl]);

    return (
        <div>
            <div className="row mb-3">
                <div className="col-md-9"></div>
                <div className="col-md-3">
                    <div className="d-flex justify-content-end">
                        <button onClick={onTrue} className="btn btn-sm btn btn-theme ">Assign Calls</button>
                    </div>

                </div>
            </div>
            {toggle && <AssignLeadsModal close={onFalse} />}
            <TableList
                pageState={pageState}
                onPageClick={setPage}
                tableData={tableData}
            />
        </div>
    );
}

export default UnAssignedLeads;