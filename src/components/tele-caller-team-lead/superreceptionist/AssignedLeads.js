import React, { useEffect } from 'react';
import urls from '../../../apiService/urls';
import usePagination from '../../../hooks/http/usePagination';
import DateFormat from '../../common/DateFormat';
import TableList from '../../common/TableList';

const tableData = [
    {
        th: "Call Date & Time",
        td: ({ start_time }) => <DateFormat date={start_time} />
    },
    {
        th: "Customer Number",
        td: ({ caller_id }) => caller_id
    },
    {
        th: "Call Status",
        td: ({ call_status }) => call_status
    },
    {
        th: "Duration",
        td: ({ duration }) => duration
    },
    {
        th: "Tele-Caller",
        td: ({ employee }) => employee?.user?.first_name ??  ""
    },

]


const AssignedLeads = () => {
    const [pageState, { setUrl, setPage }] = usePagination();

    useEffect(() => {
        setUrl({
            baseUrl: `${urls.superReceptionist}`,
            filters: { tab: 1 }
        });
    }, [setUrl]);
    return (
        <div>
            <TableList
                pageState={pageState}
                onPageClick={setPage}
                tableData={tableData}
            />
        </div>
    );
}

export default AssignedLeads;