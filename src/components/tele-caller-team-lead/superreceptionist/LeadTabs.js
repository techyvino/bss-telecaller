import React from 'react';
import Tabs, { TabBtnList, TabPane } from '../../common/Tabs';
import AssignedLeads from './AssignedLeads';
import UnAssignedLeads from './UnAssignedLeads';

const tabs = ["Un Assigned Calls", "Assigned Calls"]
const LeadTabs = () => {
    return (
        <Tabs tabs={tabs}>
            <TabBtnList />
            <TabPane tabId={tabs[0]}>
                <UnAssignedLeads />
            </TabPane>

            <TabPane tabId={tabs[1]}>
                <AssignedLeads />
            </TabPane>

        </Tabs>
    );
}

export default LeadTabs;