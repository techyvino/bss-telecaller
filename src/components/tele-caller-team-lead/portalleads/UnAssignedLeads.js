import React, { useEffect } from 'react';
import urls from '../../../apiService/urls';
import usePagination from '../../../hooks/http/usePagination';
import useToggle from '../../../hooks/useToggle';
import DateFormat from '../../common/DateFormat';
import TableList from '../../common/TableList';
import AssignLeadsModal from './AssignLeadsModel';

const tableData = [
    {
        th: "Portal",
        td: ({ portal }) => portal
    },
    {
        th: "Project",
        td: ({ project }) => project
    },
    {
        th: "Customer Name",
        td: ({ name }) => name
    },
    {
        th: "Mobile Number",
        td: ({ phone_number }) => phone_number
    },
    {
        th: "Email",
        td: ({ email }) => email
    },
    {
        th: "Address",
        td: ({ address }) => address
    },
    {
        th: "City",
        td: ({ city }) => city
    },
    {
        th: "Country",
        td: ({ country }) => country
    },
    {
        th: "Chat Content",
        td: ({ chat_content }) => chat_content
    },
    {
        th: "Chat Link",
        td: ({ chat_link }) => chat_link
    },
    {
        th: "Apartment Type",
        td: ({ apartment_type }) => apartment_type
    },
    {
        th: "Budget",
        td: ({ budget }) => budget
    },
    {
        th: "Utm Source",
        td: ({ utm_source }) => utm_source
    },
    {
        th: "Utm Campaign",
        td: ({ utm_campaign }) => utm_campaign
    },
    {
        th: "Contact Time",
        td: ({ contact_time }) => contact_time
    },
    {
        th: "Time to Buy",
        td: ({ time_to_buy }) => time_to_buy
    },
    {
        th: "Created On",
        td: ({ created_on }) => <DateFormat date={created_on} />,
    },

]

const UnAssignedLeads = () => {
    const [pageState, { setUrl, setPage }] = usePagination();
    const { toggle, onTrue, onFalse } = useToggle()

    useEffect(() => {
        setUrl({
            baseUrl: `${urls.portalLeads}`,
            filters: { tab: 0 }
        });
    }, [setUrl]);

    const { page, allIds } = pageState;
    const list = allIds[page];

    return (
        <div>
            <div className="row mb-3">
                <div className="col-md-9"></div>
                <div className="col-md-3">
                    {Array.isArray(list) && list.length > 0 &&
                        <div className="d-flex justify-content-end">

                            <button onClick={onTrue} className="btn btn-sm btn btn-theme ">Assign Leads</button>
                        </div>
                    }

                </div>
            </div>
            {toggle && <AssignLeadsModal close={onFalse} />}
            <TableList
                pageState={pageState}
                onPageClick={setPage}
                tableData={tableData}
            />
        </div>
    );
}

export default UnAssignedLeads;