import React, { useEffect } from 'react';
import urls from '../../../apiService/urls';
import usePagination from '../../../hooks/http/usePagination';
import DateFormat from '../../common/DateFormat';
import TableList from '../../common/TableList';

const tableData = [
    {
        th: "Portal",
        td: ({ portal }) => portal
    },
    {
        th: "Project",
        td: ({ project }) => project
    },
    {
        th: "Customer Name",
        td: ({ name }) => name
    },
    {
        th: "Mobile Number",
        td: ({ phone_number }) => phone_number
    },
    {
        th: "Email",
        td: ({ email }) => email
    },
    {
        th: "Address",
        td: ({ address }) => address
    },
    {
        th: "City",
        td: ({ city }) => city
    },
    {
        th: "Country",
        td: ({ country }) => country
    },
    {
        th: "Chat Content",
        td: ({ chat_content }) => chat_content
    },
    {
        th: "Chat Link",
        td: ({ chat_link }) => chat_link
    },
    {
        th: "Apartment Type",
        td: ({ apartment_type }) => apartment_type
    },
    {
        th: "Budget",
        td: ({ budget }) => budget
    },
    {
        th: "Utm Source",
        td: ({ utm_source }) => utm_source
    },
    {
        th: "Utm Campaign",
        td: ({ utm_campaign }) => utm_campaign
    },
    {
        th: "Contact Time",
        td: ({ contact_time }) => contact_time
    },
    {
        th: "Time to Buy",
        td: ({ time_to_buy }) => time_to_buy
    },
    {
        th: "Assigned Telecaller",
        td: ({ assigned_tele_caller }) => assigned_tele_caller?.user ? `${assigned_tele_caller?.user.first_name} ${assigned_tele_caller?.user.last_name}` : "-"
    },
    {
        th: "Assigned On",
        td: ({ assigned_tele_caller_on }) => <DateFormat date={assigned_tele_caller_on} />
    },
    {
        th: "Created On",
        td: ({ created_on }) => <DateFormat date={created_on} />,
    },
]


const AssignedLeads = () => {
    const [pageState, { setUrl, setPage }] = usePagination();

    useEffect(() => {
        setUrl({
            baseUrl: `${urls.portalLeads}`,
            filters: { tab: 1 }
        });
    }, [setUrl]);
    return (
        <div>
            <TableList
                pageState={pageState}
                onPageClick={setPage}
                tableData={tableData}
            />
        </div>
    );
}

export default AssignedLeads;