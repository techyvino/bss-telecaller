import React, { useEffect } from 'react';
import { useMemo } from 'react';
import urls, { teleCallerUrls } from '../../../apiService/urls';
import useFormatAll from '../../../hooks/form/useFormatAll';
import useValidateAll from '../../../hooks/form/useValidateAll';
import useFetchData from '../../../hooks/http/useFetchData';
import FieldArray from '../../form/FieldArray';
import Form from '../../form/Form';
import FormSubmitBtn from '../../form/FormSubmitBtn';
import Modal from '../../modal/Modal';
import ModalBody from '../../modal/ModalBody';
import ModalHeader from '../../modal/ModalHeader';



const assignLeadsForm = (
    isloading = false,
    tele_caller,
) => [

        {
            name: "tele_caller",
            type: "select",
            label: "TL",
            inputProps: {
                placeholder: "TeleCaller TL",
                options: tele_caller,
                isMulti: true,
                isloading,
            },
            conClass: "col-md-12",
            required: true,
        },
    ];


const AssignLeads = ({ fetching, data, succFunc }) => {

    const tele_caller = data ? data.map(x => ({ label: `${x?.user?.first_name} ${x?.user?.last_name}`, value: x.id })) : []
    const allIds = useMemo(() => assignLeadsForm(fetching, tele_caller), [fetching, tele_caller]);

    const validateForm = useValidateAll(allIds);
    const formatData = useFormatAll(allIds);
    return (
        <Form
            resetOnSuccess
            initValues={{ "tele_caller": tele_caller }}
            validateForm={validateForm}
            formatData={formatData}
            config={{
                url: urls.assignPortalLeads,
                method: "POST",
            }}
            handleSuccess={succFunc}
        >
            <div className="form-row">
                <FieldArray allIds={allIds} />
                <div className="col-md-2 form-group">
                    <FormSubmitBtn>
                        Assign
                    </FormSubmitBtn>
                </div>
            </div>
        </Form>
    );
}



const AssignLeadsModal = ({ close }) => {
    const [{ fetching, data }, { setUrl }] = useFetchData();
    useEffect(() => {
        setUrl(teleCallerUrls.teamList);
    }, [setUrl])

    return (
        <Modal isOpen close={close}>
            <ModalHeader title={`Assign to Portal Leads`} close={close} />
            <ModalBody>
                {!fetching &&
                    <AssignLeads fetching={fetching} data={data} />
                }
            </ModalBody>
        </Modal>
    );
}

export default AssignLeadsModal;