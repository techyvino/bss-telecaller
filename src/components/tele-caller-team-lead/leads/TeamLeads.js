import React, { useState } from "react";
import DynamicContent from "../../common/DynamicContent";
import SelectIp from "../../form/SelectIp";
import LeadTabs from "../../tele-caller/leads/LeadTabs";
import useTeamList from "../useTeamList";

const TeamTable = ({ options }) => {
    const [employee, setEmployee] = useState(options[0]);

    return (
        <>
            <div className="row">
                <div className="col-md-4 form-group offset-md-4">
                    <SelectIp
                        value={employee}
                        placeholder="Select Team Member"
                        options={options}
                        setValue={(val) => setEmployee(val)}
                    />
                </div>
            </div>
            {employee && employee.value && (
                <LeadTabs employee_id={employee.value} />
            )}
        </>
    );
};

const TeamLeadsTable = () => {
    const [fetching, options] = useTeamList();
    const isLoaded = options.length > 0;

    return (
        <DynamicContent fetching={fetching} isLoaded={isLoaded}>
            {isLoaded && <TeamTable options={options} />}
        </DynamicContent>
    );
};

export default TeamLeadsTable;
