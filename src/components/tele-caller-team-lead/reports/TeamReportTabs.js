import React from "react";
import Tabs, { TabBtnList, TabPane } from "../../common/Tabs";
import ReportTabs from "../../tele-caller/reports/ReportTabs";

const tabs = ["My Report", "Team"];

const TeamReportTabs = ({ options }) => {
    return (
        <Tabs tabs={tabs}>
            <div className="flex-center">
                <TabBtnList />
            </div>
            <TabPane tabId={tabs[0]}>
                <ReportTabs options={options} team={0} />
            </TabPane>
            <TabPane tabId={tabs[1]}>
                <ReportTabs team={1} options={options} />
            </TabPane>
        </Tabs>
    );
};

export default TeamReportTabs;
