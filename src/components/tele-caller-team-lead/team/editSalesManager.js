import React, { useMemo } from 'react';
import { teleCallerUrls } from '../../../apiService/urls';
import useFormatAll from '../../../hooks/form/useFormatAll';
import useValidateAll from '../../../hooks/form/useValidateAll';
import { useMasterValues } from '../../common/MasterData';
import FieldArray from '../../form/FieldArray';
import Form from '../../form/Form';
import FormSubmitBtn from '../../form/FormSubmitBtn';


const editSalesManagerForm = (
    isloading = false,
    master,
    { sales_manager = [] }
) => [

        {
            name: "sales_manager",
            type: "select",
            label: "Sales Manager",
            inputProps: {
                placeholder: "Sales Manager",
                options: master.sales_manager.map((x) => ({ label: `${x?.user?.first_name} ${x?.user?.last_name}`, value: x.id })),
                isMulti: true,
                isloading,
            },
            conClass: "col-md-12",
            required: true,
        },
    ];

const EditSalesManagerModel = ({ id, succFunc, sales_manager, ...restProps }) => {
    const [isloading, master] = useMasterValues();

    const allIds = useMemo(() => editSalesManagerForm(isloading, master, restProps), [isloading, master, restProps]);

    const validateForm = useValidateAll(allIds);
    const formatData = useFormatAll(allIds);

    return (
        <Form
            resetOnSuccess
            initValues={{ "sales_manager": sales_manager.map((x) => ({ label: `${x?.user?.first_name} ${x?.user?.last_name}`, value: x.id })) }}
            validateForm={validateForm}
            formatData={formatData}
            config={{
                url: teleCallerUrls.addSalesManager(id),
                method: "POST",
            }}
            handleSuccess={succFunc}
        >
            <div className="form-row">
                <FieldArray allIds={allIds} />
                <div className="col-md-2 form-group">
                    <FormSubmitBtn>
                        Submit
                    </FormSubmitBtn>
                </div>
            </div>
        </Form>
    );
}

export default EditSalesManagerModel;