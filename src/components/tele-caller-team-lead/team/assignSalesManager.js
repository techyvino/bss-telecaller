import React, { useCallback, useEffect, useState } from "react";
import { isArray } from "validate.js";
import { teleCallerUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import DynamicContent from "../../common/DynamicContent";
import Modal from "../../modal/Modal";
import EditSalesManagerModel from "./editSalesManager";
import ModalHeader from "../../modal/ModalHeader";
import ModalBody from "../../modal/ModalBody";



const TeamDetailsTable = ({ data, succFunc }) => {
    const [telecaller, setTelecaller] = useState();

    return (
        <div>
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Telecaller Name</th>
                        <th>Assigned Projects</th>
                        <th>Sales Manager</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((x, idx) => (
                        <tr key={x.id}>
                            <td>{idx + 1}</td>
                            <td>{x?.user?.first_name} {x?.user?.last_name}</td>
                            <td>
                                <ul>
                                    {x?.projects.map((project, idex) => (
                                        <li key={project.id}>{project.title}</li>
                                    ))}</ul>
                            </td>
                            <td>

                                <ul>

                                    {x.sales_manager.map((manager) => (
                                        <li key={manager.id}>{manager?.user?.first_name} {manager?.user?.last_name}</li>
                                    ))}
                                </ul>
                                <button onClick={() => setTelecaller(x)} className="btn btn-sm btn-outline-black">
                                    {x.sales_manager.length > 0 ? "Edit / Change Sales Manager" : "Add Sales Manager"}
                                </button>


                            </td>

                        </tr>
                    ))}

                </tbody>

            </table>
            {telecaller?.user && (
                <Modal isOpen close={() => setTelecaller()}>
                    <ModalHeader title={`${telecaller?.user?.first_name} ${telecaller?.user?.last_name}`} close={() => setTelecaller()} />
                    <ModalBody>
                        <EditSalesManagerModel {...telecaller} succFunc={succFunc} />
                    </ModalBody>
                </Modal>
            )}
        </div>
    )
}


const AssignSalesManger = () => {
    const [{ fetching, data }, { setUrl, reload }] = useFetchData();
    useEffect(() => {
        setUrl(teleCallerUrls.teamList);
    }, [setUrl])

    const succFunc = useCallback(() => {
        reload(teleCallerUrls.teamList);
    }, [reload]);

    const isLoaded = isArray(data) && data.length > 0;


    return (
        <DynamicContent fetching={fetching} isLoaded={isLoaded}>
            {isLoaded && <TeamDetailsTable data={data} succFunc={succFunc} />}
        </DynamicContent>
    );
};

export default AssignSalesManger;
