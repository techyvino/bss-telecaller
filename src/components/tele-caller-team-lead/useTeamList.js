import { useEffect } from "react";
import { teleCallerUrls } from "../../apiService/urls";
import useFetchData from "../../hooks/http/useFetchData";
import { isArray } from "../../utils";

const getList = (data = []) =>
    isArray(data)
        ? data.map((x) => ({
              label: x.user ? `${x.user.first_name} ${x.user.last_name}` : "",
              value: x.id,
          }))
        : [];

const useTeamList = () => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(teleCallerUrls.teamList);
    }, [setUrl]);

    return [state.fetching, getList(state.data)];
};

export default useTeamList;
