import React, { useEffect } from "react";
import urls from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";

const tableData = [
    {
        th: "Month",
        td: (data) => <DateFormat date={data.date} format="MMMM YYYY" />,
    },
    {
        th: "Project",
        td: ({ type }) => (type && type.title ? type.title : ""),
    },
    {
        th: "Target",
        td: ({ target }) => target,
    },
    {
        th: "Achieved",
        td: ({ archived_target }) => archived_target || 0,
    },
    {
        th: "Achieved %",
        td: ({ archived_target_per }) => `${archived_target_per} %` || 0,
    },
];

const MyTargets = () => {
    const [pageState, { setUrl, setPage }] = usePagination();

    useEffect(() => {
        setUrl({ baseUrl: urls.targetList + "my_target/" });
    }, [setUrl]);

    return (
        <TableList
            pageState={pageState}
            onPageClick={setPage}
            tableData={tableData}
        />
    );
};

export default MyTargets;
