import dayjs from "dayjs";
import React, { useCallback, useEffect, useMemo } from "react";
import urls from "../../../apiService/urls";
import useValidateAll from "../../../hooks/form/useValidateAll";
import usePagination from "../../../hooks/http/usePagination";
import useUser from "../../../hooks/user/useUser";
import DateFormat from "../../common/DateFormat";
import { useMasterValues } from "../../common/MasterData";
import TableList from "../../common/TableList";
import FieldArray from "../../form/FieldArray";
import Form from "../../form/Form";
import formatAllData from "../../form/formatAllData";
import FormSubmitBtn from "../../form/FormSubmitBtn";
import formTypes from "../../form/formTypes";
import useTeamList from "../useTeamList";

const tableData = [
    {
        th: "Month",
        td: (data) => <DateFormat date={data.date} format="MMMM YYYY" />,
    },
    {
        th: "Project",
        td: ({ type }) => (type && type.title ? type.title : ""),
    },
    {
        th: "Telecaller",
        td: ({ employee }) =>
            employee && employee.user
                ? `${employee.user.first_name} ${employee.user.last_name}`
                : "",
    },
    {
        th: "Target",
        td: ({ target }) => target,
    },
    {
        th: "Achieved",
        td: ({ archived_target }) => archived_target || 0,
    },
    {
        th: "Achieved %",
        td: ({ archived_target_per }) => `${archived_target_per} %` || 0,
    },
];

const addForm = (
    [teamLoading, teamOptions = []],
    [masterLoading, master],
    id
) => [
    {
        name: "date",
        type: "date",
        // label: "Type",
        inputProps: {
            placeholder: "Month",
            showMonthYearPicker: true,
            dateFormat: "MMM yyyy",
            minDate: dayjs().startOf("M").toDate(),
        },
        conClass: "col-md-2",
        required: true,
    },
    {
        name: "employee_id",
        type: formTypes.select,
        inputProps: {
            placeholder: "Select Team member",
            isLoading: teamLoading,
            options: [{ label: "Myself", value: id }, ...teamOptions],
        },
        conClass: "col-md-3",
        required: true,
    },
    {
        name: "type_id",
        type: formTypes.select,
        inputProps: {
            placeholder: "Select project",
            isLoading: masterLoading,
            options: Array.isArray(master?.target_type)
                ? master.target_type.map((x) => ({ value: x.id, label: x.title }))
                : [],
        },
        conClass: "col-md-3",
        required: true,
    },
    {
        name: "target",
        type: formTypes.number,
        inputProps: {
            placeholder: "Enter Target",
        },
        conClass: "col-md-2",
        required: true,
    },
];

const TeamTargets = () => {
    const { id } = useUser()[0];
    const [pageState, { setUrl, setPage, clear }] = usePagination();
    const team = useTeamList();
    const master = useMasterValues();

    const allIds = useMemo(() => {
        return addForm(team, master, id);
    }, [team, master, id]);

    const validateForm = useValidateAll(allIds);
    const formatData = useCallback(
        (values) => {
            const data = formatAllData(allIds, values);
            return data;
        },
        [allIds]
    );

    const loadList = useCallback(() => {
        setUrl({ baseUrl: urls.targetList + "my_team/" });
    }, [setUrl]);

    const handleSuccess = useCallback(() => {
        clear();
        loadList();
    }, [loadList, clear]);

    useEffect(() => {
        loadList();
    }, [loadList]);

    return (
        <>
            <Form
                resetOnSuccess
                validateForm={validateForm}
                formatData={formatData}
                config={{
                    url: urls.addTarget,
                    method: "POST",
                }}
                handleSuccess={handleSuccess}
            >
                <div className="form-row">
                    <FieldArray allIds={allIds} />
                    <div className="col-md-2">
                        <FormSubmitBtn>Add</FormSubmitBtn>
                    </div>
                </div>
            </Form>
            <TableList
                pageState={pageState}
                onPageClick={setPage}
                tableData={tableData}
            />
        </>
    );
};

export default TeamTargets;
