import React from 'react';
import { teleCallerUrls } from '../../../apiService/urls';
import keysToQuery from '../../../utils/keysToQuery';
import UrlParamTabList from '../../common/UrlParamTabList';
import LeadsTable from './LeadsTable';

const ApprovalTabs = ({ filters, projectID }) => {
    const renderTable = ({ id, is_sub_menu, is_edit = false }) => {
        return (<>
            <LeadsTable
                tab={id}
                filters={filters}
                project={projectID}
                isEdit={is_edit}
            />

        </>)
    }

    const url = `${teleCallerUrls.tabs}${keysToQuery(filters ? { ...filters, project: projectID } : { project: projectID })}`;
    return (
        <UrlParamTabList url={url}>{renderTable}</UrlParamTabList>
    );
}

export default ApprovalTabs;