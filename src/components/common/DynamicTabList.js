import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import useFetchData from "../../hooks/http/useFetchData";
import useQuery from "../../hooks/useQuery";
import Loader from "../form/Loader";

const DynamicTabList = ({ url, children }) => {
    const [{ fetching, data }, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(url);
    }, [setUrl, url]);

    if (fetching) {
        return (
            <div className="flex-center mt-5 mb-5">
                <Loader />
            </div>
        );
    }
    if (Array.isArray(data) && data.length > 0) {
        return <TabList list={data}>{children}</TabList>;
    }
    return null;
};

const TabList = ({ list, children }) => {
    const [tab, setTab] = useState(null);
    const history = useHistory();
    const query = useQuery();

    useEffect(() => {
        const [first] = list;
        if (first && first.id) {
            setTab(first);

        }
    }, [list]);

    return (
        <>
            <div className="tab-btn-list-con">
                {list.map((x) => (
                    <button
                        className={`btn btn-tab${tab?.id === x.id ? " btn-tab-active" : ""
                            }`}
                        onClick={() => {
                            setTab(x)
                            query.set("page", 1);
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        key={x.id}
                    >
                        {x.title} ({x.count})
                    </button>
                ))}
            </div>
            {tab && tab.id && children && children(tab)}
        </>
    );
};

export default DynamicTabList;
