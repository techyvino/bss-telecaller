import React from "react";
import empty from "../../images/nodata.svg";

const EmptyList = ({ title = "" }) => {
    return (
        <div className="text-center col-12 pt-3">
            <img className="img-fluid mb-3" width="200px" src={empty} alt="" />
            <p className="text-uppercase">{title}</p>
        </div>
    );
};

export default EmptyList;
