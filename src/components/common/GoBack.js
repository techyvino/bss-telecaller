import React from "react";
import { ArrowLeft } from "react-feather";
import { useHistory } from "react-router-dom";

const GoBack = () => {
    const history = useHistory();
    return (
        <button onClick={history.goBack} className="btn p-0">
            <ArrowLeft size={18} /> Go Back
        </button>
    );
};

export default GoBack;
