import React, { useRef, useEffect } from "react";
import useOnScreen from "../../hooks/layout/useOnScreen";

const InfiniteList = ({
    loader = null,
    fetching = false,
    allIds = [],
    byId = {},
    RenderItem = () => null,
    hasNext,
    loadMore = null,
    count,
    emptyList = null,
}) => {
    return (
        <>
            {Array.isArray(allIds) && allIds.length > 0
                ? allIds.map((item, idx) => (
                      <RenderItem {...byId[item]} key={item + "---" + idx} />
                  ))
                : count === 0
                ? emptyList
                : null}
            {fetching ? loader : hasNext ? <LoadMore loadMore={loadMore} /> : null}
        </>
    );
};

const LoadMore = ({ loadMore = null }) => {
    const ref = useRef(null);
    const more = useRef(null);
    const onScreen = useOnScreen(ref);

    useEffect(() => {
        more.current = loadMore;
    }, [loadMore]);

    useEffect(() => {
        if (more.current && onScreen) {
            more.current();
        }
    }, [onScreen]);

    return <div ref={ref} />;
};

export default InfiniteList;
