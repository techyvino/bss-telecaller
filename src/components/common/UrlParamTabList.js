import React, { useEffect, useMemo } from "react";
import { useHistory } from "react-router-dom";
import useFetchData from "../../hooks/http/useFetchData";
import useQuery from "../../hooks/useQuery";
import Loader from "../form/Loader";

const UrlTabList = ({ list, children, tabName = "tab" }) => {
    const history = useHistory();
    const query = useQuery();

    const active = parseInt(query.get(tabName) ?? list[0].id, 10);
    const tab = useMemo(() => {
        return list.filter((x) => x.id === active)?.[0];
    }, [active, list]);

    return (
        <>
            <div className="tab-btn-list-con">
                {list.map((x) => (
                    <button
                        className={`btn btn-tab${
                            active === x.id ? " btn-tab-active" : ""
                        }`}
                        onClick={() => {
                            query.set(tabName, x.id);
                            query.set("page", 1);
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        key={x.id}
                    >
                        {x.title} ({x.count})
                    </button>
                ))}
            </div>
            {tab && tab.id && children && children(tab)}
        </>
    );
};

const UrlParamTabList = ({ url, children, tabName }) => {
    const [{ fetching, data }, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(url);
    }, [setUrl, url]);

    if (fetching) {
        return (
            <div className="flex-center mt-5 mb-5">
                <Loader />
            </div>
        );
    }
    if (Array.isArray(data) && data.length > 0) {
        return (
            <UrlTabList tabName={tabName} list={data}>
                {children}
            </UrlTabList>
        );
    }
    return null;
};

export default UrlParamTabList;
