import React from "react";
import useDateFormat from "../../hooks/useDateFormat";

const DateFormat = ({ date, format = "lll" }) => {
    const dt = useDateFormat(date, format)
    return <span>{dt}</span>;
};

export default DateFormat;
