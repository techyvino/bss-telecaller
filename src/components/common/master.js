import { isArray } from "../../utils";

export const salutations = [
    {
        label: "Mr",
        value: "Mr",
    },
    {
        label: "Ms",
        value: "Ms",
    },
    {
        label: "Mrs",
        value: "Mrs",
    },
    {
        label: "Dr",
        value: "Dr",
    },
    {
        label: "Prof",
        value: "Prof",
    },
    {
        label: "M/S",
        value: "M/S",
    },
];

export const relations = [
    {
        label: "Son",
        value: "Son",
    },
    {
        label: "Wife",
        value: "Wife",
    },
    {
        label: "Daughter",
        value: "Daughter",
    },
];

export const paymentModes = [
    {
        title: "Loan",
        value: "Loan",
    },
    {
        title: "Outright",
        value: "Outright",
    },
];

export const bookingPaymentModes = [
    {
        title: "Loan",
        value: "Loan",
    },
    {
        title: "Own Contribution",
        value: "Own Contribution",
    },
];

export const deadReasons = [
    {
        label: "Distance",
        value: "Distance",
    },
    {
        label: "Low Budget",
        value: "Low Budget",
    },
    {
        label: "Loan Issue",
        value: "Loan Issue",
    },
    {
        label: "Personal Reasons",
        value: "Personal Reasons",
    },
    {
        label: "Vastu Complaint",
        value: "Vastu Complaint",
    },
    {
        label: "Booked Other Property",
        value: "Booked Other Property",
    },
    {
        label: "Invalid Lead",
        value: "Invalid Lead",
    },
    {
        label: "Broker",
        value: "Broker",
    },
    {
        label: "Land Owner",
        value: "Land Owner",
    },
    {
        label: "Channel Partner",
        value: "Channel Partner",
    },
    {
        label: "Competitor",
        value: "Competitor",
    },
];

export const NA = "NA";

export const getCampaigns = (sources, id) =>
    isArray(sources)
        ? sources.filter((x) => x.id === id)[0]?.campaigns ?? []
        : [];

export const getPrimary = (campaigns, id) =>
    isArray(campaigns)
        ? campaigns.filter((x) => x.id === id)[0]?.primary_sources ?? []
        : [];
