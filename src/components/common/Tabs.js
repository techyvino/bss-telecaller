import React, { useState, useContext, useCallback, createContext } from "react";
import { useHistory } from "react-router";
import useQuery from "../../hooks/useQuery";
const TabContext = createContext([]);
const useTabContext = () => useContext(TabContext);

const Tabs = ({ tabs = [], children, qName }) => {
    const query = useQuery();
    const { push } = useHistory();
    const initQ = query.get(qName);
    const initTab = initQ ? initQ : tabs[0];
    const [tab, setTab] = useState(initTab);

    const changeTab = useCallback(
        (id) => {
            setTab(id);
            console.log(qName)
            if (qName) {
                query.set(qName, id);
                query.set("page", 1);
                push({
                    search: query.toString(),
                });
            }
        },
        [qName, query, push]
    );

    return (
        <TabContext.Provider value={{ tabs, tab, changeTab }}>
            {children}
        </TabContext.Provider>
    );
};

export const TabBtn = ({
    className = "btn btn-tab",
    activeClassName = "btn-tab-active",
    tabId,
    ...rest
}) => {
    const { tab, changeTab } = useTabContext();

    return (
        <button
            {...rest}
            onClick={() => {
                changeTab(tabId)
            }
            }
            className={
                tab === tabId ? `${className} ${activeClassName}` : className
            }
        />
    );
};

export const TabBtnList = ({
    conClassName = "tab-btn-list-con",
    btnClassName,
    activeClassName,
}) => {
    const { tabs } = useTabContext();

    return (
        <div className={conClassName}>
            {tabs.map((item) => (
                <TabBtn
                    key={item}
                    tabId={item}
                    className={btnClassName}
                    activeClassName={activeClassName}
                >
                    {item}
                </TabBtn>
            ))}
        </div>
    );
};

export const TabPane = ({ children, tabId }) => {
    const { tab } = useTabContext();
    if (tab === tabId) {
        return children || null;
    }
    return null;
};

export default Tabs;
