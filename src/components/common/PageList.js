import React, { useCallback } from "react";
import { ChevronLeft, ChevronRight } from "react-feather";
import pageList from "../../utils/pageList";

const PageLi = ({ isActive, onClick, disabled, children }) => {
    return (
        <li
            className={`page-item${isActive ? " active" : ""}${
                disabled ? " disabled" : ""
            }`}
        >
            <button disabled={disabled} className="page-link" onClick={onClick}>
                {children}
            </button>
        </li>
    );
};

const PrevLi = ({ onPageClick, activePage }) => {
    const isPrev = activePage > 1;

    const onPrevClick = useCallback(() => {
        if (isPrev) {
            onPageClick(activePage - 1);
        }
    }, [onPageClick, isPrev, activePage]);

    return (
        <PageLi onClick={onPrevClick} disabled={!isPrev}>
            <ChevronLeft size={16} />
        </PageLi>
    );
};

const NextLi = ({ onPageClick, activePage, noOfPages }) => {
    const isNext = activePage < noOfPages;
    

    const onNextClick = useCallback(() => {
        if (isNext) {
            onPageClick(activePage + 1);
        }
    }, [onPageClick, activePage, isNext]);

    return (
        <PageLi onClick={onNextClick} disabled={!isNext}>
            <ChevronRight size={16} />
        </PageLi>
    );
};

const PageNumber = ({ page, onPageClick, activePage }) => {

    const onClick = useCallback(() => {
        onPageClick(page);
    }, [page, onPageClick]);

    return (
        <PageLi isActive={activePage === page} onClick={onClick}>
            {page}
        </PageLi>
    );
};

const PageList = ({ noOfPages = 1, onPageClick, activePage }) => {
    if (noOfPages > 1 && activePage > 0) {
        return (
            <ul className="pagination justify-content-end">
                <PrevLi
                    onPageClick={onPageClick}
                    noOfPages={noOfPages}
                    activePage={activePage}
                />
                {pageList(activePage, noOfPages).map((page) => (
                    <PageNumber
                        key={"page--" + page}
                        page={page}
                        activePage={activePage}
                        onPageClick={onPageClick}
                    />
                ))}
                <NextLi
                    onPageClick={onPageClick}
                    noOfPages={noOfPages}
                    activePage={activePage}
                />
            </ul>
        );
    }
    return null;
};

export default PageList;
