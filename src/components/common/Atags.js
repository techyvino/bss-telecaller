import React from "react";

export const aTagTyps = {
    email: "email",
    tel: "tel",
};

const getHref = (type, href) => {
    switch (type) {
        case aTagTyps.email:
            return `mailto:${href}`;
        case aTagTyps.tel:
            return `tel:${href}`;
        default:
            return href;
    }
};

const Atags = ({ type = "", href = "", target = "", children, ...rest }) => {
    const link = getHref(type, href);

    return (
        <a type={type} href={link} target={target} {...rest}>
            {children}
        </a>
    );
};

export default Atags;
