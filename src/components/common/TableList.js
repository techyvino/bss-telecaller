import React, { useCallback, useEffect, useMemo, useState } from "react";
import CheckBox from "../form/CheckBox";
import Loader from "../form/Loader";
import SubmitBtn from "../form/SubmitBtn";
import EmptyList from "./EmptyList";
import HideTableItems from "./HideTableItems";
import PageList from "./PageList";

const Actions = ({
    actions = [],
    onAction = () => {},
    selected,
    actionLoading = false,
}) => {
    const [actionType, setActionType] = useState("");

    return (
        <div className="d-flex align-items-stretch">
            <select
                value={actionType}
                onChange={(e) => setActionType(e.target.value)}
                className="form-control mr-2"
            >
                <option value="">Select an action</option>
                {actions.map((x) => (
                    <option key={x} value={x}>
                        {x} ({selected.length})
                    </option>
                ))}
            </select>
            <SubmitBtn
                onClick={() => onAction({ type: actionType, selected })}
                disabled={!actionType}
                className="btn btn-theme"
                fetching={actionLoading}
            >
                Submit
            </SubmitBtn>
        </div>
    );
};
const Table = ({
    tblClassName,
    pageState,
    thList = [],
    actions = [],
    onAction,
    actionLoading,
}) => {
    const [selected, setSelected] = useState([]);
    const { page, allIds, byId, limit } = pageState;
    const list = allIds[page];

    const hasActions = actions.length > 0;

    useEffect(() => {
        setSelected([]);
    }, [list]);

    const sel = useMemo(() => selected.filter((x) => x !== "*"), [selected]);

    return (
        <>
            {hasActions && sel.length > 0 && (
                <div className="row">
                    <div className="col-md-3 mb-2">
                        <Actions
                            actions={actions}
                            selected={sel}
                            onAction={onAction}
                            actionLoading={actionLoading}
                        />
                    </div>
                </div>
            )}
            <div className="table-responsive">
                <table className={tblClassName}>
                    <thead>
                        <tr>
                            {hasActions && (
                                <th>
                                    <CheckBox
                                        name={`th-${page}`}
                                        value={selected.includes("*")}
                                        setValue={(val) => {
                                            if (val) {
                                                setSelected(["*", ...list]);
                                            } else {
                                                setSelected([]);
                                            }
                                        }}
                                    />
                                </th>
                            )}
                            <th>Sl no.</th>
                            {thList.map((x, idx) => (
                                <th scope="col" key={idx}>
                                    {x.th || null}
                                </th>
                            ))}
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((id, slNo) => (
                            <tr key={id}>
                                {hasActions && (
                                    <td>
                                        <CheckBox
                                            name={`td-${page}-${id}`}
                                            value={selected.includes(id)}
                                            setValue={(val) => {
                                                if (val) {
                                                    setSelected((s) => [
                                                        ...s,
                                                        id,
                                                    ]);
                                                } else {
                                                    setSelected((s) =>
                                                        s.filter(
                                                            (x) =>
                                                                x !== id &&
                                                                x !== "*"
                                                        )
                                                    );
                                                }
                                            }}
                                        />
                                    </td>
                                )}

                                <td>{slNo + 1 + limit * (page - 1)}</td>
                                {thList.map((x, idx) => (
                                    <td key={idx}>{x.td(byId[id])}</td>
                                ))}
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    );
};

const TableList = ({
    tblClassName = "table table-bordered",
    pageState,
    onPageClick,
    tableData,
    actions,
    onAction,
    actionLoading,
}) => {
    const { noOfPages, page, allIds, fetching } = pageState;
    const [hideItems, setHideItems] = useState([]);

    useEffect(() => {
        setHideItems([]);
    }, [tableData]);

    const onChange = useCallback((items) => {
        setHideItems(items);
    }, []);

    const tbList = useMemo(
        () => tableData.filter((x) => !hideItems.includes(x.th)),
        [hideItems, tableData]
    );

    if (fetching[page]) {
        return (
            <div className="flex-center mt-5 mb-5">
                <Loader />
            </div>
        );
    }
    const list = allIds[page];
    if (Array.isArray(list)) {
        if (list.length === 0) {
            return <EmptyList title="No data found" />;
        }
        return (
            <>
                <Table
                    tblClassName={tblClassName}
                    thList={tbList}
                    pageState={pageState}
                    actions={actions}
                    onAction={onAction}
                    actionLoading={actionLoading}
                />
                <div className="flex-between flex-wrap">
                    <div className="mb-3">
                        <HideTableItems
                            hideItems={hideItems}
                            tableData={tableData}
                            onChange={onChange}
                        />
                    </div>
                    <div className="mb-3">
                        <PageList
                            noOfPages={noOfPages}
                            activePage={page}
                            onPageClick={onPageClick}
                        />
                    </div>
                </div>
            </>
        );
    }
    return null;
};

export default TableList;
