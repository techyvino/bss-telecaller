import React from "react";
import useToggle from "../../hooks/useToggle";
import Switch from "../form/Switch";
import Modal from "../modal/Modal";
import ModalHeader from "../modal/ModalHeader";

const HideTableItems = ({ hideItems = [], tableData, onChange }) => {
    const { toggle, onFalse, onTrue } = useToggle();

    return (
        <>
            <button onClick={onTrue} className="btn p-0 text-underline btn-sm">
                Show Items
            </button>
            <Modal
                isOpen={toggle}
                close={onFalse}
                contentStyle={{ maxWidth: "350px" }}
            >
                <ModalHeader title="Show Items" desc="" close={onFalse} />
                <div>
                    {tableData.map((x) => (
                        <div className="flex-between p-3" key={x.th}>
                            <div>{x.th}</div>
                            <Switch
                                checked={!hideItems.includes(x.th)}
                                setValue={(isChecked) => {
                                    if (isChecked) {
                                        onChange(
                                            hideItems.filter((s) => s !== x.th)
                                        );
                                    } else {
                                        onChange([...hideItems, x.th]);
                                    }
                                }}
                            />
                        </div>
                    ))}
                </div>
            </Modal>
        </>
    );
};

export default HideTableItems;
