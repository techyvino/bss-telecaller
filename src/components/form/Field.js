import React from "react";
import FieldCon from "./FieldCon";
import SelectField from "./SelectField";
import useField from "../../hooks/form/useField";

const Field = ({
    type,
    name = "",
    label,
    required,
    infoText,
    conClass,
    inputProps,
}) => {
    const field = useField(name);

    return (
        <FieldIp
            type={type}
            {...field}
            name={name}
            label={label}
            required={required}
            infoText={infoText}
            conClass={conClass}
            inputProps={inputProps}
        />
    );
};

const FieldIp = ({
    type = "text",
    label,
    required,
    infoText,
    conClass,
    inputProps = {},
    name = "",
    err,
    value,
    setValue,
}) => {
    return (
        <FieldCon
            err={err}
            required={required}
            label={label}
            infoText={infoText}
            conClass={conClass}
        >
            <SelectField
                type={type}
                value={value}
                setValue={setValue}
                name={name}
                {...inputProps}
            />
        </FieldCon>
    );
};

export default Field;
