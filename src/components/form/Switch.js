import React from "react";

const Switch = ({ setValue, checked = false }) => {
    return (
        <label className={`switch switch-${checked ? "active" : "inactive"}`}>
            <input
                type="checkbox"
                checked={checked}
                onChange={(e) => setValue(e.target.checked)}
            />
            <div></div>
        </label>
    );
};

export default Switch;
