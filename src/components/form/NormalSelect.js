import React from "react";

const NormalSelect = ({
    className = "form-control",
    setValue = null,
    value = "",
    placeholder = "Select an option",
    options = [],
    isLoading = false,
    disabled
}) => {
    return (
        <select
            value={value}
            onChange={(e) => {
                if (setValue) {
                    setValue(e.target.value);
                }
            }}
            className={className}
            disabled={isLoading || disabled}
        >
            <option value="">{placeholder}</option>
            {
                options.map(x => {
                    return <option key={x.value} value={x.value}>{x.label}</option>
                })
            }
        </select>
    );
};

export default NormalSelect;