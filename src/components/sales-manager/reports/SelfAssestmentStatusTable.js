import React, { useEffect } from "react";
import { isArray } from "validate.js";
import { salesManagerUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import DynamicContent from "../../common/DynamicContent";


const ReportTable = ({ data = [] }) => {
    const header = data.length > 0
        ? data[0].status : [];
    return (
        <table className="table table-bordered table-responsive-l">
            <thead>
                <tr className="f-14">
                    <th scope="col">Project</th>
                    {header.map((x, idx) => (
                        <th key={`${idx}`}>{x.title}</th>
                    ))}
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                {data.map((x, idx) => (
                    <tr key={`${x.name}---${idx}`}>
                        <td>{x.title}</td>
                        {x.status.map((st) => (
                            <td key={st.title}>{st.count}</td>
                        ))}
                        <td>{x.total}</td>
                    </tr>
                ))}

            </tbody>
        </table>
    )
}

const SelfAssestmentStatusTable = ({ project }) => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(`${salesManagerUrls.leads}status_wise_report/` +project ?? `?project=${project}`);
    }, [setUrl, project]);

    const isLoaded = isArray(state?.data);


    return (
        <DynamicContent fetching={state.fetching} isLoaded={isLoaded}>
            {isLoaded && (
                <ReportTable {...state} />)}
        </DynamicContent>
    );
};

export default SelfAssestmentStatusTable;
