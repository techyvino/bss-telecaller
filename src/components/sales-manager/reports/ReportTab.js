import React from "react";
import Tabs, { TabBtnList, TabPane } from "../../common/Tabs";
import SelfAssestmentStatusTable from "./SelfAssestmentStatusTable";
import SelfAssestmentTable from "./SelfAssestmentTable";

const tabs = [
    "Status Wise",
    "Self Assestment",

];


const ReportTabs = ({ projectID }) => {
    return (
        <Tabs tabs={tabs}>
            <TabBtnList />
            <TabPane tabId={tabs[0]}>
                <SelfAssestmentStatusTable project={projectID} />

            </TabPane>
            <TabPane tabId={tabs[1]}>
                <SelfAssestmentTable project={projectID} />
            </TabPane>
        </Tabs>
    )
}

export default ReportTabs;