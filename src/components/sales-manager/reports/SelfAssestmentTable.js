import React, { useEffect } from "react";
import { useHistory } from "react-router";
import { isArray } from "validate.js";
import { salesManagerUrls } from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import useQuery from "../../../hooks/useQuery";
import { getFilters } from "../../../utils/keysToQuery";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import LeadEmail from "../../tele-caller/leads/LeadEmail";
import LeadNumber from "../../tele-caller/leads/LeadNumber";
import LeadFilters from "../leads/LeadFilters";


const tbData = [
    {
        th: 'Project',
        td: ({ project }) => project.title,
    },
    {
        th: 'Enquiry Date',
        td: ({ enquiry_date }) => <DateFormat date={enquiry_date} format="DD/MM/YYYY" />,
    },
    {
        th: 'Date Entered',
        td: ({ created_on }) => <DateFormat date={created_on} format="DD/MM/YYYY" />,
    },
    {
        th: 'Assigned Date',
        td: ({ assigned_on }) => <DateFormat date={assigned_on} format="DD/MM/YYYY" />,
    },

    {
        th: 'Closed Date',
        td: ({ cold_reason_date }) => <DateFormat date={cold_reason_date} format="DD/MM/YYYY" />,
    },
    {
        th: 'Cancelled Date',
        td: ({ id }) => '-',
    },
    {
        th: "Lead ID",
        td: ({ id }) => id
    },
    {
        th: 'Customer Name',
        td: ({ name }) => name,
    },
    {
        th: 'Status',
        td: ({ status }) => status ? status.title : "-"
    },
    {
        th: "Dead Reason",
        td: ({ dead_reason }) => dead_reason ?? "-"

    },
    {
        th: 'TeleCaller',
        td: ({ created_by }) => created_by?.user
            ? `${created_by.user.first_name} ${created_by.user.last_name}`
            : "-",
    },
    {
        th: 'Sales Manager',
        td: ({ assigned_to }) => assigned_to?.user
            ? `${assigned_to.user.first_name} ${assigned_to.user.last_name}`
            : "-",
    },
    {
        th: 'Mobile1',
        td: ({ number, secondary_number }) => <LeadNumber number={number} secondary_number={secondary_number} />,
    },
    { th: 'Email ID', td: ({ email, secondary_email }) => <LeadEmail email={email} secondary_email={secondary_email} /> },
    {
        th: "Source",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign?.source?.title ?? "-",
    },
    {
        th: "Campaign",
        td: ({ source_of_campaigning }) => source_of_campaigning?.campaign.title ?? "-",
    },
    {
        th: "Primary Source Campaign",
        td: ({ source_of_campaigning }) => source_of_campaigning?.title ?? "-",
    },
    {
        th: 'Followup Date',
        td: ({ follow_up_date }) => <DateFormat date={follow_up_date} format="DD/MM/YYYY" />,
    },
    {
        th: 'Site Visit Date',
        td: ({ site_visit_date }) => <DateFormat date={site_visit_date} format="DD/MM/YYYY" />,
    },
    {
        th: 'Address',
        td: ({ address }) => address,
    },
    {
        th: 'Area',
        td: ({ area }) => area,
    },
    {
        th: 'City',
        td: ({ city }) => city,
    },
    {
        th: 'Pin Code',
        td: ({ pincode }) => pincode,
    },
    {
        th: 'Customer Interest',
        td: ({ customer_interest }) =>
            isArray(customer_interest)
                ? customer_interest.map((x) => x.title).join("\n, ")
                : "-",
    },
    {
        th: 'Preferred Location',
        td: ({ interested_location }) =>
            isArray(interested_location)
                ? interested_location.map((x) => x.title).join("\n, ")
                : "-",
    },
    { th: 'Purpose', td: ({ purpose }) => purpose, },
    { th: 'Mode Of Payment', td: ({ mode_of_payment }) => mode_of_payment, },
    { th: 'Transport Required', td: ({ is_transport_required }) => is_transport_required, },
    { th: 'Initial Feedback', td: ({ initial_feedback }) => initial_feedback, },
    { th: 'Budget', td: ({ budget }) => budget?.title ?? "-" },

]


const SelfAssestmentTable = ({ project }) => {
    const [state, { setUrl, setPage }] = usePagination();
    const history = useHistory();
    const query = useQuery()

    const filterQuery = query.get("filters") || "";

    useEffect(() => {
        setUrl({
            baseUrl: `${salesManagerUrls.reports}`,
            filters: { project, ...getFilters(filterQuery) },
        });
    }, [setUrl, project, filterQuery]);

    const onFilter = (f = {}) => {
        query.set("filters", JSON.stringify(f));
        history.replace({
            search: `?${query.toString()}`,
        });
    };

    return (
        <>
            <LeadFilters onFilter={onFilter} />
            <TableList
                pageState={state}
                onPageClick={setPage}
                tableData={tbData}
            />
        </>
    );
};

export default SelfAssestmentTable;
