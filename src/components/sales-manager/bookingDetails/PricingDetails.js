import React, { useMemo } from "react";
import curFormat from "../../../utils/curFormat";

const PricingDetails = ({ breakup = [], net = 0, totalA = 0 }) => {
    const [aList, blist] = useMemo(() => {
        let aList = [];
        let bList = [];
        breakup.forEach((x) => {
            if (x.pricing_categories === "A") {
                aList.push(x);
                return;
            } else {
                bList.push(x);
            }
        });
        return [aList, bList];
    }, [breakup]);

    return (
        <table className="table table-bordered table-responsive-md f-14">
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>Description</th>
                    <th className="text-right">Amount</th>
                </tr>
            </thead>
            <tbody>
                {aList.map((x, idx) => (
                    <tr key={`${x.id}`}>
                        <td>{idx + 1}</td>
                        <td>{x.description}</td>
                        <td className="text-right">{curFormat(x.amount)}</td>
                    </tr>
                ))}
                <tr>
                    <th colSpan="2" className="text-right">
                        Total A
                    </th>
                    <th className="text-right">{curFormat(totalA)}</th>
                </tr>
                {blist.map((x, idx) => (
                    <tr key={`${x.id}`}>
                        <td>{aList.length + idx + 1}</td>
                        <td>{x.description}</td>
                        <td className="text-right">{curFormat(x.amount)}</td>
                    </tr>
                ))}
                <tr className="bg-light">
                    <th colSpan="2" className="text-right">
                        Net Amount
                    </th>
                    <th className="text-right">{curFormat(net)}</th>
                </tr>
            </tbody>
        </table>
    );
};

export default PricingDetails;
