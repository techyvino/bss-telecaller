import dayjs from "dayjs";
import React from "react";
import DateFormat from "../../common/DateFormat";
import LeadEmail from "../../tele-caller/leads/LeadEmail";
import LeadNumber from "../../tele-caller/leads/LeadNumber";

const getAge = (val) => {
    return dayjs().diff(val, "year");
};

const Item = ({ item, data }) => {
    return (
        <div className={item.conClass} key={item.label}>
            <div className="form-group">
                <label className="form-label">{item.label}</label>
                <div className="form-control">{item.value(data)}</div>
            </div>
        </div>
    );
};

const renderItems = (list, data) =>
    list.map((item) => <Item item={item} data={data} key={item.label} />);

const addr = [
    {
        label: "Address",
        conClass: "col-md-4 address",
        value: ({ address }) => address,
    },
    {
        label: "Area",
        conClass: "col-md-4",
        value: ({ area }) => area,
    },
    {
        label: "City",
        conClass: "col-md-4",
        value: ({ city }) => city?.title,
    },
    {
        label: "State",
        conClass: "col-md-4",
        value: ({ state }) => state?.title,
    },
    {
        label: "Country",
        conClass: "col-md-4",
        value: ({ country }) => country?.title,
    },
    {
        label: "Pincode",
        conClass: "col-md-4",
        value: ({ pincode }) => pincode,
    },
];

const comp = [
    {
        label: "Company Name",
        conClass: "col-md-4",
        value: ({ name }) => name,
    },
    {
        label: "Designation",
        conClass: "col-md-4",
        value: ({ designation }) => designation,
    },
    {
        label: "Phone Number",
        conClass: "col-md-4",
        value: ({ phone }) => phone,
    },
    ...addr
];

export const CompanyDetails = ({ data }) => {
    return <div className="row">{renderItems(comp, data)}</div>;
};

export const Address = ({ data }) => {
    return <div className="row">{renderItems(addr, data)}</div>;
};

const tbl = [
    {
        label: "Salutation",
        conClass: "col-md-4",
        value: ({ salutation }) => salutation,
    },
    {
        label: "Applicant Name",
        conClass: "col-md-8",
        value: ({ name }) => name,
    },
    {
        label: "Applicant Type",
        conClass: "col-md-4",
        value: ({ applicant_type }) => applicant_type || "",
    },
    {
        label: "Relation",
        conClass: "col-md-4",
        value: ({ relationship }) => relationship || "",
    },
    {
        label: "Relation Name",
        conClass: "col-md-4",
        value: ({ relationName }) => relationName || "",
    },
    {
        label: "Date of Birth",
        conClass: "col-md-6",
        value: ({ dob }) => <DateFormat format="ll" date={dob} />,
    },
    {
        label: "Age",
        conClass: "col-md-6",
        value: ({ dob }) => getAge(dob),
    },
    {
        label: "PAN Number",
        conClass: "col-md-6",
        value: ({ pan_no }) => pan_no || "",
    },
    {
        label: "Adhaar Number",
        conClass: "col-md-6",
        value: ({ adhaar }) => adhaar || "",
    },
    {
        label: "Phone Number",
        conClass: "col-md-6",
        value: ({ number, secondary_number }) => (
            <LeadNumber number={number} secondary_number={secondary_number} />
        ),
    },
    {
        label: "Email",
        conClass: "col-md-6",
        value: ({ email, secondary_email }) => (
            <LeadEmail email={email} secondary_email={secondary_email} />
        ),
    },
];

const ApplicantDetails = ({ data = {} }) => {
    return <div className="row">{renderItems(tbl, data)}</div>;
};

export default ApplicantDetails;
