import React from "react";
import { isFunction } from "validate.js";

const basicTbl = [
    {
        th: "Name",
        td: ({ salutation, name }) => `${salutation} ${name}`,
    },
    {
        th: "Number",
        td: ({ number }) => number,
    },
    {
        th: "Email",
        td: ({ email }) => email,
    },
    {
        th: "Pan No",
        td: ({ pan_no }) => pan_no,
    },

    {
        th: "Aadhar No",
        td: ({ aadhar_no, aadhar_document }) => aadhar_no,
    },
];

const Basicinfo = ({ data, coapplicantdetails }) => {
    return (
        <div>
            <div className="row mb-3">
                {basicTbl.map((row) => (
                    <div
                        className={`f-14 mb-3 ${
                            row?.tdClassName ? row.tdClassName : "col-md-4"
                        }`}
                        key={row.th}
                    >
                        <div className="form-row">
                            <div className="col-5">{row.th}</div>
                            <div className="col-7 f-500">
                                {isFunction(row.td) ? row.td(data) : null}
                            </div>
                        </div>
                    </div>
                ))}
            </div>
            {coapplicantdetails && (
                <div className="row mb-3">
                    <div className="col-lg-12 pb-3">
                        <b>Co Applicant</b>
                    </div>
                    {basicTbl.map((row) => (
                        <div
                            className={`f-14 mb-3 ${
                                row?.tdClassName ? row.tdClassName : "col-md-4"
                            }`}
                            key={row.th}
                        >
                            <div className="form-row">
                                <div className="col-5 f-500">{row.th}</div>
                                <div className="col-7">
                                    {isFunction(row.td)
                                        ? row.td(coapplicantdetails)
                                        : null}
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default Basicinfo;
