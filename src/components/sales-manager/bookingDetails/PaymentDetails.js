import React from "react";
import { isArray } from "../../../utils";
import DateFormat from "../../common/DateFormat";

const PaymentDetails = ({ list = [] }) => {
    return (
        <table className="table table-bordered table-responsive-md f-14">
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>Mode of payment</th>
                    <th>Transaction details</th>
                    <th>Name of transaction</th>
                    <th>Bank Name</th>
                    <th>Branch Name</th>
                    <th>Amount</th>
                    <th>Cheque / Wire Transfer Date</th>
                    <th>Reference document</th>
                    <th>Comments</th>
                    <th>Created On</th>
                </tr>
            </thead>
            <tbody>
                {isArray(list) &&
                    list.map((x, idx) => (
                        <tr key={`${x.id}`}>
                            <td>{idx + 1}</td>
                            <td>{x.mode_of_payment}</td>
                            <td>{x.transaction_details}</td>
                            <td>{x.name_of_transaction}</td>
                            <td>{x.bank_name}</td>
                            <td>{x.branch_name}</td>
                            <td>{x.amount}</td>
                            <td>
                                <DateFormat date={x.transaction_date}  format="DD/MM/YYYY"/>
                            </td>
                            <td>
                                {x.reference_doc ? (
                                    <a
                                        href={x.reference_doc}
                                        download={`Reference Document ${x.id}`}
                                    >
                                        Download
                                    </a>
                                ) : (
                                    "-"
                                )}
                            </td>
                            <td>{x.comments || "-"}</td>
                            <td>
                                <DateFormat date={x.created_on} />
                            </td>
                        </tr>
                    ))}
            </tbody>
        </table>
    );
};

export default PaymentDetails;
