import React, { useCallback, useState } from "react";
import { Check, Loader, X } from "react-feather";
import { useHistory } from "react-router";
import { toast } from "react-toastify";
import { salesGmUrls } from "../../../apiService/urls";
import useSubmit from "../../../hooks/http/useSubmit";
import useQuery from "../../../hooks/useQuery";
import useToggle from "../../../hooks/useToggle";
import SubmitBtn from "../../form/SubmitBtn";
import TextArea from "../../form/TextArea";
import Modal from "../../modal/Modal";
import ModalBody from "../../modal/ModalBody";
import ModalHeader from "../../modal/ModalHeader";

const style = { height: "30px", width: "30px" };

export const Reason = ({ bookings = [], close }) => {
    const query = useQuery();
    const { replace } = useHistory();

    const [fetching, submit] = useSubmit({
        success: () => {
            toast.dark("Booking has been rejected");
            close();
            query.set("tab", 3);
            replace({
                search: `?${query.toString()}`,
            });
        },
    });
    const [value, setValue] = useState("");

    const onClick = () => {
        if (value && bookings.length) {
            submit({
                url: salesGmUrls.approveBooking,
                method: "POST",
                data: { reason: value, status: false, bookings },
            });
        }
    };

    return (
        <Modal isOpen>
            <ModalHeader
                title="Reject Reason"
                close={close}
                desc="Enter your resaon"
            />
            <ModalBody>
                <div className="form-group">
                    <TextArea
                        placeholder="Reason"
                        setValue={setValue}
                        value={value}
                    />
                </div>
                <SubmitBtn
                    className="btn btn-theme btn-block form-group"
                    disabled={!value}
                    fetching={fetching}
                    onClick={onClick}
                >
                    Submit
                </SubmitBtn>
            </ModalBody>
        </Modal>
    );
};

export const useApprove = () => {
    const query = useQuery();
    const { replace } = useHistory();

    const [fetching, submit] = useSubmit({
        success: () => {
            toast.success("Booking has been approved");
            query.set("tab", 2);
            replace({
                search: `?${query.toString()}`,
            });
        },
    });

    const onApprove = useCallback(
        (bookings) => {
            submit({
                url: salesGmUrls.approveBooking,
                method: "POST",
                data: { reason: "", status: true, bookings },
            });
        },
        [submit]
    );

    return [fetching, onApprove];
};

const Approve = ({ id }) => {
    const { toggle, onFalse, onTrue } = useToggle();
    const [fetching, onApprove] = useApprove();

    return (
        <div className="d-flex">
            <button
                style={style}
                className="btn p-0 btn-success rounded-circle mr-2"
                title="Approve"
                disabled={fetching}
                onClick={() => onApprove([id])}
            >
                {fetching ? (
                    <Loader size={18} />
                ) : (
                    <Check size={18} color="#fff" />
                )}
            </button>
            <button
                style={style}
                className="btn p-0 btn-danger rounded-circle"
                title="Reject"
                disabled={fetching}
                onClick={onTrue}
            >
                <X size={18} color="#fff" />
            </button>
            {toggle && <Reason bookings={[id]} close={onFalse} />}
        </div>
    );
};

export default Approve;
