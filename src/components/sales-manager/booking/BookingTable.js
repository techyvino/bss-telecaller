import React, { useCallback, useEffect, useState } from "react";
import { salesManagerUrls } from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import { useRouteMatch, Link } from "react-router-dom";
import LeadNumber from "../../tele-caller/leads/LeadNumber";
import LeadEmail from "../../tele-caller/leads/LeadEmail";
import Approve, { Reason, useApprove } from "./Approve";

const ViewLink = ({
    id,
    className = "text-body text-underline",
    title = "View Details",
}) => {
    let { url } = useRouteMatch();

    return (
        <Link className={className} to={`${url}/booking-details/${id}`}>
            {title}
        </Link>
    );
};

const tbData = [
    // {
    //     th: "Lead Id",
    //     td: ({ lead }) => `${lead?.id ?? ""}`,
    // },
    // {
    //     th: "Booking Id",
    //     td: ({ booking_no }) => `${booking_no}`,
    // },
    {
        th: "Project",
        td: ({ apartment_details }) => (
            <div>
                <div>
                    {apartment_details?.floor?.tower?.project?.title || ""}
                </div>
                {/* <div>{apartment_details?.floor?.tower?.project?.address}</div> */}
            </div>
        ),
    },
    {
        th: "Unit no.",
        td: ({ apartment_details }) => apartment_details?.alias_name ?? "-",
    },
    {
        th: "Customer Name",
        td: ({ salutation, name }) => (
            <div>
                {salutation} {name}
            </div>
        ),
    },
    {
        th: "Date of Booking",
        td: ({ created_on }) => (
            <DateFormat date={created_on} format="DD/MM/YYYY" />
        ),
    },
    {
        th: "Sales Manager Name",
        td: ({ lead }) =>
            lead?.assigned_to?.user
                ? `${lead.assigned_to.user.first_name} ${lead.assigned_to.user.last_name}`
                : "",
    },
    {
        th: "Lead Source",
        td: ({ lead }) =>
            lead?.source_of_campaigning?.campaign?.source?.title ?? "",
    },
    {
        th: "Campaign",
        td: ({ lead }) => lead?.source_of_campaigning?.campaign.title ?? "",
    },
    {
        th: "Primary Source Campaign",
        td: ({ lead }) => lead?.source_of_campaigning?.title ?? "",
    },
    {
        th: "Customer Mobile",
        td: LeadNumber,
    },
    {
        th: "Customer Email",
        td: LeadEmail,
    },
];

const view = {
    th: "View",
    td: ({ id }) => <ViewLink id={id} />,
};

const action = {
    th: "Actions",
    td: ({ id }) => <Approve id={id} />,
};

const reason = {
    th: "Rejected Reason",
    td: ({ reject_reason = "" }) => `${reject_reason}`,
};

const crmReason = {
    th: "CRM assignee",
    td: () => null,
};

const gm = (tab, isApprove) => {
    switch (tab) {
        case 1:
            return isApprove ? [action, view] : [view];
        case 2:
            return [crmReason, view];
        case 3:
            return [reason, view];
        default:
            return [view];
    }
};

const actions = ["Approve", "Reject"];

const BookingTable = ({ tab, filters, isApprove }) => {
    const [state, { setUrl, setPage }] = usePagination();

    const [fetching, onApprove] = useApprove();
    const [rejected, setRejected] = useState([]);

    const onAction = useCallback(
        ({ type, selected }) => {
            if (type === actions[0]) {
                onApprove(selected);
            }
            if (type === actions[1]) {
                setRejected(selected);
            }
        },
        [onApprove]
    );

    useEffect(() => {
        setUrl({
            baseUrl: salesManagerUrls.booking,
            filters: { tab, ...filters },
        });
    }, [setUrl, tab, filters]);

    return (
        <>
            <TableList
                pageState={state}
                onPageClick={setPage}
                tableData={[...tbData, ...gm(tab, isApprove)]}
                // eslint-disable-next-line eqeqeq
                actions={isApprove && tab == 1 ? actions : []}
                onAction={onAction}
                actionLoading={fetching}
            />
            {rejected.length > 0 && <Reason bookings={rejected} close={() => setRejected([])} />}
        </>
    );
};

export default BookingTable;
