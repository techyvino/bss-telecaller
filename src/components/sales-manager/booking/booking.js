import { isArray } from "../../../utils";
import { bookingPaymentModes, relations, salutations } from "../../common/master";
import { formatKEY } from "../../form/formatAllData";
import formTypes from "../../form/formTypes";

export const addressKeys = {
    address: "address",
    area: "area",
    city: "city",
    state: "state",
    country: "country",
    pincode: "pincode",
};

const countryOpt = (x) => ({
    label: x.name,
    value: x.id,
});

const countryOpts = (country = []) =>
    isArray(country) ? country.map(countryOpt) : [];

export const createAddress = (formatKey, required = true) => ({
    countries,
    cities,
    states,
}) => {
    return [
        {
            name: formatKey + addressKeys.address,
            type: formTypes.text,
            label: "Address",
            inputProps: {
                placeholder: "Enter address",
            },
            conClass: "col-md-6",
            required,
        },
        {
            name: formatKey + addressKeys.area,
            type: formTypes.text,
            label: "Area",
            inputProps: {
                placeholder: "Enter area",
            },
            conClass: "col-md-6",
            required,
        },
        {
            name: formatKey + addressKeys.country,
            type: formTypes.select,
            label: "Country",
            inputProps: {
                placeholder: "Select a country",
                options: countryOpts(countries),
            },
            conClass: "col-md-6",
            required,
        },
        {
            name: formatKey + addressKeys.state,
            type: formTypes.select,
            label: "State",
            inputProps: {
                placeholder: "Select a state",
                options: countryOpts(states),
            },
            conClass: "col-md-6",
            required,
        },
        {
            name: formatKey + addressKeys.city,
            type: formTypes.select,
            label: "City",
            inputProps: {
                placeholder: "Enter city",
                options: countryOpts(cities),
            },
            conClass: "col-md-6",
            required,
        },
        {
            name: formatKey + addressKeys.pincode,
            type: formTypes.number,
            label: "Pincode",
            inputProps: {
                placeholder: "Enter pincode",
            },
            conClass: "col-md-6",
            required,
        },
    ];
};

export const COMM_ADR = "communication_address";
export const commAddrKey = `${COMM_ADR}${formatKEY}`;
export const commAddr = createAddress(commAddrKey);

export const PERM_ADR = "permanent_address";
export const permAddrKey = `${PERM_ADR}${formatKEY}`;
export const permAddr = createAddress(permAddrKey);

export const formKeys = Object.freeze({
    salutation: "salutation",
    name: "name",
    relation: "relationship",
    relationName: "relation_name",
    applicantType: "applicant_type",
    pan: "pan_no",
    adhaar: "aadhar_no",
    number: "number",
    secondary_number: "secondary_number",
    email: "email",
    secondary_email: "secondary_email",
    dob: "dob",
});

export const fileKeys = {
    panFile: "pan_document",
    adhaarFile: "aadhar_document",
    copanFile: "co_pan_document",
    coadhaarFile: "co_aadhar_document",
    referenceDocument: "reference_document",
    photo: "photograph",
    coPhoto: "co_photograph",
};

const companyKeys = {
    companyName: "name",
    designation: "designation",
    phone: "phone",
};

export const createCompany = (formatKey) => () => {
    return [
        {
            name: formatKey + companyKeys.companyName,
            type: formTypes.text,
            label: "Company Name",
            inputProps: {
                placeholder: "Enter address",
            },
            conClass: "col-md-4",
            required: false,
        },
        {
            name: formatKey + companyKeys.designation,
            type: formTypes.text,
            label: "Designation",
            inputProps: {
                placeholder: "Enter designation",
            },
            conClass: "col-md-4",
            required: false,
        },
        {
            name: formatKey + companyKeys.phone,
            type: formTypes.tel,
            label: "Phone Nummber",
            inputProps: {
                placeholder: "Enter phone number",
            },
            conClass: "col-md-4",
            required: false,
        },
    ];
};

export const createApplicant = (formatKey, isCo) => () => {
    return [
        {
            name: formatKey + formKeys.salutation,
            label: "Salutation",
            type: formTypes.select,
            inputProps: {
                placeholder: "Select Salutation",
                options: salutations,
            },
            conClass: "col-md-3",
            required: true,
        },
        {
            name: formatKey + formKeys.name,
            label: "Applicant Name",
            type: formTypes.text,
            inputProps: {
                placeholder: "Enter applicant name",
            },
            conClass: "col-md-5",
            required: true,
        },
        {
            name: formatKey + formKeys.applicantType,
            label: "Applicant Type",
            type: formTypes.text,
            inputProps: {
                placeholder: "Enter applicant type",
            },
            conClass: "col-md-4",
        },
        {
            name: formatKey + formKeys.relation,
            label: "Relation",
            type: formTypes.select,
            inputProps: {
                placeholder: "Select relation",
                options: relations,
            },
            conClass: "col-md-3",
        },
        {
            name: formatKey + formKeys.relationName,
            label: "Relation Name",
            type: formTypes.text,
            inputProps: {
                placeholder: "Enter relation Name",
            },
            conClass: "col-md-5",
        },
        {
            name: formatKey + formKeys.dob,
            type: formTypes.date,
            label: "Date of Birth",
            inputProps: {
                placeholder: "Enter date of birth",
                showMonthDropdown: true,
                showYearDropdown: true,
                dropdownMode: "select",
                maxDate: new Date(),
            },
            conClass: "col-md-4",
            required: true,
        },
        {
            name: formatKey + formKeys.pan,
            label: "PAN Number",
            type: formTypes.text,
            inputProps: {
                placeholder: "Enter PAN number",
            },
            conClass: "col-md-6",
            required: isCo ? false : true,
        },
        {
            name: isCo ? fileKeys.copanFile : fileKeys.panFile,
            label: "PAN File",
            type: formTypes.file,
            inputProps: {
                placeholder: "",
            },
            conClass: "col-md-6",
            required: false,
        },
        {
            name: formatKey + formKeys.adhaar,
            label: "Adhaar Number",
            type: formTypes.text,
            inputProps: {
                placeholder: "Enter Adhaar number",
            },
            conClass: "col-md-6",
            required: isCo ? false : true,
        },
        {
            name: isCo ? fileKeys.coadhaarFile : fileKeys.adhaarFile,
            label: "Adhaar File",
            type: formTypes.file,
            inputProps: {
                placeholder: "",
            },
            conClass: "col-md-6",
            required: false,
        },
        {
            name: formatKey + formKeys.number,
            label: "Primary Phone Number",
            type: formTypes.text,
            inputProps: {
                placeholder: "Enter phone number",
            },
            conClass: "col-md-3",
            required: true,
        },
        {
            name: formatKey + formKeys.secondary_number,
            type: formTypes.text,
            label: "Secondary Phone Number",
            inputProps: {
                placeholder: "Enter secondary phone number",
            },
            conClass: "col-md-3",
            required: false,
        },
        {
            name: formatKey + formKeys.email,
            type: formTypes.text,
            label: "Primary Email",
            inputProps: {
                placeholder: "Enter primary email",
            },
            conClass: "col-md-3",
            required: true,
        },
        {
            name: formatKey + formKeys.secondary_email,
            type: formTypes.text,
            label: "Secondary Email",
            inputProps: {
                placeholder: "Enter secondary email",
            },
            conClass: "col-md-3",
            required: false,
        },
        {
            name: isCo ? fileKeys.coPhoto : fileKeys.photo,
            label: `${isCo ? "Co " : ""}Applicant Photograph`,
            type: formTypes.file,
            inputProps: {
                placeholder: "",
            },
            conClass: "col-md-4",
            required: false,
        },
    ];
};

//aplicant
export const APPLICANT = "applicant";
export const applicantKey = `${APPLICANT}${formatKEY}`;
export const applicant = createApplicant(applicantKey);
export const getApplicantKey = (formKey) => applicantKey + formKey;

//aplicant company
export const APPLICANT_COMPANY = "applicant_company";
export const companyKey = `${APPLICANT_COMPANY}${formatKEY}`;
export const applicantCompany = createCompany(companyKey);

//aplicant company address
export const APPLICANT_COMPANY_ADR = "applicant_company_address";
export const aplCompAddrKey = `${APPLICANT_COMPANY_ADR}${formatKEY}`;
export const aplCompAddr = createAddress(aplCompAddrKey, false);

//co applicant
export const CO_APPLICANT = "co_applicant";
export const coApplicantKey = `${CO_APPLICANT}${formatKEY}`;
export const coApplicant = createApplicant(coApplicantKey, true);

export const getCoApplicantKey = (formKey) => coApplicantKey + formKey;

//co pplicant company
export const CO_APPLICANT_COMPANY = "co_applicant_company";
export const coCompanyKey = `${CO_APPLICANT_COMPANY}${formatKEY}`;
export const coApplicantCompany = createCompany(coCompanyKey);

//aplicant company address
export const CO_APPLICANT_COMPANY_ADR = "co_applicant_company_address";
export const coaplCompAddrKey = `${CO_APPLICANT_COMPANY_ADR}${formatKEY}`;
export const coaplCompAddr = createAddress(coaplCompAddrKey, false);

export const paymentKeys = {
    mode: "payment_mode",
};


export const payment = () => {
    return [
        {
            name: paymentKeys.mode,
            label: "Payment Mode",
            type: formTypes.radiogroup,
            inputProps: {
                placeholder: "Select payment mode",
                options: bookingPaymentModes,
            },
            conClass: "col-md-6",
            required: true,
        },
    ];
};

export const PAYMENT = "payment";
export const paymentKey = `${PAYMENT}${formatKEY}`;

export const bookingKeys = {
    mode: "mode_of_payment",
    transactionNo: "transaction_details",
    bankName: "bank_name",
    branchName: "branch_name",
    amountRecieved: "amount",
    transferDate: "transaction_date",
    amount: "amount",
};

export const booking = () => {
    return [
        {
            name: paymentKey + bookingKeys.mode,
            label: "Mode of Payment",
            type: formTypes.select,
            inputProps: {
                placeholder: "Select mode of payment",
                options: [
                    {
                        label: "Cheque",
                        value: "Cheque",
                    },
                    {
                        label: "Cash",
                        value: "Cash",
                    },
                    {
                        label: "DD",
                        value: "DD",
                    },
                    {
                        label: "Wire Transfer",
                        value: "Wire Transfer",
                    },
                    {
                        label: "Credit Card",
                        value: "Credit Card",
                    },
                ],
            },
            conClass: "col-md-4",
            required: true,
        },
        {
            name: paymentKey + bookingKeys.transactionNo,
            label: "Cheque /DD No./ Name & Transac. No. ",
            type: formTypes.text,
            inputProps: {
                placeholder: "Enter detail",
            },
            conClass: "col-md-4",
            required: true,
        },

        {
            name: paymentKey + bookingKeys.bankName,
            label: "Bank Name",
            type: formTypes.text,
            inputProps: {
                placeholder: "Enter bank name",
            },
            conClass: "col-md-4",
            required: true,
        },
        {
            name: paymentKey + bookingKeys.branchName,
            label: "Branch Name",
            type: formTypes.text,
            inputProps: {
                placeholder: "Enter branch name",
            },
            conClass: "col-md-4",
            required: true,
        },
        {
            name: paymentKey + bookingKeys.amount,
            label: "Amount",
            type: formTypes.text,
            inputProps: {
                placeholder: "Enter Amount",
            },
            conClass: "col-md-4",
            required: true,
        },
        {
            name: paymentKey + bookingKeys.transferDate,
            label: "Cheque / Wire Transfer Date",
            type: formTypes.date,
            inputProps: {
                placeholder: "Enter cheque / wire transfer Date",
                maxDate: new Date(),
            },
            conClass: "col-md-4",
            required: true,
        },
        {
            name: fileKeys.referenceDocument,
            label: "Reference Document",
            type: formTypes.file,
            inputProps: {},
            conClass: "col-md-4",
            required: false,
        },
    ];
};

export const comments = [
    {
        name: "comments",
        label: "Comments",
        type: formTypes.textarea,
        inputProps: {
            placeholder: "Enter your comment",
        },
        conClass: "col-md-12",
    },
];

const getVal = (val) => (val === undefined || val === null ? undefined : val);

export const getInitValues = (lead = {}) => {
    return {
        [getApplicantKey(formKeys.salutation)]: lead.salutation
            ? { value: lead.salutation, label: lead.salutation }
            : null,
        [getApplicantKey(formKeys.name)]: getVal(lead?.name),
        [getApplicantKey(formKeys.email)]: getVal(lead?.email),
        [getApplicantKey(formKeys.secondary_email)]: getVal(
            lead?.secondary_email
        ),
        [getApplicantKey(formKeys.number)]: getVal(lead?.number),
        [getApplicantKey(formKeys.secondary_number)]: getVal(
            lead?.secondary_number
        ),
    };
};
