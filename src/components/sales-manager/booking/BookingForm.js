import React, { useCallback, useEffect, useMemo, useState } from "react";
import useFormReducer from "../../../hooks/form/useFormReducer";
import DynamicContent from "../../common/DynamicContent";
import { useMasterValues } from "../../common/MasterData";
import CheckBox from "../../form/CheckBox";
import FieldArray from "../../form/FieldArray";
import { FormProvider } from "../../form/Form";
import SubmitBtn from "../../form/SubmitBtn";
import Switch from "../../form/Switch";
import {
    applicant,
    applicantCompany,
    booking,
    coApplicant,
    coApplicantCompany,
    commAddr,
    payment,
    permAddr,
    comments,
    getInitValues,
    addressKeys,
    commAddrKey,
    permAddrKey,
    coaplCompAddr,
    aplCompAddr,
    aplCompAddrKey,
    coaplCompAddrKey,
    COMM_ADR,
    APPLICANT,
    PAYMENT,
    fileKeys,
    CO_APPLICANT,
    APPLICANT_COMPANY,
    APPLICANT_COMPANY_ADR,
    CO_APPLICANT_COMPANY,
    CO_APPLICANT_COMPANY_ADR,
    getApplicantKey,
    formKeys,
    getCoApplicantKey,
} from "./booking";
import useToggle from "../../../hooks/useToggle";
import Modal from "../../modal/Modal";
import {
    resetForm,
    setErrors,
    setValue,
} from "../../../hooks/form/formReducer";
import useFetchData from "../../../hooks/http/useFetchData";
import { salesManagerUrls } from "../../../apiService/urls";
import { AptBasicDetail } from "../availability/ApartDetails";
import validateAll from "../../form/validateAll";
import useScrollToError from "../../../hooks/form/useScrollToError";
import formatAllData from "../../form/formatAllData";
import useSubmit from "../../../hooks/http/useSubmit";
import { isArray } from "../../../utils";
import useField from "../../../hooks/form/useField";
// import { useHistory } from "react-router-dom";
import dayjs from "dayjs";
import { toast } from "react-toastify";
import {
    isEmailorNA,
    isNA,
    isPhoneorNA,
} from "../../tele-caller/leads/leadValidations";
import curFormat from "../../../utils/curFormat";
import SelectApartment from "../availability/SelectApartment";
import ModalHeader from "../../modal/ModalHeader";
import ModalBody from "../../modal/ModalBody";
import { form2, formKeys as costSheetKeys } from "../costsheet/costform";
import keysToQuery from "../../../utils/keysToQuery";

const APARTMENT_DETAILS = "apartment_details";
const AMENITIES = "amenities";

const AprtListSelect = ({ projectId, onAptClick, title = "" }) => {
    return (
        <div className="container-fluid pt-3 pb-3">
            <h5>{title}</h5>
            {projectId && (
                <SelectApartment
                    projectId={projectId}
                    onAptClick={onAptClick}
                />
            )}
        </div>
    );
};

const getInit = (data) => {
    return {
        [costSheetKeys.plc]: data?.price_breakup?.plc,
        [costSheetKeys.frc]: data?.price_breakup?.floor_rise_rate,
        [costSheetKeys.noOfCar]: data?.price_breakup?.no_of_carpark,
        [costSheetKeys.baseRate]: data?.price_breakup?.base_rate,
        [costSheetKeys.amenities]: data?.price_breakup?.amenities_charges,
    };
};

const CostSheetForm = ({ data, onSubmit }) => {
    const form = useFormReducer({ values: getInit(data) });
    const [{ values }, dispatch] = form;

    const onClick = (e) => {
        e.preventDefault();
        const errors = validateAll(form2, values);
        if (Object.keys(errors).length > 0) {
            dispatch(setErrors(errors));
            return false;
        }
        onSubmit(values);
    };

    return (
        <FormProvider form={form}>
            <div className="form-row">
                <FieldArray allIds={form2} />
            </div>
            <SubmitBtn onClick={onClick} className="btn btn-theme btn-block">
                Submit
            </SubmitBtn>
        </FormProvider>
    );
};

const CostSheet = ({ id, onSubmit }) => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(salesManagerUrls.aprtDetail(id));
    }, [setUrl, id]);

    const isLoaded = !!state?.data?.id;

    return (
        <DynamicContent fetching={state.fetching} isLoaded={isLoaded}>
            {isLoaded && (
                <CostSheetForm data={state.data} onSubmit={onSubmit} />
            )}
        </DynamicContent>
    );
};

const SelectAprt = ({ project, aptSelect }) => {
    const [selected, set] = useState(null);
    const { toggle, onFalse, onTrue } = useToggle();

    const onAptClick = useCallback(
        (apartment) => {
            if (apartment.id) {
                set(apartment.id);
                onFalse();
            }
        },
        [onFalse]
    );

    if (project.id) {
        return (
            <>
                <div
                    role="button"
                    onClick={onTrue}
                    className="btn p-0 f-14 text-underline"
                >
                    Select Apartment
                </div>
                <Modal
                    isOpen={toggle}
                    close={onFalse}
                    contentStyle={{ maxWidth: "1000px" }}
                >
                    <AprtListSelect
                        projectId={project.id}
                        title={project.title}
                        onAptClick={onAptClick}
                    />
                </Modal>
                {selected && (
                    <Modal isOpen close={() => set(null)}>
                        <ModalHeader
                            title="Pricing Details"
                            close={() => set(null)}
                        />
                        <ModalBody>
                            <CostSheet
                                id={selected}
                                onSubmit={(values) => {
                                    aptSelect({
                                        id: selected,
                                        pricing: values,
                                    });
                                    set(null);
                                }}
                            />
                        </ModalBody>
                    </Modal>
                )}
            </>
        );
    }
    return null;
};

const AmenityBox = ({ value = [], setValue, item }) => {
    const isActive = value.includes(item.id);

    const onClick = () => {
        if (isActive) {
            setValue(value.filter((x) => x !== item.id));
        } else {
            setValue([...value, item.id]);
        }
    };

    return (
        <div
            className={`mb-3 border btn text-left rounded mr-3 ${
                isActive ? "bg-success text-white" : ""
            }`}
            onClick={onClick}
        >
            <div className="f-500">{item.name}</div>
            <div className="text-right font-weight-light">{item.count}</div>
        </div>
    );
};

const Amenities = ({ list }) => {
    const { value, setValue } = useField(AMENITIES);

    useEffect(() => {
        setValue([]);
    }, [list, setValue]);

    const val = isArray(value) ? value : [];

    return (
        <>
            <div className="f-16 f-500 mb-2 pt-2">Select Amenities</div>
            <div className="d-flex align-items-center flex-wrap">
                {list.map((item) => (
                    <AmenityBox item={item} value={val} setValue={setValue} />
                ))}
            </div>
        </>
    );
};

const Apt = ({ data }) => {
    const amenities = isArray(data.amenities) ? data.amenities : [];

    return (
        <>
            <div className="mb-3">
                <AptBasicDetail data={data} />
            </div>
            <div className="row">
                <div className="col-md-8">
                    <table className="table f-14 table-bordered">
                        <tbody>
                            <tr>
                                <td className="bg-light">
                                    Total Cost Without Registration
                                </td>
                                <th>
                                    {curFormat(
                                        data?.price_breakup?.total_a_amount
                                    )}
                                </th>
                            </tr>
                            <tr>
                                <td className="bg-light">
                                    Total Cost With Registration
                                </td>
                                <th>
                                    {curFormat(data?.price_breakup?.net_amount)}
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <Amenities list={amenities} />
        </>
    );
};

const AprtDetails = ({ id, pricing }) => {
    const [state, { setUrl }] = useFetchData();

    const q = `${salesManagerUrls.aprtDetail(id)}${keysToQuery(pricing)}`;

    useEffect(() => {
        setUrl(q);
    }, [setUrl, q]);

    const isLoaded = !!state?.data?.id;

    return (
        <DynamicContent fetching={state.fetching} isLoaded={isLoaded}>
            {isLoaded && <Apt data={state.data} />}
        </DynamicContent>
    );
};

const FormTitle = ({ children, title = "" }) => {
    return (
        <div className="pt-3 mb-3 d-flex justify-content-between align-items-center">
            <h5 className="text-underline">{title}</h5>
            {children}
        </div>
    );
};

const BookingForm = ({ leadInit }) => {
    const [fetching, master] = useMasterValues();
    const initValues = getInitValues(leadInit);

    const isLoaded = !!master?.project;

    return (
        <DynamicContent fetching={fetching} isLoaded={isLoaded}>
            {isLoaded && (
                <Booking
                    master={master}
                    initProject={leadInit.project}
                    initValues={initValues}
                    leadId={leadInit.id}
                    source={leadInit.source_of_campaigning}
                />
            )}
        </DynamicContent>
    );
};

const DisableIp = ({ value, label }) => {
    return (
        <div className="form-group">
            <label className="form-label">{label}</label>
            <input className="form-control" value={value} disabled />
        </div>
    );
};

const useCountry = (countries = [], values = {}, dispatch, formatKey) => {
    const country = values[formatKey + addressKeys.country]?.value;
    const state = values[formatKey + addressKeys.state]?.value;

    useEffect(() => {
        if (country) {
            dispatch(setValue(formatKey + addressKeys.state, null));
        }
    }, [country, dispatch, formatKey]);

    useEffect(() => {
        if (state) {
            dispatch(setValue(formatKey + addressKeys.city, null));
        }
    }, [state, dispatch, formatKey]);

    const states = useMemo(() => {
        if (country) {
            const [st] = countries.filter((x) => x.id === country);
            return st.states;
        }
        return [];
    }, [countries, country]);

    const cities = useMemo(() => {
        if (state) {
            const [ct] = states.filter((x) => x.id === state);
            return ct.cities;
        }
        return [];
    }, [state, states]);

    return { countries, states, cities };
};

const validateApplicant = (values, errors, isCo) => {
    const getKey = (key) =>
        isCo ? getCoApplicantKey(key) : getApplicantKey(key);
    const number = values[getKey(formKeys.number)];
    const secNumber = values[getKey(formKeys.secondary_number)];

    const numberErr = isPhoneorNA(number, true);
    const secNumberErr = isPhoneorNA(secNumber, false);

    if (numberErr) {
        Object.assign(errors, { [getKey(formKeys.number)]: numberErr });
    }
    if (secNumberErr) {
        Object.assign(errors, {
            [getKey(formKeys.secondary_number)]: numberErr,
        });
    }
    if (!numberErr && !secNumberErr && !isNA(secNumber)) {
        if (number === secNumber) {
            Object.assign(errors, {
                [getKey(
                    formKeys.secondary_number
                )]: "Cannot be same as primary number",
            });
        }
    }

    const email = values[getKey(formKeys.email)];
    const secEmail = values[getKey(formKeys.secondary_email)];

    const emailErr = isEmailorNA(email, true);
    const secEmailErr = isEmailorNA(secEmail);
    if (emailErr) {
        Object.assign(errors, { [getKey(formKeys.email)]: emailErr });
    }
    if (secEmailErr) {
        Object.assign(errors, { [formKeys.secondary_email]: secEmailErr });
    }
    if (!emailErr && !secEmailErr && !isNA(secEmail)) {
        if (email === secEmail) {
            Object.assign(errors, {
                [getKey(
                    formKeys.secondary_email
                )]: "cannot be same as primary email",
            });
        }
    }
};

const Booking = ({
    master = {},
    initValues = {},
    initProject = null,
    leadId,
    source = {},
}) => {
    // const history = useHistory();

    const [co, set] = useState(false);
    const [isSameAdr, setSameAddr] = useState(true);
    const form = useFormReducer({ values: initValues });
    const [{ values, errors }, dispatch] = form;

    const [fetching, submit] = useSubmit({
        success: () => {
            dispatch(resetForm());
            toast.success("Booked Successfully");
        },
    });

    const { country } = master;

    const applicantIds = useMemo(() => applicant(master), [master]);
    const companyIds = useMemo(() => applicantCompany(country, values), [
        country,
        values,
    ]);

    // applicant company address
    const aplOptions = useCountry(country, values, dispatch, aplCompAddrKey);
    const aplAddrIds = useMemo(() => aplCompAddr(aplOptions), [aplOptions]);

    const coApplicantIds = useMemo(() => coApplicant(master), [master]);
    const coApplicantCompanyIds = useMemo(() => coApplicantCompany(country), [
        country,
    ]);

    // co applicant company address
    const coApplOptions = useCountry(
        country,
        values,
        dispatch,
        coaplCompAddrKey
    );
    const coApplAddrIds = useMemo(() => coaplCompAddr(coApplOptions), [
        coApplOptions,
    ]);

    //communication address
    const comOptions = useCountry(country, values, dispatch, commAddrKey);
    const commAddrIds = useMemo(() => commAddr(comOptions), [comOptions]);

    //permanent address
    const permOptions = useCountry(country, values, dispatch, permAddrKey);
    const permAddrIds = useMemo(() => permAddr(permOptions), [permOptions]);

    const paymentIds = useMemo(() => payment(master), [master]);
    const bookingIds = useMemo(() => booking(master), [master]);

    const aptSelect = useCallback(
        (apt) => {
            dispatch(setValue(APARTMENT_DETAILS, apt));
        },
        [dispatch]
    );

    const scroll = useScrollToError();

    const onSubmit = (e) => {
        e.preventDefault();

        const coIds = co
            ? [...coApplicantIds, ...coApplicantCompanyIds, ...coApplAddrIds]
            : [];

        let commonIds = [
            ...applicantIds,
            ...companyIds,
            ...aplAddrIds,
            ...commAddrIds,
            ...bookingIds,
            comments,
            ...coIds,
        ];

        const errors = validateAll(
            [...commonIds, !isSameAdr ? permAddrIds : []],
            values
        );
        if (!values[APARTMENT_DETAILS]) {
            Object.assign(errors, {
                [APARTMENT_DETAILS]: "Apartment is required",
            });
        }
        validateApplicant(values, errors);
        if (co) {
            validateApplicant(values, errors, true);
        }
        if (Object.keys(errors).length > 0) {
            dispatch(setErrors(errors));
            scroll();
            return false;
        }
        const data = formatAllData(commonIds, values);

        let postData = new FormData();

        //leadId
        postData.append("lead_id", leadId);

        //apartment id
        const aptdetails = values[APARTMENT_DETAILS];
        postData.append("apartment_details_id", aptdetails.id);
        postData.append(
            "apartment_details_pricing",
            JSON.stringify(aptdetails.pricing)
        );

        //amenities
        if (isArray(values[AMENITIES]) && values[AMENITIES].length > 0) {
            const amnList = values[AMENITIES].map((x) => ({ amenities_id: x }));
            postData.append(AMENITIES, JSON.stringify(amnList));
        }

        //aplicant
        const applicant = data[APPLICANT] || {};
        Object.keys(applicant).forEach((apl) => {
            if (!isNA(applicant[apl])) {
                postData.append(apl, applicant[apl]);
            }
        });
        const companyDetails = [];
        const applicant_comapny = data[APPLICANT_COMPANY] || {};
        const applicant_addr = data[APPLICANT_COMPANY_ADR] || {};
        const companyObj = { ...applicant_comapny, ...applicant_addr };


        if (Object.keys(companyObj).length > 0) {
            companyDetails.push({
                type: "Applicant",
                ...companyObj,
            });
        }

        //co applicant
        if (co) {
            let coApplicant = data[CO_APPLICANT] || {};
            Object.keys(coApplicant).forEach((key) => {
                if (isNA(coApplicant[key])) {
                    delete coApplicant[key];
                }
            });
            postData.append("coapplicantdetails", JSON.stringify(coApplicant));

            const co_applicant_comapny = data[CO_APPLICANT_COMPANY] || {};
            const co_applicant_addr = data[CO_APPLICANT_COMPANY_ADR] || {};
            const co_companyObj = {
                ...co_applicant_comapny,
                ...co_applicant_addr,
            };
            if (Object.keys(co_companyObj).length > 0) {
                companyDetails.push({
                    type: "Co Applicant",
                    ...co_companyObj,
                });
            }
        }

        postData.append("company_details", JSON.stringify(companyDetails));

        // address
        const commuAddr = data[COMM_ADR] || {};
        
        const permAddr = isSameAdr
            ? commuAddr
            : formatAllData(permAddrIds, values);

        const permAddr_new = permAddr?.permanent_address? permAddr.permanent_address : permAddr
        const address = [
            {
                type: "Communication",
                ...commuAddr,
            },
            {
                type: "Permanent",
                ...permAddr_new,
            },
        ];

        postData.append("address", JSON.stringify(address));

        //payment
        const payment = data[PAYMENT] || {};
        Object.assign(payment, { name_of_transaction: "Booking Amount" });
        postData.append("payments", JSON.stringify([payment]));

        // files
        Object.values(fileKeys).forEach((x) => {
            if (isArray(data[x])) {
                data[x].forEach((file) => {
                    if (file) {
                        postData.append(x, file);
                    }
                });
            }
        });

        //coment
        if (values[comments]) {
            postData.append("comments", values[comments]);
        }

        submit({
            url: salesManagerUrls.booking,
            processData: false,
            mimeType: "multipart/form-data",
            contentType: false,
            data: postData,
            method: "POST",
        });
    };

    const apBday = dayjs().diff(values[getApplicantKey(formKeys.dob)], "year");
    const coApBday = dayjs().diff(
        values[getCoApplicantKey(formKeys.dob)],
        "year"
    );

    return (
        <FormProvider form={form}>
            <form autoComplete="off" noValidate onSubmit={onSubmit}>
                <FormTitle title="Applicant Details" />
                <div className="row">
                    <FieldArray allIds={applicantIds.slice(0, 6)} />
                    <div className="col-md-4">
                        {apBday > 0 && (
                            <DisableIp label="Applicant Age" value={apBday} />
                        )}
                    </div>
                </div>
                <div className="row">
                    <FieldArray
                        allIds={applicantIds.slice(6, applicantIds.length)}
                    />
                </div>
                <FormTitle title="Applicant Company Details" />
                <div className="row">
                    <FieldArray allIds={companyIds} />
                    <FieldArray allIds={aplAddrIds} />
                </div>

                <hr />
                <FormTitle title="Co Applicant">
                    <Switch checked={co} setValue={(checked) => set(checked)} />
                </FormTitle>
                {co && (
                    <>
                        <div className="row">
                            <FieldArray allIds={coApplicantIds.slice(0, 6)} />
                            <div className="col-md-4">
                                {coApBday > 0 && (
                                    <DisableIp
                                        label="Co Applicant Age"
                                        value={coApBday}
                                    />
                                )}
                            </div>
                        </div>
                        <div className="row">
                            <FieldArray
                                allIds={coApplicantIds.slice(
                                    6,
                                    coApplicantIds.length
                                )}
                            />
                        </div>
                        <FormTitle title="Co Applicant Company Details" />
                        <div className="row">
                            <FieldArray allIds={coApplicantCompanyIds} />
                            <FieldArray allIds={coApplAddrIds} />
                        </div>
                    </>
                )}
                <hr />
                <FormTitle title="Communication Address" />
                <div className="row">
                    <FieldArray allIds={commAddrIds} />
                </div>
                <FormTitle title="Permanent Address" />
                <div className="row">
                    <div className="col-md-12 mb-3">
                        <CheckBox
                            name="isSameAdr"
                            value={isSameAdr}
                            setValue={(val) => setSameAddr(val)}
                            title="Same as Communication Address"
                        />
                    </div>
                    {!isSameAdr && <FieldArray allIds={permAddrIds} />}
                </div>
                <hr />
                <div className="row">
                    <div className="col-md-4">
                        <DisableIp
                            label="Source"
                            value={source?.campaign?.source?.title}
                        />
                    </div>
                    <div className="col-md-4">
                        <DisableIp
                            label="Campaign"
                            value={source?.campaign?.title}
                        />
                    </div>
                    <div className="col-md-4">
                        <DisableIp
                            label="Source of campaigning"
                            value={source?.title}
                        />
                    </div>
                </div>
                <hr />
                <FormTitle title="Apartment Details">
                    <SelectAprt project={initProject} aptSelect={aptSelect} />
                </FormTitle>
                {errors[APARTMENT_DETAILS] && (
                    <div className="errMsg f-13">
                        {errors[APARTMENT_DETAILS]}
                    </div>
                )}
                {values[APARTMENT_DETAILS] && (
                    <AprtDetails {...values[APARTMENT_DETAILS]} />
                )}
                <hr />
                <div className="row">
                    <FieldArray allIds={paymentIds} />
                </div>
                <hr />
                <FormTitle title="Booking Payment Details" />
                <div className="row">
                    <FieldArray allIds={bookingIds} />
                </div>
                <hr />
                <div className="row">
                    <FieldArray allIds={comments} />
                </div>
                <SubmitBtn
                    fetching={fetching}
                    className="btn btn-theme btn-block mt-3"
                >
                    Submit
                </SubmitBtn>
            </form>
        </FormProvider>
    );
};

export default BookingForm;
