import React from "react";
import { isArray } from "../../../utils";

const report3List = [
    { th: "Type", tdKey: "apartment_type" },
    { th: "Total No Of Units", tdKey: "total_flat_type_own_units" },
    {
        th: "Total Saleable Area",
        tdKey: "total_flat_type_own_units_saleable_area",
    },
    { th: "Actual Sold Units", tdKey: "total_flat_type_sold_units" },
    {
        th: "Actual Sold Saleable Area",
        tdKey: "total_flat_type_sold_units_saleable_area",
    },
    { th: "Blocked Units", tdKey: "total_flat_type_blocked_units" },
    {
        th: "Blocked Saleable Area",
        tdKey: "total_flat_type_blocked_units_saleable_area",
    },
    { th: "Available Units", tdKey: "total_flat_type_available_units" },
    {
        th: "Available Saleable Area",
        tdKey: "total_flat_type_available_units_saleable_area",
    },
    { th: "Land Owner Units", tdKey: "total_flat_type_land_owner_units" },
    {
        th: "Land Owner Saleable Area",
        tdKey: "total_flat_type_land_owner_units_saleable_area",
    },
    { th: "Total Units", tdKey: "total_flat_type_units" },
    { th: "Total Saleable Area", tdKey: "total_flat_type_units_saleable_area" },
];

const Report3 = ({ flat_type = [], total = {} }) => {
    return (
        <div className="table-responsive">
            <table className="table table-bordered vertical-middle-table text-center">
                <thead>
                    <tr>
                        <th colSpan={9}>
                            BAASHYAAM - SOLD & UNSOLD - TYPE WISE DETAILS
                        </th>
                        <th colSpan={2}>LAND OWNER</th>
                        <th colSpan={2}>GRAND TOTAL</th>
                    </tr>
                    <tr>
                        {report3List.map((x, idx) => (
                            <th key={idx}>{x.th}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {flat_type.map((item, idx) => (
                        <tr key={idx}>
                            {report3List.map((x, idx2) => (
                                <td key={idx2}>
                                    {x.tdKey ? item?.[x.tdKey] : ""}
                                </td>
                            ))}
                        </tr>
                    ))}
                    <tr className="bg-light">
                        <th>Grand Total</th>
                        {report3List
                            .slice(1, report3List.length)
                            .map((x, idx) => (
                                <th key={idx}>
                                    {x.tdKey ? total?.[x.tdKey] : ""}
                                </th>
                            ))}
                    </tr>
                </tbody>
            </table>
        </div>
    );
};

const BookingItem = ({ title = "", units = 0, saleable_area = "-" }) => {
    return (
        <>
            <td>{title}</td>
            <td>{units}</td>
            <td>{saleable_area ? saleable_area : "-"}</td>
        </>
    );
};

const ReportRow = ({ data = [], title = "" }) => {
    const list = isArray(data) ? data : [];
    if (list.length > 0) {
        return (
            <>
                <tr>
                    <th rowSpan={list.length}>{title}</th>
                    {<BookingItem {...list[0]} />}
                </tr>
                {list.length > 1 &&
                    list.slice(1, list.length).map((item) => (
                        <tr key={item.title}>
                            <BookingItem {...item} />
                        </tr>
                    ))}
            </>
        );
    }
    return null;
};

const TowerReportTable = ({
    management,
    sales_team,
    total_available_units = 0,
    total_available_units_saleable_area = 0,
    total_units__sold_saleable_area = 0,
    total_units_saleable_area = 0,
    total_units = 0,
    total_units_sold = 0,
}) => {
    return (
        <table className="table table-bordered vertical-middle-table text-center">
            <thead>
                <tr>
                    <th colSpan={2}>Particulars</th>
                    <th>Units</th>
                    <th>Saleable Area</th>
                </tr>
            </thead>
            <tbody>
                <ReportRow data={sales_team} title="Sales Team Sold" />
                <tr>
                    <th colSpan={2}>Management</th>
                    <td>{management?.units ?? 0}</td>
                    <td>{management?.saleable_area ?? "-"}</td>
                </tr>
                <tr>
                    <th colSpan={2}>Total Sold</th>
                    <td>{total_units_sold}</td>
                    <td>
                        {total_units__sold_saleable_area?.toFixed(2) ?? "-"}
                    </td>
                </tr>
                <tr>
                    <th colSpan={2}>Total Unsold</th>
                    <td>{total_available_units}</td>
                    <td>
                        {total_available_units_saleable_area?.toFixed(2) ?? "-"}
                    </td>
                </tr>
                <tr className="bg-light">
                    <th colSpan={2}>Total Flats</th>
                    <th>{total_units}</th>
                    <th>{total_units_saleable_area?.toFixed(2) ?? "-"}</th>
                </tr>
            </tbody>
        </table>
    );
};

const TowerWiseItem = ({
    sales_team = {},
    management = {},
    total_sold_units = 0,
    total_available_units = 0,
    total_blocking_units = 0,
    total_land_owner_units = 0,
    total_units = 0,
    tower = "",
}) => {
    return (
        <tr>
            <td>{tower}</td>
            <td>{sales_team?.pre_lunch_sold_units}</td>
            <td>{sales_team?.post_lunch_sold_units}</td>
            <td>{sales_team?.total_sold_units}</td>
            <td>{management?.total_sold_units}</td>
            <td>{total_sold_units}</td>
            <td>{total_blocking_units}</td>
            <td>{total_available_units}</td>
            <td>{total_units}</td>
            <td>{total_land_owner_units}</td>
        </tr>
    );
};

const TowerWiseReport = ({ report = [], total = {} }) => {
    return (
        <div className="table-responsive">
            <table className="table table-bordered vertical-middle-table text-center">
                <thead>
                    <tr>
                        <th rowSpan={2}>Tower / Block</th>
                        <th colSpan={3}>Sales Team Sold</th>
                        <th>Management Sold</th>
                        <th rowSpan={2}>Tower/Block Sold</th>
                        <th rowSpan={2}>Blocked by Sales Team / Management</th>
                        <th rowSpan={2}>Available</th>
                        <th rowSpan={2}>Total Units</th>
                        <th rowSpan={2}>Land Owners Share</th>
                    </tr>
                    <tr>
                        <th>Pre Launch</th>
                        <th>Since Launch</th>
                        <th>Total Sales Team Sold</th>
                        <th>Confirmed</th>
                    </tr>
                </thead>
                <tbody>
                    {report.map((item) => (
                        <TowerWiseItem {...item} key={item.tower} />
                    ))}
                    {total &&
                        <tr>
                            <th>Total</th>
                            <th>{total?.pre_lunch_sold_units}</th>
                            <th>{total?.post_lunch_sold_units}</th>
                            <th>{total?.sales_total_sold_units}</th>
                            <th>{total?.management_total_sold_units}</th>
                            <th>{total?.total_sold_units}</th>
                            <th>{total?.total_blocking_units}</th>
                            <th>{total?.total_available_units}</th>
                            <th>{total?.total_units}</th>
                            <th>{total?.total_land_owner_units}</th>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    );
};

const MasterReports = ({ report_1, report_2 = {}, report_3 = {}, report_1_total = {} }) => {
    return (
        <>
            <h4 className="text-center mb-3">MASTER AVAILABILITY</h4>
            <TowerWiseReport report={report_1} total={report_1_total} />

            <TowerReportTable {...report_2} />
            <Report3 {...report_3} />
        </>
    );
};

export default MasterReports;
