import React from "react";
import { salesManagerUrls } from "../../../apiService/urls";
import useSubmit from "../../../hooks/http/useSubmit";
import DateFormat from "../../common/DateFormat";
import SubmitBtn from "../../form/SubmitBtn";
import Modal from "../../modal/Modal";
import ModalBody from "../../modal/ModalBody";
import ModalHeader from "../../modal/ModalHeader";
import { BLOCKED, BOOKED } from "./StatusBox";

const BookingDetails = ({
    bookingStatus,
    booking_no = "",
    name = "",
    created_on,
    created_by,
}) => {
    return (
        <table className="table table-bordered">
            <tbody>
                <tr>
                    <th>
                        {bookingStatus === BOOKED
                            ? "Booking"
                            : bookingStatus === BLOCKED
                            ? "Blocking"
                            : ""}{" "}
                        Number
                    </th>
                    <td>{booking_no}</td>
                </tr>
                <tr>
                    <th>Customer Name</th>
                    <td>{name}</td>
                </tr>
                <tr>
                    <th>{bookingStatus} Date</th>
                    <td>
                        <DateFormat date={created_on} />
                    </td>
                </tr>
                <tr>
                    <th>Sales Person Name</th>
                    <td>{created_by}</td>
                </tr>
            </tbody>
        </table>
    );
};

const Unblock = ({ apartmentId }) => {
    const [fetching, submit] = useSubmit({
        success: () => {
            window.location.reload();
        },
    });

    const onClick = () => {
        submit({
            url: salesManagerUrls.unBlockApartment(apartmentId),
            method: "POST",
        });
    };

    return (
        <SubmitBtn
            onClick={onClick}
            fetching={fetching}
            className="btn btn-block btn-theme"
        >
            UNBLOCK
        </SubmitBtn>
    );
};

const BlockingDetails = ({
    blocking_from,
    blocking_to,
    reason,
    created_by,
    apartmentId,
}) => {
    return (
        <>
            <table className="table table-bordered">
                <tbody>
                    <tr>
                        <th>Reason</th>
                        <td>{reason}</td>
                    </tr>
                    <tr>
                        <th>Blocking From</th>
                        <td>
                            <DateFormat date={blocking_from} format="ll" />
                        </td>
                    </tr>
                    <tr>
                        <th>Blocking To</th>
                        <td>
                            <DateFormat date={blocking_to} format="ll" />
                        </td>
                    </tr>

                    <tr>
                        <th>Blocked By</th>
                        <td>{created_by}</td>
                    </tr>
                </tbody>
            </table>
            <Unblock apartmentId={apartmentId} />
        </>
    );
};

const ApartmentModal = ({ booking, blocking, close, status, apartmentId }) => {
    return (
        <Modal isOpen close={close}>
            <ModalHeader title={`${status} Details`} close={close} />
            <ModalBody>
                {booking ? (
                    <BookingDetails
                        {...booking}
                        bookingStatus={status}
                        apartmentId={apartmentId}
                    />
                ) : blocking ? (
                    <BlockingDetails {...blocking} apartmentId={apartmentId} />
                ) : null}
            </ModalBody>
        </Modal>
    );
};

export default ApartmentModal;
