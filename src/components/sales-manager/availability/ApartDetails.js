import React, { useMemo } from "react";
import { isFunction } from "validate.js";
import { isArray } from "../../../utils";
import curFormat from "../../../utils/curFormat";
import Terms from "./Terms";

const getNum = (num = 0) => {
    const pr = parseFloat(num);
    return isNaN(pr) ? 0 : pr;
};

const basicTbl = [
    {
        th: "Tower/Block",
        td: ({ floor }) => floor?.tower?.title ?? "-",
    },
    {
        th: "Floor",
        td: ({ floor }) => floor?.title ?? "-",
    },
    {
        th: "Flat No",
        td: ({ unit_no }) => `${unit_no}`,
    },
    {
        th: "Unit No",
        td: ({ alias_name }) => alias_name,
    },
    {
        th: "Flat Type",
        td: ({ apartment_type }) => apartment_type?.title ?? "-",
    },
    {
        th: "No of car parking",
        td: ({ price_breakup }) => price_breakup?.no_of_carpark ?? 0,
    },
    {
        th: "BUA in sq.ft",
        td: ({ sales_area_round }) => `${sales_area_round}`,
    },
    {
        th: "Private Terrace Area in sq.ft",
        td: ({ terrace_area }) => `${terrace_area}`,
    },
    {
        th: "RERA Carpet Area in sq.ft",
        td: ({ carpet_area_round }) => `${carpet_area_round}`,
    },
    {
        th: "UDS in sq.ft",
        td: ({ uds_area }) => `${uds_area}`,
    },
    {
        th: "Basic Rate per sq.ft",
        td: ({ price_breakup }) => curFormat(price_breakup?.base_rate),
    },
    {
        th: "Floor rise rate per sq.ft",
        td: ({ price_breakup }) => curFormat(price_breakup?.floor_rise_rate),
    },
    {
        th: "PLC per sq.ft",
        td: ({ price_breakup }) => curFormat(price_breakup?.plc),
    },
    {
        th: "Total Rate per sq.ft",
        td: ({ price_breakup }) =>
            curFormat(
                getNum(price_breakup?.base_rate) +
                    getNum(price_breakup?.plc) +
                    getNum(price_breakup?.floor_rise_rate)
            ),
    },
    {
        th: "Facing",
        td: ({ facing }) => facing,
    },
    {
        th: "Guideline Value",
        td: ({ floor }) => curFormat(floor?.tower?.project?.guide_line_value),
    },
];

export const AptBasicDetail = ({ data }) => {
    return (
        <div className="container-fluid border-top border-left">
            <div className="row">
                {basicTbl.map((row) => (
                    <div
                        className="col-lg-4 f-14 border-bottom border-right"
                        key={row.th}
                    >
                        <div className="row align-items-stretch h-100">
                            <div className="col-6 bg-light border-right pt-3 pb-3">
                                {row.th}
                            </div>
                            <div className="col-6 f-500 bg-white pt-3 pb-3">
                                {isFunction(row.td) ? row.td(data) : null}
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

const PaymentSchedule = ({
    schedule = [],
    maintainance = [],
    gstList = [],
}) => {
    const payTotal = useMemo(() => {
        const arr = [...schedule, ...maintainance, ...gstList];
        return arr.reduce(
            (acc, cur) => {
                return {
                    gst: acc.gst + getNum(cur.gst),
                    amount: acc.amount + getNum(cur.amount),
                    total: acc.total + getNum(cur.total),
                    percentage: acc.percentage + getNum(cur.percentage),
                };
            },
            { gst: 0, amount: 0, total: 0, percentage: 0 }
        );
    }, [schedule, maintainance, gstList]);

    return (
        <table className="table table-bordered table-responsive-md  f-14">
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>Description</th>
                    <th className="text-right">%</th>
                    <th className="text-right">Milestone Amount</th>
                    <th className="text-right">GST</th>
                    <th className="text-right">Total</th>
                </tr>
            </thead>
            <tbody>
                {schedule.map((x, idx) => (
                    <tr key={x.title}>
                        <td
                            rowSpan={
                                idx + 1 === schedule.length
                                    ? maintainance.length + 1
                                    : 1
                            }
                            style={{ verticalAlign: "middle" }}
                        >
                            {idx + 1}
                        </td>
                        <td>{x.title}</td>
                        <td className="text-right">{x.percentage}%</td>
                        <td className="text-right">{curFormat(x.amount)}</td>
                        <td className="text-right">{curFormat(x.gst)}</td>
                        <td className="text-right">{curFormat(x.total)}</td>
                    </tr>
                ))}
                {maintainance.map((mt, idx) => (
                    <tr key={mt.title}>
                        <td colSpan="2">{mt.title}</td>
                        <td className="text-right">{curFormat(mt.amount)}</td>
                        <td className="text-right">{curFormat(mt.gst)}</td>
                        <td className="text-right">{curFormat(mt.total)}</td>
                    </tr>
                ))}
                {gstList.map((gt, idx) => (
                    <tr key={gt.title}>
                        <td colSpan="2">{gt.title}</td>
                        <td className="text-right">{curFormat(gt.amount)}</td>
                        <td className="text-right">{curFormat(gt.gst)}</td>
                        <td className="text-right">{curFormat(gt.total)}</td>
                    </tr>
                ))}
                <tr>
                    <th colSpan="2" className="text-right">
                        Total Cost
                    </th>
                    <th className="text-right">{payTotal.percentage}%</th>
                    <th className="text-right">{curFormat(payTotal.amount)}</th>
                    <th className="text-right">{curFormat(payTotal.gst)}</th>
                    <th className="text-right">{curFormat(payTotal.total)}</th>
                </tr>
            </tbody>
        </table>
    );
};

const ApartDetails = ({ data = {} }) => {
    const apartmentPrice = isArray(data?.price_breakup?.price_breakup_list)
        ? data.price_breakup.price_breakup_list
        : [];
    const otherPriceList = isArray(data?.price_breakup?.other_price_breakup_list)
    ? data.price_breakup.other_price_breakup_list
    : [];

    return (
        <>
            <div className="mb-3">
                <AptBasicDetail data={data} />
            </div>
            <h5 className="pt-2 pb-2">Cost Breakups</h5>
            <table className="table table-bordered table-responsive-md f-14">
                <tbody>
                    {apartmentPrice.map((x) => (
                        <tr key={x.title}>
                            <td>{x.title}</td>
                            <td className="text-right">
                                {curFormat(x.amount)}
                            </td>
                        </tr>
                    ))}
                    <tr>
                        <th className="text-right">Total (A)</th>
                        <th className="text-right">
                            {curFormat(data?.price_breakup?.total_a_amount)}
                        </th>
                    </tr>
                    {otherPriceList.map((x) => (
                        <tr key={x.title}>
                            {x.is_bold?<th>{x.title}</th>:<td>{x.title}</td>}
                            <td className="text-right">
                                {curFormat(x.amount)}
                            </td>
                        </tr>
                    ))}
                   
                    <tr>
                        <th className="text-right">Net Amount (A + B + C)</th>
                        <th className="text-right">
                            {curFormat(data?.price_breakup?.net_amount)}
                        </th>
                    </tr>
                </tbody>
            </table>
            <h5 className="pt-2 pb-2">Terms & Conditions</h5>
            <div className="mb-3">
                <Terms terms={data?.floor?.tower?.project?.terms_conditions} />
            </div>
            <h5 className="pt-2 pb-2">Payment Schedule</h5>
            <PaymentSchedule
                schedule={data?.price_breakup?.payment_schedule?.schedule}
                maintainance={
                    data?.price_breakup?.payment_schedule?.last_payment_list
                }
            />
        </>
    );
};

export default ApartDetails;
