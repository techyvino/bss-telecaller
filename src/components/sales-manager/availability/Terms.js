import React from "react";

const Terms = ({ terms = [] }) => {
    return (
        <ol>
            {terms.map((x) => (
                <li className="mb-3" key={x.id}>{x.name}</li>
            ))}
        </ol>
    );
};

export default Terms;
