import React, { useEffect, memo } from "react";
import { Check } from "react-feather";
import { isArray } from "validate.js";
import { salesManagerUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import keysToQuery from "../../../utils/keysToQuery";
import DynamicContent from "../../common/DynamicContent";
import EmptyList from "../../common/EmptyList";
import { statusCls } from "./StatusBox";

const Unit = memo(({ unit, isSelected = false, onAptClick }) => {
    return (
        <td
            className={`pointer relative ${
                unit.id ? statusCls(unit.status) : ""
            }`}
            onClick={() => onAptClick(unit)}
        >
            {isSelected ? (
                <div className="block-select-round">
                    <Check strokeWidth={3} size={14} />
                </div>
            ) : null}
            <div className="flex-center flex-column relative">
                {unit.id ? (
                    <>
                        <div className="f-500 f-18">{unit.alias_name}</div>
                        <div className="f-12">
                            <div>
                                {unit.sales_area
                                    ? `${unit.sales_area}sq.ft`
                                    : ""}
                            </div>
                        </div>
                    </>
                ) : (
                    <b className="text-danger">No Flat</b>
                )}
            </div>
        </td>
    );
});

const Units = ({ units = [], onAptClick, selected = {} }) => {
    return units.map((unit = {}, idx) => (
        <Unit
            unit={unit}
            isSelected={unit && unit.id ? !!selected[unit.id] : false}
            onAptClick={onAptClick}
            key={idx}
        />
    ));
};

const TowersWithFilter = ({
    floors = [],
    flatType = [],
    onAptClick,
    selected,
}) => {
    return (
        <div>
            <table className="apartment-table table-bordered">
                <thead>
                    <tr>
                        <th>Flat Type</th>
                        {flatType.map((typ) => (
                            <th key={typ.unit_no}>
                                Flat {typ.unit_no} -{" "}
                                {isArray(typ?.sales_area)
                                    ? typ.sales_area.map((x) => `${x}sq.ft `)
                                    : ""}
                            </th>
                        ))}
                    </tr>
                    <tr>
                        <th>Facing</th>
                        {flatType.map((typ) => (
                            <th key={typ.unit_no}>
                                {isArray(typ?.facing)
                                    ? typ.facing.toString()
                                    : ""}
                            </th>
                        ))}
                    </tr>
                    <tr>
                        <th>Apartment Type</th>
                        {flatType.map((typ) => (
                            <th key={typ.unit_no}>
                                {isArray(typ?.apartment_type)
                                    ? typ.apartment_type.toString()
                                    : ""}
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {floors.map((floor) => (
                        <tr key={floor.floor}>
                            <td className="vertical-middle">{floor.floor}</td>
                            <Units
                                units={floor.units}
                                onAptClick={onAptClick}
                                selected={selected}
                            />
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

const Listing = ({
    available = 0,
    blocked = 0,
    booked = 0,
    total = 0,
    flat_type = [],
    floors = [],
    onAptClick,
    selected,
}) => {
    return (
        <>
            <div className="p-2">
                <div className="row text-center font-weight-bold">
                    <div className="text-success col-md-3 col-6 mb-3">
                        Available - {available}
                    </div>
                    <div className="text-secondary col-md-3 col-6 mb-3">
                        Blocked - {blocked}
                    </div>
                    <div className="text-danger col-md-3 col-6 mb-3">
                        Booked - {booked}
                    </div>
                    <div className="col-md-3 col-6">Total - {total}</div>
                </div>
            </div>
            {floors.length > 0 ? (
                <TowersWithFilter
                    floors={floors}
                    flatType={flat_type}
                    onAptClick={onAptClick}
                    selected={selected}
                />
            ) : (
                <EmptyList title="No Data Found" />
            )}
        </>
    );
};

export const ApartListing = ({ filters = {}, onAptClick, selected }) => {
    const [state, { setUrl }] = useFetchData();
    const q = keysToQuery(filters);

    useEffect(() => {
        setUrl(`${salesManagerUrls.projects}${q}`);
    }, [q, setUrl]);

    return (
        <>
            <DynamicContent fetching={state.fetching} isLoaded={true}>
                {state.data && (
                    <Listing
                        {...state.data}
                        onAptClick={onAptClick}
                        selected={selected}
                    />
                )}
            </DynamicContent>
        </>
    );
};

const ApartList = ({ filters, activeStatus, onAptClick, selected }) => {
    return (
        <ApartListing
            filters={filters}
            activeStatus={activeStatus}
            onAptClick={onAptClick}
            selected={selected}
        />
    );
};

export default ApartList;
