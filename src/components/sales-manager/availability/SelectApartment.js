/* eslint-disable eqeqeq */
import React, { useEffect, useMemo, useState } from "react";
import { AvailSelect } from "./Avail";
import { isArray } from "../../../utils";
import ApartList from "./ApartList";
import { salesManagerUrls } from "../../../apiService/urls";
import DynamicContent from "../../common/DynamicContent";
import useFetchData from "../../../hooks/http/useFetchData";
import { AVAILABLE } from "./StatusBox";

const SelectApartmentList = ({ projectId, towers = [], onAptClick }) => {
    const [towerId, setTowerId] = useState(towers?.[0]?.id);
    const [floorId, setFloorId] = useState("");
    const [search, setSearch] = useState("");

    const floors = useMemo(
        () => towers.filter((x) => x.id == towerId)?.[0]?.floors,
        [towerId, towers]
    );

    const floorList = isArray(floors) ? floors : [];

    return (
        <>
            <div className="row">
                <div className="col-lg-12">
                    <div className="form-row justify-content-center">
                        {towers.length > 1 && (
                            <div className="col-md-12 mb-3">
                                <AvailSelect
                                    onChange={(val) => setTowerId(val)}
                                    options={towers}
                                    value={towerId}
                                    label="Tower"
                                />
                            </div>
                        )}
                    </div>
                    <div className="form-row">
                        <div className="col-md-6 mb-3">
                            <AvailSelect
                                onChange={(val) => setFloorId(val)}
                                options={floorList}
                                value={floorId}
                                label="Floor"
                                hasAll
                            />
                        </div>
                        <div className="col-md-6 mb-3">
                            <input
                                className="form-control"
                                placeholder="Search Apartment"
                                onBlur={(e) => {
                                    setSearch(e.target.value);
                                }}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <ApartList
                filters={{
                    project: projectId,
                    floor: floorId,
                    tower: towerId,
                    search,
                    status: AVAILABLE,
                }}
                onAptClick={onAptClick}
            />
        </>
    );
};

export const SelectApartment = ({ projectId, onAptClick }) => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(salesManagerUrls.towers);
    }, [setUrl]);

    const isLoaded = !!state.data;

    return (
        <DynamicContent isLoaded={isLoaded} fetching={state.fetching}>
            {isLoaded && (
                <SelectApartmentList
                    towers={
                        state.data.filter((x) => x.id == projectId)?.[0]?.towers
                    }
                    projectId={projectId}
                    onAptClick={onAptClick}
                />
            )}
        </DynamicContent>
    );
};

export default SelectApartment;
