import React from "react";

const FloorPlans = ({ plans = [] }) => {
    return (
        <div className="row">
            {plans.map((plan) => (
                <div className="col-md-4 mb-3" key={`${plan.id}`}>
                    <a
                        href={plan.image}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <img className="w-100" src={plan.image} alt="" />
                    </a>
                </div>
            ))}
        </div>
    );
};

export default FloorPlans;
