import React, { useEffect } from "react";
import { salesManagerUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import { isArray } from "../../../utils";
import keysToQuery from "../../../utils/keysToQuery";
import DynamicContent from "../../common/DynamicContent";

const BookingItem = ({
    title = "",
    units = 0,
    saleable_area = "-",
    titleSpan = 1,
}) => {
    return (
        <>
            <td colSpan={titleSpan}>{title}</td>
            <td>{units}</td>
            <td>{saleable_area ? saleable_area : "-"}</td>
        </>
    );
};

const ReportRow = ({ data = [], title = "" }) => {
    const list = isArray(data) ? data : [];
    const titleSpan = title ? 1 : 2;
    if (list.length > 0) {
        return (
            <>
                <tr>
                    {title && <th rowSpan={list.length}>{title}</th>}
                    {<BookingItem {...list[0]} titleSpan={titleSpan} />}
                </tr>
                {list.length > 1 &&
                    list.slice(1, list.length).map((item) => (
                        <tr key={item.title}>
                            <BookingItem {...item} titleSpan={titleSpan} />
                        </tr>
                    ))}
            </>
        );
    }
    return null;
};

const TowerReportTable = ({ availability, booking }) => {
    return (
        <table className="table table-bordered vertical-middle-table text-center">
            <thead>
                <tr className="bg-light">
                    <th colSpan={2}>Particulars</th>
                    <th>Units</th>
                    <th>Saleable Area</th>
                </tr>
            </thead>
            <tbody>
                <ReportRow data={booking?.sales_team} title="Sales Team Sold" />
                <ReportRow data={booking?.management} title="Management Sold" />
                <tr className="bg-light">
                    <th colSpan={2}>Total Sold</th>
                    <th>{booking?.total_units_sold ?? 0}</th>
                    <th>{booking?.total_saleable_area ?? "-"}</th>
                </tr>
                <tr className="bg-light">
                    <th colSpan={2}>Sold in %</th>
                    <th colSpan={2}>
                        {booking?.total_units_sold_percentage ?? 0}%
                    </th>
                </tr>
                <ReportRow data={availability?.land_owner} title="Land Owner" />
                <ReportRow data={availability?.own} />
                <tr className="bg-light">
                    <th colSpan={2}>Total Flats</th>
                    <th>{availability?.total?.units ?? 0}</th>
                    <th>
                        {availability?.total?.saleable_area?.toFixed(2) ?? "-"}
                    </th>
                </tr>
            </tbody>
        </table>
    );
};

const TowerReport = ({ projectId, towerId }) => {
    const [state, { setUrl }] = useFetchData();

    const q = `${salesManagerUrls.towerReport}${keysToQuery({
        project: projectId,
        tower: towerId,
    })}`;

    useEffect(() => {
        setUrl(q);
    }, [setUrl, q]);

    const isLoaded = !!state?.data?.availability;

    return (
        <DynamicContent isLoaded={isLoaded} fetching={state.fetching}>
            {isLoaded && <TowerReportTable {...state.data} />}
        </DynamicContent>
    );
};

export default TowerReport;
