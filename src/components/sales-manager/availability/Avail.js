/* eslint-disable eqeqeq */
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Link, useHistory, useLocation, useRouteMatch } from "react-router-dom";
import { salesManagerUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import useUser from "../../../hooks/user/useUser";
import { isArray } from "../../../utils";
import keysToQuery from "../../../utils/keysToQuery";
import DynamicContent from "../../common/DynamicContent";
import ApartList from "./ApartList";
import ApartmentModal from "./ApartmentModal";
import StatusBox, { AVAILABLE, BLOCKED, BOOKED } from "./StatusBox";
import TowerReport from "./TowerReport";

function useQuery() {
    const { search } = useLocation();
    return new URLSearchParams(search);
}

function useCanViewReport() {
    const { role } = useUser()[0];
    return [7, 8].includes(role?.id);
}

export const AvailSelect = ({
    value,
    options = [],
    onChange,
    label = "",
    hasAll,
}) => {
    return (
        <select
            className="form-control"
            onChange={(e) => {
                onChange(e.target.value);
            }}
            value={value}
        >
            {hasAll && <option value="">All {label}</option>}
            {options.map((x) => (
                <option value={x.id} key={x.id}>
                    {`${label ? `${label} - ` : ""}${x.title}`}
                </option>
            ))}
        </select>
    );
};

const Reportlink = (props) => {
    const { url } = useRouteMatch();
    return (
        <Link
            className="text-body text-underline"
            to={`${url}/tower-wise-availability${keysToQuery(props)}`}
        >
            View Master Reports
        </Link>
    );
};

const ProjectAvail = ({ projects = [], onAptClick, selected }) => {
    const canViewReport = useCanViewReport();

    const query = useQuery();
    const { push } = useHistory();

    const searchVal = query.get("search") ?? "";
    const [search, setSearch] = useState(searchVal);

    const projectQuery = query.get("project");
    const projectId = projectQuery ? projectQuery : projects[0].id;

    const towers = useMemo(
        () => projects.filter((x) => x.id == projectId)?.[0]?.towers,
        [projects, projectId]
    );
    const towerList = isArray(towers) ? towers : [];
    const towerQuery = query.get("tower");
    const towerId = towerQuery ? towerQuery : towerList?.[0]?.id;

    const floors = useMemo(
        () => towerList.filter((x) => x.id == towerId)?.[0]?.floors,
        [towerId, towerList]
    );
    const floorList = isArray(floors) ? floors : [];
    const floorQuery = query.get("floor");
    const floorId = floorQuery ? floorQuery : "";

    const activeStatus = query.get("status") ?? "";

    return (
        <>
            <div className="row">
                <div className="offset-md-3 col-md-6">
                    <div className="form-row justify-content-center">
                        <div className="col-md-6 mb-3">
                            <AvailSelect
                                onChange={(val) => {
                                    query.set("project", val);
                                    query.delete("tower");
                                    query.delete("floor");
                                    push({
                                        search: `?${query.toString()}`,
                                    });
                                }}
                                options={projects}
                                value={projectId}
                            />
                        </div>
                        {towerList.length > 1 && (
                            <div className="col-md-6 mb-3">
                                <AvailSelect
                                    onChange={(val) => {
                                        query.set("tower", val);
                                        query.delete("floor");
                                        push({
                                            search: `?${query.toString()}`,
                                        });
                                    }}
                                    options={towerList}
                                    value={towerId}
                                    label="Tower"
                                />
                            </div>
                        )}
                    </div>
                    <div className="form-row">
                        <div className="col-md-6 mb-3">
                            <AvailSelect
                                onChange={(val) => {
                                    query.set("floor", val);
                                    push({
                                        search: `?${query.toString()}`,
                                    });
                                }}
                                options={floorList}
                                value={floorId}
                                label="Floor"
                                hasAll
                            />
                        </div>
                        <div className="col-md-6 mb-3">
                            <input
                                value={search}
                                className="form-control"
                                placeholder="Search Apartment"
                                onChange={(e) => setSearch(e.target.value)}
                                onBlur={(e) => {
                                    const val = e.target.value;
                                    if (val) {
                                        query.set("search", val);
                                    } else {
                                        query.delete("search", val);
                                    }
                                    push({
                                        search: `?${query.toString()}`,
                                    });
                                }}
                            />
                        </div>
                    </div>
                    <div className="d-flex justify-content-between mb-4">
                        {[AVAILABLE, BLOCKED, BOOKED].map((x) => (
                            <StatusBox
                                title={x}
                                status={x}
                                activeStatus={activeStatus}
                                key={x}
                                onClick={(st) => {
                                    if (st) {
                                        query.set("status", st);
                                    } else {
                                        query.delete("status");
                                    }
                                    push({
                                        search: `?${query.toString()}`,
                                    });
                                }}
                            />
                        ))}
                    </div>
                </div>
            </div>
            {projectId && canViewReport && (
                <div className="d-flex justify-content-end mb-3">
                    <Reportlink project={projectId} />
                </div>
            )}
            {projectId && (
                <div className="mb-5">
                    <ApartList
                        filters={{
                            project: projectId,
                            floor: floorId,
                            tower: towerId,
                            search: searchVal,
                            status: activeStatus,
                        }}
                        onAptClick={onAptClick}
                        selected={selected}
                    />
                </div>
            )}
            {projectId && towerId && canViewReport && (
                <div className="mb-5 flex-center">
                    <TowerReport projectId={projectId} towerId={towerId} />
                </div>
            )}
        </>
    );
};

export const AvailWithProjects = ({ onAptClick, selected = {} }) => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(salesManagerUrls.towers);
    }, [setUrl]);

    const isLoaded = !!state.data;

    return (
        <DynamicContent isLoaded={isLoaded} fetching={state.fetching}>
            {isLoaded && (
                <ProjectAvail
                    projects={state.data}
                    onAptClick={onAptClick}
                    selected={selected}
                />
            )}
        </DynamicContent>
    );
};

const Avail = () => {
    const canViewReport = useCanViewReport();

    const { url } = useRouteMatch();
    const { push } = useHistory();

    const [details, setDetails] = useState(null);

    const onAptClick = useCallback(
        (apartment) => {
            if (apartment.status === AVAILABLE) {
                push(`${url}/apartment/${apartment.id}`);
                return false;
            }
            if (canViewReport && apartment.status === BOOKED) {
                setDetails({
                    status: BOOKED,
                    booking: apartment?.booking_details,
                    blocking: apartment?.blocking_details,
                    apartmentId: apartment?.id,
                });
                return false;
            }
            if (canViewReport && apartment.status === BLOCKED) {
                setDetails({
                    status: BLOCKED,
                    blocking: apartment?.blocking_details,
                    booking: apartment?.booking_details,
                    apartmentId: apartment?.id,
                });
                return false;
            }
        },
        [push, url, canViewReport]
    );

    return (
        <>
            <AvailWithProjects
                canViewReport={canViewReport}
                onAptClick={onAptClick}
            />
            {details && (
                <ApartmentModal
                    title="Details"
                    {...details}
                    close={() => setDetails(null)}
                />
            )}
        </>
    );
};

export default Avail;
