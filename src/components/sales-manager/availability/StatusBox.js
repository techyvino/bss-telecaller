import React from "react";

export const AVAILABLE = "Available";
export const BLOCKED = "Blocked";
export const BOOKED = "Booked";

export const statusCls = (status) => {
    switch (status) {
        case AVAILABLE:
            return "bg-success text-white";
        case BLOCKED:
            return "bg-secondary text-white";
        case BOOKED:
            return "bg-danger text-white";
        default:
            return "";
    }
};

const size = "18px";
const stl = { width: size, height: size };

const StatusBox = ({ title, status, activeStatus, onClick }) => {
    const isActive = activeStatus === status;
    return (
        <button
            className="btn p-0 d-flex align-items-center"
            onClick={() => onClick(isActive ? "" : status)}
        >
            <div className={"mr-1 " + statusCls(status)} style={stl} />
            <span className={isActive ? `font-weight-bold text-striked` : ""}>{title}</span>
        </button>
    );
};

export default StatusBox;
