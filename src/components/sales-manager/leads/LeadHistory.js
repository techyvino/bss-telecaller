import React, { useCallback, useEffect, useState } from "react";
import useToggle from "../../../hooks/useToggle";
import Modal from "../../modal/Modal";
import ModalHeader from "../../modal/ModalHeader";
import ModalBody from "../../modal/ModalBody";
import FieldCon from "../../form/FieldCon";
import Loader from "../../form/Loader";
import useDateFormat from "../../../hooks/useDateFormat";
import TextArea from "../../form/TextArea";
import useSubmit from "../../../hooks/http/useSubmit";
import SubmitBtn from "../../form/SubmitBtn";
import urls from "../../../apiService/urls";
import { useMasterValues } from "../../common/MasterData";
import SelectIp from "../../form/SelectIp";
import InfiniteList from "../../common/InfiniteList";
import useInfiniteList from "../../../hooks/http/useInfiniteList";
import { ArrowRight } from "react-feather";
import useSalesTeam from "../../tele-caller/leads/useSalesTeam";
import DateIp from "../../form/DateIp";
import { dateformat } from "../../form/formatAllData";
import EmptyList from "../../common/EmptyList";

const activities = ["Scheduled", "Held", "Not Held", "Closed"];

const getColor = (activity) => {
    switch (activity) {
        case activities[0]:
            return "orange";
        case activities[1]:
            return "green";
        case activities[2]:
            return "blue";
        case activities[3]:
            return "gray";
        default:
            return "black";
    }
};

const activityOptions = activities.map((x) => ({
    label: x,
    value: x,
}));

const HistoryItem = ({
    description,
    created_on,
    status_from = {},
    status_to = {},
    created_by = {},
    activity_status = "",
}) => {
    const createdOn = useDateFormat(created_on, "lll");
    const bg = getColor(activity_status);

    return (
        <div className="border-bottom mb-2 p-2 f-14">
            <div className="d-flex justify-content-between align-items-center">
                <div className="f-500 d-flex align-items-center mr-2">
                    {status_from.title}{" "}
                    <ArrowRight className="ml-2 mr-2" size={14} />{" "}
                    {status_to.title}
                </div>
                {activity_status && (
                    <span
                        className="badge badge-info"
                        style={{ lineHeight: "inherit", backgroundColor: bg }}
                    >
                        {activity_status}
                    </span>
                )}
            </div>

            <div className="mb-1">{description}</div>
            <div className="d-flex justify-content-between flex-wrap text-muted">
                <small>
                    {created_by?.user
                        ? `${created_by.user.first_name}${created_by.user.last_name}`
                        : ""}
                </small>
                <small>{createdOn}</small>
            </div>
        </div>
    );
};

const AddHistory = ({ id, succFunc, projectId }) => {
    const [isLoading, { followup_status = [] }] = useMasterValues();
    const [followStatus, setFollowStatus] = useState(null);
    const [leadStatus, setLeadStatus] = useState(null);
    const [description, setValue] = useState("");
    const [assignTo, setAssignTo] = useState(null);
    const [dt, setDt] = useState(null);

    const statusId =
        followStatus && followStatus.value ? followStatus.value : null;
    const leadStatusId =
        leadStatus && leadStatus.value ? leadStatus.value : null;
    const salesTeam = useSalesTeam(projectId);

    const salesTeamList = Array.isArray(salesTeam.data)
        ? salesTeam.data.map((x) => ({
              label: x.user
                  ? `${x.user.first_name} ${x.user.last_name}`
                  : x.emp_id,
              value: x.id,
          }))
        : [];

    const [fetching, submit] = useSubmit({
        success: (data) => {
            succFunc(data);
            setFollowStatus(null);
            setLeadStatus(null);
            setAssignTo(null);
            setValue("");
            setDt(null);
        },
    });

    const onSubmit = () => {
        const data = {
            description,
            status: statusId,
            activity_status: leadStatusId,
        };
        Object.assign(data, { follow_up_date: dateformat(dt) });
        if (statusId === 9 && assignTo && assignTo.value) {
            Object.assign(data, { assigned_to_id: assignTo.value });
        }
        submit({
            url: urls.addFollowUp(id),
            method: "POST",
            data,
        });
    };

    const extraValue = statusId === 5 ? dt : followStatus === 9 ? assignTo : 1;
    const minDate = new Date();

    return (
        <>
            <FieldCon>
                <SelectIp
                    isLoading={isLoading}
                    setValue={setFollowStatus}
                    value={followStatus}
                    options={followup_status.map((x) => ({
                        label: x.title,
                        value: x.id,
                    }))}
                    placeholder="Select activity"
                />
            </FieldCon>
            <FieldCon>
                <DateIp
                    setValue={setDt}
                    value={dt}
                    placeholder="Date"
                    minDate={minDate}
                />
            </FieldCon>
            {statusId === 9 && (
                <FieldCon>
                    <SelectIp
                        setValue={setAssignTo}
                        value={assignTo}
                        options={salesTeamList}
                        placeholder="Assign To"
                        isLoading={salesTeam.fetching}
                    />
                </FieldCon>
            )}
            <FieldCon>
                <SelectIp
                    isLoading={isLoading}
                    setValue={(val) => setLeadStatus(val)}
                    value={leadStatus}
                    options={activityOptions}
                    placeholder="Select Status"
                />
            </FieldCon>
            <FieldCon>
                <TextArea
                    setValue={setValue}
                    value={description}
                    placeholder="Enter remarks"
                />
            </FieldCon>
            <SubmitBtn
                fetching={fetching}
                disabled={
                    !(description && statusId && extraValue && leadStatusId)
                }
                className="btn btn-theme btn-block"
                onClick={onSubmit}
            >
                Add
            </SubmitBtn>
        </>
    );
};

const HistoryModal = ({ id, projectId, isEdit }) => {
    const [state, { setUrl, loadMore, addData }] = useInfiniteList();

    useEffect(() => {
        setUrl({ baseUrl: urls.leadHistory(id) });
    }, [setUrl, id]);

    const succFunc = useCallback(
        (data) => {
            addData(data);
        },
        [addData]
    );

    return (
        <ModalBody>
            <div className="row">
                {isEdit && (
                    <div className="col-md-6">
                        <AddHistory
                            projectId={projectId}
                            id={id}
                            succFunc={succFunc}
                        />
                    </div>
                )}
                <div
                    className={`col-md-${isEdit ? 6 : 12}`}
                    style={{ height: "320px", overflowY: "auto" }}
                >
                    <InfiniteList
                        loader={
                            <div className="flex-center">
                                <Loader size={26} />
                            </div>
                        }
                        {...state}
                        RenderItem={HistoryItem}
                        loadMore={loadMore}
                        emptyList={<EmptyList title="No data found" />}
                    />
                </div>
            </div>
        </ModalBody>
    );
};

const LeadHistory = ({ id, projectId, isEdit }) => {
    const { toggle, onFalse, onTrue } = useToggle();

    return (
        <>
            <button onClick={onTrue} className="btn f-13 p-0 text-underline">
                View{isEdit ? "/Add" : ""}
            </button>
            {toggle && (
                <Modal
                    isOpen
                    close={onFalse}
                    contentStyle={{ maxWidth: "800px" }}
                >
                    <ModalHeader
                        title="Lead Activity"
                        desc="Add & view lead activities"
                        close={onFalse}
                    />
                    <HistoryModal
                        isEdit={isEdit}
                        id={id}
                        projectId={projectId}
                    />
                </Modal>
            )}
        </>
    );
};

export default LeadHistory;
