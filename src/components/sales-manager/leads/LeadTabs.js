import React from "react";
import { salesGmUrls, salesManagerUrls } from "../../../apiService/urls";
import keysToQuery from "../../../utils/keysToQuery";
import Tabs, { TabBtnList, TabPane } from "../../common/Tabs";
import UrlParamTabList from "../../common/UrlParamTabList";
import LeadsTable from "./LeadsTable";

const tbs = ["Today", "Overall"];

const LeadTabs = ({ filters, projectID, employeeId, isGm = false, team }) => {
    const renderTable = ({ id, is_sub_menu, is_edit = false }) => {
        if (is_sub_menu) {
            return (
                <Tabs qName="sub_tab" tabs={tbs}>
                    <div className="d-flex justify-content-center">
                        <TabBtnList />
                    </div>
                    <TabPane tabId={tbs[0]}>
                        <LeadsTable
                            tab={id}
                            filters={filters}
                            employeeId={employeeId}
                            project={projectID}
                            subTab={1}
                            isEdit={is_edit}
                            isGm={isGm}
                            team={team}
                        />
                    </TabPane>
                    <TabPane tabId={tbs[1]}>
                        <LeadsTable
                            tab={id}
                            filters={filters}
                            employeeId={employeeId}
                            project={projectID}
                            subTab={2}
                            isEdit={is_edit}
                            isGm={isGm}
                            team={team}
                        />
                    </TabPane>
                </Tabs>
            );
        }
        return (
            <>
                <LeadsTable
                    tab={id}
                    filters={filters}
                    employeeId={employeeId}
                    project={projectID}
                    isEdit={is_edit}
                    isGm={isGm}
                    team={team}
                />
                
            </>
        );
    };

    const url = `${isGm ? salesGmUrls.tabs : salesManagerUrls.tabs
        }${keysToQuery(filters ? { ...filters, employee_id: employeeId, team: team, project: projectID } : { project: projectID, employee_id: employeeId, team })}`;

    return <UrlParamTabList url={url}>{renderTable}</UrlParamTabList>;
};

export default LeadTabs;
