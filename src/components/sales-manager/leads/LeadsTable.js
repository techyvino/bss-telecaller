import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Link, useHistory, useRouteMatch } from "react-router-dom";
import { salesManagerUrls, salesGmUrls } from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import LeadEmail from "../../tele-caller/leads/LeadEmail";
import LeadHistory from "./LeadHistory";
import LeadNumber from "../../tele-caller/leads/LeadNumber";
import Status from "../../tele-caller/leads/Status";
import Approve, { useApprove, Reason } from "./Approve";
import ViewLeadLink from "../../tele-caller/leads/ViewLeadLink";
import LeadFilters from "./LeadFilters";
import useQuery from "../../../hooks/useQuery";
import { getFilters } from "../../../utils/keysToQuery";

const EditLink = ({ id }) => {
    let { url } = useRouteMatch();

    return <Link to={`${url}/edit/${id}`}>Edit</Link>;
};

const tbData = (tab) => [
    {
        th: "Enquiry No",
        td: ({ id, lead_no = "" }) => (
            <ViewLeadLink id={id}>{lead_no}</ViewLeadLink>
        ),
    },
    {
        th: "Project",
        td: ({ project }) => project?.title ?? "",
    },
    {
        th: "Enquiry On",
        td: ({ enquiry_date }) => (
            <DateFormat format="ll" date={enquiry_date} />
        ),
    },
    {
        th: "Customer Name",
        td: ({ name }) => name,
    },
    {
        th: "Customer Email",
        td: LeadEmail,
    },
    {
        th: "Customer Phone",
        td: LeadNumber,
    },
    {
        th: "Source",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign?.source?.title ?? "",
    },
    {
        th: "Campaign",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign.title ?? "",
    },
    {
        th: "Primary Source",
        td: ({ source_of_campaigning }) => source_of_campaigning?.title ?? "",
    },
    {
        th: "Status",
        td: ({ status = {} }) => <Status {...status} />,
    },
    {
        th: "Created On",
        td: ({ created_on }) => <DateFormat date={created_on} />,
    },
    {
        th: "Tele-caller",
        td: ({ created_by }) =>
            created_by?.user
                ? `${created_by.user.first_name} ${created_by.user.last_name}`
                : "",
    },
    {
        th: "Assigned On",
        td: ({ assigned_on }) => <DateFormat date={assigned_on} />,
    },
    {
        th: "Followup Date",
        td: ({ follow_up_date }) => (
            <DateFormat date={follow_up_date} format="ll" />
        ),
    },
];

const hotData = [
    {
        th: "Convert",
        td: ({ id }) => <ConvertLink id={id} />,
    },
];

const editData = (isEdit) => {
    return [
        {
            th: "History",
            td: ({ project, id }) => (
                <LeadHistory
                    id={id}
                    isEdit={isEdit}
                    projectId={project && project.id ? project.id : null}
                />
            ),
        },
        ...(isEdit
            ? [
                  {
                      th: "Edit",
                      td: ({ id }) => <EditLink id={id} />,
                  },
              ]
            : []),
    ];
};

const ConvertLink = ({ id }) => {
    const { url } = useRouteMatch();

    return (
        <Link
            to={`${url}/convert/${id}`}
            className="btn btn-sm f-14 btn-theme rounded-pill"
        >
            Convert
        </Link>
    );
};

const salesPerson = {
    th: "Sales Person",
    td: ({ assigned_to }) =>
        `${assigned_to?.user?.first_name} ${assigned_to?.user?.last_name}`,
};

const gm = (tab) => {
    const actions = {
        th: "Actions",
        td: ({ id }) => <Approve id={id} />,
    };

    if (tab === 1) {
        return [actions];
    }
    return [];
};

const actions = ["Approve", "Reject"];

const LeadsTable = ({
    tab = 1,
    subTab,
    employeeId,
    project,
    team,
    isEdit = false,
    isGm = false,
}) => {
    const query = useQuery();
    const history = useHistory();
    const [state, { setUrl, setPage }] = usePagination();

    const filterQuery = query.get("filters") || "";

    const filters = useMemo(() => {
        const tableFilters = getFilters(filterQuery);
        return {
            ...tableFilters,
            tab,
            sub_tab: subTab,
            employee_id: employeeId,
            team,
            project,
        };
    }, [tab, subTab, employeeId, team, project, filterQuery]);

    const onFilter = (f = {}) => {
        query.set("filters", JSON.stringify(f));
        history.replace({
            search: `?${query.toString()}`,
        });
    };

    useEffect(() => {
        setUrl({
            baseUrl: isGm ? salesGmUrls.lead : salesManagerUrls.leads,
            filters,
        });
    }, [setUrl, isGm, filters]);

    const [fetching, onApprove] = useApprove();
    const [rejected, setRejected] = useState([]);

    const onAction = useCallback(
        ({ type, selected }) => {
            if (type === actions[0]) {
                onApprove(selected);
            }
            if (type === actions[1]) {
                setRejected(selected);
            }
        },
        [onApprove]
    );

    return (
        <>
            <LeadFilters onFilter={onFilter} />
            <TableList
                pageState={state}
                onPageClick={setPage}
                tableData={[
                    ...tbData(tab),
                    ...(isGm || team ? [salesPerson] : []),
                    ...(tab === 8 ? hotData : []),
                    ...(isGm ? gm(tab) : []),
                    ...editData(isEdit),
                ]}
                // eslint-disable-next-line eqeqeq
                actions={isGm && tab == 1 ? actions : ""}
                onAction={onAction}
                actionLoading={fetching}
            />
            {rejected.length > 0 && (
                <Reason leads={rejected} onFalse={() => setRejected([])} />
            )}
        </>
    );
};

export default LeadsTable;
