import React, { useCallback, useState } from "react";
import { Check, X } from "react-feather";
import { useHistory } from "react-router";
import { toast } from "react-toastify";
import { salesGmUrls } from "../../../apiService/urls";
import useSubmit from "../../../hooks/http/useSubmit";
import useQuery from "../../../hooks/useQuery";
import useToggle from "../../../hooks/useToggle";
import Loader from "../../form/Loader";
import SubmitBtn from "../../form/SubmitBtn";
import TextArea from "../../form/TextArea";
import Modal from "../../modal/Modal";
import ModalBody from "../../modal/ModalBody";
import ModalHeader from "../../modal/ModalHeader";

const style = { height: "30px", width: "30px" };

export const useApprove = () => {
    const query = useQuery();
    const { replace } = useHistory();

    const [fetching, submit] = useSubmit({
        success: () => {
            toast.success("Lead has been approved");
            query.set("tab", 2);
            replace({
                search: `?${query.toString()}`,
            });
        },
    });

    const onApprove = useCallback(
        (leads) => {
            submit({
                url: salesGmUrls.approveMul,
                method: "POST",
                data: { reason: "", status: true, leads },
            });
        },
        [submit]
    );

    return [fetching, onApprove];
};

export const Reason = ({ leads, onFalse }) => {
    const [value, setValue] = useState();
    const query = useQuery();
    const { replace } = useHistory();

    const [fetching, submit] = useSubmit({
        success: () => {
            toast.dark("Lead has been rejected");
            onFalse();
            query.set("tab", 3);
            replace({
                search: `?${query.toString()}`,
            });
        },
    });

    const onClick = () => {
        if (value) {
            submit({
                url: salesGmUrls.approveMul,
                method: "POST",
                data: { reason: value, status: false, leads },
            });
        }
    };

    return (
        <Modal isOpen close={onFalse}>
            <ModalHeader
                close={onFalse}
                title="Reject Reason"
                desc="Enter your resaon"
            />
            <ModalBody>
                <div className="form-group">
                    <TextArea
                        placeholder="Reason"
                        setValue={setValue}
                        value={value}
                    />
                </div>
                <SubmitBtn
                    className="btn btn-theme btn-block form-group"
                    disabled={!value}
                    fetching={fetching}
                    onClick={onClick}
                >
                    Submit
                </SubmitBtn>
            </ModalBody>
        </Modal>
    );
};

const Approve = ({ id }) => {
    const { toggle, onFalse, onTrue } = useToggle();
    const [fetching, onApprove] = useApprove();

    return (
        <div className="d-flex">
            <button
                style={style}
                className="btn p-0 btn-success rounded-circle mr-2"
                title="Approve"
                disabled={fetching}
                onClick={() => onApprove([id])}
            >
                {fetching ? (
                    <Loader size={18} />
                ) : (
                    <Check size={18} color="#fff" />
                )}
            </button>
            <button
                style={style}
                className="btn p-0 btn-danger rounded-circle"
                title="Reject"
                disabled={fetching}
                onClick={onTrue}
            >
                <X size={18} color="#fff" />
            </button>
            {toggle && <Reason leads={[id]} onFalse={onFalse} />}
        </div>
    );
};

export default Approve;
