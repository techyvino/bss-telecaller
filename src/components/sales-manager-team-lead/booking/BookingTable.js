import React, { useEffect } from "react";
import { salesManagerUrls } from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import { useRouteMatch, Link } from "react-router-dom";
import LeadNumber from "../../tele-caller/leads/LeadNumber";
import LeadEmail from "../../tele-caller/leads/LeadEmail";

const ViewLink = ({
    id,
    className = "text-body text-underline",
    title = "View Details",
}) => {
    let { url } = useRouteMatch();

    return (
        <Link className={className} to={`${url}/booking-details/${id}`}>
            {title}
        </Link>
    );
};

const tbData = [
    {
        th: "Lead Id",
        td: ({ lead }) => `${lead?.lead_no ?? ""}`,
    },
    {
        th: "Booking Id",
        td: ({ booking_no }) => `${booking_no}`,
    },
    {
        th: "Booking Date",
        td: ({ created_on }) => <DateFormat date={created_on} />,
    },
    {
        th: "Customer Name",
        td: ({ salutation, name, relation_name, relationship }) => (
            <div>
                {salutation} {name}
                {relationship && relation_name
                    ? `, ${relationship} of ${relation_name}`
                    : ""}
            </div>
        ),
    },
    {
        th: "Customer Email",
        td: LeadEmail,
    },
    {
        th: "Customer Phone",
        td: LeadNumber,
    },
    {
        th: "Project",
        td: ({ apartment_details }) => (
            <div>
                <div>
                    {apartment_details?.floor?.tower?.project?.title || ""}
                </div>
                {/* <div>{apartment_details?.floor?.tower?.project?.address}</div> */}
            </div>
        ),
    },
    {
        th: "Tower",
        td: ({ apartment_details }) =>
            `${apartment_details?.floor?.tower?.title ?? ""}`,
    },
    {
        th: "Floor",
        td: ({ apartment_details }) =>
            `${apartment_details?.floor?.title ?? ""}`,
    },
    {
        th: "Apartment",
        td: ({ apartment_details, alias }) =>
            `${apartment_details?.apartment_no ?? ""}${
                alias ? ` (${alias})` : ""
            }`,
    },
    {
        th: "Source",
        td: ({ lead }) =>
            lead?.source_of_campaigning?.campaign?.source?.title ?? "",
    },
    {
        th: "Campaign",
        td: ({ lead }) => lead?.source_of_campaigning?.campaign.title ?? "",
    },
    {
        th: "Primary Source",
        td: ({ lead }) => lead?.source_of_campaigning?.title ?? "",
    },
    {
        th: "Sales manager",
        td: ({ created_by }) =>
            created_by && created_by.user
                ? `${created_by.user.first_name} ${created_by.user.last_name}`
                : "",
    },
    {
        th: "View",
        td: ({ id }) => <ViewLink id={id} />,
    },
];

const BookingTable = ({ tab, tableData = tbData }) => {
    const [state, { setUrl, setPage }] = usePagination();

    useEffect(() => {
        setUrl({
            baseUrl: salesManagerUrls.booking,
            filters: { tab },
        });
    }, [setUrl, tab]);

    return (
        <TableList
            pageState={state}
            onPageClick={setPage}
            tableData={tableData}
        />
    );
};

export default BookingTable;
