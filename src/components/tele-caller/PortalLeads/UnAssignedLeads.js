import React, { useEffect } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import urls from '../../../apiService/urls';
import usePagination from '../../../hooks/http/usePagination';
import DateFormat from '../../common/DateFormat';
import TableList from '../../common/TableList';


const AddEnquiry = ({ id }) => {
    let { url } = useRouteMatch();

    return <Link to={`${url}/add-enquiry/${id}`}>Add Enquiry</Link>;
};

const tableData = [
    {
        th: "Portal",
        td: ({ portal }) => portal
    },
    {
        th: "Project",
        td: ({ project }) => project
    },
    {
        th: "Customer Name",
        td: ({ name }) => name
    },
    {
        th: "Mobile Number",
        td: ({ phone_number }) => phone_number
    },
    {
        th: "Email",
        td: ({ email }) => email
    },
    {
        th: "Address",
        td: ({ address }) => address
    },
    {
        th: "City",
        td: ({ city }) => city
    },
    {
        th: "Country",
        td: ({ country }) => country
    },
    {
        th: "Chat Content",
        td: ({ chat_content }) => chat_content
    },
    {
        th: "Chat Link",
        td: ({ chat_link }) => chat_link
    },
    {
        th: "Apartment Type",
        td: ({ apartment_type }) => apartment_type
    },
    {
        th: "Budget",
        td: ({ budget }) => budget
    },
    {
        th: "Utm Source",
        td: ({ utm_source }) => utm_source
    },
    {
        th: "Utm Campaign",
        td: ({ utm_campaign }) => utm_campaign
    },
    {
        th: "Contact Time",
        td: ({ contact_time }) => contact_time
    },
    {
        th: "Time to Buy",
        td: ({ time_to_buy }) => time_to_buy
    },
    {
        th: "Created On",
        td: ({ created_on }) => <DateFormat date={created_on} />,
    },
    {
        th: "Take Enquiry",
        td: ({ id}) => <AddEnquiry id={id}/>,
    }

]

const UnAssignedLeads = () => {
    const [pageState, { setUrl, setPage }] = usePagination();

    useEffect(() => {
        setUrl({
            baseUrl: `${urls.portalLeads}`,
            filters: { tab: 0 }
        });
    }, [setUrl]);


    return (
        <TableList
            pageState={pageState}
            onPageClick={setPage}
            tableData={tableData}
        />
    );
}

export default UnAssignedLeads;