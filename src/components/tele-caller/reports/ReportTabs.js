import React from "react";
import { useHistory } from "react-router";
import { teleCallerUrls } from "../../../apiService/urls";
import useQuery from "../../../hooks/useQuery";
import keysToQuery from "../../../utils/keysToQuery";
import DynamicTabList from "../../common/DynamicTabList";
import ReportTable from "./ReportTable";


const ReportTabs = ({ options = [], team }) => {
    const query = useQuery();
    const history = useHistory();
    const manager = query.get("manager") ?? "";
    const filter = keysToQuery({ manager: manager, team: typeof team === "number" ? team : "" })

    return (
        <>
            <div className="row">
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("manager", val);
                            } else {
                                query.delete("manager");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={manager}
                    >
                        <option value="">
                            All Sales Managers
                            </option>
                        {options.map((x) => (
                            <option value={x.id} key={x.id}>
                                {x?.user?.first_name} {x?.user?.last_name ?? ""}
                            </option>
                        ))}
                    </select>
                </div>
            </div>
            <DynamicTabList
                url={`${teleCallerUrls.reportTabs}${filter}`}>
                {({ id }) => <ReportTable tab={id} team={team} manager={manager} />}
            </DynamicTabList>
        </>
    );
};

export default ReportTabs;
