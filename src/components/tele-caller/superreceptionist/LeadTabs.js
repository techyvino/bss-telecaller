import React from 'react';
import Tabs, { TabBtnList, TabPane } from '../../common/Tabs';
import UnAssignedLeads from './UnAssignedLeads';

const tabs = ["Assigned Calls"]
const LeadTabs = () => {
    return (
        <Tabs tabs={tabs}>
            <TabBtnList />
            <TabPane tabId={tabs[0]}>
                <UnAssignedLeads />
            </TabPane>
        </Tabs>
    );
}

export default LeadTabs;