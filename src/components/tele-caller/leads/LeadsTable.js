import React from "react";
import { Link, useRouteMatch } from "react-router-dom";
import Status from "./Status";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import LeadHistory from "./LeadHistory";
import LeadNumber from "./LeadNumber";
import LeadEmail from "./LeadEmail";
import ViewLeadLink from "./ViewLeadLink";

const EditLink = ({ id }) => {
    let { url } = useRouteMatch();

    return <Link to={`${url}/edit/${id}`}>Edit</Link>;
};

const tableData = [
    {
        th: "Enquiry No",
        td: (data) => <ViewLeadLink id={data.id}>{data?.lead_no}</ViewLeadLink>,
    },
    {
        th: "Project",
        td: ({ project }) => project.title,
    },
    {
        th: "Enquiry On",
        td: ({ enquiry_date }) => <DateFormat  format="ll" date={enquiry_date} />,
    },
    {
        th: "Customer Name",
        td: ({ name, salutation }) =>
            `${salutation ? `${salutation} ` : ""}${name}`,
    },
    {
        th: "Customer Email",
        td: LeadEmail,
    },
    {
        th: "Customer Phone",
        td: LeadNumber,
    },
    {
        th: "Source",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign?.source?.title ?? "",
    },
    {
        th: "Campaign",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign.title ?? "",
    },
    {
        th: "Primary Source",
        td: ({ source_of_campaigning }) => source_of_campaigning?.title ?? "",
    },
    {
        th: "Status",
        td: ({ status = {} }) => <Status {...status} />,
    },
    {
        th: "Created On",
        td: ({ created_on }) => <DateFormat date={created_on} />,
    },
    {
        th: "Followup Date",
        td: ({ follow_up_date }) =>
            follow_up_date ? (
                <DateFormat date={follow_up_date} format="ll" />
            ) : (
                "-"
            ),
    },
    {
        th: "Existing Enquiry",
        td: ({ existing_lead }) =>
        existing_lead?.id ? (
            <ViewLeadLink id={existing_lead.id}>{existing_lead?.lead_no}</ViewLeadLink>
            ) : (
                "-"
            ),
    },
];

const historyTd = (employee_id) => {
    return {
        th: "History",
        td: ({ project, id, status }) => (
            <LeadHistory
                id={id}
                projectId={project && project.id ? project.id : null}
                hideAdd={status?.id === 3 || employee_id}
            />
        ),
    };
};

const edit = {
    th: "Edit",
    td: ({ id, status }) => (status?.id === 3 ? "-" : <EditLink id={id} />),
};

const LeadsTable = ({ state, onPageClick, isEdit = false, employee_id }) => {
    const list = [...tableData, historyTd(employee_id)];

    return (
        <TableList
            pageState={state}
            onPageClick={onPageClick}
            tableData={isEdit ? [...list, edit] : list}
        />
    );
};

export default LeadsTable;
