import React from "react";
import { Link, useRouteMatch } from "react-router-dom";

const ViewLeadLink = ({ id, children, className = "text-body text-underline" }) => {
    let { url } = useRouteMatch();

    return (
        <Link className={className} to={`${url}/view/${id}`}>
            {children}
        </Link>
    );
};

export default ViewLeadLink;
