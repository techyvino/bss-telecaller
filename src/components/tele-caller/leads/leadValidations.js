import { NA } from "../../common/master";

export function validateEmail(email = "") {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase()) ? "" : "Invalid Email";
}

export function validatePhone(val = "") {
    const re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{1,6})$/;
    return re.test(val) ? "" : "Invalid Phone number";
}

export const isNA = (val) => val === NA;

export const isEmailorNA = (val = "", required = false) => {
    if (typeof val !== "string") return "Not a valid string";
    const len = val.trim().length;
    if (len === 0) return required ? "This field is required" : "";
    return isNA(val) ? "" : validateEmail(val);
};

export const isPhoneorNA = (val = "", required = false) => {
    if (typeof val !== "string") return "Not a valid string";
    const len = val.trim().length;
    if (len === 0) return required ? "This field is required" : "";
    return isNA(val) ? "" : validatePhone(val);
};

export const deleteNA = (values = {}, keys = []) => {
    keys.forEach((key) => {
        if (isNA(values[key])) {
            delete values[key];
        }
    });
    return values;
};
