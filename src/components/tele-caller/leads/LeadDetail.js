import React from "react";
import { isArray, isFunction } from "../../../utils";
import DateFormat from "../../common/DateFormat";
import LeadEmail from "./LeadEmail";
import LeadNumber from "./LeadNumber";
import Status from "./Status";

const list = [
    {
        th: "Enquiry No",
        td: (data) => data?.lead_no,
    },
    {
        th: "Project",
        td: ({ project }) => project.title,
    },
    {
        th: "Enquiry On",
        td: ({ enquiry_date }) => (
            <DateFormat format="ll" date={enquiry_date} />
        ),
    },
    {
        th: "Customer Name",
        td: ({ name, salutation }) =>
            `${salutation ? `${salutation} ` : ""}${name}`,
    },
    {
        th: "Customer Email",
        td: LeadEmail,
    },
    {
        th: "Customer Phone",
        td: LeadNumber,
    },
    {
        th: "Source",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign?.source?.title ?? "",
    },
    {
        th: "Campaign",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign.title ?? "",
    },
    {
        th: "Primary Source",
        td: ({ source_of_campaigning }) => source_of_campaigning?.title ?? "",
    },
    {
        th: "Reference",
        td: ({ references }) => references?.title ?? "-",
    },
    {
        th: "Reference Details",
        td: ({ references_details }) =>
            isArray(references_details)
                ? references_details.map((x) => (
                      <div className="mb-2">
                          <div className="text-capitalize">{x.key_name}</div>
                          <div className="ml-3">- {x.value}</div>
                      </div>
                  ))
                : "-",
    },
    {
        th: "Present Address",
        td: ({ address }) => address || "-",
    },
    {
        th: "Area",
        td: ({ area }) => area || "-",
    },
    {
        th: "City",
        td: ({ city }) => city || "-",
    },
    {
        th: "Pincode",
        td: ({ pincode }) => pincode || "-",
    },
    {
        th: "Budget",
        td: ({ budget }) => budget?.title ?? "-",
    },
    {
        th: "Customer Interest",
        td: ({ customer_interest }) =>
            isArray(customer_interest)
                ? customer_interest.map((x) => x.title).join("\n, ")
                : "-",
    },
    {
        th: "Mode of Payment",
        td: ({ mode_of_payment }) => mode_of_payment || "-",
    },
    {
        th: "Purpose",
        td: ({ purpose }) => purpose || "-",
    },
    {
        th: "Intial Feedback",
        td: ({ initial_feedback }) => initial_feedback || "-",
    },
    {
        th: "Site Visit",
        td: ({ is_site_visit }) => (is_site_visit ? "Yes" : "No"),
    },
    {
        th: "Site Visit Date",
        td: ({ site_visit_date }) =>
            site_visit_date ? (
                <DateFormat date={site_visit_date} format="ll" />
            ) : (
                "-"
            ),
    },
    {
        th: "Transport Required",
        td: ({ is_transport_required }) => is_transport_required || "-",
    },
    {
        th: "Followup Date",
        td: ({ follow_up_date }) =>
            follow_up_date ? (
                <DateFormat date={follow_up_date} format="ll" />
            ) : (
                "-"
            ),
    },
    {
        th: "Followup Status",
        td: ({ follow_up_status }) => follow_up_status?.title ?? "-",
    },
    {
        th: "Created By",
        td: ({ created_by }) =>
            created_by && created_by.user
                ? `${created_by.user.first_name} ${created_by.user.last_name}`
                : "-",
    },
    {
        th: "Created On",
        td: ({ created_on }) => <DateFormat date={created_on} />,
    },
];

const Item = ({ heading = "", content = "-" }) => (
    <tr className="f-14">
        <th style={{ width: "200px" }}>{heading}</th>
        <td>{content}</td>
    </tr>
);

const LeadDetail = ({ data = {} }) => {
    return (
        <div>
            <table className="table table-bordered">
                <tbody>
                    {list.map((item, idx) => (
                        <Item
                            heading={isFunction(item.th) ? item.th() : item.th}
                            content={item.td ? item.td(data) : null}
                            key={idx}
                        />
                    ))}
                </tbody>
            </table>
            <h6 className="mb-3">Sales Details</h6>
            <table className="table table-bordered">
                <tbody>
                    <Item
                        heading="Sales Person"
                        content={
                            data.assigned_to && data.assigned_to.user
                                ? `${data.assigned_to.user.first_name} ${data.assigned_to.user.last_name}`
                                : "-"
                        }
                    />
                    <Item
                        heading="Assigned On"
                        content={
                            data.assigned_on ? (
                                <DateFormat date={data.assigned_on} />
                            ) : (
                                "-"
                            )
                        }
                    />
                    <Item
                        heading="Status"
                        content={<Status {...data.status} />}
                    />
                    {data.cold_reason && (
                        <Item
                            heading="Cold Reason"
                            content={data.cold_reason}
                        />
                    )}
                </tbody>
            </table>
            {data.booking_status && (
                <>
                    <h6 className="mb-3">Booking Status Details</h6>
                    <table className="table table-bordered">
                        <tbody>
                            <Item
                                heading="Booking on"
                                content={ <DateFormat date={data.booking_status.created_on} />}
                            />
                            <Item
                                heading="Booking Status"
                                content={
                                    data.booking_status.status
                                        ? "Approved"
                                        : data.booking_status.status === false
                                        ? "Rejected"
                                        : "Pending"
                                }
                            />
                            {data.booking_status.reject_reason && (
                                <Item
                                    heading="Booking Reject Reason"
                                    content={data.booking_status.reject_reason}
                                />
                            )}
                        </tbody>
                    </table>
                </>
            )}
            {data.dead_reason && (
                <>
                    <h6 className="mb-3">Dead Status Details</h6>
                    <table className="table table-bordered">
                        <tbody>
                            <Item
                                heading="Dead Reason"
                                content={data.dead_reason}
                            />
                            <Item
                                heading="Dead Reason Approval Status"
                                content={
                                    data.dead_approval
                                        ? "Approved"
                                        : data.dead_approval === false
                                        ? "Rejected"
                                        : "Pending"
                                }
                            />
                            {data.dead_approval_reason && (
                                <Item
                                    heading="Dead Reject Reason"
                                    content={data.dead_approval_reason}
                                />
                            )}
                        </tbody>
                    </table>
                </>
            )}
        </div>
    );
};

export default LeadDetail;
