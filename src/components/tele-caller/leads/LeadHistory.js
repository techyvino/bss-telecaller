import React, { useCallback, useEffect, useState } from "react";
import useToggle from "../../../hooks/useToggle";
import Modal from "../../modal/Modal";
import ModalHeader from "../../modal/ModalHeader";
import ModalBody from "../../modal/ModalBody";
import FieldCon from "../../form/FieldCon";
import Loader from "../../form/Loader";
import useDateFormat from "../../../hooks/useDateFormat";
import TextArea from "../../form/TextArea";
import useSubmit from "../../../hooks/http/useSubmit";
import SubmitBtn from "../../form/SubmitBtn";
import urls from "../../../apiService/urls";
import { useMasterValues } from "../../common/MasterData";
import SelectIp from "../../form/SelectIp";
import InfiniteList from "../../common/InfiniteList";
import useInfiniteList from "../../../hooks/http/useInfiniteList";
import { ArrowRight } from "react-feather";
import useSalesTeam from "./useSalesTeam";
import DateIp from "../../form/DateIp";
import { dateformat } from "../../form/formatAllData";

const HistoryItem = ({
    description,
    created_on,
    status_from = {},
    status_to = {},
    created_by = {},
}) => {

    const createdOn = useDateFormat(created_on, "lll");

    return (
        <div className="border-bottom mb-2 p-2 f-14">
            <div className="f-500 d-flex align-items-center mr-2">
                {status_from.title}{" "}
                <ArrowRight className="ml-2 mr-2" size={14} /> {status_to.title}
            </div>
            <div className="mb-1">{description}</div>
            <div className="d-flex justify-content-between flex-wrap text-muted">
                <small>
                    {created_by?.user
                        ? `${created_by.user.first_name}${created_by.user.last_name}`
                        : ""}
                </small>
                <small>{createdOn}</small>
            </div>
        </div>
    );
};

const AddHistory = ({ id, succFunc, projectId }) => {
    const [isLoading, { followup_status = [] }] = useMasterValues();
    const [status, setStatus] = useState(null);
    const [description, setValue] = useState("");
    const [assignTo, setAssignTo] = useState(null);
    const [dt, setDt] = useState(null);

    const statusId = status && status.value ? status.value : null;
    const salesTeam = useSalesTeam(projectId);

    const salesTeamList = Array.isArray(salesTeam.data)
        ? salesTeam.data.map((x) => ({
              label: x.user
                  ? `${x.user.first_name} ${x.user.last_name}`
                  : x.emp_id,
              value: x.id,
          }))
        : [];

    const [fetching, submit] = useSubmit({
        success: (data) => {
            succFunc(data);
            setStatus(null);
            setAssignTo(null);
            setValue("");
            setDt(null);
        },
    });

    const onSubmit = () => {
        const data = { description, status: statusId };
        Object.assign(data, { follow_up_date: dateformat(dt) });

        if (statusId === 9 && assignTo && assignTo.value) {
            Object.assign(data, { assigned_to_id: assignTo.value });
        }
        submit({
            url: urls.addFollowUp(id),
            method: "POST",
            data,
        });
    };

    const extraValue = statusId === 5 ? dt : status === 9 ? assignTo : 1;
    const minDate = new Date();

    return (
        <>
            <FieldCon>
                <SelectIp
                    isLoading={isLoading}
                    setValue={setStatus}
                    value={status}
                    options={followup_status.map((x) => ({
                        label: x.title,
                        value: x.id,
                    }))}
                    placeholder="Select activity"
                />
            </FieldCon>
            <FieldCon>
                <DateIp
                    setValue={setDt}
                    value={dt}
                    placeholder="Date"
                    minDate={minDate}
                />
            </FieldCon>
            {statusId === 9 && (
                <FieldCon>
                    <SelectIp
                        setValue={setAssignTo}
                        value={assignTo}
                        options={salesTeamList}
                        placeholder="Assign To"
                        isLoading={salesTeam.fetching}
                    />
                </FieldCon>
            )}
            <FieldCon>
                <TextArea
                    setValue={setValue}
                    value={description}
                    placeholder="Enter remarks"
                    style={{ minHeight: `50px` }}
                />
            </FieldCon>
            <SubmitBtn
                fetching={fetching}
                disabled={!(description && statusId && extraValue)}
                className="btn btn-theme btn-block"
                onClick={onSubmit}
            >
                Add
            </SubmitBtn>
        </>
    );
};

const HistoryModal = ({ id, projectId, hideAdd = false }) => {
    const [state, { setUrl, loadMore, addData }] = useInfiniteList();

    useEffect(() => {
        setUrl({ baseUrl: urls.leadHistory(id) });
    }, [setUrl, id]);

    const succFunc = useCallback(
        (data) => {
            addData(data);
        },
        [addData]
    );

    return (
        <ModalBody>
            <div className="row">
                {!hideAdd && (
                    <div className="col-md-6">
                        <AddHistory
                            projectId={projectId}
                            id={id}
                            succFunc={succFunc}
                        />
                    </div>
                )}
                <div
                    className={`col-md-${hideAdd ? 12 : 6}`}
                    style={{ height: "400px", overflowY: "scroll" }}
                >
                    <InfiniteList
                        loader={
                            <div className="flex-center">
                                <Loader size={26} />
                            </div>
                        }
                        {...state}
                        RenderItem={HistoryItem}
                        loadMore={loadMore}
                    />
                </div>
            </div>
        </ModalBody>
    );
};

const LeadHistory = ({ id, projectId, hideAdd = false }) => {
    const { toggle, onFalse, onTrue } = useToggle();

    return (
        <>
            <button onClick={onTrue} className="btn f-13 p-0 text-underline">
                View {hideAdd ? "" : "/ Add"}
            </button>
            {toggle && (
                <Modal
                    isOpen
                    close={onFalse}
                    contentStyle={{ maxWidth: hideAdd ? "375px" : "800px" }}
                >
                    <ModalHeader
                        title="Lead Activity"
                        desc="Add & view lead activities"
                        close={onFalse}
                    />
                    <HistoryModal
                        id={id}
                        hideAdd={hideAdd}
                        projectId={projectId}
                    />
                </Modal>
            )}
        </>
    );
};

export default LeadHistory;
