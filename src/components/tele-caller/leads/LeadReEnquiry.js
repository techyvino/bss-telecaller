import React, { useState } from 'react';
import urls from '../../../apiService/urls';
import useSubmit from '../../../hooks/http/useSubmit';
import useToggle from '../../../hooks/useToggle';
import FieldCon from '../../form/FieldCon';
import SubmitBtn from '../../form/SubmitBtn';
import TextArea from '../../form/TextArea';
import Modal from '../../modal/Modal';
import ModalBody from '../../modal/ModalBody';
import ModalHeader from '../../modal/ModalHeader';

const ResendEnquiry = ({ id }) => {
    const { toggle, onFalse, onTrue } = useToggle();
    const [description, setValue] = useState("");

    const [fetching, submit] = useSubmit({
        success: (data) => {
            console.log(data);
            setValue("");
            onFalse()
        },
    });

    const onSubmit = () => {
        const data = { description };

        submit({
            url: urls.resendEnquiry(id),
            method: "POST",
            data,
        });
    };


    return (
        <div>
            <button onClick={onTrue} className="btn btn-outline-primary btn-sm">
                Resend Enquiry
            </button>
            {toggle && (
                <Modal
                    isOpen
                    close={onFalse}
                    contentStyle={{ maxWidth: "375px" }}
                >
                    <ModalHeader
                        title="Resend Enquiry"
                        desc="Re-Enquiry Lead"
                        close={onFalse}
                    />
                    <ModalBody>
                        <h6>Re-Enquiry Feedback</h6>
                        <FieldCon>
                            <TextArea
                                setValue={setValue}
                                value={description}
                                placeholder="Enter remarks"
                                style={{ minHeight: `50px` }}
                            />
                        </FieldCon>
                        <SubmitBtn
                            fetching={fetching}
                            disabled={!(description)}
                            className="btn btn-theme btn-block"
                            onClick={onSubmit}
                        >
                            Add
                        </SubmitBtn>
                    </ModalBody>
                </Modal>
            )}
        </div>
    )
}

export default ResendEnquiry;
