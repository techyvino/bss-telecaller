import React from "react";
import Atags, { aTagTyps } from "../../common/Atags";

const LeadEmail = ({ email, secondary_email }) => (
    <>
        {email ? (
            <Atags className="text-body" type={aTagTyps.email} href={email}>
                {email}
            </Atags>
        ) : (
            "NA"
        )}
        {" / "}
        {secondary_email ? (
            <Atags
                className="text-body"
                type={aTagTyps.email}
                href={secondary_email}
            >
                {secondary_email}
            </Atags>
        ) : (
            "NA"
        )}
    </>
);

export default LeadEmail;
