import { useCallback, useState } from "react";
import urls from "../../../apiService/urls";
import useSubmit from "../../../hooks/http/useSubmit";
import { isNA } from "./leadValidations";

const useNumber = () => {
    const [numberValid, setNumberValid] = useState(null);
    const [fetching, submit] = useSubmit({
        success: () => {
            setNumberValid(null);
        },
        error: (data) => {
            setNumberValid(data);
        },
        hideToast: true,
    });

    const onNumberBlur = useCallback(
        (value) => {
            if (value && !numberValid && !isNA(value)) {
                submit({
                    url: urls.checkEnquiry,
                    data: { number: value },
                    method: "POST",
                });
            }
        },
        [submit, numberValid]
    );

    const clearValid = useCallback(() => {
        setNumberValid(null);
    }, []);

    return {
        onNumberBlur,
        isChecking: fetching,
        numberValid,
        clearValid,
        setNumberValid,
    };
};

export default useNumber;
