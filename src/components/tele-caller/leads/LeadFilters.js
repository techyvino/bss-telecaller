import React, { useEffect, useMemo, useRef } from "react";
import leadfilters, {
    primarySrcIp,
    campaignIp,
} from "../../../forms/filters/leadfilters";
import formatAllData from "../../form/formatAllData";
import { setValue } from "../../../hooks/form/formReducer";
import useFormReducer from "../../../hooks/form/useFormReducer";
import { useMasterValues } from "../../common/MasterData";
import Field from "../../form/Field";
import FieldArray from "../../form/FieldArray";
import { FormProvider } from "../../form/Form";
import { getFilters } from "../../../utils/keysToQuery";
import useQuery from "../../../hooks/useQuery";

const getCampaigns = (sources = [], source) => {
    return sources.find((x) => x.id === parseInt(source, 10))?.campaigns ?? [];
};

const getPrimary = (campaigns = [], campaign) =>
    campaigns.find((x) => x.id === parseInt(campaign, 10))?.primary_sources ?? [];


const LeadFilters = ({ onFilter }) => {
    const query = useQuery();
    const filterQuery = query.get("filters") || "";
    const initValues = getFilters(filterQuery)

    const master = useMasterValues();
    const form = useFormReducer({ values: initValues });
    const [{ values }, dispatch] = form;
    const { source, campaign } = values;
    const sourceRef = useRef(false);
    const campaignRef = useRef(false);

    const sources = master?.[1]?.sources ?? [];

    const allIds = useMemo(() => leadfilters(...master), [master]);
    const campaigns = useMemo(() => source ? getCampaigns(sources, source) : [], [sources, source]);
    const primarySources = useMemo(() => campaign ? getPrimary(campaigns, campaign) : [], [campaigns, campaign]);

    const campaignField = useMemo(() => {
        return campaignIp(campaigns);
    }, [campaigns]);

    const primarySrcField = useMemo(() => {
        return primarySrcIp(primarySources);
    }, [primarySources]);

    useEffect(() => {
        if (sourceRef.current) {
            dispatch(setValue("campaign", ""));
        }
        sourceRef.current = true;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [source]);

    useEffect(() => {
        if (campaignRef.current) {
            dispatch(setValue("source_of_campaigning_id", ""));
        }
        campaignRef.current = true;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [campaign]);

    const onSubmit = (e) => {
        e.preventDefault();
        const ids = [...allIds, campaignField, primarySrcField];
        const { date, ...rest } = formatAllData(ids, values);
        onFilter({
            ...(date && date.from
                ? { min_date: date && date.from ? date.from : "" }
                : {}),
            ...(date && date.to
                ? { max_date: date && date.to ? date.to : "" }
                : {}),
            ...rest,
        });
    };

    return (
        <FormProvider form={form}>
            <form noValidate onSubmit={onSubmit}>
                <div className="form-row">
                    <FieldArray allIds={allIds} />
                    <Field {...campaignField} />
                    <Field {...primarySrcField} />
                    <div className="col-md-3 form-group">
                        <button
                            type="submit"
                            className="btn btn-outline-black btn-block"
                        >
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </FormProvider>
    );
};

export default LeadFilters