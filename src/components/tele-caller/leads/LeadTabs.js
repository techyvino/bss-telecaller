import React from "react";
import { teleCallerUrls } from "../../../apiService/urls";
import DynamicTabList from "../../common/DynamicTabList";
import LeadTableWithFilter from "./LeadTableWithFilter";

const LeadTabs = ({ filters = {}, employee_id, isEdit }) => {
    return (
        <DynamicTabList
            url={`${teleCallerUrls.enquiryTabs}${
                employee_id ? `?employee_id=${employee_id}` : ""
            }`}
        >
            {({ id }) => (
                <LeadTableWithFilter
                    tab={id}
                    filters={filters}
                    employee_id={employee_id}
                    isEdit={isEdit}
                />
            )}
        </DynamicTabList>
    );
};

export default LeadTabs;
