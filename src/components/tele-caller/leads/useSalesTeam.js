import { useEffect } from "react";
import urls from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";

const useSalesTeam = (projectId) => {
    const [apiState, { setUrl }] = useFetchData();

    useEffect(() => {
        if (projectId) {
            setUrl(urls.getSalesTeam(projectId));
        }
    }, [projectId, setUrl]);

    return apiState;
};

export default useSalesTeam;
