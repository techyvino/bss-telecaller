import React, { useCallback, useEffect } from "react";
import urls from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import LeadFilters from "./LeadFilters";
import LeadsTable from "./LeadsTable";

const LeadTableWithFilter = ({
    tab = 1,
    filters = {},
    isEdit,
    employee_id,
}) => {
    const [state, { setUrl, setPage }] = usePagination();

    const loadLeads = useCallback(
        (f = {}) => {
            setUrl({
                baseUrl: urls.leads,
                filters: {
                    ...f,
                    ...filters,
                    tab,
                    ...(employee_id ? { employee_id } : {}),
                },
            });
        },
        [tab, setUrl, filters, employee_id]
    );

    useEffect(() => {
        loadLeads();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [tab]);

    return (
        <>
            <LeadFilters onFilter={loadLeads} />
            <LeadsTable
                onPageClick={setPage}
                state={state}
                isEdit={isEdit}
                employee_id={employee_id}
            />
        </>
    );
};

export default LeadTableWithFilter;
