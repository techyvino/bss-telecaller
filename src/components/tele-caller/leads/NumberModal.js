import React from "react";
import DateFormat from "../../common/DateFormat";
import Modal from "../../modal/Modal";
import ModalBody from "../../modal/ModalBody";
import ModalHeader from "../../modal/ModalHeader";
import Status from "./Status";
import { useHistory } from "react-router-dom";

const DetailItem = ({ title = "", desc = null }) => {
    return (
        <div className="col-md-4 mb-3">
            <small className="text-secondary">{title}</small>
            <div className="text-break">{desc}</div>
        </div>
    );
};

const EnquiryDetails = ({
    name,
    number,
    secondary_number,
    email,
    secondary_email,
    source_of_campaigning = {},
    status = {},
    created_by = {},
    created_on,
    assigned_to = {},
    assigned_on,
    primary_code,
    secondary_code,
    project,
}) => {
    return (
        <div className="row">
            <DetailItem title="Name" desc={name} />
            <DetailItem
                title="Phone Number"
                desc={`${number ? `${primary_code?.dial_code ?? ""}${number}` : "NA"
                    } / ${secondary_number
                        ? `${secondary_code?.dial_code ?? ""
                        }${secondary_number}`
                        : "NA"
                    }`}
            />
            <DetailItem
                title="Email"
                desc={`${email || "NA"} / ${secondary_email || "NA"}`}
            />
            <DetailItem
                title="Project"
                desc={
                    project
                        ? project.title

                        : ""
                }
            />
            <DetailItem
                title="Source"
                desc={
                    source_of_campaigning.campaign
                        ? source_of_campaigning.campaign.source
                            ? source_of_campaigning.campaign.source.title
                            : ""
                        : ""
                }
            />
            <DetailItem
                title="Campaign"
                desc={
                    source_of_campaigning.campaign
                        ? source_of_campaigning.campaign.title
                        : ""
                }
            />
            <DetailItem
                title="Primary Source"
                desc={source_of_campaigning.title}
            />
            <DetailItem title="Current Status" desc={<Status {...status} />} />
            {created_by && created_by.user && (
                <DetailItem
                    title="Telecaller"
                    desc={
                        created_by.user
                            ? `${created_by.user.first_name} ${created_by.user.last_name}`
                            : ""
                    }
                />
            )}
            {created_on && (
                <DetailItem
                    title="Created On"
                    desc={<DateFormat date={created_on} />}
                />
            )}
            {assigned_to && assigned_to.user && (
                <DetailItem
                    title="Assigned To"
                    desc={`${assigned_to.user.first_name} ${assigned_to.user.last_name}`}
                />
            )}
            {assigned_on && (
                <DetailItem
                    title="Assigned On"
                    desc={assigned_on !=="None" ? <DateFormat date={assigned_on} /> : "-"}
                />
            )}
        </div>
    );
};

const NumberModal = ({ enquiry = {}, close }) => {
    return (
        <Modal isOpen close={close} contentStyle={{ maxWidth: "800px" }}>
            <ModalHeader
                title={`Enquiry - ${enquiry.lead_no}`}
                desc={
                    <b className="text-danger">This enquiry already exists</b>
                }
                close={close}
            />
            <ModalBody>
                <EnquiryDetails {...enquiry} />
            </ModalBody>
        </Modal>
    );
};

const GoBack = () => {
    let history = useHistory();

    return (
        <button onClick={history.goBack} className="btn">
            Go Back
        </button>
    );
};

export const SuccessModal = ({ enquiry = {}, close, isEdit = false }) => {
    const desc = `Enquiry has been successfully ${isEdit ? "edited" : "added"
        }.${enquiry?.status?.id === 1
            ? ` Lead has been transfered to ${enquiry?.assigned_to?.user?.first_name ?? ""
            } ${enquiry?.assigned_to?.user?.last_name ?? ""}`
            : ""
        }`;

    return (
        <Modal isOpen close={close} contentStyle={{ maxWidth: "800px" }}>
            <ModalHeader
                title={`Enquiry - ${enquiry.lead_no}`}
                desc={<b className="text-success">{desc}</b>}
                close={close}
            />
            <ModalBody>
                <EnquiryDetails {...enquiry} />
            </ModalBody>
            {isEdit && (
                <div className="d-flex justify-content-end p-2">
                    <GoBack />
                    <button onClick={close} className="btn btn-theme">
                        Continue Editing
                    </button>
                </div>
            )}
        </Modal>
    );
};

export default NumberModal;
