import React from "react";
import AddLeadForm from "./AddLeadForm";
import { getInitValues, getPortalLeadInitValues } from "./addleads";

const EditLeadForm = ({ data = {}, portal_id = null, leadId = null }) => {
    const values = portal_id ? getPortalLeadInitValues(data) : getInitValues(data, leadId);
    
    if (portal_id) {
        return <AddLeadForm initValues={values} portal_id={portal_id} />;
    }
    else if (data.id) {
        return <AddLeadForm initValues={values} leadId={leadId} editId={data.id} />;
    }
    return null;
};

export default EditLeadForm;
