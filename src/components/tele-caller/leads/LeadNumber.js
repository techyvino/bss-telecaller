import React from "react";
import Atags, { aTagTyps } from "../../common/Atags";

const LeadNumber = ({
    number,
    secondary_number,
    secondary_code,
    primary_code,
}) => (
    <>
        {number ? (
            <Atags
                className="text-body"
                type={aTagTyps.tel}
                href={`${primary_code?.dial_code ?? ""}${number}`}
            >
                {`${primary_code?.dial_code ?? ""}${number}`}
            </Atags>
        ) : (
            "NA"
        )}
        {" / "}
        {secondary_number ? (
            <Atags
                className="text-body"
                type={aTagTyps.tel}
                href={`${secondary_code?.dial_code ?? ""}${secondary_number}`}
            >
                {`${secondary_code?.dial_code ?? ""}${secondary_number}`}
            </Atags>
        ) : (
            "NA"
        )}
    </>
);

export default LeadNumber;
