import React from "react";

const badgeCls = (id) => {
    switch (parseFloat(id)) {
        case 1:
            return "badge-danger";
        case 2:
            return "badge-warning";
        case 3:
            return "badge-secondary";
        default:
            return "";
    }
};

const Status = ({ id, title }) => {
    return (
        <span className={`badge text-uppercase ${badgeCls(id)}`}>{title}</span>
    );
};

export default Status;
