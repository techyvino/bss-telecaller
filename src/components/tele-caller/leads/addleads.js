import { isArray, isPlainObject, toBoolean } from "../../../utils";
import { deadReasons, paymentModes, salutations } from "../../common/master";
import formTypes from "../../form/formTypes";

export const formKeys = {
    project_id: "project_id",
    enquiry_date: "enquiry_date",
    salutation: "salutation",
    name: "name",
    number_code: "number_code",
    number: "number",
    secondary_number_code: "secondary_number_code",
    secondary_number: "secondary_number",
    email: "email",
    secondary_email: "secondary_email",
    source: "source",
    campaign: "campaign",
    source_of_campaigning_id: "source_of_campaigning_id",
    references_id: "references_id",
    employee_name: "employee_name",
    employee_department: "employee_department",
    employee_designation: "employee_designation",
    management_name: "management_name",
    customer_name: "customer_name",
    customer_flat_number: "customer_flat_number",
    customer_project: "customer_project",
    address: "address",
    area: "area",
    city: "city",
    pincode: "pincode",
    customer_interest_id: "customer_interest_id",
    budget_id: "budget_id",
    interested_location_id: "interested_location_id",
    mode_of_payment: "mode_of_payment",
    purpose: "purpose",
    initial_feedback: "initial_feedback",
    status_id: "status_id",
    assigned_to_id: "assigned_to_id",
    follow_up_date: "follow_up_date",
    follow_up_status_id: "follow_up_status_id",
    followup_description: "followup_description",
    is_site_visit: "is_site_visit",
    site_visit_date: "site_visit_date",
    is_transport_required: "is_transport_required",
    dead_reason: "dead_reason",
    cold_reason: "cold_reason",
};

// Transport Required
const mapOpt = (opt) => ({ label: opt.title, value: opt.id });
const mapOpts = (options = []) => options.map(mapOpt);

//country
const getDialCode = (opt) => ({ label: opt.dial_string, value: opt.id });
const getDialCodes = (options = []) => options.map(getDialCode);

export const form1 = (project = [], isEdit = false) => [
    {
        name: formKeys.project_id,
        type: formTypes.select,
        label: "Project",
        inputProps: {
            options: mapOpts(project),
            placeholder: "Select an option",
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: formKeys.enquiry_date,
        type: formTypes.date,
        label: "Enquiry Date",
        inputProps: {
            placeholder: "Select a date",
            showMonthDropdown: true,
            showYearDropdown: true,
            dropdownMode: "select",
            maxDate: new Date(),
            disabled: isEdit,
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: formKeys.salutation,
        type: formTypes.select,
        label: "Salutation",
        inputProps: {
            placeholder: "Select Saluation",
            options: salutations,
        },
        conClass: "col-md-3",
        required: true,
    },
    {
        name: formKeys.name,
        type: formTypes.text,
        label: "Customer Name",
        inputProps: {
            placeholder: "Enter customer name",
        },
        conClass: "col-md-7",
        required: true,
    },
];

export const countrCodeIps = (country = [], isEdit, isSecEdit) => {
    const options = getDialCodes(country);
    return [
        {
            name: formKeys.number_code,
            type: formTypes.select,
            label: "Primary Country Code",
            inputProps: {
                placeholder: "Select an option",
                disabled: isEdit,
                options,
            },
            conClass: "col-md-3",
            required: true,
        },
        {
            name: formKeys.secondary_number_code,
            type: formTypes.select,
            label: "Secondary Country Code",
            inputProps: {
                placeholder: "Select an option",
                disabled: isSecEdit,
                options,
            },
            conClass: "col-md-3",
            required: false,
        },
    ];
};

export const sourceIp = (sources = [], isEdit) => {
    return {
        name: formKeys.source,
        type: formTypes.select,
        label: "Source",
        inputProps: {
            options: mapOpts(sources),
            placeholder: "Select an option",
            isDisabled: toBoolean(isEdit),
        },
        conClass: "col-md-4",
        required: true,
    };
};

export const campaignIp = (campaigns = [], isEdit = false) => {
    return {
        name: formKeys.campaign,
        type: formTypes.select,
        label: "Campaign",
        inputProps: {
            options: mapOpts(campaigns),
            placeholder: "Select an option",
            isDisabled: toBoolean(isEdit),
        },
        conClass: "col-md-4",
        required: true,
    };
};

export const primarySrcIp = (primary_sources = [], isEdit = false) => {
    return {
        name: formKeys.source_of_campaigning_id,
        type: formTypes.select,
        label: "Primary Source Campaigning",
        inputProps: {
            options: mapOpts(primary_sources),
            placeholder: "Select an option",
            isDisabled: toBoolean(isEdit),
        },
        conClass: "col-md-4",
        required: true,
    };
};

export const referenceForm = (sourceOfCamp) => {
    switch (sourceOfCamp) {
        case 96:
            return [
                {
                    name: formKeys.employee_name,
                    type: formTypes.text,
                    label: "Name",
                    inputProps: {
                        placeholder: "Enter Employee Name",
                    },
                    conClass: "col-md-4",
                    required: false,
                },
                {
                    name: formKeys.employee_department,
                    type: formTypes.text,
                    label: "Department",
                    inputProps: {
                        placeholder: "Enter Employee Department",
                    },
                    conClass: "col-md-4",
                    required: false,
                },
                {
                    name: formKeys.employee_designation,
                    type: formTypes.text,
                    label: "Designation",
                    inputProps: {
                        placeholder: "Enter Employee Designation",
                    },
                    conClass: "col-md-4",
                    required: false,
                },
            ];
        case 97:
            return [
                {
                    name: formKeys.management_name,
                    type: formTypes.select,
                    label: "Management Name",
                    inputProps: {
                        placeholder: "Select Management Name",
                        options: [
                            {
                                value: "MD SIR",
                                label: "MD SIR",
                            },
                            {
                                value: "AY SIR",
                                label: "AY SIR",
                            },
                            {
                                value: "BY SIR",
                                label: "BY SIR",
                            },
                        ],
                    },
                    conClass: "col-md-4",
                    required: false,
                },
            ];
        case 95:
            return [
                {
                    name: formKeys.customer_name,
                    type: formTypes.text,
                    label: "Name",
                    inputProps: {
                        placeholder: "Enter Customer Name",
                    },
                    conClass: "col-md-4",
                    required: false,
                },
                {
                    name: formKeys.customer_flat_number,
                    type: formTypes.text,
                    label: "Flat Number",
                    inputProps: {
                        placeholder: "Enter Customer Flat Number",
                    },
                    conClass: "col-md-4",
                    required: false,
                },
                {
                    name: formKeys.customer_project,
                    type: formTypes.text,
                    label: "Project",
                    inputProps: {
                        placeholder: "Enter project",
                    },
                    conClass: "col-md-4",
                    required: false,
                },
            ];
        default:
            return [];
    }
};

export const form2 = ({
    flat_type = [],
    locations = [],
    customer_budget = [],
    lead_status = [],
}) => [
    {
        name: formKeys.address,
        type: formTypes.textarea,
        label: "Present Address",
        inputProps: {
            placeholder: "Enter present address",
        },
        conClass: "col-md-12",
        required: false,
    },
    {
        name: formKeys.area,
        type: formTypes.text,
        label: "Area",
        inputProps: {
            placeholder: "Enter area",
        },
        conClass: "col-md-4",
        required: false,
    },
    {
        name: formKeys.city,
        type: formTypes.text,
        label: "City",
        inputProps: {
            placeholder: "Enter city",
        },
        conClass: "col-md-4",
        required: false,
    },
    {
        name: formKeys.pincode,
        type: formTypes.number,
        label: "Pincode",
        inputProps: {
            placeholder: "Enter pincode",
        },
        conClass: "col-md-4",
        required: false,
    },
    {
        name: formKeys.customer_interest_id,
        type: formTypes.checkgroup,
        label: "Customer Interest",
        inputProps: {
            options: flat_type.map((x) => ({ title: x.title, value: x.id })),
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: formKeys.budget_id,
        type: formTypes.select,
        label: "Budget",
        inputProps: {
            options: mapOpts(customer_budget),
            placeholder: "Select an option",
        },
        conClass: "col-md-6",
        required: false,
    },
    {
        name: formKeys.interested_location_id,
        type: formTypes.select,
        label: "Interested Location",
        inputProps: {
            options: mapOpts(locations),
            placeholder: "Select an option",
        },
        conClass: "col-md-6",
        required: false,
    },
    {
        name: formKeys.mode_of_payment,
        type: formTypes.radiogroup,
        label: "Mode Of Payment",
        inputProps: {
            options: paymentModes,
            placeholder: "Select an option",
        },
        conClass: "col-md-4",
        required: false,
    },
    {
        name: formKeys.purpose,
        type: formTypes.radiogroup,
        label: "Purpose",
        inputProps: {
            options: [
                {
                    title: "Investment",
                    value: "Investment",
                },
                {
                    title: "Own Dwelling",
                    value: "Own Dwelling",
                },
            ],
            placeholder: "Select an option",
        },
        conClass: "col-md-4",
        required: false,
    },
    {
        name: formKeys.initial_feedback,
        type: formTypes.textarea,
        label: "Initial Feedback",
        inputProps: {
            placeholder: "Enter initial feedback",
        },
        conClass: "col-md-12",
        required: false,
    },
    {
        name: formKeys.status_id,
        type: formTypes.radiogroup,
        label: "Status",
        inputProps: {
            options: lead_status.map((x) => ({ title: x.title, value: x.id })),
        },
        conClass: "col-md-12",
        required: true,
    },
];

const mapAssignTo = (x) => ({
    label: x.user ? `${x.user.first_name} ${x.user.last_name}` : x.emp_id,
    value: x.id,
});

export const statusForm = (status, followup_status = [], team) => {
    const getFields = () => {
        switch (status) {
            case 1:
                return [
                    {
                        name: formKeys.assigned_to_id,
                        type: formTypes.select,
                        label: "Assign Lead",
                        inputProps: {
                            options: isArray(team?.data)
                                ? team.data.map(mapAssignTo)
                                : [],
                            placeholder: "Select an option",
                            isloading: team?.fetching,
                        },
                        conClass: "col-md-4",
                        required: true,
                    },
                ];
            case 2:
                return [
                    {
                        name: formKeys.follow_up_status_id,
                        type: formTypes.select,
                        label: "Follow Up Status",
                        inputProps: {
                            placeholder: "Select an option",
                            options: mapOpts(followup_status),
                        },
                        conClass: "col-md-4",
                        required: true,
                    },
                ];
            case 3:
                return [
                    {
                        name: formKeys.cold_reason,
                        type: formTypes.select,
                        label: "Reason",
                        inputProps: {
                            placeholder: "Select an option",
                            options: deadReasons,
                        },
                        conClass: "col-md-4",
                        required: true,
                    },
                ];
            case 11:
            case 12:
                return [
                    {
                        name: formKeys.dead_reason,
                        type: formTypes.select,
                        label: "Reason",
                        inputProps: {
                            placeholder: "Select an option",
                            options: deadReasons,
                        },
                        conClass: "col-md-4",
                        required: true,
                    },
                ];
            default:
                return [];
        }
    };

    if (status) {
        return [
            {
                name: formKeys.follow_up_date,
                type: formTypes.date,
                label: "Follow Up Date",
                inputProps: {
                    placeholder: "Select a date",
                    minDate: new Date(),
                },
                conClass: "col-md-4",
                required: true,
            },
            ...getFields(),
        ];
    }
    return [];
};

export const form4 = (is_site_visit) => [
    {
        name: formKeys.is_site_visit,
        type: formTypes.radiogroup,
        label: "Site Visit",
        inputProps: {
            options: [
                {
                    title: "Yes",
                    value: true,
                },
                {
                    title: "No",
                    value: false,
                },
            ],
        },
        conClass: "col-md-12",
        required: true,
    },
    ...(is_site_visit
        ? [
              {
                  name: formKeys.site_visit_date,
                  type: formTypes.date,
                  label: "Site Visit Date",
                  inputProps: {
                      placeholder: "Select a date",
                  },
                  conClass: "col-md-4",
                  required: true,
              },
          ]
        : []),
    {
        name: formKeys.is_transport_required,
        type: formTypes.radiogroup,
        label: "Transport Required",
        inputProps: {
            options: [
                {
                    title: "Own",
                    value: "Own",
                },
                {
                    title: "Cab",
                    value: "Cab",
                },
            ],
        },
        conClass: "col-md-12",
        required: false,
    },
];

export const init = {
    [formKeys.enquiry_date]: new Date(),
    [formKeys.is_site_visit]: false,
    [formKeys.number_code]: { value: 1, label: "+91 (IND)" },
    [formKeys.secondary_number_code]: { value: 1, label: "+91 (IND)" },
    [formKeys.salutation]: salutations[0],
};

const getSelectVal = (val) => (isPlainObject(val) ? mapOpt(val) : null);

export const getInitValues = (data, leadId) => {
    const sourceOfCamp = data.source_of_campaigning;
    const campaign =
        sourceOfCamp && sourceOfCamp.campaign ? sourceOfCamp.campaign : null;
    const source = campaign && campaign.source ? campaign.source : null;

    let values = {};

    if (isArray(data?.references_details)) {
        data.references_details.forEach((x) => {
            const key = x.key_name.split(" ").join("_");
            if (key === formKeys.management_name) {
                values[key] = { label: x.value, value: x.value };
            } else {
                values[key] = x.value;
            }
        });
    }

    Object.assign(values, {
        [formKeys.project_id]: data.project ? mapOpt(data.project) : null,
        [formKeys.enquiry_date]: data.enquiry_date && !leadId
            ? new Date(data.enquiry_date)
            : leadId? new Date() : undefined,
        [formKeys.salutation]: data.salutation
            ? { label: data.salutation, value: data.salutation }
            : null,
        [formKeys.name]: data.name || "",
        [formKeys.number_code]: data.primary_code
            ? getDialCode(data.primary_code)
            : null,
        [formKeys.number]: data.number || "NA",
        [formKeys.secondary_number_code]: data.secondary_code
            ? getDialCode(data.secondary_code)
            : null,
        [formKeys.secondary_number]: data.secondary_number || "NA",
        [formKeys.email]: data.email || "NA",
        [formKeys.secondary_email]: data.secondary_email || "NA",
        [formKeys.source]: getSelectVal(source),
        [formKeys.campaign]: getSelectVal(campaign),
        [formKeys.source_of_campaigning_id]: getSelectVal(sourceOfCamp),
        [formKeys.references_id]: data?.references?.id ?? undefined,
        [formKeys.address]: data.address || "",
        [formKeys.area]: data.area || "",
        [formKeys.city]: data.city || "",
        [formKeys.pincode]: data.pincode || "",
        [formKeys.customer_interest_id]: isArray(data.customer_interest)
            ? data.customer_interest.map((x) => x.id)
            : [],
        [formKeys.budget_id]: getSelectVal(data.budget),
        [formKeys.interested_location_id]: getSelectVal(
            data.interested_location
        ),
        [formKeys.mode_of_payment]: data.mode_of_payment || "",
        [formKeys.purpose]: data.purpose || "",
        [formKeys.initial_feedback]: "",
        [formKeys.status_id]: data?.status?.id ?? undefined,
        //
        [formKeys.assigned_to_id]: isPlainObject(data.assigned_to)
            ? mapAssignTo(data.assigned_to)
            : null,
        [formKeys.dead_reason]: getSelectVal(data.dead_reason),
        [formKeys.cold_reason]: getSelectVal(data.cold_reason),
        //
        [formKeys.follow_up_date]: data.follow_up_date
            ? new Date(data.follow_up_date)
            : undefined,
        [formKeys.follow_up_status_id]: getSelectVal(data.follow_up_status),
        [formKeys.followup_description]: "",
        //
        [formKeys.is_site_visit]: toBoolean(data.is_site_visit),
        [formKeys.site_visit_date]: data.site_visit_date
            ? new Date(data.site_visit_date)
            : undefined,
        //
        [formKeys.is_transport_required]: data.is_transport_required,
        [formKeys.initial_feedback]: data.initial_feedback || "",
    });
    return values;
};



export const getPortalLeadInitValues = (data) => {
    const sourceOfCamp = data.source_of_campaigning;
    const campaign =
        sourceOfCamp && sourceOfCamp.campaign ? sourceOfCamp.campaign : null;
    const source = campaign && campaign.source ? campaign.source : null;

    let values = {};

    if (isArray(data?.references_details)) {
        data.references_details.forEach((x) => {
            const key = x.key_name.split(" ").join("_");
            if (key === formKeys.management_name) {
                values[key] = { label: x.value, value: x.value };
            } else {
                values[key] = x.value;
            }
        });
    }

    Object.assign(values, {
        [formKeys.project_id]: data.project_id ? mapOpt(data.project_id) : null,
        [formKeys.enquiry_date]: data.created_on
            ? new Date(data.created_on)
            : undefined,
        [formKeys.salutation]: data.salutation
            ? { label: data.salutation, value: data.salutation }
            : null,
        [formKeys.name]: data.name || "",
        [formKeys.number_code]: data.primary_code
            ? getDialCode(data.primary_code)
            : null,
        [formKeys.number]: data.phone_number || "NA",
        [formKeys.secondary_number_code]: data.secondary_code
            ? getDialCode(data.secondary_code)
            : null,
        [formKeys.secondary_number]: data.secondary_number || "NA",
        [formKeys.email]: data.email || "NA",
        [formKeys.secondary_email]: data.secondary_email || "NA",
        [formKeys.source]: getSelectVal(source),
        [formKeys.campaign]: getSelectVal(campaign),
        [formKeys.source_of_campaigning_id]: getSelectVal(sourceOfCamp),
        [formKeys.references_id]: data?.references?.id ?? undefined,
        [formKeys.address]: data.address || "",
        [formKeys.area]: data.area || "",
        [formKeys.city]: data.city || "",
        [formKeys.pincode]: data.pincode || "",
        [formKeys.customer_interest_id]: isArray(data.customer_interest)
            ? data.customer_interest.map((x) => x.id)
            : [],
        [formKeys.budget_id]: getSelectVal(data.budget),
        [formKeys.interested_location_id]: getSelectVal(
            data.interested_location
        ),
        [formKeys.mode_of_payment]: data.mode_of_payment || "",
        [formKeys.purpose]: data.purpose || "",
        [formKeys.initial_feedback]: "",
        [formKeys.status_id]: data?.status?.id ?? undefined,
        //
        [formKeys.assigned_to_id]: isPlainObject(data.assigned_to)
            ? mapAssignTo(data.assigned_to)
            : null,
        [formKeys.dead_reason]: getSelectVal(data.dead_reason),
        [formKeys.cold_reason]: getSelectVal(data.cold_reason),
        //
        [formKeys.follow_up_date]: data.follow_up_date
            ? new Date(data.follow_up_date)
            : undefined,
        [formKeys.follow_up_status_id]: getSelectVal(data.follow_up_status),
        [formKeys.followup_description]: "",
        //
        [formKeys.is_site_visit]: toBoolean(data.is_site_visit),
        [formKeys.site_visit_date]: data.site_visit_date
            ? new Date(data.site_visit_date)
            : undefined,
        //
        [formKeys.is_transport_required]: data.is_transport_required,
        [formKeys.initial_feedback]: data.initial_feedback || "",
    });
    return values;
};
