import React, {
    useCallback,
    useEffect,
    useMemo,
    useRef,
    useState,
} from "react";
import { FormProvider } from "../../form/Form";
import FieldArray from "../../form/FieldArray";
import {
    formKeys,
    campaignIp,
    form1,
    form2,
    form4,
    statusForm,
    primarySrcIp,
    referenceForm,
    sourceIp,
    countrCodeIps,
    init,
} from "./addleads";
import { useMasterValues } from "../../common/MasterData";
import Field from "../../form/Field";
import {
    resetForm,
    setErrors,
    setValue,
    setValues,
} from "../../../hooks/form/formReducer";
import useFormReducer from "../../../hooks/form/useFormReducer";
import SubmitBtn from "../../form/SubmitBtn";
import validateAll from "../../form/validateAll";
import formatAllData, { getValue } from "../../form/formatAllData";
import useScrollToError from "../../../hooks/form/useScrollToError";
import useSubmit from "../../../hooks/http/useSubmit";
import urls from "../../../apiService/urls";
import useSalesTeam from "./useSalesTeam";
import DynamicContent from "../../common/DynamicContent";
import FieldCon from "../../form/FieldCon";
import Input from "../../form/Input";
import useField from "../../../hooks/form/useField";
import NumberModal, { SuccessModal } from "./NumberModal";
import { getCampaigns, getPrimary } from "../../common/master";
import useNumber from "./useNumber";
import { isEmailorNA, isNA, isPhoneorNA } from "./leadValidations";

const CheckLeadIp = ({
    disabled = false,
    placeholder = "",
    onBlur = null,
    label = "",
    required = false,
    conClass = "",
    isChecking = false,
    name,
}) => {
    const { value, err, setValue } = useField(name);

    return (
        <FieldCon
            conClass={conClass}
            required={required}
            label={label}
            infoText={isChecking ? "Checking duplication..." : ""}
            err={err}
        >
            <Input
                placeholder={placeholder}
                value={value}
                setValue={(val) => setValue(val)}
                onBlur={() => onBlur(value)}
                disabled={disabled}
            />
        </FieldCon>
    );
};

const yesIds = [8, 9, 10, 11];
const refIds = [95, 96, 97];

const AddLead = ({ masterData = {}, initValues, editId, portal_id, leadId }) => {
    const campaingRef = useRef(false);
    const sourceRef = useRef(false);
    const [successData, setSuccess] = useState(null);

    const clearSuccess = useCallback(() => {
        setSuccess(null);
    }, []);

    const isEdit = leadId ? false : !!editId;

    const {
        project = [],
        sources = [],
        followup_status = [],
        country,
        ...master
    } = masterData;

    const form = useFormReducer({ values: initValues });
    const [{ values }, dispatch] = form;
    const { source, campaign, status_id, is_site_visit, project_id } = values;
    const {
        isChecking,
        numberValid,
        onNumberBlur,
        clearValid,
        setNumberValid,
    } = useNumber();

    const [fetching, submit] = useSubmit({
        success: (data) => {
            if (!isEdit) {
                dispatch(resetForm());
                dispatch(setValues(init));
            }
            setSuccess(data);
        },
        error: (data) => {
            if (data && data.id) {
                setNumberValid(data);
            }
        },
    });

    const numberEdit =
        isEdit &&
        initValues[formKeys.number] &&
        !isNA(initValues[formKeys.number]);
    const secEmailEdit =
        isEdit &&
        initValues[formKeys.secondary_email] &&
        !isNA(initValues[formKeys.secondary_email]);
    const secNumEdit =
        isEdit &&
        initValues[formKeys.secondary_number] &&
        !isNA(initValues[formKeys.secondary_number]);
    const emailEdit =
        isEdit &&
        initValues[formKeys.email] &&
        !isNA(initValues[formKeys.email]);

    const [priCodeIp, secCodeIp] = countrCodeIps(
        country,
        numberEdit,
        secNumEdit
    );

    const allIds1 = useMemo(() => {
        return form1(project, isEdit);
    }, [project, isEdit]);

    const sourceField = useMemo(() => {
        return sourceIp(sources, isEdit);
    }, [sources, isEdit]);

    const campaigns = useMemo(() => {
        return source && source.value
            ? getCampaigns(sources, source.value)
            : [];
    }, [source, sources]);

    const campaignField = useMemo(() => {
        return campaignIp(campaigns, isEdit);
    }, [campaigns, isEdit]);

    const primarySrcField = useMemo(() => {
        const primary_sources =
            campaign && campaign.value
                ? getPrimary(campaigns, campaign.value)
                : [];
        return primarySrcIp(primary_sources, isEdit);
    }, [campaign, campaigns, isEdit]);

    useEffect(() => {
        if (campaingRef.current) {
            dispatch(setValue(formKeys.campaign, null));
        }
        if (source && source.value) {
            campaingRef.current = true;
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [source]);

    useEffect(() => {
        if (sourceRef.current) {
            dispatch(setValue(formKeys.source_of_campaigning_id, null));
        }
        if (campaign && campaign.value) {
            sourceRef.current = true;
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [campaign]);

    const sourceOfCamp = values?.[formKeys.source_of_campaigning_id]?.value;

    const referenceIds = useMemo(() => {
        return referenceForm(sourceOfCamp);
    }, [sourceOfCamp]);

    const allIds2 = useMemo(() => form2(master), [master]);
    const projectId = project_id && project_id.value ? project_id.value : null;
    const salesTeam = useSalesTeam(projectId);

    const statusIds = useMemo(() => {
        return statusForm(status_id, followup_status, salesTeam);
    }, [status_id, followup_status, salesTeam]);

    const allIds4 = useMemo(() => form4(is_site_visit), [is_site_visit]);

    const scroll = useScrollToError();

    const onSubmit = (e) => {
        e.preventDefault();

        const allIds = [
            ...allIds1,
            sourceField,
            campaignField,
            primarySrcField,
            ...allIds2,
            ...allIds4,
            ...statusIds,
        ];

        const validateIds = [...allIds, ...referenceIds];
        const errors = validateAll(validateIds, values);

        const numberCode = values[formKeys.number_code];
        const number = values[formKeys.number];
        const secNumberCode = values[formKeys.secondary_number_code];
        const secNumber = values[formKeys.secondary_number];

        const numberErr = isPhoneorNA(number, true);
        const secNumberErr = isPhoneorNA(secNumber, false);

        if (!numberCode?.value) {
            Object.assign(errors, {
                [formKeys.number_code]: "This field is required",
            });
        }
        if (numberErr) {
            Object.assign(errors, { [formKeys.number]: numberErr });
        }
        if (secNumberErr) {
            Object.assign(errors, { [formKeys.secondary_number]: numberErr });
        }
        if (!numberErr && !secNumberErr && !isNA(secNumber)) {
            if (number === secNumber) {
                Object.assign(errors, {
                    [formKeys.secondary_number]:
                        "Cannot be same as primary number",
                });
            }
        }

        const email = values[formKeys.email];
        const secEmail = values[formKeys.secondary_email];

        const emailErr = isEmailorNA(email, true);
        const secEmailErr = isEmailorNA(secEmail);
        if (emailErr) {
            Object.assign(errors, { [formKeys.email]: emailErr });
        }
        if (secEmailErr) {
            Object.assign(errors, { [formKeys.secondary_email]: secEmailErr });
        }
        if (!emailErr && !secEmailErr && !isNA(secEmail)) {
            if (email === secEmail) {
                Object.assign(errors, {
                    [formKeys.secondary_email]:
                        "cannot be same as primary email",
                });
            }
        }
        if (Object.keys(errors).length > 0) {
            dispatch(setErrors(errors));
            scroll();
            return false;
        }
        const data = formatAllData(allIds, values);

        if (refIds.includes(sourceOfCamp)) {
            const refernceVals = referenceIds.reduce((acc, cur) => {
                const val = getValue(cur, values[cur.name]);
                if (val) {
                    return [
                        ...acc,
                        { key_name: cur.name.split("_").join(" "), value: val },
                    ];
                }
                return acc;
            }, []);

            Object.assign(data, {
                references_details: refernceVals,
            });
        }

        Object.assign(data, {
            primary_code_id: numberCode.value,
        });
        if (number && !isNA(number)) {
            Object.assign(data, {
                number: number,
            });
        }
        if (email && !isNA(email)) {
            Object.assign(data, {
                email: email,
            });
        }
        if (secNumberCode?.value) {
            Object.assign(data, {
                secondary_code_id: secNumberCode.value,
            });
        }
        if (secNumber && !isNA(secNumber)) {
            Object.assign(data, {
                secondary_number: secNumber,
            });
        }
        if (secEmail && !isNA(secEmail)) {
            Object.assign(data, {
                secondary_email: secEmail,
            });
        }
        if (portal_id) {
            Object.assign(data, {
                portal_id: portal_id,
            });
        }
        if (leadId) {
            Object.assign(data, {
                ex_lead_id: leadId,
            });
        }
        submit({
            url: urls.leads + (leadId ? "" : editId ? `${editId}/` : ""),
            method: leadId ? "POST" : editId ? "PUT" : "POST",
            data,
        });
    };

    //status change
    const siteVisitDate = values[formKeys.site_visit_date];
    useEffect(() => {
        const status = parseInt(status_id, 10);
        if (yesIds.includes(status)) {
            dispatch(setValue(formKeys.is_site_visit, true));
        } else if (!siteVisitDate) {
            dispatch(setValue(formKeys.is_site_visit, false));
        }
    }, [status_id, siteVisitDate, dispatch]);

    return (
        <FormProvider form={form}>
            <form autoComplete="off" noValidate onSubmit={onSubmit}>
                <div className="row">
                    <FieldArray allIds={allIds1} />
                </div>
                <div className="row">
                    <Field {...priCodeIp} />
                    <CheckLeadIp
                        name={formKeys.number}
                        conClass="col-md-3"
                        required
                        label="Primary Phone Number"
                        placeholder="Enter primary phone number"
                        onBlur={onNumberBlur}
                        disabled={numberEdit}
                        isChecking={isChecking}
                        isNumber
                    />
                    <Field {...secCodeIp} />
                    <CheckLeadIp
                        name={formKeys.secondary_number}
                        conClass="col-md-3"
                        label="Secondary Phone Number"
                        placeholder="Enter secondary phone number"
                        onBlur={onNumberBlur}
                        disabled={secNumEdit}
                        isChecking={isChecking}
                        isNumber
                    />
                    <CheckLeadIp
                        name={formKeys.email}
                        conClass="col-md-6"
                        required
                        label="Primary Email"
                        placeholder="Enter primary email"
                        onBlur={onNumberBlur}
                        disabled={emailEdit}
                        isChecking={isChecking}
                    />
                    <CheckLeadIp
                        name={formKeys.secondary_email}
                        conClass="col-md-6"
                        label="Secondary Email"
                        placeholder="Enter secondary email"
                        onBlur={onNumberBlur}
                        disabled={secEmailEdit}
                        isChecking={isChecking}
                    />
                </div>
                <div className="row">
                    <Field {...sourceField} />
                    <Field {...campaignField} />
                    <Field {...primarySrcField} />
                </div>
                <div className="row">
                    <FieldArray allIds={referenceIds} />
                </div>
                <div className="row">
                    <FieldArray allIds={allIds2} />
                </div>
                <div className="row">
                    <FieldArray allIds={statusIds} />
                </div>
                <div className="row">
                    <FieldArray allIds={allIds4} />
                </div>
                <SubmitBtn
                    disabled={isChecking}
                    fetching={fetching}
                    className="btn btn-theme btn-block"
                >
                    Submit
                </SubmitBtn>
            </form>
            {successData && successData.id && (
                <SuccessModal
                    enquiry={successData}
                    isEdit={isEdit}
                    close={clearSuccess}
                />
            )}
            {numberValid && numberValid.id && (
                <NumberModal enquiry={numberValid} close={clearValid} />
            )}
        </FormProvider>
    );
};

const AddLeadForm = ({ leadId, editId, portal_id, initValues = init }) => {
    const [isloading, data] = useMasterValues();

    return (
        <DynamicContent isLoaded={!!data.project} fetching={isloading}>
            <AddLead
                initValues={initValues}
                masterData={data}
                editId={editId}
                leadId={leadId}
                portal_id={portal_id}
            />
        </DynamicContent>
    );
};

export default AddLeadForm;
