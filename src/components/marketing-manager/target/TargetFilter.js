import React from 'react';
import { FormProvider } from '../../form/Form';
import FieldArray from '../../form/FieldArray';
import { useMemo } from 'react';
import formatAllData from '../../form/formatAllData';
import useFormReducer from '../../../hooks/form/useFormReducer';
import { useMasterValues } from '../../common/MasterData';


const filterTarget = (
    isloading,
    { target_type = [] },
) => [
        {
            name: "date",
            type: "date",
            // label: "Type",
            inputProps: {
                placeholder: "Month",
                showMonthYearPicker: true,
                dateFormat: "MMM yyyy",
            },
            conClass: "col-md-3",
            required: true,
        },
        {
            name: "type",
            type: "select",
            // label: "Type",
            inputProps: {
                placeholder: "Project",
                options: target_type.map((x) => ({ label: x.title, value: x.id })),
                isloading
            },
            conClass: "col-md-2",
            required: true,
        },
    ];


const TargetFilter = ({ onFilter }) => {
    const [isloading, master] = useMasterValues();
    const form = useFormReducer();
    const [{ values }] = form;

    const allIds = useMemo(() => filterTarget(isloading, master), [master, isloading]);

    const onClick = () => {
        const { ...restData } = formatAllData(allIds, values);
        onFilter({
            ...restData
        });
    };
    return (
        <FormProvider form={form}>
            <div className="form-row align-items-stretch">
                <FieldArray allIds={allIds} />
                <div className="col-md-2">
                    <button
                        onClick={onClick}
                        className="btn btn-theme btn-block"
                    >
                        Search
                    </button>
                </div>
            </div>
        </FormProvider>
    );
}

export default TargetFilter;