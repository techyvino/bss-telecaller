import React, { useCallback, useEffect } from "react";
import { useState } from "react";
import urls from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import AddTarget from "./AddTarget";
import TargetFilter from "./TargetFilter";

const tableData = [
    {
        th: "Month",
        td: (data) => <DateFormat date={data.date} format="MMMM YYYY" />,
    },
    {
        th: "Type",
        td: (data) => (data.type ? data.type.title : ""),
    },
    {
        th: "Team Lead",
        td: (data) =>
            data.employee && data.employee.user
                ? `${data.employee.user.first_name} ${data.employee.user.last_name}`
                : "",
    },
    {
        th: "Assigned",
        td: ({ target }) => target,
    },
    {
        th: "Achieved",
        td: ({ archived_target }) => archived_target || 0,
    },
];

const TargetList = () => {

    const [pageState, { setUrl, setPage, clear }] = usePagination();
    const [filters, setFilters] = useState({});

    useEffect(() => {
        setUrl({
            baseUrl: urls.targetList,
            filters: { ...filters },
        });
    }, [setUrl, filters]);

    const succFunc = useCallback(() => {
        clear();
        setUrl({
            baseUrl: urls.targetList,
            filters: { ...filters },
        });
    }, [clear, setUrl, filters]);

    const onFilter = (filters = {}) => {
        setFilters(filters);
    };


    return (
        <div className="row">
            <div className="col-md-12">
                <AddTarget succFunc={succFunc} />
            </div>

            <div className="col-md-12">
                <TargetFilter onFilter={onFilter} />

            </div>

            <div className="col-md-12">
                <TableList
                    tblClassName="table table-bordered"
                    pageState={pageState}
                    onPageClick={setPage}
                    tableData={tableData}
                />
            </div>
        </div>
    );
};

export default TargetList;
