import dayjs from "dayjs";
import React, { useEffect } from "react";
import urls from "../../../apiService/urls";
import useFormatAll from "../../../hooks/form/useFormatAll";
import useValidateAll from "../../../hooks/form/useValidateAll";
import useFetchData from "../../../hooks/http/useFetchData";
import { useMasterValues } from "../../common/MasterData";
import FieldArray from "../../form/FieldArray";
import Form from "../../form/Form";
import FormSubmitBtn from "../../form/FormSubmitBtn";

const addTarget = (
    isloading = false,
    { target_type = [] },
    { fetching = false, data = [] }
) => [
    {
        name: "date",
        type: "date",
        // label: "Type",
        inputProps: {
            placeholder: "Month",
            showMonthYearPicker: true,
            dateFormat: "MMM yyyy",
            minDate: dayjs().startOf("M").toDate(),
        },
        conClass: "col-md-3",
        required: true,
    },
    {
        name: "type_id",
        type: "select",
        // label: "Type",
        inputProps: {
            placeholder: "Project",
            options: target_type.map((x) => ({ label: x.title, value: x.id })),
            isloading,
        },
        conClass: "col-md-2",
        required: true,
    },
    {
        name: "employee_id",
        type: "select",
        // label: "Employee",
        inputProps: {
            placeholder: "Team Lead",
            isLoading: fetching,
            options: data.map((x) => ({
                label: `${x.user.first_name} ${x.user.last_name}`,
                value: x.id,
            })),
        },
        conClass: "col-md-3",
        required: true,
    },
    {
        name: "target",
        type: "number",
        // label: "Target",
        inputProps: {
            placeholder: "Target",
        },
        conClass: "col-md-2",
        required: true,
    },
];

const AddTarget = ({ succFunc }) => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(urls.emplList);
    }, [setUrl]);

    const [isloading, master] = useMasterValues();
    const allIds = addTarget(isloading, master, state);

    const validateForm = useValidateAll(allIds);
    const formatData = useFormatAll(allIds);

    return (
        <Form
            resetOnSuccess
            validateForm={validateForm}
            formatData={formatData}
            config={{
                url: urls.addTarget,
                method: "POST",
            }}
            handleSuccess={succFunc}
        >
            <div className="form-row">
                <FieldArray allIds={allIds} />
                <div className="col-md-2 form-group">
                    <FormSubmitBtn>Add Target</FormSubmitBtn>
                </div>
            </div>
        </Form>
    );
};

export default AddTarget;
