import React from 'react';
import Tabs, { TabBtnList, TabPane } from '../../common/Tabs';
import AssignedLeads from './AssignedLeads';
import TelecallerCalls from './TelecallersCalls';
import UnAssignedLeads from './UnAssignedLeads';

const tabs = ["Un Assigned Telecaller", "Assigned Telecaller TL", "Telecallers Calls"]
const LeadTabs = () => {
    return (
        <Tabs tabs={tabs}>
            <TabBtnList />

            <TabPane tabId={tabs[0]}>
                <UnAssignedLeads />
            </TabPane>

            <TabPane tabId={tabs[1]}>
                <AssignedLeads />
            </TabPane>

            <TabPane tabId={tabs[2]}>
                <TelecallerCalls />
            </TabPane>

        </Tabs>
    );
}

export default LeadTabs;