import React, { useCallback, useEffect, useMemo } from "react";
import { masterUrls } from "../../../apiService/urls";
import formatAllData from "../../form/formatAllData";
import validateAll from "../../form/validateAll";
import {
    resetForm,
    setErrors,
    setValue,
} from "../../../hooks/form/formReducer";
import useFormReducer from "../../../hooks/form/useFormReducer";
import useSubmit from "../../../hooks/http/useSubmit";
import { useMasterValues } from "../../common/MasterData";
import FieldArray from "../../form/FieldArray";
import { FormProvider } from "../../form/Form";
import SubmitBtn from "../../form/SubmitBtn";
import { toast } from "react-toastify";

const addBudget = (isloading = false, { sources = [] }, campaigns = []) => [
    {
        name: "source",
        type: "select",
        label: "Source",
        inputProps: {
            options: sources.map((x) => ({
                label: x.title,
                value: x.id,
                campaigns: Array.isArray(x.campaigns) ? x.campaigns : [],
            })),
            placeholder: "Select an option",
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: "campaign_id",
        type: "select",
        label: "Campaign",
        inputProps: {
            placeholder: "Campaign",
            options: campaigns.map((x) => ({
                label: x.title,
                value: x.id,
            })),
            isloading,
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: "amount",
        type: "number",
        label: "Budget",
        inputProps: {
            placeholder: "Enter Budget",
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: "amount_spent",
        type: "number",
        label: "Amount Spent",
        inputProps: {
            placeholder: "Enter Amount Spent",
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: "duration",
        type: "daterange",
        label: "Duration",
        inputProps: {
            placeholder: "Date",
        },
        conClass: "col-md-12",
        required: true,
    },
];

const getCampaigns = (source = {}) =>
    Array.isArray(source.campaigns) ? source.campaigns : [];

const AddBudgetForm = () => {
    const form = useFormReducer();
    const [{ values }, dispatch] = form;

    const [isloading, master] = useMasterValues();
    const { source } = values;

    const [fetching, submit] = useSubmit({
        success: () => {
            dispatch(resetForm());
            toast.success("Budget Added");
        },
    });

    const allIds = useMemo(
        () => addBudget(isloading, master, getCampaigns(source)),
        [master, isloading, source]
    );

    useEffect(() => {
        dispatch(setValue("campaign_id", null));
    }, [source, dispatch]);

    const onSubmit = useCallback(
        (e) => {
            e.preventDefault();
            const errors = validateAll(allIds, values);
            if (Object.keys(errors).length > 0) {
                dispatch(setErrors(errors));
                return false;
            }
            const { duration = {}, campaign_id, amount } = formatAllData(
                allIds,
                values
            );
            submit({
                url: masterUrls.campaignBudget,
                method: "POST",
                data: {
                    start_date: duration.from,
                    end_date: duration.to,
                    campaign_id,
                    amount,
                },
            });
        },
        [allIds, dispatch, values, submit]
    );

    return (
        <FormProvider form={form}>
            <form noValidate onSubmit={onSubmit}>
                <div className="row">
                    <FieldArray allIds={allIds} />
                    <div className="col-md-12">
                        <SubmitBtn
                            fetching={fetching}
                            className="btn btn-theme btn-block"
                        >
                            Submit
                        </SubmitBtn>
                    </div>
                </div>
            </form>
        </FormProvider>
    );
};

export default AddBudgetForm;
