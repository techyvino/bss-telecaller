import React, { useEffect } from "react";
import { Plus } from "react-feather";
import { Link } from "react-router-dom";
import { masterUrls } from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import { SectionTitle } from "../../layout/Section";

const tableData = [
    {
        th: "Duration",
        td: (data) => (
            <span>
                <DateFormat date={data.start_date} format="ll" /> to{" "}
                <DateFormat date={data.end_date} format="ll" />
            </span>
        ),
    },
    {
        th: "Campaign",
        td: ({ campaign }) => `${campaign?.source.title ?? ""} / ${campaign?.title ?? ""}`,
    },
    {
        th: "Budget",
        td: (data) => data.amount,
    },
    {
        th: "Total Actual Spent",
        td: (data) => data.amount_spent || "-",
    },
    {
        th: "Total Enquiries",
        td: ({ report }) => report?.total_enquiries ?? "",
    },
    {
        th: "Cost per Enquiry",
        td: ({ report }) => report?.cp_total_enquiries ?? "",
    },
    {
        th: "Total Leads",
        td: ({ report }) => report?.total_lead ?? "",
    },
    {
        th: "Cost per Lead",
        td: ({ report }) => report?.cp_total_lead ?? "",
    },
    {
        th: "Total Conversion",
        td: ({ report }) => report?.total_conversion ?? "",
    },
    {
        th: "Cost per Conversion",
        td: ({ report }) => report?.cp_total_conversion ?? "",
    },
];

const BudgetList = () => {
    const [pageState, { setUrl, setPage }] = usePagination();

    useEffect(() => {
        setUrl({ baseUrl: masterUrls.campaignBudget });
    }, [setUrl]);

    return (
        <div>
            <div className="d-flex justify-content-between align-items-center">
                <SectionTitle title="Budget" />
                <Link
                    to="/marketing-manager/campaign-budget/add"
                    className="btn btn-theme d-flex align-items-center mb-3 btn-sm"
                >
                    <Plus size={16} /> Add Budget
                </Link>
            </div>
            <TableList
                pageState={pageState}
                onPageClick={setPage}
                tableData={tableData}
            />
        </div>
    );
};

export default BudgetList;
