import React, { useEffect } from "react";
import { marketingManagerUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import { isArray } from "../../../utils";
import DynamicContent from "../../common/DynamicContent";

const FtdTable = ({ list = [], heading = "", total = 0 }) => {
    if (isArray(list) && list.length > 0) {
        return (
            <div className="col-md-3">
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">{heading}</th>
                            <th scope="col">FTD</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((x) => (
                            <tr key={x.project}>
                                <td>{x.project}</td>
                                <td>{x.count}</td>
                            </tr>
                        ))}
                        <tr>
                            <th scope="row">Total</th>
                            <th scope="row">{total}</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
    return null;
};

const FtdList = () => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(marketingManagerUrls.dashboard);
    }, [setUrl]);

    return (
        <DynamicContent fetching={state.fetching} isLoaded={!!state.data}>
            <div className="row">
                <FtdTable
                    list={state?.data?.lead_allocation}
                    heading="Lead Allocation"
                    total={state?.data?.lead_allocation_total}
                />
                <FtdTable
                    list={state?.data?.enquiry}
                    heading="Enquiry"
                    total={state?.data?.enquiry_total}
                />
                <FtdTable
                    list={state?.data?.site_visit}
                    heading="Site Visit"
                    total={state?.data?.site_visit_total}
                />
                <FtdTable
                    list={state?.data?.digital}
                    heading="Digital"
                    total={state?.data?.digital_total}
                />
            </div>
        </DynamicContent>
    );
};

export default FtdList;
