import React, { useCallback, useState } from "react";
import { Edit, Plus } from "react-feather";
import useSubmit from "../../../hooks/http/useSubmit";
import useToggle from "../../../hooks/useToggle";
import { noop } from "../../../utils";
import FieldCon from "../../form/FieldCon";
import Input from "../../form/Input";
import SubmitBtn from "../../form/SubmitBtn";
import Modal from "../../modal/Modal";
import ModalBody from "../../modal/ModalBody";
import ModalHeader from "../../modal/ModalHeader";

const AddMaster = ({
    url,
    succFunc = noop,
    method = "POST",
    initVal = "",
    extraAddData = {},
}) => {
    const [title, setTitle] = useState(initVal);
    const [fetching, submit] = useSubmit({
        success: succFunc,
    });

    const onSubmit = (e) => {
        e.preventDefault();
        submit({
            url,
            method,
            data: { title, ...extraAddData },
        });
    };

    return (
        <form noValidate className="row" onSubmit={onSubmit}>
            <FieldCon conClass="col-md-12">
                <Input
                    type="text"
                    setValue={(val) => setTitle(val)}
                    placeholder="Title"
                    value={title}
                />
            </FieldCon>
            <div className="col-md-12">
                <SubmitBtn
                    disabled={!title}
                    className="btn btn-block btn-theme"
                    fetching={fetching}
                >
                    Submit
                </SubmitBtn>
            </div>
        </form>
    );
};

export const AddMasterBtn = ({ title = "", url, add, extraAddData = {} }) => {
    const { toggle, onFalse, onTrue } = useToggle();

    const succFunc = useCallback(
        (data) => {
            add(data);
            onFalse();
        },
        [add, onFalse]
    );

    return (
        <>
            <button
                onClick={onTrue}
                className="btn f-500 btn-sm btn-outline-black d-flex align-items-center"
            >
                <Plus size={16} className="mr-2" /> Add
            </button>
            <Modal
                isOpen={toggle}
                close={onFalse}
                contentStyle={{ maxWidth: "375px" }}
            >
                <ModalHeader close={onFalse} title={`Add ${title}`} />
                <ModalBody>
                    <AddMaster
                        url={url}
                        succFunc={succFunc}
                        extraAddData={extraAddData}
                    />
                </ModalBody>
            </Modal>
        </>
    );
};

export const EditMasterBtn = ({
    title = "",
    url,
    update,
    extraAddData = {},
}) => {
    const { toggle, onFalse, onTrue } = useToggle();

    const succFunc = useCallback(
        (data) => {
            update(data);
            onFalse();
        },
        [update, onFalse]
    );

    return (
        <>
            <button onClick={onTrue} className="btn p-0">
                <Edit size={20} />
            </button>
            <Modal
                isOpen={toggle}
                close={onFalse}
                contentStyle={{ maxWidth: "375px" }}
            >
                <ModalHeader close={onFalse} title={`Edit ${title}`} />
                <ModalBody>
                    <AddMaster
                        url={url}
                        method="PATCH"
                        succFunc={succFunc}
                        initVal={title}
                        extraAddData={extraAddData}
                    />
                </ModalBody>
            </Modal>
        </>
    );
};

export default AddMaster;
