import { useCallback, useReducer } from "react";

const initValue = [];

const typs = {
    load: "LOAD",
    add: "ADD",
    remove: "REMOVE",
    update: "UPDATE",
};

const masterReducer = (state = [], action) => {
    switch (action.type) {
        case typs.load:
            return action.payload;
        case typs.add:
            return [action.payload, ...state];
        case typs.remove:
            return state.filter((x) => x.id === action.payload);
        case typs.update:
            return state.map((x) => {
                if (x.id === action.payload.id) {
                    return {
                        ...x,
                        ...action.payload,
                    };
                }
                return x;
            });
        default:
            return state;
    }
};

const useMasterTable = (init = initValue) => {
    const [state, dispatch] = useReducer(masterReducer, init);

    const load = useCallback((payload) => {
        dispatch({
            type: typs.load,
            payload,
        });
    }, []);

    const add = useCallback((payload) => {
        dispatch({
            type: typs.add,
            payload,
        });
    }, []);

    const update = useCallback((payload) => {
        dispatch({
            type: typs.update,
            payload,
        });
    }, []);

    const remove = useCallback((payload) => {
        dispatch({
            type: typs.remove,
            payload,
        });
    }, []);

    return [state, { load, add, update, remove }];
};

export default useMasterTable;
