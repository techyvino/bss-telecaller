import React, { useEffect } from "react";
import useMasterTable from "./useMasterTable";
import { Toggle } from "./MasterTable";
import { masterUrls } from "../../../apiService/urls";

const ProjectList = ({ list = [] }) => {
    const [state, { load, update }] = useMasterTable(list);

    useEffect(() => {
        load(list);
    }, [load, list]);

    if (state.length === 0) {
        return null;
    }
    return (
        <table className="table table-bordered table-responsive-md f-14">
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>Title</th>
                    <th>Maintainance GST</th>
                    <th>Basic Rate</th>
                    <th>Registration Amount</th>
                    <th>Floor Rise</th>
                    <th>PLC</th>
                    <th>Guideline Value</th>
                    <th>Sales Area</th>
                    <th>Land Area</th>
                    <th>Brochure</th>
                    <th>Is Active</th>
                </tr>
            </thead>
            <tbody>
                {state.map((x, idx) => (
                    <tr key={`${x.id}`}>
                        <td>{idx + 1}</td>
                        <td>
                            {x.title}
                            <div>
                                <small>({x.address})</small>
                            </div>
                        </td>
                        <td>{x.maintenance_gst}</td>
                        <td>{x.basic_rate}</td>
                        <td>{x.registration_amount}</td>
                        <td>{x.floor_rise}</td>
                        <td>{x.plc}</td>
                        <td>{x.guide_line_value}</td>
                        <td>{x.sales_area}</td>
                        <td>{x.land_area}</td>
                        <td>
                            {x.brochure ? (
                                <a
                                    href={x.brochure}
                                    className="text-body text-underline"
                                    download={x.title}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    Download
                                </a>
                            ) : (
                                "-"
                            )}
                        </td>
                        <td>
                            <Toggle
                                id={x.id}
                                isActive={x.is_active}
                                update={update}
                                url={masterUrls.addProject}
                            />
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
};

export default ProjectList;
