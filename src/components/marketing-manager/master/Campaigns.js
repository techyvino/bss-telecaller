import React, { useEffect, useState } from "react";
import { masterUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import { isArray, toBoolean } from "../../../utils";
import DynamicContent from "../../common/DynamicContent";
import MasterTable from "./MasterTable";
import Select from "react-select";
import SourceCampaigns from "./SourceCampaigns";

const Campaigns = ({ sources = [] }) => {
    const [id, setId] = useState(sources[0]);

    useEffect(() => {
        setId(sources[0]);
    }, [sources]);

    return (
        <div className="mt-3">
            <h6 className="mb-3">Campaigns</h6>
            <div className="row">
                <div className="col-md-3">
                    <Select
                        options={sources}
                        value={id}
                        onChange={(val) => setId(val)}
                        getOptionLabel={(op) => op.title}
                        getOptionValue={(op) => op.id}
                        classNamePrefix="react-select"
                        placeholder="Select Campaign"
                    />
                </div>
            </div>
            {id && id.id && <CampaignList id={id.id} />}
        </div>
    );
};

const CampaignList = ({ id }) => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(`${masterUrls.addCampaign}?filter=${id}`);
    }, [setUrl, id]);

    return (
        <DynamicContent
            fetching={state.fetching}
            isLoaded={toBoolean(state?.data)}
        >
            {isArray(state?.data) && (
                <>
                    <MasterTable
                        addUrl={masterUrls.addCampaign}
                        list={state.data}
                        title="Campaigns"
                        extraAddData={{ source_id: id }}
                    />
                    <SourceCampaigns list={state.data} />
                </>
            )}
        </DynamicContent>
    );
};

export default Campaigns;
