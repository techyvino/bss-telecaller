import React, { useEffect } from "react";
import { masterUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import { isArray } from "../../../utils";
import DynamicContent from "../../common/DynamicContent";
import Tabs, { TabBtnList, TabPane } from "../../common/Tabs";
import MasterTable from "./MasterTable";
import { Link } from "react-router-dom";
import { Plus } from "react-feather";
import { pages } from "../../../pages/marketing-manager";
import ProjectList from "./ProjectList";
import Campaigns from "./Campaigns";

const tabs = ["Projects", "Interested Locations", "Source"];

const MasterTabs = () => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(masterUrls.master);
    }, [setUrl]);

    return (
        <Tabs tabs={tabs}>
            <TabBtnList />
            <TabPane tabId={tabs[0]}>
                <DynamicContent
                    fetching={state.fetching}
                    isLoaded={state?.data?.project}
                >
                    <div className="d-flex justify-content-end align-items-center mb-3">
                        <Link
                            to={pages.addProject}
                            className="btn f-500 btn-sm btn-outline-black d-flex align-items-center"
                        >
                            <Plus size={16} className="mr-2" /> Add
                        </Link>
                    </div>
                    {isArray(state?.data?.project) && (
                        <ProjectList list={state?.data?.project} />
                    )}
                </DynamicContent>
            </TabPane>
            <TabPane tabId={tabs[1]}>
                <DynamicContent
                    fetching={state.fetching}
                    isLoaded={state?.data?.locations}
                >
                    {isArray(state?.data?.locations) && (
                        <MasterTable
                            list={state?.data?.locations}
                            title={"Interested Location"}
                            addUrl={masterUrls.addLocation}
                        />
                    )}
                </DynamicContent>
            </TabPane>
            <TabPane tabId={tabs[2]}>
                <DynamicContent
                    fetching={state.fetching}
                    isLoaded={state?.data?.sources}
                >
                    {isArray(state?.data?.sources) && (
                        <>
                            <MasterTable
                                list={state.data.sources}
                                title={"Sources"}
                                addUrl={masterUrls.addSource}
                            />
                            <Campaigns sources={state.data.sources} />
                        </>
                    )}
                </DynamicContent>
            </TabPane>
        </Tabs>
    );
};

export default MasterTabs;
