import React, { useEffect } from "react";
import useSubmit from "../../../hooks/http/useSubmit";
import { toBoolean } from "../../../utils";
import DateFormat from "../../common/DateFormat";
import EmptyList from "../../common/EmptyList";
import Loader from "../../form/Loader";
import Switch from "../../form/Switch";
import { AddMasterBtn, EditMasterBtn } from "./AddMaster";
import useMasterTable from "./useMasterTable";

export const Toggle = ({ id, isActive = false, update, url }) => {
    const [fetching, submit] = useSubmit({
        success: update,
    });

    const setValue = (val) => {
        submit({
            url: `${url}${id}/`,
            method: "PATCH",
            data: {
                is_active: val,
            },
        });
    };

    return (
        <div className="d-flex align-items-center">
            <Switch
                disabled={fetching}
                checked={toBoolean(isActive)}
                setValue={setValue}
            />
            {fetching && (
                <div>
                    <Loader size={10} />
                </div>
            )}
        </div>
    );
};

const MasterTable = ({ title = "", list = [], addUrl, extraAddData = {} }) => {
    const [state, { load, update, add }] = useMasterTable(list);

    useEffect(() => {
        load(list);
    }, [load, list]);

    return (
        <>
            <div className="d-flex justify-content-end align-items-center mb-3">
                <AddMasterBtn title={title} url={addUrl} add={add} extraAddData={extraAddData} />
            </div>
            {state.length > 0 ? (
                <div>
                    <table className="table table-bordered responsive-sm f-14">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Created on</th>
                                <th>Is Active</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            {state.map((item) => (
                                <tr key={item.id}>
                                    <td>{item.title}</td>
                                    <td>
                                        <DateFormat date={item.created_on} />
                                    </td>
                                    <td>
                                        <Toggle
                                            id={item.id}
                                            isActive={item.is_active}
                                            update={update}
                                            url={addUrl}
                                        />
                                    </td>
                                    <td>
                                        <EditMasterBtn
                                            url={`${addUrl}${item.id}/`}
                                            update={update}
                                            title={item.title}
                                            extraAddData={extraAddData}
                                        />
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            ) : (
                <EmptyList title="No data found" />
            )}
        </>
    );
};

export default MasterTable;
