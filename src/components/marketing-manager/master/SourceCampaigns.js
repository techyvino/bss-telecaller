import React, { useEffect, useState } from "react";
import { masterUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import { isArray, toBoolean } from "../../../utils";
import DynamicContent from "../../common/DynamicContent";
import MasterTable from "./MasterTable";
import Select from "react-select";

const SourceCampaigns = ({ list = [] }) => {
    const [id, setId] = useState(list[0]);

    useEffect(() => {
        setId(list[0]);
    }, [list]);

    return (
        <div className="mt-3">
            <h6 className="mb-3">Primary Source Campaign</h6>
            <div className="row">
                <div className="col-md-3">
                    <Select
                        options={list}
                        value={id}
                        onChange={(val) => setId(val)}
                        getOptionLabel={(op) => op.title}
                        getOptionValue={(op) => op.id}
                        classNamePrefix="react-select"
                        placeholder="Select Campaign"
                    />
                </div>
            </div>
            {id && id.id && <List id={id.id} />}
        </div>
    );
};

const List = ({ id }) => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(`${masterUrls.addPrimarySource}?filter=${id}`);
    }, [setUrl, id]);

    return (
        <DynamicContent
            fetching={state.fetching}
            isLoaded={toBoolean(state?.data)}
        >
            {isArray(state?.data) && (
                <MasterTable
                    addUrl={masterUrls.addPrimarySource}
                    list={state.data}
                    title="Primary Source Campaign"
                    extraAddData={{ campaign_id: id }}
                />
            )}
        </DynamicContent>
    );
};

export default SourceCampaigns;
