import React from "react";
import { masterUrls } from "../../../apiService/urls";
import { resetForm, setErrors } from "../../../hooks/form/formReducer";
import useFormReducer from "../../../hooks/form/useFormReducer";
import useSubmit from "../../../hooks/http/useSubmit";
import { isArray } from "../../../utils";
import FieldArray from "../../form/FieldArray";
import { FormProvider } from "../../form/Form";
import formatAllData from "../../form/formatAllData";
import formTypes from "../../form/formTypes";
import SubmitBtn from "../../form/SubmitBtn";
import validateAll from "../../form/validateAll";

const allIds = [
    {
        name: "title",
        type: formTypes.text,
        label: "Title",
        inputProps: {
            placeholder: "Enter title",
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: "address",
        type: formTypes.textarea,
        label: "Address",
        inputProps: {
            placeholder: "Enter address",
        },
        conClass: "col-md-12",
        required: true,
    },
    {
        name: "maintenance_gst",
        type: formTypes.number,
        label: "Maintainance GST",
        inputProps: {
            placeholder: "Enter maintainance gst",
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: "registration_amount",
        type: formTypes.number,
        label: "Registration Amount",
        inputProps: {
            placeholder: "Enter registration amount",
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: "basic_rate",
        type: formTypes.number,
        label: "Basic Rate",
        inputProps: {
            placeholder: "Enter basic rate",
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: "floor_rise",
        type: formTypes.number,
        label: "Floor Rise",
        inputProps: {
            placeholder: "Enter floor rise",
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: "plc",
        type: formTypes.number,
        label: "PLC",
        inputProps: {
            placeholder: "Enter PLC",
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: "guide_line_value",
        type: formTypes.number,
        label: "Guideline Value",
        inputProps: {
            placeholder: "Enter guideline value",
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: "sales_area",
        type: formTypes.number,
        label: "Sales Area",
        inputProps: {
            placeholder: "Enter sales area",
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: "land_area",
        type: formTypes.number,
        label: "Land Area",
        inputProps: {
            placeholder: "Enter land area",
        },
        conClass: "col-md-6",
        required: true,
    },
    {
        name: "brochure",
        type: formTypes.file,
        label: "Brochure",
        inputProps: {},
        conClass: "col-md-12",
        required: true,
    },
];

const AddProjectForm = ({ init, editId }) => {
    const form = useFormReducer(init);
    const [{ values }, dispatch] = form;

    const [fetching, submit] = useSubmit({
        success: () => {
            dispatch(resetForm());
        },
    });

    const onSubmit = (e) => {
        e.preventDefault();
        const errs = validateAll(allIds, values);
        if (Object.keys(errs).length > 0) {
            dispatch(setErrors(errs));
            return;
        }
        const { brochure, ...rest } = formatAllData(allIds, values);
        let postData = new FormData();
        Object.keys(rest).forEach((key) => {
            if (rest[key]) {
                postData.append(key, rest[key]);
            }
        });
        if (isArray(brochure) && brochure.length) {
            postData.append("brochure", brochure[0]);
        }
        submit({
            url: masterUrls.addProject,
            processData: false,
            mimeType: "multipart/form-data",
            contentType: false,
            data: postData,
            method: editId ? "PATCH" : "POST",
        });
    };

    return (
        <FormProvider form={form}>
            <form noValidate onSubmit={onSubmit}>
                <div className="row">
                    <FieldArray allIds={allIds} />
                    <div className="col-md-12">
                        <SubmitBtn
                            fetching={fetching}
                            className="btn btn-theme btn-block"
                        >
                            Submit
                        </SubmitBtn>
                    </div>
                </div>
            </form>
        </FormProvider>
    );
};

const init = (data) => {
    return {
        
    };
};

export const EditProjectForm = ({ data = {} }) => {
    if (!data.id) {
        return null;
    }
    const values = init(data);
    return <AddProjectForm init={{ values: values }} />;
};

export default AddProjectForm;
