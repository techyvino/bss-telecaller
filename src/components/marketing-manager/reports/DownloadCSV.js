import React from 'react';
import keysToQuery from '../../../utils/keysToQuery';
import { DownloadAsXL } from '../../sales-general-manager/DownloadLink';

const DownloadCSV = ({ link, filter, tab }) => {
    const url = link + keysToQuery({...filter, tab})
    return (
        <DownloadAsXL link={url } />
    );
}

export default DownloadCSV;