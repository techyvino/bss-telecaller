import React from "react";

const TargetTable = () => {
    return (
        <table className="table table-bordered table-responsive-l">
            <thead>
                <tr className="f-14">
                    <th scope="col">Month</th>
                    <th scope="col">Type </th>
                    <th scope="col">Team</th>
                    <th scope="col">Target</th>
                    <th scope="col">Achieved</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    );
};

export default TargetTable;
