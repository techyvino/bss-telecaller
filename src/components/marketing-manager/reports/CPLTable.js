import React from "react";

const CPLTable = () => {
    return (
        <table className="table table-bordered table-responsive-l">
            <thead>
                <tr className="f-14">
                    <th scope="col">Month</th>
                    <th scope="col">Campaign </th>
                    <th scope="col">Budget</th>
                    <th scope="col">Total Amount Spent</th>
                    <th scope="col">Total Amount Spent</th>
                    <th scope="col">Total Enquiry</th>
                    <th scope="col">No. of Leads</th>
                    <th scope="col">Cost per Lead</th>
                    <th scope="col">No of Conversion</th>
                    <th scope="col">Cost per Conversion</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    );
};

export default CPLTable;
