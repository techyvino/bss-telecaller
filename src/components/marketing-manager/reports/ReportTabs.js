import React from "react";
import Tabs, { TabBtnList, TabPane } from "../../common/Tabs";
import BookingTable from "./BookingTable";
import GeneratedReportTable from "./GeneratedReportTable";
import LeadReportTable from "./LeadReportTable";
import ClosedLeadReportTable from "./ClosedLeadReportTable";
import AutoClosedLeadTable from "./AutoClosedLeadTable";
import SalesLeadReportTable from "./SalesLeadReportTable";
import BudgetList from "../budget/BudgetList";
import SiteVisitReportTable from "./SiteVisitReportTable";

const tabs = [
    "TC Customer Feedback",
    "TC Generated Report",
    "TC Lead Report",
    "Closed Lead Report",
    "Auto Closed Lead",
    "Sales Lead Report",
    "Site Visit Report",
    "Booking Report",
    "CPL (budget)",
];

const ReportTabs = () => {
    return (
        <Tabs tabs={tabs}>
            <TabBtnList />
            <TabPane tabId={tabs[1]}>
                <GeneratedReportTable />
            </TabPane>
            <TabPane tabId={tabs[2]}>
                <LeadReportTable />
            </TabPane>
            <TabPane tabId={tabs[3]}>
                <ClosedLeadReportTable />
            </TabPane>
            <TabPane tabId={tabs[4]}>
                <AutoClosedLeadTable />
            </TabPane>
            <TabPane tabId={tabs[5]}>
                <SalesLeadReportTable />
            </TabPane>
            <TabPane tabId={tabs[6]}>
                <SiteVisitReportTable />
            </TabPane>
            <TabPane tabId={tabs[7]}>
                <BookingTable />
            </TabPane>
            <TabPane tabId={tabs[8]}>
                <BudgetList />
            </TabPane>
        </Tabs>
    );
};

export default ReportTabs;
