import React, { useEffect, useState } from "react";
import { isArray } from "validate.js";
import urls, { marketingManagerUrls } from "../../../apiService/urls";
import DownloadCSV from "./DownloadCSV";
import usePagination from "../../../hooks/http/usePagination";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import Filters from "./Filters";

const tbData = [
    {
        th: 'Project',
        td: ({ lead }) => lead?.project?.title,
    },
    {
        th: 'Enquiry Date',
        td: ({ lead }) => <DateFormat date={lead?.enquiry_date} format="DD/MM/YYYY" />,
    },
    {
        th: 'Date Entered',
        td: ({ lead }) => <DateFormat date={lead?.created_on} format="DD/MM/YYYY" />,
    },
    {
        th: 'Assigned Date',
        td: ({ lead }) => <DateFormat date={lead?.created_on} format="DD/MM/YYYY" />,
    },
    {
        th: "Closed Date",
        td: ({ lead }) => "-"
    },
    {
        th: "Booking Date",
        td: ({ created_on }) => <DateFormat date={created_on} format="DD/MM/YYYY" />,
    },
    {
        th: "Lead ID",
        td: ({ lead }) => lead.lead_no
    },
    {
        th: "Customer Name",
        td: ({ name, salutation }) => `${salutation}.${name}`,
    },
    {
        th: "Status",
        td: ({ status }) => status === true ? "Booked" : status === null ? "Pending" : "Rejected",
    },
    {
        th: "Dead Reason",
        td: ({ lead }) => "-",
    },
    {
        th: "Telecaller Name",
        td: ({ lead }) =>
            lead?.created_by?.user
                ? `${lead.created_by.user.first_name} ${lead.created_by.user.last_name}`
                : "",
    },
    {
        th: "Sales Managers Name",
        td: ({ lead }) =>
            lead?.assigned_to?.user
                ? `${lead.assigned_to.user.first_name} ${lead.assigned_to.user.last_name}`
                : "",
    },
    {
        th: 'Mobile1',
        td: ({ lead }) => lead.number ? `+${lead.primary_code?.dial_code}-${lead.number}` : "NA",
    },
    {
        th: 'Mobile2',
        td: ({ lead }) => lead.secondary_number ? `+${lead.secondary_code?.dial_code}-${lead.secondary_number}` : "NA",
    },
    {
        th: 'Email ID1',
        td: ({ lead }) => lead.email ?? "NA"
    },
    {
        th: 'Email ID2',
        td: ({ lead }) => lead.secondary_email ?? "NA"
    },
    {
        th: "Source",
        td: ({ lead }) =>
            lead?.source_of_campaigning?.campaign?.source?.title ?? "",
    },
    {
        th: "Campaign",
        td: ({ lead }) => lead?.source_of_campaigning?.campaign.title ?? "",
    },
    {
        th: "Primary Source Campaign",
        td: ({ lead }) => lead?.source_of_campaigning?.title ?? "",
    },
    {
        th: "Walkin Source",
        td: ({ source_of_campaigning }) => "-",
    },
    {
        th: "Referred Customer Name",
        td: ({ lead }) => lead?.referred_by_customer?.name ?? "-",
    },
    {
        th: "Referred Customer Flat No",
        td: ({ lead }) => lead?.referred_by_customer?.flat ?? "-",
    },
    {
        th: "Referred Customer Project",
        td: ({ lead }) => lead?.referred_by_customer?.project ?? "-",
    },
    {
        th: "Employee Name",
        td: ({ lead }) => lead?.referred_by_employee?.name ?? "-",
    },
    {
        th: "Employee Department",
        td: ({ lead }) => lead?.referred_by_employee?.department ?? "-",
    },
    {
        th: "Employee Designation",
        td: ({ lead }) => lead?.referred_by_employee?.designation ?? "-",
    },
    {
        th: "Followup Date",
        td: ({ lead }) => <DateFormat date={lead.follow_up_date} format="DD/MM/YYYY" />,
    },
    {
        th: "Site Visit",
        td: ({ lead }) => lead?.is_site_visit ? "Yes" : "No",
    },
    {
        th: "Site Visit Date",
        td: ({ lead }) => <DateFormat date={lead.site_visit_date} format="DD/MM/YYYY" />,
    },
    {
        th: 'Profile',
        td: ({ lead }) => lead.profile ?? "NA",
    },
    {
        th: "Address",
        td: ({ lead }) => lead.address,
    },
    {
        th: "Area",
        td: ({ lead }) => lead.area,
    },
    {
        th: "City",
        td: ({ lead }) => lead.city,
    },
    {
        th: "Pin Code",
        td: ({ lead }) => lead.pincode,
    },
    {
        th: "Customer Interest",
        td: ({ lead }) => isArray(lead.customer_interest)
            ? lead.customer_interest.map((x) => x.title).join("\n, ")
            : "-",
    },
    {
        th: "Interested Location",
        td: ({ lead }) => isArray(lead.interested_location)
            ? lead.interested_location.map((x) => x.title).join("\n, ")
            : "-",
    },
    {
        th: "Purpose",
        td: ({ lead }) => lead?.purpose ?? "",
    },
    {
        th: "Mode Payment",
        td: ({ lead }) => lead?.mode_of_payment ?? "",
    },
    {
        th: "Transport Required",
        td: ({ lead }) => lead?.is_transport_required ?? "",
    },
    { th: 'TeleCaller Feedback', td: ({ lead }) => lead.initial_feedback, },
    { th: 'Sales Feedback', td: ({ lead }) => lead?.sales_feedback ?? "-" },
    { th: 'First Followup Date', td: ({ lead }) => lead?.first_followup?.created_on ?? "-" },
    { th: 'First Followup Remarks', td: ({ lead }) => lead?.first_followup?.description ?? "-" },
    { th: 'Last Followup Date', td: ({ lead }) => lead?.last_followup?.created_on ?? "-" },
    { th: 'Last Followup Remarks', td: ({ lead }) => lead?.last_followup?.description ?? "-" },
]


const BookingTable = () => {
    const [filters, setFilters] = useState({});
    const [state, { setUrl, setPage }] = usePagination();

    const onFilter = (filters = {}) => {
        setFilters(filters);
    };


    useEffect(() => {
        setUrl({
            baseUrl: `${marketingManagerUrls.reports}reports/`,
            filters: { ...filters, tab: 8 },
        });
    }, [setUrl, filters]);

    return (
        <>
            <Filters onFilter={onFilter} />
            <div className="mb-2">
                <DownloadCSV link={`${urls.baseURL}export/marketing-manager/reports_csv/`} filter={filters} tab={8} />
            </div>
            <TableList
                pageState={state}
                onPageClick={setPage}
                tableData={tbData}
            />
        </>
    );
}

export default BookingTable
