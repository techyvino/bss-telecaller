import React from "react";

const SalesTable = () => {
    return (
        <table className="table table-bordered table-responsive-l">
            <thead>
                <tr className="f-14">
                    <th scope="col">Project</th>
                    <th scope="col">Lead Id </th>
                    <th scope="col">Enquiry Date</th>
                    <th scope="col">Assigned Date</th>
                    <th scope="col">Customer Name</th>
                    <th scope="col">Customer Mobile Number</th>
                    <th scope="col">Customer Email</th>
                    <th scope="col">Tele-caller</th>
                    <th scope="col">Sales Manager</th>
                    <th scope="col">Source Campaign</th>
                    <th scope="col">Tele-caller Feedback</th>
                    <th scope="col">View Details</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    );
};

export default SalesTable;
