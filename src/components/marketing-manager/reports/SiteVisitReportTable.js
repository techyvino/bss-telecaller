import React, { useEffect, useState } from "react";
import { isArray } from "validate.js";
import { marketingManagerUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import { toBoolean } from "../../../utils";
import keysToQuery from "../../../utils/keysToQuery";
import DynamicContent from "../../common/DynamicContent";
import { useMasterValues } from "../../common/MasterData";
import Downloads from "../../sales-general-manager/reports/Downloads";


const Tr = ({
    title, sales_manager, mtd_ql, mtd_ql_site_visit, idx,
    sales_lead, sales_lead_site_visit, sales_lead_site_visit_month,
    without_site_visit, over_site_visit_month, total_site_visit
}) => {
    return (
        <tr>
            <td>{idx + 1}</td>
            <td>{title}</td>
            <td>{sales_manager}</td>
            <td>{mtd_ql}</td>
            <td>{mtd_ql_site_visit}</td>
            <td>{sales_lead}</td>
            <td>{sales_lead_site_visit}</td>
            <td>{sales_lead_site_visit_month}</td>
            <td>{without_site_visit}</td>
            <td>{over_site_visit_month}</td>
            <td>{total_site_visit}</td>
        </tr>
    );
};

const SiteVisitReportTable = () => {
    const [state, { setUrl }] = useFetchData();
    const [project, setProject] = useState();

    const [fetching, data] = useMasterValues();
    const options = isArray(data?.project) ? data.project : [];

    useEffect(() => {
        const q = keysToQuery({ "tab": 7, "project": project })

        setUrl(`${marketingManagerUrls.reports}reports/${q}`);

    }, [setUrl, project]);

    const isLoaded = toBoolean(state.data?.result);

    return (
        <DynamicContent isLoaded={isLoaded} fetching={state.fetching || fetching}>

            {isLoaded && (
                <div>
                    <div className="row">
                        <div className="col-md-3 mb-3">
                            <select
                                className="form-control"
                                onChange={(e) => setProject(e.target.value)}
                                value={project}
                            >
                                <option value="">
                                    All Project
                                </option>
                                {options.map((x) => (
                                    <option value={x.id} key={x.id}>
                                        {x.title}
                                    </option>
                                ))}
                            </select>
                        </div>
                    </div>
                    <Downloads
                        title="Weekly  Sales Review Report"
                        tableId={"SiteVisitTable"}
                    />
                    <table className="table table-bordered table-responsive-l" id={"SiteVisitTable"}>
                        <thead>
                            <tr className="f-14">
                                <th scope="col">S.No</th>
                                <th scope="col">Project</th>
                                <th scope="col">Sales Manager</th>
                                <th scope="col">MTD QL</th>
                                <th scope="col">MTD Site Visit</th>
                                <th scope="col">Open Leads</th>
                                <th scope="col">Open Leads Site Visit</th>
                                <th scope="col">Open Leads SV in MTD</th>
                                <th scope="col">Leads Without SV</th>
                                <th scope="col">MTD SV</th>
                                <th scope="col">Total SV</th>
                            </tr>
                        </thead>
                        <tbody>
                            {isArray(state.data?.result) &&
                                state.data?.result.map((x, idx) => (
                                    <Tr {...x} idx={idx} key={idx} />
                                ))
                            }


                            {state.data?.total &&
                                <tr>
                                    <th scope="col" colSpan={3}>Total</th>
                                    <th scope="col">{state.data?.total?.total_mtd_ql ?? "0"}</th>
                                    <th scope="col">{state.data?.total?.total_mtd_ql_site_visit ?? "0"}</th>
                                    <th scope="col">{state.data?.total?.total_sales_lead ?? "0"}</th>
                                    <th scope="col">{state.data?.total?.total_sales_lead_site_visit ?? "0"}</th>
                                    <th scope="col">{state.data?.total?.total_sales_lead_site_visit_month ?? "0"}</th>
                                    <th scope="col">{state.data?.total?.total_without_site_visit ?? "0"}</th>
                                    <th scope="col">{state.data?.total?.total_without_site_visit ?? "0"}</th>
                                    <th scope="col">{state.data?.total?.total_mtd_ql0 ?? "0"}</th>
                                </tr>
                            }

                        </tbody>
                    </table></div>)
            }
        </DynamicContent>
    );
};

export default SiteVisitReportTable;
