import React, { useEffect, useState } from "react";
import { isArray } from "validate.js";
import urls, { marketingManagerUrls } from "../../../apiService/urls";
import DownloadCSV from "./DownloadCSV";
import usePagination from "../../../hooks/http/usePagination";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import Filters from "./Filters";

const tbData = [
    {
        th: 'Project',
        td: ({ project }) => project.title,
    },
    {
        th: 'Enquiry Date',
        td: ({ enquiry_date }) => <DateFormat date={enquiry_date} format="DD/MM/YYYY" />,
    },
    {
        th: 'Date Entered',
        td: ({ created_on }) => <DateFormat date={created_on} format="DD/MM/YYYY" />,
    },
    {
        th: 'Assigned Date',
        td: ({ assigned_on }) => assigned_on ? <DateFormat date={assigned_on} format="DD/MM/YYYY" /> : "-",
    },

    {
        th: 'Closed Date',
        td: ({ closing_date }) => closing_date ? <DateFormat date={closing_date} format="DD/MM/YYYY" /> : "-"
    },
    {
        th: 'Booking Date',
        td: ({ booking_status }) => booking_status?.created_on ? <DateFormat date={booking_status.created_on} format="DD/MM/YYYY" /> : "-",
    },
    {
        th: 'Cancelled Date',
        td: ({ booking_status }) => booking_status?.canceled_on ? <DateFormat date={booking_status.canceled_on} format="DD/MM/YYYY" /> : "-",
    },
    {
        th: "Lead ID",
        td: ({ lead_no }) => lead_no
    },
    {
        th: 'Customer Name',
        td: ({ name }) => name,
    },
    {
        th: 'Status',
        td: ({ status }) => status ? status.title : "-"
    },
    {
        th: "Dead Reason",
        td: ({ dead_reason }) => dead_reason ?? "-"

    },
    {
        th: 'TeleCaller',
        td: ({ created_by }) => created_by?.user
            ? `${created_by.user.first_name} ${created_by.user.last_name}`
            : "-",
    },
    {
        th: 'Sales Manager',
        td: ({ assigned_to }) => assigned_to?.user
            ? `${assigned_to.user.first_name} ${assigned_to.user.last_name}`
            : "-",
    },
    {
        th: 'Mobile1',
        td: ({ number, primary_code }) => number ? `+${primary_code?.dial_code}-${number}` : "NA",
    },
    {
        th: 'Mobile2',
        td: ({ secondary_code, secondary_number }) => secondary_number ? `+${secondary_code?.dial_code}-${secondary_number}` : "NA",
    },
    {
        th: 'Email ID1',
        td: ({ email }) => email ?? "NA"
    },
    {
        th: 'Email ID2',
        td: ({ secondary_email }) => secondary_email ?? "NA"
    },
    {
        th: "Source",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign?.source?.title ?? "-",
    },
    {
        th: "Campaign",
        td: ({ source_of_campaigning }) => source_of_campaigning?.campaign.title ?? "-",
    },
    {
        th: "Primary Source Campaign",
        td: ({ source_of_campaigning }) => source_of_campaigning?.title ?? "-",
    },
    {
        th: "Walkin Source",
        td: ({ source_of_campaigning }) => "-",
    },
    {
        th: "Referred Customer Name",
        td: ({ referred_by_customer }) => referred_by_customer?.name ?? "-",
    },
    {
        th: "Referred Customer Flat No",
        td: ({ referred_by_customer }) => referred_by_customer?.flat ?? "-",
    },
    {
        th: "Referred Customer Project",
        td: ({ referred_by_customer }) => referred_by_customer?.project ?? "-",
    },
    {
        th: "Employee Name",
        td: ({ referred_by_employee }) => referred_by_employee?.name ?? "-",
    },
    {
        th: "Employee Department",
        td: ({ referred_by_employee }) => referred_by_employee?.department ?? "-",
    },
    {
        th: "Employee Designation",
        td: ({ referred_by_employee }) => referred_by_employee?.designation ?? "-",
    },
    {
        th: 'Followup Date',
        td: ({ follow_up_date }) => <DateFormat date={follow_up_date} format="DD/MM/YYYY" />,
    },
    {
        th: 'Site Visit Date',
        td: ({ site_visit_date }) => <DateFormat date={site_visit_date} format="DD/MM/YYYY" />,
    },
    {
        th: 'Profile',
        td: ({ profile }) => profile ?? "NA",
    },
    {
        th: 'Address',
        td: ({ address }) => address,
    },
    {
        th: 'Area',
        td: ({ area }) => area,
    },
    {
        th: 'City',
        td: ({ city }) => city,
    },
    {
        th: 'Pin Code',
        td: ({ pincode }) => pincode,
    },
    {
        th: 'Customer Interest',
        td: ({ customer_interest }) =>
            isArray(customer_interest)
                ? customer_interest.map((x) => x.title).join("\n, ")
                : "-",
    },
    {
        th: 'Preferred Location',
        td: ({ interested_location }) =>
            isArray(interested_location)
                ? interested_location.map((x) => x.title).join("\n, ")
                : "-",
    },
    { th: 'Purpose', td: ({ purpose }) => purpose, },
    { th: 'Mode Of Payment', td: ({ mode_of_payment }) => mode_of_payment, },
    { th: 'Transport Required', td: ({ is_transport_required }) => is_transport_required, },
    { th: 'TeleCaller Feedback', td: ({ initial_feedback }) => initial_feedback, },
    { th: 'Sales Feedback', td: ({ sales_feedback }) => sales_feedback ?? "-" },
    { th: 'First Followup Date', td: ({ first_followup }) => first_followup?.created_on ?? "-" },
    { th: 'First Followup Remarks', td: ({ first_followup }) => first_followup?.description ?? "-" },
    { th: 'Last Followup Date', td: ({ last_followup }) => last_followup?.created_on ?? "-" },
    { th: 'Last Followup Remarks', td: ({ last_followup }) => last_followup?.description ?? "-" },

]



const ClosedLeadReportTable = () => {
    const [filters, setFilters] = useState({});
    const [state, { setUrl, setPage }] = usePagination();

    const onFilter = (filters = {}) => {
        setFilters(filters);
    };


    useEffect(() => {
        setUrl({
            baseUrl: `${marketingManagerUrls.reports}reports/`,
            filters: { ...filters, tab: 4 },
        });
    }, [setUrl, filters]);

    return (
        <>
            <Filters onFilter={onFilter} />
            <div className="mb-2">
                <DownloadCSV link={`${urls.baseURL}export/marketing-manager/reports_csv/`} filter={filters} tab={4} />
            </div>
            <TableList
                pageState={state}
                onPageClick={setPage}
                tableData={tbData}
            />
        </>
    );
}

export default ClosedLeadReportTable
