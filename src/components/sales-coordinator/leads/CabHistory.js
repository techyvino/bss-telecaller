import React, { useCallback, useEffect } from "react";
import useToggle from "../../../hooks/useToggle";
import Modal from "../../modal/Modal";
import ModalHeader from "../../modal/ModalHeader";
import ModalBody from "../../modal/ModalBody";
import Loader from "../../form/Loader";
import useDateFormat from "../../../hooks/useDateFormat";
import { salesCOUrls } from "../../../apiService/urls";
import InfiniteList from "../../common/InfiniteList";
import useInfiniteList from "../../../hooks/http/useInfiniteList";
import EmptyList from "../../common/EmptyList";
import formTypes from "../../form/formTypes";
import Form from "../../form/Form";
import FieldArray from "../../form/FieldArray";
import useValidateAll from "../../../hooks/form/useValidateAll";
import useFormatAll from "../../../hooks/form/useFormatAll";
import FormSubmitBtn from "../../form/FormSubmitBtn";

const CAB_ASSIGNED = "CAB ASSIGNED";
const CAB_CANCELLED = "CAB CANCELLED";

const Item = ({ title, desc }) => {
    return (
        <div className="col-6 mb-3">
            <div className="f-12 f-500">{title}</div>
            <div>{desc}</div>
        </div>
    );
};

const HistoryItem = ({
    cab_number,
    company,
    driver_name,
    pickup_time,
    status,
}) => {
    const pickup = useDateFormat(pickup_time, "lll");

    return (
        <div className="border-bottom f-14 mb-3">
            <div className="form-row">
                <Item
                    title="Status"
                    desc={
                        <span
                            className={
                                status === CAB_ASSIGNED
                                    ? "text-success f-500"
                                    : "text-danger f-500"
                            }
                        >
                            {status}
                        </span>
                    }
                />
                <Item title="Company" desc={company} />
                <Item title="Cab Number" desc={cab_number} />
                <Item title="Driver Name" desc={driver_name} />
                <Item title="Assigned Time" desc={pickup} />
            </div>
        </div>
    );
};

const formKeys = {
    company: "company",
    driveName: "driver_name",
    cabnumber: "cab_number",
    assignedTime: "pickup_time",
    status: "status",
};

const allIds = [
    {
        name: formKeys.company,
        type: formTypes.text,
        inputProps: {
            placeholder: "Enter Cab Company",
        },
        required: true,
    },
    {
        name: formKeys.driveName,
        type: formTypes.text,
        inputProps: {
            placeholder: "Enter Drive Name",
        },
        required: true,
    },
    {
        name: formKeys.cabnumber,
        type: formTypes.text,
        inputProps: {
            placeholder: "Enter Cab Number",
        },
        required: true,
    },
    {
        name: formKeys.assignedTime,
        type: formTypes.datetime,
        inputProps: {
            placeholder: "Enter Assigned  Date time",
        },
        required: true,
    },
    {
        name: formKeys.status,
        type: formTypes.select,
        inputProps: {
            placeholder: "Select status",
            options: [
                {
                    label: CAB_ASSIGNED,
                    value: CAB_ASSIGNED,
                },
                {
                    label: CAB_CANCELLED,
                    value: CAB_CANCELLED,
                },
            ],
        },
        required: true,
    },
];

const AddHistory = ({ id, succFunc }) => {
    const validateForm = useValidateAll(allIds);
    const formatData = useFormatAll(allIds);

    return (
        <Form
            validateForm={validateForm}
            formatData={formatData}
            handleSuccess={succFunc}
            config={{
                url: salesCOUrls.allocateCab(id),
                method: "POST",
            }}
            resetOnSuccess
        >
            <FieldArray allIds={allIds} />
            <FormSubmitBtn className="btn btn-theme btn-block">
                Submit
            </FormSubmitBtn>
        </Form>
    );
};

const HistoryModal = ({ id }) => {
    const [state, { setUrl, loadMore, addData }] = useInfiniteList();

    useEffect(() => {
        setUrl({ baseUrl: salesCOUrls.allocateCab(id) });
    }, [setUrl, id]);

    const succFunc = useCallback(
        (data) => {
            addData(data);
        },
        [addData]
    );

    return (
        <ModalBody>
            <div className="row">
                <div className="col-md-6">
                    <AddHistory id={id} succFunc={succFunc} />
                </div>
                <div
                    className={`col-md-6`}
                    style={{ height: "320px", overflowY: "auto" }}
                >
                    <InfiniteList
                        loader={
                            <div className="flex-center">
                                <Loader size={26} />
                            </div>
                        }
                        {...state}
                        RenderItem={HistoryItem}
                        loadMore={loadMore}
                        emptyList={<EmptyList title="No data found" />}
                    />
                </div>
            </div>
        </ModalBody>
    );
};

const CabHistory = ({ id, projectId, isEdit }) => {
    const { toggle, onFalse, onTrue } = useToggle();

    return (
        <>
            <button onClick={onTrue} className="btn btn-theme btn-sm">
                Assign/View
            </button>
            {toggle && (
                <Modal
                    isOpen
                    close={onFalse}
                    contentStyle={{ maxWidth: "800px" }}
                >
                    <ModalHeader
                        title="Cab History"
                        desc="Add & view lead activities"
                        close={onFalse}
                    />
                    <HistoryModal
                        isEdit={isEdit}
                        id={id}
                        projectId={projectId}
                    />
                </Modal>
            )}
        </>
    );
};

export default CabHistory;
