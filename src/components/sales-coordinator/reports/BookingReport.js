import React, { useEffect, useState } from "react";
import urls, { salesCOUrls } from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import keysToQuery from "../../../utils/keysToQuery";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import Downloads from "./Downloads";
import Filters from "./Filters";

const tableData = [
    { th: "Project Name", td: ({ lead }) => lead?.project?.title ?? "" },
    {
        th: "Enquiry Date",
        td: ({ lead }) => <DateFormat format="ll" date={lead?.enquiry_date} />,
    },
    {
        th: "Assigned Date",
        td: ({ lead }) => <DateFormat date={lead?.assigned_on} />,
    },
    {
        th: "Booking Date",
        td: ({ created_on }) => <DateFormat date={created_on} />,
    },
    { th: "Enquiry No", td: ({ lead }) => lead?.lead_no },
    { th: "Customer Name", td: ({ lead }) => lead?.name },
    {
        th: "Flat No",
        td: ({ apartment_details }) => apartment_details?.alias_name ?? "",
    },
    {
        th: "Saleable Area",
        td: ({ apartment_details }) =>
            apartment_details?.sales_area_round ?? "",
    },
    {
        th: "Telecaller Name",
        td: ({ lead }) =>
            lead?.created_by?.user
                ? `${lead.created_by.user.first_name} ${lead.created_by.user.last_name}`
                : "",
    },
    {
        th: "Sales Managers Name",
        td: ({ lead }) =>
            lead?.assigned_to?.user
                ? `${lead.assigned_to.user.first_name} ${lead.assigned_to.user.last_name}`
                : "",
    },
    {
        th: "Source",
        td: ({ lead }) =>
            lead?.source_of_campaigning?.campaign?.source?.title ?? "",
    },
    {
        th: "Campaign",
        td: ({ lead }) => lead?.source_of_campaigning?.campaign.title ?? "",
    },
    {
        th: "Primary Source Campaign",
        td: ({ lead }) => lead?.source_of_campaigning?.title ?? "",
    },
];

const BookingReport = () => {
    const [state, { setUrl, setPage }] = usePagination();
    const [filters, setFilters] = useState({});

    const onFilter = (filters = {}) => {
        setFilters(filters);
    };

    useEffect(() => {
        setUrl({
            baseUrl: salesCOUrls.reports,
            filters: { ...filters, tab: 3 },
        });
    }, [setUrl, filters]);

    const q = keysToQuery(filters);

    return (
        <>
            <Filters onFilter={onFilter} />
            <Downloads
                pdfLink={`${urls.baseURL}users/gm/report/pdf/9/${q}`}
                excelLink={`${urls.baseURL}users/export/report/csv/3/${q}`}
            />
            <TableList
                pageState={state}
                onPageClick={setPage}
                tableData={tableData}
            />
        </>
    );
};

export default BookingReport;
