import React, { useEffect, useState } from "react";
import urls, { salesCOUrls } from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import keysToQuery from "../../../utils/keysToQuery";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import Downloads from "./Downloads";
import Filters from "./Filters";

const tableData = [
    {
        th: "Month",
        td: ({ created_on }) => <DateFormat date={created_on} format="MMM" />,
    },
    { th: "Pre Launch and Launch Details", td: () => "-" },
    {
        th: "Booking Date",
        td: (item) => (
            <DateFormat date={item?.created_on} format="DD/MM/YYYY" />
        ),
    },
    { th: "Financial Year", td: (item) => item?.financial_year ?? "-" },
    { th: "Quarter", td: ({ quarter }) => quarter },
    {
        th: "Customer Name",
        td: ({ salutation = "", name = "" }) => `${salutation} ${name}`,
    },
    {
        th: "Booked By",
        td: (item) =>
            `${item?.created_by?.user?.first_name ?? ""} ${
                item?.created_by?.user?.last_name ?? ""
            }`,
    },
    { th: "CRM Followed By", td: () => "-" },
    {
        th: "Source",
        td: (item) => item?.lead?.source_of_campaigning?.title ?? "-",
    },
    { th: "BSP/sqft", td: (item) => item?.basic_rate ?? "-" },
    {
        th: "Tower",
        td: (item) => item?.apartment_details?.floor?.tower?.title ?? "-",
    },
    {
        th: "Flat Type",
        td: (item) => item?.apartment_details?.apartment_type?.title ?? "-",
    },
    {
        th: "Flat No.",
        td: (item) => item?.apartment_details?.unit_no ?? "-",
    },
    {
        th: "Unit Number",
        td: (item) => item?.apartment_details?.alias_name ?? "-",
    },
    { th: "Size", td: (item) => item?.apartment_details?.sales_area ?? "-" },
    {
        th: "Private Terrace Area",
        td: (item) => item?.apartment_details?.terrace_area ?? "-",
    },
    { th: "Floor", td: (item) => item?.apartment_details?.floor?.title ?? "-" },
    { th: "FRC", td: (item) => item?.floor_rise_rate ?? "-" },
    { th: "PLC", td: (item) => item?.plc ?? "-" },
    {
        th: "Cheque/RTGS No",
        td: (item) => item?.booking_payment?.transaction_details ?? "-",
    },
    {
        th: "Cheque/RTGS Date",
        td: (item) => (
            <DateFormat
                date={item?.booking_payment?.transaction_date}
                format="DD/MM/YYYY"
            />
        ),
    },
    {
        th: "Booking Amount",
        td: (item) => item?.booking_payment?.amount ?? "-",
    },
    { th: "Total Basic Cost", td: (item) => item?.total_basic_amount ?? "-" },
    { th: "Total Cost", td: (item) => item?.net_amount ?? "-" },
    {
        th: "Actual Outstanding Amount",
        td: (item) => item?.total_collection?.total_outstanding,
    },
    {
        th: "AOA in %",
        td: (item) =>
            item?.total_collection?.total_outstanding_percentage ?? "-",
    },
    {
        th: "Till Date Collection Amount",
        td: (item) => item?.total_collection?.total_collection ?? "",
    },
    {
        th: "Collected % Till Date",
        td: (item) => item?.total_collection?.total_collection_percentage ?? "",
    },
    {
        th: "Booking Status",
        td: (item) => (item?.status ? "Booked" : "Cancelled"),
    },
];

const SalesReport = () => {
    const [state, { setUrl, setPage }] = usePagination();
    const [filters, setFilters] = useState({});

    const onFilter = (filters = {}) => {
        setFilters(filters);
    };

    useEffect(() => {
        setUrl({
            baseUrl: salesCOUrls.reports,
            filters: { ...filters, tab: 1 },
        });
    }, [setUrl, filters]);

    const q = keysToQuery(filters);

    return (
        <>
            <Filters onFilter={onFilter} />
            <Downloads
                pdfLink={`${urls.baseURL}users/gm/report/pdf/5/${q}`}
                excelLink={`${urls.baseURL}users/export/report/csv/1/${q}`}
            />
            <TableList
                pageState={state}
                onPageClick={setPage}
                tableData={tableData}
            />
        </>
    );
};

export default SalesReport;
