import React, { useEffect, useState } from "react";
import urls, { salesCOUrls } from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import keysToQuery from "../../../utils/keysToQuery";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import Status from "../../tele-caller/leads/Status";
import Downloads from "./Downloads";
import Filters from "./Filters";

const tableData = [
    { th: "Project Name", td: ({ project }) => project?.title ?? "" },
    {
        th: "Enquiry Date",
        td: ({ enquiry_date }) => (
            <DateFormat format="ll" date={enquiry_date} />
        ),
    },
    {
        th: "Assigned Date",
        td: ({ assigned_on }) => <DateFormat date={assigned_on} />,
    },
    { th: "Enquiry No", td: ({ lead_no }) => lead_no },
    { th: "Customer Name", td: ({ name }) => name },
    {
        th: "Telecaller Name",
        td: ({ created_by }) =>
            created_by?.user
                ? `${created_by.user.first_name} ${created_by.user.last_name}`
                : "",
    },
    {
        th: "Sales Managers Name",
        td: ({ assigned_to }) =>
            assigned_to?.user
                ? `${assigned_to.user.first_name} ${assigned_to.user.last_name}`
                : "",
    },
    {
        th: "Source",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign?.source?.title ?? "",
    },
    {
        th: "Campaign",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign.title ?? "",
    },
    {
        th: "Primary Source Campaign",
        td: ({ source_of_campaigning }) => source_of_campaigning?.title ?? "",
    },
    {
        th: "Site visit date",
        td: ({ site_visit_date }) => (
            <DateFormat format="ll" date={site_visit_date} />
        ),
    },
    { th: "Status", td: ({ status = {} }) => <Status {...status} /> },
];

const SiteVisitReport = () => {
    const [state, { setUrl, setPage }] = usePagination();
    const [filters, setFilters] = useState({});

    const onFilter = (filters = {}) => {
        setFilters(filters);
    };

    useEffect(() => {
        setUrl({
            baseUrl: salesCOUrls.reports,
            filters: { ...filters, tab: 2 },
        });
    }, [setUrl, filters]);

    const q = keysToQuery(filters);

    return (
        <>
            <Filters onFilter={onFilter} />
            <Downloads
                pdfLink={`${urls.baseURL}users/gm/report/pdf/8/${q}`}
                excelLink={`${urls.baseURL}users/export/report/csv/2/${q}`}
            />
            <TableList
                pageState={state}
                onPageClick={setPage}
                tableData={tableData}
            />
        </>
    );
};

export default SiteVisitReport;
