import React, { useMemo } from "react";
import useFormReducer from "../../../hooks/form/useFormReducer";
import { useMasterValues } from "../../common/MasterData";
import FieldArray from "../../form/FieldArray";
import { FormProvider } from "../../form/Form";
import formatAllData from "../../form/formatAllData";


const filters = (isLoading, { project = [] }) => [
    {
        name: "project",
        type: "select",
        inputProps: {
            options: project.map((x) => ({ label: x.title, value: x.id })),
            placeholder: "Project",
            isLoading,
            isClearable: true,
        },
        conClass: "col-md-4",
        required: false,
    },
    // {
    //     name: "date",
    //     type: "date",
    //     inputProps: {
    //         placeholder: "Date",
    //     },
    //     conClass: "col-md-4",
    //     required: false,
    // },
];

const Filters = ({ onFilter }) => {
    const master = useMasterValues();
    const form = useFormReducer();
    const [{ values }] = form;

    const allIds = useMemo(() => filters(...master), [master]);

    const onClick = () => {
        const data = formatAllData(allIds, values);
        onFilter(data);
    };

    return (
        <FormProvider form={form}>
            <div className="form-row align-items-stretch">
                <FieldArray allIds={allIds} />
                <div className="col-md-2">
                    <button
                        onClick={onClick}
                        className="btn btn-theme btn-block"
                    >
                        Search
                    </button>
                </div>
            </div>
        </FormProvider>
    );
};

export default Filters
