import React from "react";
// import ReactHTMLTableToExcel from "react-html-table-to-excel";
import {
    DownloadAsPDF,
    DownloadAsXL,
} from "../../sales-general-manager/DownloadLink";

const Downloads = ({ pdfLink, excelLink }) => {
    return (
        <div className="d-flex align-items-end form-group">
            <div className="mr-3">
                <DownloadAsXL link={excelLink} />
            </div>
            <div className="mr-3">
                <DownloadAsPDF link={pdfLink} />
            </div>
        </div>
    );
};

export default Downloads;
