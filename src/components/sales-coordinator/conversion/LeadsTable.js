import React, { useEffect } from "react";
import { Link, useRouteMatch } from "react-router-dom";
import { salesCOUrls } from "../../../apiService/urls";
import usePagination from "../../../hooks/http/usePagination";
import DateFormat from "../../common/DateFormat";
import TableList from "../../common/TableList";
import LeadEmail from "../../tele-caller/leads/LeadEmail";
import LeadNumber from "../../tele-caller/leads/LeadNumber";
import Status from "../../tele-caller/leads/Status";
import ViewLeadLink from "../../tele-caller/leads/ViewLeadLink";

const ConvertLink = ({ id }) => {
    const { url } = useRouteMatch();

    return (
        <Link
            to={`${url}/convert/${id}`}
            className="btn btn-sm f-14 btn-theme rounded-pill"
        >
            Convert
        </Link>
    );
};

const tbData = [
    {
        th: "Enquiry No",
        td: ({ id, lead_no = "" }) => <ViewLeadLink id={id}>{lead_no}</ViewLeadLink>,
    },
    {
        th: "Project",
        td: ({ project }) => project?.title ?? "",
    },
    {
        th: "Enquiry On",
        td: ({ enquiry_date }) => (
            <DateFormat format="ll" date={enquiry_date} />
        ),
    },
    {
        th: "Customer Name",
        td: ({ name }) => name,
    },
    {
        th: "Customer Email",
        td: LeadEmail,
    },
    {
        th: "Customer Phone",
        td: LeadNumber,
    },
    {
        th: "Source",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign?.source?.title ?? "",
    },
    {
        th: "Campaign",
        td: ({ source_of_campaigning }) =>
            source_of_campaigning?.campaign.title ?? "",
    },
    {
        th: "Primary Source",
        td: ({ source_of_campaigning }) => source_of_campaigning?.title ?? "",
    },
    {
        th: "Status",
        td: ({ status = {} }) => <Status {...status} />,
    },
    {
        th: "Created On",
        td: ({ created_on }) => <DateFormat date={created_on} />,
    },
    {
        th: "Tele-caller",
        td: ({ created_by }) =>
            created_by?.user
                ? `${created_by.user.first_name} ${created_by.user.last_name}`
                : "",
    },
    {
        th: "Assigned On",
        td: ({ assigned_on }) => <DateFormat date={assigned_on} />,
    },
    {
        th: "Sales Person",
        td: ({ assigned_to }) =>
            assigned_to?.user
                ? `${assigned_to.user.first_name} ${assigned_to.user.last_name}`
                : "",
    },
    {
        th: "Convert",
        td: ({ id }) => <ConvertLink id={id} />,
    },
];

const LeadsTable = ({ tab = 1, project, employee_id, team }) => {
    const [state, { setUrl, setPage }] = usePagination();

    useEffect(() => {
        setUrl({
            baseUrl: salesCOUrls.conversion,
            filters: { tab, project, employee_id, team },
        });
    }, [setUrl, tab, project, employee_id, team]);

    return (
        <>
            <TableList
                pageState={state}
                onPageClick={setPage}
                tableData={tbData}
            />
        </>
    );
};

export default LeadsTable;
