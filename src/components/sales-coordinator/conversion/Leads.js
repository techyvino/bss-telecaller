/* eslint-disable eqeqeq */
import React, { useEffect, useMemo } from "react";
import { useHistory } from "react-router";
import { salesCOUrls, salesGmUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import useQuery from "../../../hooks/useQuery";
import { ProjectSelect } from "../../../pages/sales-manager/Bookings";
import { isArray } from "../../../utils";
import keysToQuery from "../../../utils/keysToQuery";
import DynamicContent from "../../common/DynamicContent";
import UrlParamTabList from "../../common/UrlParamTabList";
import LeadsTable from "./LeadsTable";

const LeadsWithFilters = ({ projects = [], teams = [] }) => {
    const query = useQuery();
    const history = useHistory();
    const project_id = query.get("project") ?? "";
    const team = query.get("team") ?? "";
    const employee_id = query.get("employee") ?? "";

    const team_mates = useMemo(() => {
        const teamId = team ? parseInt(team, 10) : "";
        const list = teams.filter((x) => x.value === teamId)?.[0];
        if (list && isArray(list.team_mates) && list.employee) {
            return [list.employee, ...list.team_mates];
        }
        return [];
    }, [teams, team]);
    const employees = isArray(team_mates) ? team_mates : [];

    const url = useMemo(() => {
        const filters = keysToQuery({ project: project_id, employee_id, team });
        return `${salesCOUrls.conversionTab}${filters}`;
    }, [project_id, employee_id, team]);

    return (
        <>
            <div className="form-row">
                <div className="col-md-3 mb-3">
                    <ProjectSelect
                        onChange={(val) => {
                            if (val) {
                                query.set("project", val);
                            } else {
                                query.delete("project");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        options={projects}
                        value={project_id}
                    />
                </div>
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("team", val);
                            } else {
                                query.delete("team");
                            }
                            query.set("employee", val);
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={team}
                    >
                        <option value="">All Teams</option>
                        {teams.map((x) => (
                            <option value={x.value} key={x.value}>
                                {x.label}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("employee", val);
                            } else {
                                query.delete("employee");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={employee_id}
                    >
                        <option value="">All Sales Person</option>
                        {employees.map((x) => (
                            <option value={x.id} key={x.id}>
                                {`${x?.user?.first_name ?? ""} ${
                                    x?.user?.last_name ?? ""
                                }`}
                            </option>
                        ))}
                    </select>
                </div>
            </div>
            <UrlParamTabList url={url}>
                {({ id }) => (
                    <LeadsTable
                        tab={id}
                        project={project_id}
                        team={team}
                        employee_id={employee_id}
                    />
                )}
            </UrlParamTabList>
        </>
    );
};

const mapTeams = (team = []) =>
    team.map((x) => ({
        value: x.head.id,
        label: `${x?.head?.user?.first_name ?? ""} ${
            x?.head?.user?.last_name ?? ""
        }`,
        team_mates: x.team_mates,
        employee: { id: x.head.id, user: x.head.user },
    }));

const Leads = ({ projects = [] }) => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(salesGmUrls.salesTeamLead);
    }, [setUrl]);

    const isLoaded = isArray(state.data);

    return (
        <DynamicContent fetching={state.fetching} isLoaded={isLoaded}>
            {isLoaded && (
                <LeadsWithFilters
                    projects={projects}
                    teams={mapTeams(state.data)}
                />
            )}
        </DynamicContent>
    );
};

export default Leads;
