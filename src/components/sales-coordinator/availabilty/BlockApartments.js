import React, { useCallback, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import ApartmentModal from "../../sales-manager/availability/ApartmentModal";
import { AvailWithProjects } from "../../sales-manager/availability/Avail";
import {
    AVAILABLE,
    BLOCKED,
    BOOKED,
} from "../../sales-manager/availability/StatusBox";
import BlockModal from "./BlockModal";

const uniq = (list = {}, cur) => {
    if (list[cur.id]) {
        //remove item immutably
        const key = cur.id;
        const { [key]: value, ...withoutSecond } = list;
        return withoutSecond;
    }
    return { ...list, [cur.id]: cur };
};

const BlockApartments = () => {
    const location = useLocation();
    const [apartments, setApartments] = useState({});
    const [details, setDetails] = useState(null);

    useEffect(() => {
        setApartments({});
    }, [location]);

    const onAptClick = useCallback(
        (apartment = {}) => {
            if (apartment.id && apartment.status === AVAILABLE) {
                setApartments((apt) => uniq(apt, apartment));
            }
            if (apartment.status === BOOKED) {
                setDetails({
                    status: BOOKED,
                    apartmentId: apartment?.id,
                    booking: apartment?.booking_details,
                    blocking: apartment?.blocking_details,
                });
                return false;
            }
            if (apartment.status === BLOCKED) {
                setDetails({
                    status: BLOCKED,
                    blocking: apartment?.blocking_details,
                    booking: apartment?.booking_details,
                    apartmentId: apartment?.id,
                });
                return false;
            }
        },
        [setApartments]
    );

    const list = Object.keys(apartments);

    return (
        <>
            <AvailWithProjects selected={apartments} onAptClick={onAptClick} />
            {list.length > 0 && (
                <BlockModal apartments={apartments} selected={list} />
            )}
            {details && (
                <ApartmentModal
                    title="Details"
                    {...details}
                    close={() => setDetails(null)}
                />
            )}
        </>
    );
};

export default BlockApartments;
