import dayjs from "dayjs";
import React, { useCallback } from "react";
import { isDate } from "validate.js";
import { salesManagerUrls } from "../../../apiService/urls";
import useToggle from "../../../hooks/useToggle";
import Portal from "../../common/Portal";
import FieldArray from "../../form/FieldArray";
import Form from "../../form/Form";
import FormSubmitBtn from "../../form/FormSubmitBtn";
import formTypes from "../../form/formTypes";
import Modal from "../../modal/Modal";
import ModalBody from "../../modal/ModalBody";
import ModalHeader from "../../modal/ModalHeader";

const formKeys = {
    date: "blocking",
    reason: "reason",
};

const allIds = (id) => [
    {
        name: `${formKeys.date}---${id}`,
        type: formTypes.daterange,
        inputProps: {
            placeholder: "Date",
        },
        required: true,
        conClass: "col-6",
    },
    {
        name: `${formKeys.reason}---${id}`,
        type: formTypes.textarea,
        inputProps: {
            placeholder: "Reason",
            style: { minHeight: "70px" },
        },
        required: true,
        conClass: "col-6",
    },
];

const validateDate = (date = {}) => {
    if (!isDate(date?.from)) {
        return "From date is required";
    }
    if (!isDate(date?.to)) {
        return "To date is required";
    }
    return "";
};

const validate = (id, values = {}) => {
    let err = {};
    if (!values?.[`${formKeys.reason}---${id}`]) {
        err[`${formKeys.reason}---${id}`] = "Reason is required";
    }
    const date = values?.[`${formKeys.date}---${id}`] ?? {};
    const dateErr = validateDate(date);
    if (dateErr) {
        err[`${formKeys.date}---${id}`] = dateErr;
    }
    return err;
};

const Block = ({ selected = [], apartments = {} }) => {
    const validateForm = useCallback(
        (values) => {
            let errors = {};
            selected.forEach((id) => {
                Object.assign(errors, validate(id, values));
            });
            return errors;
        },
        [selected]
    );

    const formatData = useCallback(
        (values) => {
            let data = [];
            selected.forEach((id) => {
                const reason = values?.[`${formKeys.reason}---${id}`] ?? "";
                const blocking = values?.[`${formKeys.date}---${id}`] ?? {};
                data.push({
                    reason,
                    blocking_from: blocking?.from
                        ? dayjs(blocking?.from).format("YYYY-MM-DD")
                        : "",
                    blocking_to: blocking?.to
                        ? dayjs(blocking?.to).format("YYYY-MM-DD")
                        : "",
                    apartment: id,
                });
            });
            return data;
        },
        [selected]
    );

    return (
        <Form
            validateForm={validateForm}
            formatData={formatData}
            config={{
                url: salesManagerUrls.blockApartment,
                method: "POST",
            }}
            handleSuccess={() => {
                window.location.reload();
            }}
            resetOnSuccess
        >
            {selected.map((id) => (
                <div className="form-row align-items-center" key={id}>
                    <div className="col-12 mb-1 f-500">
                        Apartment - {apartments?.[id]?.alias_name ?? ""}
                    </div>
                    <FieldArray allIds={allIds(id)} />
                </div>
            ))}
            <FormSubmitBtn className="btn btn-theme btn-block">
                Submit
            </FormSubmitBtn>
        </Form>
    );
};

const BlockModal = ({ selected, apartments }) => {
    const { toggle, onFalse, onTrue } = useToggle();

    return (
        <Portal>
            <button
                onClick={onTrue}
                className="btn btn-theme rounded-pill block-btn"
            >
                Block ({selected.length})
            </button>
            <Modal
                isOpen={toggle}
                close={onFalse}
                contentStyle={{ maxWidth: "1000px" }}
            >
                <ModalHeader title="Block Apartments" close={onFalse} />
                <ModalBody>
                    <Block selected={selected} apartments={apartments} />
                </ModalBody>
            </Modal>
        </Portal>
    );
};

export default BlockModal;
