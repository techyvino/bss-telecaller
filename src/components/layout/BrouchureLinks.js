/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { ChevronDown } from "react-feather";
import { isArray } from "../../utils";
import { useMasterValues } from "../common/MasterData";
import { HeaderMenuItem } from "./HeaderMenu";

const BrouchureLinks = () => {
    const [fetching, data] = useMasterValues();

    const projects = isArray(data?.project) ? data.project : [];

    if (fetching) {
        return null;
    }
    const brochures = projects.filter((x) => !!x.brochure);
    const websites = projects.filter((x) => !!x.website_link);

    return (
        <>
            {brochures.length > 0 && (
                <HeaderMenuItem extraCls="drop-menu-con">
                    Brochures <ChevronDown size={14} />
                    <ul className="drop-menu">
                        {brochures.map((x) => (
                            <li key={x.id}>
                                <a
                                    className="text-body"
                                    href={x.brochure}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    {x.title}
                                </a>
                            </li>
                        ))}
                    </ul>
                </HeaderMenuItem>
            )}
            {websites.length > 0 && (
                <HeaderMenuItem extraCls="drop-menu-con">
                    Websites <ChevronDown size={14} />
                    <ul className="drop-menu">
                        {websites.map((x) => (
                            <li key={x.id}>
                                <a
                                    className="text-body"
                                    href={x.website_link}
                                    download={x.title}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    {x.title}
                                </a>
                            </li>
                        ))}
                    </ul>
                </HeaderMenuItem>
            )}
        </>
    );
};

export default BrouchureLinks;
