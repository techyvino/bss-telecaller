import React from "react";

const Section = ({ children, fluid = false }) => {
    return (
        <section className="pt-4 pb-4">
            <div className={`container${fluid ? "-fluid" : ""}`}>
                {children}
            </div>
        </section>
    );
};

export const SectionTitle = ({ title = "" }) => {
    return <div className="mb-3 h4 f-500">{title}</div>;
};

export default Section;
