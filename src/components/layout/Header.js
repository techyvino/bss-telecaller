import React, { useState } from "react";
import logo from "../../images/logo.png";
import { NavLink } from "react-router-dom";
import { Search } from "react-feather";
import UserMenu from "./UserMenu";
import useUser from "../../hooks/user/useUser";

const Header = ({ headerRight = null }) => {
    const { token } = useUser()[0];

    return (
        <div className="header">
            <div className="d-flex align-items-center">
                <NavLink to="/">
                    <img className="logo" src={logo} alt="" />
                </NavLink>
                {token && (
                    <div className="ml-3 mr-3 d-none d-md-block">
                        <SearchLeads />
                    </div>
                )}
            </div>
            {token && (
                <div className="d-flex align-items-stretch">
                    <ul className="header-right">{headerRight}</ul>
                    <UserMenu />
                </div>
            )}
        </div>
    );
};

const SearchLeads = () => {
    const [value, setValue] = useState()

    return (
        <div className="d-flex border align-items-stretch rounded-pill pl-3 pr-3">
            <input
                className="form-control border-0"
                type="search"
                placeholder="Search"
                onChange={(e) => { setValue(e.target.value) }}
            />
            <NavLink to={{ pathname: `search`,  search: `?q=${value}`}}  className="btn p-0">
                <Search strokeWidth={2} size={22} />
            </NavLink>
        </div>
    );
};

export default Header;
