import React from "react";
import { ChevronDown } from "react-feather";
import useUser from "../../hooks/user/useUser";
import { Link } from "react-router-dom";

const UserMenu = () => {
    const { token, user } = useUser()[0];

    if (!token) {
        return null;
    }
    return (
        <div className="hello-block text-center ml-4 drop-menu-con">
            <small className="f-12">Your Account</small>
            <div className="font-weight-bold f-14">
                {user && user.first_name ? user.first_name : "User"}{" "}
                <ChevronDown size={14} />
            </div>
            <ul className="drop-menu">
                <li>
                    <Link className="text-body" to="/change-password">
                        Change Password
                    </Link>
                </li>
                <li>
                    <Link className="text-body" to="/logout">
                        Logout
                    </Link>
                </li>
            </ul>
        </div>
    );
};

export default UserMenu;
