/* eslint-disable eqeqeq */
import React, { useEffect, useMemo } from "react";
import { useHistory } from "react-router";
import { salesGmUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import useQuery from "../../../hooks/useQuery";
import { isArray } from "../../../utils";
import DynamicContent from "../../common/DynamicContent";
import LeadTabs from "../../sales-manager/leads/LeadTabs";

const LeadsWithFilters = ({ projects = [], teams = [] }) => {
    const query = useQuery();
    const history = useHistory();
    const team = query.get("team") ?? "";
    const project_id = query.get("project") ?? "";
    const filters = JSON.parse(query.get("filters"));

    const team_mates = useMemo(() => {
        const teamId = team ? parseInt(team, 10) : "";
        const list = teams.filter((x) => x.value === teamId)?.[0];
        if (list && isArray(list.team_mates) && list.employee) {
            return [list.employee, ...list.team_mates];
        }
        return [];
    }, [teams, team]);

    const employees = isArray(team_mates) ? team_mates : [];
    const employee_id = query.get("employee") ?? "";

    return (
        <>
            <div className="row">
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("team", val);
                            } else {
                                query.delete("team");
                            }
                            query.set("employee", val);
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={team}
                    >
                        <option value="">All Teams</option>
                        {teams.map((x) => (
                            <option value={x.value} key={x.value}>
                                {x.label}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("employee", val);
                            } else {
                                query.delete("employee");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={employee_id}
                    >
                        <option value="">All Sales Person</option>
                        {employees.map((x) => (
                            <option value={x.id} key={x.id}>
                                {`${x?.user?.first_name ?? ""} ${
                                    x?.user?.last_name ?? ""
                                }`}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("project", val);
                            } else {
                                query.delete("project");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={project_id}
                    >
                        <option value="">All Projects</option>
                        {projects.map((x) => (
                            <option value={x.id} key={x.id}>
                                {x.title}
                            </option>
                        ))}
                    </select>
                </div>
            </div>
            <LeadTabs
                filters={filters}
                team={team}
                employeeId={employee_id}
                projectID={project_id}
            />
        </>
    );
};

const mapTeams = (team = []) =>
    team.map((x) => ({
        value: x.head.id,
        label: `${x?.head?.user?.first_name ?? ""} ${
            x?.head?.user?.last_name ?? ""
        }`,
        team_mates: x.team_mates,
        employee: { id: x.head.id, user: x.head.user },
    }));

const Leads = ({ projects = [] }) => {
    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(salesGmUrls.salesTeamLead);
    }, [setUrl]);

    const isLoaded = isArray(state.data);

    return (
        <DynamicContent fetching={state.fetching} isLoaded={isLoaded}>
            {isLoaded && (
                <LeadsWithFilters
                    projects={projects}
                    teams={mapTeams(state.data)}
                />
            )}
        </DynamicContent>
    );
};

export default Leads;
