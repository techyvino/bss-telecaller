import React from "react";
import pdfIcon from "../../images/file-icons/pdf.svg";
import csvIcon from "../../images/file-icons/csv.svg";

const DownloadLink = ({ icon, link }) => {
    return (
        <a
            className="btn p-0"
            target="_blank"
            rel="noopener noreferrer"
            href={link}
        >
            <img src={icon} alt="" height="30" />
        </a>
    );
};

export const DownloadAsPDF = ({ link }) => {
    return <DownloadLink icon={pdfIcon} link={link} />;
};

export const DownloadAsXL = ({ link }) => {
    return <DownloadLink icon={csvIcon} link={link} />;
};

export default DownloadLink;
