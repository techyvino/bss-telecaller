import React from "react";

const SwappingReport = () => {
    return (
        <div className="table-responsive">
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th rowSpan="2">Sl no</th>
                        <th colSpan="7">Swapped From</th>
                        <th colSpan="6">Swapped To</th>
                    </tr>
                    <tr>
                        <th>Customer Name</th>
                        <th>Tower</th>
                        <th>Floor</th>
                        <th>Flat No</th>
                        <th>Saleable Area</th>
                        <th>Rate per Sq.ft</th>
                        <th>Booking Date</th>
                        <th>Swapping Date</th>
                        <th>Tower</th>
                        <th>Floor</th>
                        <th>Flat No</th>
                        <th>Saleable Area</th>
                        <th>Rate per Sq.ft</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    );
};

export default SwappingReport;
