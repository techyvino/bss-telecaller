import React, { useEffect, useMemo, useState } from "react";
import urls, { salesGmUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import { isArray, toBoolean } from "../../../utils";
import DynamicContent from "../../common/DynamicContent";
// import keysToQuery from "../../../utils/keysToQuery";
import Downloads from "./Downloads";
import SelectIp from "../../form/SelectIp";
import { useMasterValues } from "../../common/MasterData";
import keysToQuery from "../../../utils/keysToQuery";

const Tr = ({ name, post_launch, pre_launch, booking_month = [] }) => {
    return (
        <tr>
            <td>{name}</td>
            <td>{pre_launch}</td>
            <td>{post_launch}</td>
            {booking_month.map((x, idx) => (
                <td key={`${idx}`}>{x.count}</td>
            ))}
        </tr>
    );
};

const tableId = "reports";

const Report1 = ({ list }) => {
    const bookingMonths = isArray(list[0]?.booking_month)
        ? list[0].booking_month
        : [];
    return (
        <div className="table-responsive">
            <table
                className="table table-bordered text-center f-14"
                id={tableId}
            >
                <thead>
                    <tr>
                        <th>Manager's Name</th>
                        <th>Pre Launch</th>
                        <th>Post Launch</th>
                        {bookingMonths.map((x, idx) => (
                            <th key={`${idx}`}>{x.date}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {list.map((x, idx) => (
                        <Tr {...x} idx={idx} key={idx} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

const ReportItem = ({ title, value }) => {
    return (
        <td>
            <table style={{ width: "100%", border: "1px solid #dee2e6", backgroundColor: "#fff" }}>
                <tbody>
                    <tr>
                        <td style={{ textAlign: "left" }}>{title}</td>
                    </tr>
                    <tr>
                        <th style={{ fontSize: "28px", fontWeight: "500", textAlign: "right" }}>
                            {value}
                        </th>
                    </tr>
                </tbody>
            </table>
        </td>
    );
};

const tab = 3;
const mailContentId = "mail-content";

const Boooking = ({ project }) => {
    const [state, { setUrl }] = useFetchData();
    const query = useMemo(() => keysToQuery({ tab, project }), [project]);

    useEffect(() => {
        setUrl(`${salesGmUrls.reports}${query}`);
    }, [setUrl, query]);

    const isLoaded = toBoolean(state.data?.report_1);

    return (
        <>
            <DynamicContent isLoaded={isLoaded} fetching={state.fetching}>
                {isLoaded && (
                    <>
                        <Downloads
                            title="Weekly  Sales Review Report"
                            tableId={tableId}
                            pdfLink={`${urls.baseURL}users/gm/report/pdf/4/${query}`}
                            mailContentId={mailContentId}
                        />
                        <div id={mailContentId}>
                            {isArray(state.data?.report_2) &&
                                state.data.report_2.length > 0 && (
                                    <table className="table table-borderless" style={{ backgroundColor: "transparent" }}>
                                        <tbody>
                                            <tr>
                                                {state.data.report_2.map(
                                                    (item) => (
                                                        <ReportItem
                                                            {...item}
                                                            key={item.title}
                                                        />
                                                    )
                                                )}
                                            </tr>
                                        </tbody>
                                    </table>
                                )}
                            {isArray(state.data?.report_1) &&
                                state.data.report_1.length > 0 && (
                                    <Report1 list={state.data.report_1} />
                                )}
                        </div>
                    </>
                )}
            </DynamicContent>
        </>
    );
};

const WithProject = ({ options = [] }) => {
    const [project, setProject] = useState(options[0]);
    return (
        <>
            <div className="row">
                <div className="col-md-4 mb-3">
                    <SelectIp
                        options={options}
                        setValue={(val) => setProject(val)}
                        value={project}
                    />
                </div>
            </div>
            {project && project.value && <Boooking project={project.value} />}
        </>
    );
};

const BoookingReport = () => {
    const [fetching, data] = useMasterValues();

    const isLoaded = isArray(data?.project) && data.project.length > 0;

    return (
        <DynamicContent isLoaded={isLoaded} fetching={fetching}>
            {isLoaded && (
                <WithProject
                    options={data.project.map((x) => ({
                        label: x.title,
                        value: x.id,
                    }))}
                />
            )}
        </DynamicContent>
    );
};

export default BoookingReport;
