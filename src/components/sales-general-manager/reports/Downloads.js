import React, { useState } from "react";
import { DownloadAsPDF } from "../DownloadLink";
import ReactHTMLTableToExcel from "react-html-table-to-excel";
import mailIcon from "../../../images/file-icons/gmail.svg";
import useToggle from "../../../hooks/useToggle";
import Modal from "../../modal/Modal";
import ModalHeader from "../../modal/ModalHeader";
import ModalBody from "../../modal/ModalBody";
import SubmitBtn from "../../form/SubmitBtn";
import CreatableSelect from "react-select/creatable";
import FieldCon from "../../form/FieldCon";
import useSubmit from "../../../hooks/http/useSubmit";
import { toast } from "react-toastify";
import { salesGmUrls } from "../../../apiService/urls";
import useFormReducer from "../../../hooks/form/useFormReducer";
import useField from "../../../hooks/form/useField";
import { FormProvider } from "../../form/Form";
import Field from "../../form/Field";
import { isArray } from "validate.js";
import { setErrors } from "../../../hooks/form/formReducer";

const components = {
    DropdownIndicator: null,
};

const createOption = (label) => ({
    label,
    value: label,
});

const keyTypes = ["Enter", "Tab"];

const isEmail = (val = "") =>
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        val
    );

const MailIp = ({ name, ...props }) => {
    const { err, value = [], setValue } = useField(name);
    const [to, setTo] = useState("");

    const setMail = (val) => {
        if (isEmail(val)) {
            setValue([...(value || []), createOption(val)]);
            setTo("");
        }
    };

    const handleKeyDown = (event) => {
        if (!to) return;
        if (keyTypes.includes(event.key)) {
            setMail(to);
        }
    };

    const handleBlur = () => {
        if (!to) return;
        setMail(to);
    };

    return (
        <FieldCon
            err={err}
            infoText="Enter valid email address & press Tab or Enter key to add new email address"
        >
            <CreatableSelect
                components={components}
                inputValue={to}
                isClearable
                isMulti
                menuIsOpen={false}
                onChange={(val) => setValue(val)}
                onInputChange={(val) => setTo(val)}
                onKeyDown={handleKeyDown}
                classNamePrefix="react-select"
                value={value}
                onBlur={handleBlur}
                {...props}
            />
        </FieldCon>
    );
};

const formkeys = {
    to: "to",
    cc: "cc",
    bcc: "bcc",
    subject: "subject",
    content: "content",
};

const validateMail = (val, required = false) => {
    if (required) {
        return isArray(val) && val.length > 0 ? "" : "This field is required";
    }
    return "";
};

const getMails = (val) => (isArray(val) ? val.map((x) => x.value) : []);

const validate = (values = {}) => {
    let errors = {};
    const toErr = validateMail(values[formkeys.to], true);
    if (toErr) {
        Object.assign(errors, { [formkeys.to]: toErr });
    }
    const ccErr = validateMail(values[formkeys.cc], false);
    if (toErr) {
        Object.assign(errors, { [formkeys.cc]: ccErr });
    }
    const bccErr = validateMail(values[formkeys.bcc], false);
    if (toErr) {
        Object.assign(errors, { [formkeys.bcc]: bccErr });
    }
    if (!values[formkeys.subject]) {
        Object.assign(errors, { [formkeys.subject]: "This field is required" });
    }
    return errors;
};

const MailForm = ({ mailContentId, close }) => {
    const form = useFormReducer();
    const [{ values }, dispatch] = form;
    const __html = document?.getElementById(mailContentId)?.innerHTML ?? "";

    const [fetching, submit] = useSubmit({
        success: () => {
            toast.success("Mail Sent");
            close();
        },
    });

    const onSubmit = () => {
        const errors = validate(values);
        if (Object.keys(errors).length > 0) {
            dispatch(setErrors(errors));
            return;
        }
        let data = {};
        data.body_content = __html;
        data[formkeys.to] = getMails(values[formkeys.to]);
        if (isArray(values[formkeys.cc]) && values[formkeys.cc].length > 0) {
            data[formkeys.cc] = getMails(values[formkeys.cc]);
        }
        if (isArray(values[formkeys.bcc]) && values[formkeys.bcc].length > 0) {
            data[formkeys.bcc] = getMails(values[formkeys.bcc]);
        }
        data[formkeys.subject] = values[formkeys.subject];
        if (values[formkeys.content]) {
            data[formkeys.content] = values[formkeys.content];
        }
        submit({
            url: salesGmUrls.sendMail,
            method: "POST",
            data,
        });
    };

    return (
        <FormProvider form={form}>
            <MailIp placeholder="To" name={formkeys.to} />
            <MailIp placeholder="CC" name={formkeys.cc} />
            <MailIp placeholder="BCC" name={formkeys.bcc} />
            <Field
                name={formkeys.subject}
                type="text"
                inputProps={{ placeholder: "Enter Subject" }}
            />
            <Field
                name={formkeys.content}
                type="textarea"
                inputProps={{ placeholder: "Enter Content" }}
            />
            <div className="form-group">
                <div style={{ overflow: "auto", maxHeight: "200px" }}>
                    <div dangerouslySetInnerHTML={{ __html }}></div>
                </div>
            </div>
            <SubmitBtn
                className="btn btn-theme btn-block"
                fetching={fetching}
                onClick={onSubmit}
            >
                Send
            </SubmitBtn>
        </FormProvider>
    );
};

const SendMail = ({ mailContentId }) => {
    const { toggle, onFalse, onTrue } = useToggle();

    return (
        <>
            <button onClick={onTrue} className="btn p-0">
                <img src={mailIcon} alt="" height="30" />
            </button>
            <Modal isOpen={toggle} close={onFalse}>
                <ModalHeader title="Send Mail" close={onFalse} />
                <ModalBody>
                    <MailForm mailContentId={mailContentId} close={onFalse} />
                </ModalBody>
            </Modal>
        </>
    );
};

const Downloads = ({
    pdfLink,
    tableId,
    title = "Report",
    mailContentId = "",
}) => {
    return (
        <div className="d-flex align-items-end form-group">
            {tableId && (
                <div className="mr-3">
                    <ReactHTMLTableToExcel
                        table={tableId}
                        filename={title}
                        sheet={title}
                        buttonText="Download Excel"
                        className="btn btn-outline-black btn-sm"
                    />
                </div>
            )}
            {pdfLink && (
                <div className="mr-3">
                    <DownloadAsPDF link={pdfLink} />
                </div>
            )}
            {mailContentId && (
                <div className="mr-3">
                    <SendMail mailContentId={mailContentId} />
                </div>
            )}
        </div>
    );
};

export default Downloads;
