import React, { useEffect, useMemo, useState } from "react";
import urls, { salesGmUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import { isArray } from "../../../utils";
import keysToQuery from "../../../utils/keysToQuery";
import DateFormat from "../../common/DateFormat";
import DynamicContent from "../../common/DynamicContent";
import Downloads from "./Downloads";
import Filters from "./Filters";

const tbData = [
    "Month",
    "Booking Date",
    "Financial Year",
    "Quarter",
    "Customer Name",
    "Booked By",
    "CRM Followed By",
    "Source",
    "Project Name",
    "BSP/sqft",
    "Tower",
    "Flat Type",
    "Unit Number",
    "Size",
    "Floor",
    "FRC",
    "PLC",
    "Cheque/RTGS No",
    "Cheque/RTGS Date",
    "Booking Amount",
    "Total Basic Cost",
    "Total Cost",
    "Actual Outstanding Amount",
    "AOA in %",
    "Till Date Collection Amount",
    "Collected % Till Date",
    "Booking Status",
    "Cancellation Month",
    "Cancellation Date",
    "Cheque Deposited Yes/No",
    "Cheque Deposited Date",
    "Agreement",
];

const tableId = "reports";
const mailContentId = "mail-content";

const Report = ({ list }) => {
    return (
        <div className="table-responsive">
            <table
                className="table table-bordered text-center f-14"
                id={tableId}
            >
                <thead>
                    <tr>
                        <td>Sl no.</td>
                        {tbData.map((x) => (
                            <th key={x}>{x}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {list.map((item, idx) => (
                        <tr key={`${item.id}`}>
                            <td>{idx + 1}</td>
                            <td>
                                <DateFormat
                                    date={item?.created_on}
                                    format="MMM"
                                />
                            </td>
                            <td>
                                <DateFormat
                                    date={item?.created_on}
                                    format="DD/MM/YYYY"
                                />
                            </td>
                            <td>{item?.financial_year ?? "-"}</td>
                            <td>{item?.quarter ?? "-"}</td>
                            <td>{`${item.salutation} ${item.name}`}</td>
                            <td>{`${item?.created_by?.user?.first_name ?? ""} ${
                                item?.created_by?.user?.last_name ?? ""
                            }`}</td>
                            <td>{"-"}</td>
                            <td>
                                {item?.lead?.source_of_campaigning?.title ??
                                    "-"}
                            </td>
                            <td>
                                {item?.apartment_details?.floor?.tower?.project
                                    ?.title ?? "-"}
                            </td>
                            <td>{item?.basic_rate ?? "-"}</td>
                            <td>
                                {item?.apartment_details?.floor?.tower?.title ??
                                    "-"}
                            </td>
                            <td>
                                {item?.apartment_details?.apartment_type
                                    ?.title ?? "-"}
                            </td>
                            <td>
                                {item?.apartment_details?.alias_name ?? "-"}
                            </td>
                            <td>
                                {item?.apartment_details?.sales_area ?? "-"}
                            </td>
                            <td>
                                {item?.apartment_details?.floor?.title ?? "-"}
                            </td>
                            <td>{item?.floor_rise_rate ?? "-"}</td>
                            <td>{item?.plc ?? "-"}</td>
                            <td>
                                {item?.booking_payment?.transaction_details ??
                                    "-"}
                            </td>
                            <td>
                                <DateFormat
                                    date={
                                        item?.booking_payment?.transaction_date
                                    }
                                    format="DD/MM/YYYY"
                                />
                            </td>
                            <td>{item?.booking_payment?.amount ?? "-"}</td>
                            <td>{item?.total_basic_amount ?? "-"}</td>
                            <td>{item?.net_amount ?? "-"}</td>
                            <td>{item?.total_collection?.total_outstanding}</td>
                            <td>
                                {
                                    item?.total_collection
                                        ?.total_outstanding_percentage
                                }
                            </td>
                            <td>{item?.total_collection?.total_collection}</td>
                            <td>
                                {
                                    item?.total_collection
                                        ?.total_collection_percentage
                                }
                            </td>
                            <td>{item?.status ? "Booked" : "Cancelled"}</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

const tab = 4;

const SalesSheetReport = () => {
    const [state, { setUrl }] = useFetchData();
    const [filters, setFilters] = useState({});

    const query = useMemo(() => keysToQuery({ tab, ...filters }), [filters]);

    useEffect(() => {
        setUrl(`${salesGmUrls.reports}${query}`);
    }, [setUrl, query]);

    const onFilter = (filters = {}) => {
        setFilters(filters);
    };

    const isLoaded = isArray(state.data);

    return (
        <>
            <Filters onFilter={onFilter} />
            <DynamicContent isLoaded={isLoaded} fetching={state.fetching}>
                {isLoaded && (
                    <>
                        <Downloads
                            title="Sales Sheet Report"
                            tableId={tableId}
                            pdfLink={`${urls.baseURL}users/gm/report/pdf/5/${query}`}
                            mailContentId={mailContentId}
                        />
                        <div id={mailContentId}>
                            {isArray(state.data) && state.data.length > 0 && (
                                <Report list={state.data} />
                            )}
                        </div>
                    </>
                )}
            </DynamicContent>
        </>
    );
};

export default SalesSheetReport;
