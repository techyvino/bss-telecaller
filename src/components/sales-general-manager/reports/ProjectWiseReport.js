import React, { useEffect, useMemo, useState } from "react";
import urls, { salesGmUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import { isArray } from "../../../utils";
import keysToQuery from "../../../utils/keysToQuery";
import DynamicContent from "../../common/DynamicContent";
import Downloads from "./Downloads";
import Filters from "./Filters";

const Tr = ({
    name,
    total_leads = {},
    total_site_vite = {},
    total_walk_in = {},
    booking = {},
    project = [],
}) => {
    return (
        <tr>
            <td>{name}</td>
            <td>{total_leads?.daily}</td>
            <td>{total_leads?.monthly}</td>
            <td>{total_site_vite?.daily}</td>
            <td>{total_site_vite?.monthly}</td>
            <td>{total_walk_in?.daily}</td>
            <td>{total_walk_in?.monthly}</td>
            <td>{booking?.today}</td>
            <td>{booking?.total_booking}</td>
            {project.map((x) => (
                <td key={x.id}>{x.count}</td>
            ))}
        </tr>
    );
};

const Table = ({ list = [], pdfLink }) => {
    const projects = isArray(list[0]?.project) ? list[0].project : [];
    return (
        <div className="table-responsive">
            <table
                className="table table-bordered text-center f-14"
                id="table-xls"
            >
                <thead>
                    <tr>
                        <th>Manager's</th>
                        <th colSpan="2">Total Leads</th>
                        <th colSpan="2">Total Site Visit</th>
                        <th colSpan="2">Total Walk In</th>
                        <th colSpan={2 + projects.length}>Booking Details</th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Daily</th>
                        <th>Monthly</th>
                        <th>Daily</th>
                        <th>Monthly</th>
                        <th>Daily</th>
                        <th>Monthly</th>
                        <th>Daily</th>
                        <th>Total Bookings</th>
                        {projects.map((x) => (
                            <th key={x.id}>{x.name}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {list.map((x, idx) => (
                        <Tr {...x} idx={idx} key={idx} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

const tab = 1;
const mailContentId = "mail-content";

const ProjectWiseReport = () => {
    const [state, { setUrl }] = useFetchData();
    const [filters, setFilters] = useState({});

    const query = useMemo(() => keysToQuery({ tab, ...filters }), [filters]);

    useEffect(() => {
        setUrl(`${salesGmUrls.reports}${query}`);
    }, [setUrl, query]);

    const onFilter = (filters = {}) => {
        setFilters(filters);
    };

    const isLoaded = isArray(state.data);

    return (
        <>
            <Filters onFilter={onFilter} />
            <DynamicContent isLoaded={isLoaded} fetching={state.fetching}>
                {isLoaded && state.data.length > 0 && (
                    <>
                        <Downloads
                            tableId="table-xls"
                            title="Daily DSR Report"
                            pdfLink={`${urls.baseURL}users/gm/report/pdf/2/${query}`}
                            mailContentId={mailContentId}
                        />
                        <div id={mailContentId}>
                            <Table list={state.data} />
                        </div>
                    </>
                )}
            </DynamicContent>
        </>
    );
};

export default ProjectWiseReport;
