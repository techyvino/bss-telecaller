import React, { useEffect, useMemo, useState } from "react";
import urls, { salesGmUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import { isArray } from "../../../utils";
import keysToQuery from "../../../utils/keysToQuery";
import DynamicContent from "../../common/DynamicContent";
import Downloads from "./Downloads";
import Filters from "./Filters";

const Tr = ({
    dead_lead,
    follow_up,
    in_process,
    marketing_source,
    name,
    other_source,
    project,
    site_visit_done,
    site_visit_done_dead,
    site_visit_done_follow_up,
    total_booking,
    total_source,
}) => {
    return (
        <tr>
            <td>{name}</td>
            <td>{marketing_source}</td>
            <td>{other_source}</td>
            <td>{total_source}</td>
            <td>{follow_up}</td>
            <td>{in_process}</td>
            <td>{dead_lead}</td>
            <td>{site_visit_done}</td>
            <td>{site_visit_done_follow_up}</td>
            <td>{site_visit_done_dead}</td>
            <td>{total_booking}</td>
            {project.map((x) => (
                <td key={x.id}>{x.count}</td>
            ))}
        </tr>
    );
};

const Report1 = ({ list, tableId }) => {
    const projects = isArray(list[0]?.project) ? list[0].project : [];
    return (
        <div className="table-responsive mb-5">
            <table
                className="table table-bordered text-center f-14"
                id={tableId}
            >
                <thead>
                    <tr>
                        <th colSpan="7">Total Lead Details</th>
                        <th colSpan="3">Total Site Visit Details</th>
                        <th colSpan={projects.length + 1}>
                            Booking Details - project wise
                        </th>
                    </tr>
                    <tr>
                        <th>Manager's Name</th>
                        <th>Marketing Source - Total Leads (A)</th>
                        <th>Own & other Source - Total Leads (B)</th>
                        <th>Total Leads (A+B)</th>
                        <th>Leads Follow-up</th>
                        <th>In-process</th>
                        <th>Dead Leads</th>
                        <th>SVD</th>
                        <th>SVD  Follow-up</th>
                        <th>SVD Dead</th>
                        <th>Total Booking</th>
                        {projects.map((x) => (
                            <th key={x.id}>{x.name}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {list.map((x, idx) => (
                        <Tr {...x} idx={idx} key={idx} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

const Report2 = ({ list = [] }) => {
    return (
        <div className="row">
            <div className="col-lg-8 offset-lg-2">
                <h5 className="text-center">Marketing Leads Target</h5>
                <div className="table-responsive mb-5">
                    <table className="table table-bordered text-center f-14">
                        <thead>
                            <tr>
                                <th>Projects</th>
                                <th>Lead Targets</th>
                                <th>Leads Achieved Till Date</th>
                                <th>Leads Yet To Achieve</th>
                                <th>% Achieved</th>
                            </tr>
                        </thead>
                        <tbody>
                            {list.map((x) => (
                                <tr key={x.projects}>
                                    <td>{x.projects}</td>
                                    <td>{x.target}</td>
                                    <td>{x.achieved}</td>
                                    <td>{x.not_achieved}</td>
                                    <td>{x.achieved_percentage}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

const tab = 2;
const tableId = "report";
const mailContentId = "mail-content";

const SalesReport = () => {
    const [state, { setUrl }] = useFetchData();
    const [filters, setFilters] = useState({});

    const query = useMemo(() => keysToQuery({ tab, ...filters }), [filters]);

    useEffect(() => {
        setUrl(`${salesGmUrls.reports}${query}`);
    }, [setUrl, query]);

    const onFilter = (filters = {}) => {
        setFilters(filters);
    };

    const isLoaded = isArray(state.data?.report_1);

    return (
        <>
            <Filters onFilter={onFilter} />
            <DynamicContent isLoaded={isLoaded} fetching={state.fetching}>
                {isLoaded && (
                    <>
                        <Downloads
                            title="Weekly  Sales Review Report"
                            tableId={tableId}
                            pdfLink={`${urls.baseURL}users/gm/report/pdf/3/${query}`}
                            mailContentId={mailContentId}
                        />
                        <div id={mailContentId}>
                            {isArray(state.data?.report_1) &&
                                state.data.report_1.length > 0 && (
                                    <Report1
                                        tableId={tableId}
                                        list={state.data.report_1}
                                    />
                                )}
                            {isArray(state.data?.report_2) &&
                                state.data.report_2.length > 0 && (
                                    <Report2 list={state.data.report_2} />
                                )}
                        </div>
                    </>
                )}
            </DynamicContent>
        </>
    );
};

export default SalesReport;
