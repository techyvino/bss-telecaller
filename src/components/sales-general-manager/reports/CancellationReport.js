import React from "react";

const tableData = [
    {
        th: "Sl no",
    },
    {
        th: "Tower",
    },
    {
        th: "Floor",
    },
    {
        th: "Flat No.",
    },
    {
        th: "Customer Name",
    },
    {
        th: "Date of Booking",
    },
    {
        th: "Booking Amount",
    },
    {
        th: "Bank Name",
    },
    {
        th: "Cheque No",
    },
    {
        th: "Cheque Date",
    },
    {
        th: "Sq.ft",
    },
    {
        th: "Sales Manager",
    },
    {
        th: "CRM Followed By",
    },
];

const CancellationReport = () => {
    return (
        <div className="table-responsive">
            <table className="table table-bordered">
                <thead>
                    <tr>
                        {tableData.map((x) => (
                            <th key={x.th}>{x.th}</th>
                        ))}
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    );
};

export default CancellationReport;
