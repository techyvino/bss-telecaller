import React, { useEffect, useMemo } from "react";
import urls, { salesGmUrls } from "../../../apiService/urls";
import useFetchData from "../../../hooks/http/useFetchData";
import { isArray } from "../../../utils";
import DynamicContent from "../../common/DynamicContent";
import { useMasterValues } from "../../common/MasterData";
import ReactHTMLTableToExcel from "react-html-table-to-excel";
import { DownloadAsPDF } from "../DownloadLink";
import useQuery from "../../../hooks/useQuery";
import { useHistory } from "react-router-dom";

const Report1 = ({ list = [] }) => {
    return list.map((tbl) => (
        <div className="col-md-4 mb-3" key={tbl.title}>
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">{tbl.title}</th>
                        <th scope="col">FTD</th>
                        <th scope="col">MTD</th>
                        <th scope="col">YTD</th>
                    </tr>
                </thead>
                <tbody>
                    {tbl.values.map((x, idx) => (
                        <tr key={idx}>
                            <td>{x.project}</td>
                            <td>{x.ftd}</td>
                            <td>{x.mtd}</td>
                            <td>{x.ytd}</td>
                        </tr>
                    ))}
                    <tr>
                        <th scope="row">Total</th>
                        <th scope="row">{tbl.ftd_total}</th>
                        <th scope="row">{tbl.mtd_total}</th>
                        <th scope="row">{tbl.ytd_total}</th>
                    </tr>
                </tbody>
            </table>
        </div>
    ));
};

const title = "Sales Manager Status Report";

const Report2 = ({ list = [], total = [], project }) => {
    return (
        <>
            <h5 className="mb-3">{title}</h5>
            <div className="d-flex align-items-center flex-wrap mb-2">
                <div className="mr-3">
                    <ReactHTMLTableToExcel
                        table="table-to-xls"
                        filename={title}
                        sheet={title}
                        buttonText="Download Excel"
                        className="btn btn-outline-black btn-sm"
                    />
                </div>
                <div className="mr-3">
                    <DownloadAsPDF link={`${urls.baseURL}users/gm/report/pdf/1/?project_id=${project}`} />
                </div>
            </div>
            <div className="table-responsive">
                <table
                    className="table table-bordered text-center vertical-middle-table"
                    id="table-to-xls"
                >
                    <thead>
                        <tr>
                            <th scope="col">SALES MANAGER</th>
                            {list[0].status.map((x) => (
                                <th scope="col" key={x.title}>
                                    {x.title}
                                </th>
                            ))}
                            <th scope="col">TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((x, idx) => (
                            <tr key={`${x.name}---${idx}`}>
                                <td>{x.name}</td>
                                {x.status.map((st) => (
                                    <td key={st.title}>{st.count}</td>
                                ))}
                                <td>{x.manager_status_count}</td>
                            </tr>
                        ))}
                        <tr className="bg-light">
                            <th scope="row">Total</th>
                            {total.map((x, idx) => (
                                <th scope="row" key={idx}>
                                    {x}
                                </th>
                            ))}
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    );
};

const mapTeams = (team = []) =>
    team.map((x) => ({
        value: x.head.id,
        label: `${x?.head?.user?.first_name ?? ""} ${x?.head?.user?.last_name ?? ""
            }`,
        team_mates: x.team_mates,
        employee: { id: x.head.id, user: x.head.user },
    }));

const DashboardFilter = ({ project }) => {
    const data = useMasterValues()[1];
    const options = isArray(data?.project) ? data.project : [];
    const query = useQuery();
    const history = useHistory();

    const team = query.get("team") ?? "";

    const [state, { setUrl }] = useFetchData();

    useEffect(() => {
        setUrl(salesGmUrls.salesTeamLead);
    }, [setUrl]);

    const isLoaded = isArray(state.data);

    const teams = mapTeams(state.data)

    const team_mates = useMemo(() => {
        const teamId = team ? parseInt(team, 10) : "";
        const list = teams.filter((x) => x.value === teamId)?.[0];
        if (list && isArray(list.team_mates) && list.employee) {
            return [list.employee, ...list.team_mates];
        }
        return [];
    }, [teams, team]);

    const employees = isArray(team_mates) ? team_mates : [];
    const employee_id = query.get("employee") ?? "";


    if (isLoaded) {
        return (
            <div className="form-row">
                <div className="col-md-4 col-9 form-group">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("project", val);
                            } else {
                                query.delete("project");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={project}
                    >
                        <option value="">All Projects</option>
                        {options.map((x) => (
                            <option value={x.id} key={x.id}>
                                {x.title}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("team", val);
                                query.set("employee", val);
                            } else {
                                query.delete("team");
                                query.delete("employee");
                            }

                            
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={team}
                    >
                        <option value="">All Teams</option>
                        {teams.map((x) => (
                            <option value={x.value} key={x.value}>
                                {x.label}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="col-md-3 mb-3">
                    <select
                        className="form-control"
                        onChange={(e) => {
                            const val = e.target.value;
                            if (val) {
                                query.set("employee", val);
                            } else {
                                query.delete("employee");
                            }
                            history.push({
                                search: `?${query.toString()}`,
                            });
                        }}
                        value={employee_id}
                    >
                        <option value="">All Sales Person</option>
                        {employees.map((x) => (
                            <option value={x.id} key={x.id}>
                                {`${x?.user?.first_name ?? ""} ${
                                    x?.user?.last_name ?? ""
                                }`}
                            </option>
                        ))}
                    </select>
                </div>
                
            </div>

        )
    }
    return <div></div>
}

const FtdList = () => {
    const query = useQuery();
    const project = query.get("project") ?? "";
    const [state, { setUrl }] = useFetchData();
    const q = `${salesGmUrls.dashboard}${`?${query.toString()}`}`;
    // console.log(query.toString())

    useEffect(() => {
        setUrl(q);
    }, [setUrl, q]);

    const isLoaded = isArray(state?.data?.report_1);

    return (
        <DynamicContent fetching={state.fetching} isLoaded={isLoaded}>
            {isLoaded && (
                <>
                    <div className="row">
                        <Report1 list={state?.data?.report_1} />
                    </div>

                    <DashboardFilter project={project} />


                    {state.data.report_2.length > 0 && (
                        <Report2
                            project={project}
                            list={state.data.report_2}
                            total={state.data.report_2_total}
                        />
                    )}
                </>
            )}
        </DynamicContent>
    );
};

export default FtdList;
