import { useMemo } from "react";
import dayjs from "dayjs";

const localizedFormat = require("dayjs/plugin/localizedFormat");
dayjs.extend(localizedFormat);

const useDateFormat = (date, format = "lll") => {
    return useMemo(() => {
        if (date) {
            return dayjs(date).format(format);
        }
        return "";
    }, [date, format]);
};

export default useDateFormat;
