import { useReducer } from "react";
import formReducer, { init } from "./formReducer";

const useFormReducer = (initValues = {}) =>
    useReducer(formReducer, { ...init, ...initValues });

export default useFormReducer;
