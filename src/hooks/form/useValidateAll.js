import { useCallback } from "react";
import validateAll from "../../components/form/validateAll";

const useValidateAll = (allIds = []) => {
    return useCallback(
        (values) => {
            return validateAll(allIds, values);
        },
        [allIds]
    );
};

export default useValidateAll;
