import reduceResults from "../../utils/reduceResults";

export const pageInit = {
    page: 1,
    allIds: [],
    byId: {},
    fetching: false,
    error: true,
    count: null,
    limit: 12,
    hasNext: false,
};

export const pageTyps = {
    nextPage: "NEXT_PAGE",
    fetching: "PAGINATION_FETCHING",
    fullFilled: "PAGINATION_FULFILLED",
    rejected: "PAGINATION_REJECTED",
    updateData: "UPDATE_DATA",
    addData: "ADD_DATA",
    clear: "PAGINATION_CLEAR",
};

const fullFilled = (state, payload) => {
    const { byId, allIds } = reduceResults(payload.results);
    return {
        ...state,
        fetching: false,
        byId: { ...state.byId, ...byId },
        allIds: [...state.allIds, ...allIds],
        count: payload.count,
        hasNext: state.page < Math.ceil(payload.count / state.limit),
    };
};

const infiniteListReducer = (state = pageInit, action) => {
    const { payload, type } = action;
    switch (type) {
        case pageTyps.nextPage:
            return {
                ...state,
                page: state.page + 1,
            };
        case pageTyps.fetching:
            return {
                ...state,
                fetching: true,
                error: false,
            };
        case pageTyps.fullFilled:
            return fullFilled(state, payload);
        case pageTyps.rejected:
            return {
                ...state,
                fetching: false,
                error: true,
            };
        case pageTyps.updateData:
            return {
                ...state,
                byId: {
                    ...state.byId,
                    [payload.id]: {
                        ...(state.byId[payload.id] || {}),
                        ...payload,
                    },
                },
            };
        case pageTyps.addData:
            return {
                ...state,
                allIds: [payload.id, ...state.allIds],
                byId: {
                    ...state.byId,
                    [payload.id]: payload,
                },
            };
        case pageTyps.clear:
            return pageInit;
        default:
            return state;
    }
};

export default infiniteListReducer;
