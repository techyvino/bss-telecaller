import { useEffect, useReducer, useState, useCallback } from "react";
import paginationReducer, { pageInit, pageTyps } from "./pageReducer";
import clientApi from "../../apiService/clientApi";
import keysToQuery from "../../utils/keysToQuery";
import { useHistory } from "react-router-dom";
import useQuery from "../useQuery";

const usePagination = () => {
    const [url, set] = useState({});
    const [state, dispatch] = useReducer(paginationReducer, pageInit);
    const { limit, noOfPages, allIds } = state;
    const history = useHistory()
    const query = useQuery()

    const pageQuery = query.get("page") ? parseInt(query.get("page")) : 1;

    const pageLoaded = Array.isArray(allIds[pageQuery]) && allIds[pageQuery].length > 0;

    useEffect(() => {
        let didCancel = false;
        const fetchData = async (fetchUrl) => {
            dispatch({ type: pageTyps.fetching });
            try {
                const { data } = await clientApi({
                    url: fetchUrl,
                });
                if (!didCancel) {
                    dispatch({
                        type: pageTyps.fullFilled,
                        payload: data,
                    });
                }
            } catch (error) {
                if (!didCancel) {
                    dispatch({ type: pageTyps.rejected });
                }
            }
        };
        if (url.baseUrl && pageQuery && limit && !pageLoaded) {
            dispatch({ type: pageTyps.setPage, payload: pageQuery });
            const q = keysToQuery({
                ...url.filters,
                offset: limit * (pageQuery - 1),
                limit,
                page: pageQuery,
            });
            fetchData(url.baseUrl + q);
        }
        return () => {
            didCancel = true;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [url, pageQuery, limit]);

    const setUrl = useCallback((x) => {
        dispatch({ type: pageTyps.clear });
        set(x);
    }, []);

    const clear = useCallback(() => {
        dispatch({ type: pageTyps.clear });
    }, []);

    const setPage = useCallback(
        (payload) => {
            if (payload <= noOfPages) {
                dispatch({ type: pageTyps.setPage, payload });
                query.set("page", payload);
                history.push({
                    search: `?${query.toString()}`,
                });
            }

        },
        [noOfPages, query, history]
    );

    const nextPage = useCallback(() => {
        const next = pageQuery + 1;
        console.log(pageQuery)
        if (next <= noOfPages) {
            dispatch({ type: pageTyps.setPage, next });
        }
    }, [pageQuery, noOfPages]);

    return [state, { setUrl, clear, setPage, nextPage }];
};

export default usePagination;
