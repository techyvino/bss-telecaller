import { useEffect, useReducer, useState, useCallback } from "react";
import infiniteListReducer, { pageInit, pageTyps } from "./infiniteListReducer";
import clientApi from "../../apiService/clientApi";
import keysToQuery from "../../utils/keysToQuery";

const useInfiniteList = () => {
    const [url, set] = useState({});
    const [state, dispatch] = useReducer(infiniteListReducer, pageInit);
    const { page, hasNext, limit } = state;

    useEffect(() => {
        let didCancel = false;
        const fetchData = async (fetchUrl) => {
            dispatch({ type: pageTyps.fetching });
            try {
                const { data } = await clientApi({
                    url: fetchUrl,
                });
                if (!didCancel) {
                    dispatch({
                        type: pageTyps.fullFilled,
                        payload: data,
                    });
                }
            } catch (error) {
                if (!didCancel) {
                    dispatch({ type: pageTyps.rejected });
                }
            }
        };
        if (url.baseUrl && page) {
            const q = keysToQuery({
                ...url.filters,
                offset: limit * (page - 1),
                limit,
            });
            fetchData(url.baseUrl + q);
        }
        return () => {
            didCancel = true;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [url, page]);

    const setUrl = useCallback((x) => {
        dispatch({ type: pageTyps.clear });
        set(x);
    }, []);

    const clear = useCallback(() => {
        dispatch({ type: pageTyps.clear });
    }, []);

    const loadMore = useCallback(() => {
        if (hasNext) {
            dispatch({ type: pageTyps.nextPage });
        }
    }, [hasNext]);

    const addData = useCallback((payload) => {
        dispatch({ type: pageTyps.addData, payload });
    }, []);

    return [state, { setUrl, clear, loadMore, dispatch, addData }];
};

export default useInfiniteList;
