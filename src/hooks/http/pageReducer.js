import reduceResults from "../../utils/reduceResults";

export const pageInit = {
    page: 1,
    allIds: {},
    byId: {},
    fetching: {},
    error: {},
    next: null,
    previous: null,
    limit: 15,
    noOfPages: null,
};

export const pageTyps = {
    setPage: "SET_PAGE",
    fetching: "PAGINATION_FETCHING",
    fullFilled: "PAGINATION_FULFILLED",
    rejected: "PAGINATION_REJECTED",
    updateData: "UPDATE_DATA",
    clear: "PAGINATION_CLEAR",
};

const fullFilled = (state, payload) => {
    const { byId, allIds } = reduceResults(payload.results);
    return {
        ...state,
        fetching: {
            ...state.fetching,
            [state.page]: false,
        },
        byId: { ...state.byId, ...byId },
        allIds: {
            ...state.allIds,
            [state.page]: allIds,
        },
        count: payload.count,
        noOfPages: Math.ceil(payload.count / state.limit),
        next: payload.next,
        previous: payload.previous,
    };
};

const paginationReducer = (state = pageInit, action) => {
    const { payload, type } = action;
    switch (type) {
        case pageTyps.setPage:
            return {
                ...state,
                page: action.payload,
            };
        case pageTyps.fetching:
            return {
                ...state,
                fetching: {
                    ...state.fetching,
                    [state.page]: true,
                },
                error: {
                    ...state.error,
                    [state.page]: false,
                },
            };
        case pageTyps.fullFilled:
            return fullFilled(state, payload);
        case pageTyps.rejected:
            return {
                ...state,
                fetching: {
                    ...state.fetching,
                    [state.page]: false,
                },
                error: {
                    ...state.error,
                    [state.page]: true,
                },
            };
        case pageTyps.updateData:
            return {
                ...state,
                byId: {
                    ...state.byId,
                    [action.name]: payload,
                },
            };
        case pageTyps.clear:
            return pageInit;
        default:
            return state;
    }
};

export default paginationReducer;
