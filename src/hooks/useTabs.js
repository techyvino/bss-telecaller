import { useState, useCallback } from "react";

const useTabs = (init) => {
    const [tab, setTab] = useState(init);

    const onTabChange = useCallback((tab) => {
        setTab(tab);
    }, []);

    return [tab, onTabChange];
};

export default useTabs;
