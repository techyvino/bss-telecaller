

export const createSeq = (start = 1, stop = 5) => {
    const arr = [];
    for (let i = start; i <= stop; i++) {
        arr.push(i);
    }
    return arr;
};

const pageList = (active, noOfPages, showLimit = 2) => {
    const start = parseInt(active) - showLimit,
        stop = parseInt(active) + showLimit;
    const listSize = showLimit * 2;

    if (start < 1) {
        return createSeq(1, Math.min(1 + listSize, noOfPages));
    }
    if (stop > noOfPages) {
        return createSeq(Math.max(noOfPages - listSize, 1), noOfPages);
    }
    return createSeq(start, stop);
};

window.pageList = pageList;

export default pageList;
