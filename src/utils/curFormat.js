const toNum = (num = 0) => {
    const val = parseFloat(num);
    return isNaN(val) ? 0 : val;
};

const curFormat = (amt = 0, type = "₹") => {
    return type + toNum(amt).toLocaleString("en-IN");
};

export default curFormat;
