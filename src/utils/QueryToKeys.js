const QueryToKeys = (query) => {
    if (query){
        const keys = JSON.parse('{"' + decodeURI(query).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
        return keys;
    }
    return ""
};

export default QueryToKeys;
