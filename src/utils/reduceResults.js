const reduceResults = (results = [], byId = {}, allIds = []) =>
    results.reduce(
        (acc, cur) => {
            return {
                byId: {
                    ...acc.byId,
                    [cur.id]: cur
                },
                allIds: acc.allIds.concat(cur.id)
            };
        },
        { byId: byId, allIds: allIds }
    );

export default reduceResults;
